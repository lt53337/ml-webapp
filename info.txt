=============================================================================================
U html stranicama smještenim u page direktorij mogu se pojavljivati sljedeće naredbe:
=============================================================================================

Sustav prepoznaje dva formata ovih naredbi:

@#ime-naredbe#(argumenti)

te

/*@#ime-naredbe#(argumenti)*/

---------------------------------------------------------------------------------------------
@#echo#(varijabla)
@#echoIfTrue#(uvjet, tekst)
@#echoIfFalse#(uvjet, tekst)

Naredba za ispis poruka ili varijabli. Dostupne su samo info-varijable (varijable koje JavaScript zadatci
vraćaju kroz atribut "variables" u metodi "questionRenderView".

Uvjet može biti:
* konstanta true
* konstanta false
* ime varijable oblika $ime
* proizvoljan tekst

Objekt koji se dobije kao uvjet pretvara se u Booleovu vrijednost na sljedeći način:
* konstante true i false odgovaraju Booleovim vrijednostima TRUE i FALSE
* varijabla koja se razriješi u objekt koji je tipa Boolean odgovara vrijednosti uvjeta
* ako je objekt broj, izjednačava se s TRUE ako je različit od nule, inače je FALSE
* ako je objekt String, izjednačava se s TRUE ako je neprazan, inače je FALSE
* u svim ostalim slučajevima, tumači se kao FALSE

Tekst može biti:
* tekst napisan pod navodnicima: koristi se doslovno
* varijabla: format je $ime; koristi se sadržaj te varijable
* ostalo: koristi se doslovno

U tijelu naredbe, na počeku, može biti zadano kodiranje koje treba primijeniti na generiranje izlaza. 
Kodiranje se specificira unutar dva znaka *, a podržana su sljedeća kodiranja:
 *raw* bez kodiranja - tipično vrlo opasna mogućnost koja se ne preporuča 
                       (posebice ne ako prenosite unos korisnika)
 *html* sve "opasne" znakove unutar tijela HTML dokumenta zamijeni sigurnim znakovima
 *html-attrib* sve "opasne" znakove unutar vrijednosti HTML atributa zamijeni sigurnim znakovima
 *js* sve "opasne" znakove unutar JavaScript stringa zamijeni sigurnim znakovima
Ako se ne navede kodiranje, primjenjuje se *html* kodiranje.

Primjer:

@#echoIfTrue#($prisutan, Unijeli ste odgovor.)
@#echoIfTrue#(*js* $prisutan, Unijeli ste odgovor.)

Primjer uporabe:

@#echoIfTrue#($prisutan, Unijeli ste odgovor.)@#echoIfFalse#($prisutan, Niste unijeli odgovor.)
---------------------------------------------------------------------------------------------
@#file-url#(ime datoteke)
@#file#(ime datoteke)

Naredba se zamjenjuje URL-om na resurs koji se mora nalaziti u direktoriju files. Služi za
postavljanje vrijednosti atributa u html-u gdje se očekuje adresa prema nekom resursu
(primjerice, za uključivanje slika).

Primjer:

@#file-url#(kodiranje2-hr.svg)

Primjer uporabe:

<img src="@#file-url#(kodiranje2-hr.svg)" width="100" height="100">
---------------------------------------------------------------------------------------------
@#branch-title#(identifikator grane)

Naredba se zamjenjuje nazivom grane lokaliziranim na korektan jezik.

Primjer:

@#branch-title#(br1)

Primjer uporabe:

<img src="@#file#(kodiranje2-hr.svg)" width="100" height="100">
---------------------------------------------------------------------------------------------
@#branch-link#(identifikator grane)

Naredba se zamjenjuje URL-om na prvu stavku navedene grane. Uporaba je dozvoljena samo na
stranici stavke koja deklarira da nudi određenu granu.

Primjer:

@#branch-link#(br1)

Primjer uporabe:

<a href="@#branch-link#(br1)">više o Ohmovom zakonu</a>
---------------------------------------------------------------------------------------------
@#page-include#(ime datoteke)  -- deprecated
@#include#(ime datoteke)       -- koristiti umjesto page-include

Naredba na mjestu pojave sebe zamjenjuje sadržajem koji nastaje kao rezultat obrade
datoteke koja je predana kao argument. Ime datoteke koje se navodi predstavlja stranicu
koja je smještena u direktoriju pages (ako se naredba izvršava u okviru procesiranja
stranice koja se šalje klijentu) odnosno u direktoriju scripts (ako se naredba izvršava u okviru
procesiranja skripte main.js kod pitanja). Treba napomenuti da se taj sadržaj prije uključivanja
također obrađuje - ako je sadržavao neke naredbe, one će biti izvršene i tek će tako generirani
sadržaj biti uključen.

Napomena: ovdje se radi o uključivanju na poslužiteljskoj strani: klijent koji dobiva sadržaj
neće biti svjestan da je vraćeni sadržaj nastao iz više povezanih dokumenata.

Poslužitelj obavlja provjeru cikličnosti uključivanja: pri detekciji ciklusa, postupak uključivanja
će biti prekinut uz pogrešku.

Primjer:

@#include#(zajednicko-hr.html)

Primjer uporabe:

<html><body>
<p>I ja koristim sljedeću skriptu.</p>

@#include#(skripta.html)

<p>Ovaj dio je različit.</p>
</body></html>
---------------------------------------------------------------------------------------------
@#common-include#(ime datoteke)

Naredba na mjestu pojave sebe zamjenjuje sadržajem koji nastaje kao rezultat obrade
datoteke koja je predana kao argument. Ime datoteke koje se navodi predstavlja datoteku
koja je smještena u direktoriju common. Treba napomenuti da se taj sadržaj prije uključivanja
također obrađuje - ako je sadržavao neke naredbe, one će biti izvršene i tek će tako generirani
sadržaj biti uključen.

Napomena: ovdje se radi o uključivanju na poslužiteljskoj strani: klijent koji dobiva sadržaj
neće biti svjestan da je vraćeni sadržaj nastao iz više povezanih dokumenata.

Poslužitelj obavlja provjeru cikličnosti uključivanja: pri detekciji ciklusa, postupak uključivanja
će biti prekinut uz pogrešku.

Naredba je namijenjena omogućavanju dijeljenja datoteka (primjerice, javascript koda) koje su potrebne 
i na poslužiteljskoj, i na klijentskoj strani.

Primjer:

@#common-include#(dijeljeno.js)

Primjer uporabe:

<html><body>
<p>I ja koristim sljedeću skriptu.</p>
<script>
@#common-include#(dijeljeno.js)
</script>

<p>Ovaj dio je različit.</p>
</body></html>
---------------------------------------------------------------------------------------------
@#raw-text#(ime varijable)

Naredba na mjestu pojave sebe zamjenjuje sadržajem koji je pohranjen u supstitucijske varijable.
Koje su varijable dostupne ovisi o kontekstu u kojem se stranica učitava. Primjerice, ako se 
stranica učitava na u kontekstu zadatka tipa js-custom, stranica će na raspolaganju imati
supstitucijsku varijablu 'jsinitdata' koja predstavlja JSON zapis trenutnog stanja zadatka.

Primjer:

@#raw-text#(jsinitdata)

Primjer uporabe:

<script>
  var state = {
  };

  var konfiguracija = @#raw-text#(jsinitdata);
  state.elems = konfiguracija.elems;
</script>
---------------------------------------------------------------------------------------------
@#video#(width=*|height=*|poster=*|src=*#type=*|src=*#type=*|subtitles=*#language=*#label=*)

Naredba služi za umetanje video sadržaja u stranicu. Video sadržaji se na poslužitelju pohranjuju
odvojeno od ostatka minilekcije. Ova naredba će se pobrinuti da sve staze izgenerira tako da pokazuju
na korektne lokacije.

Pojedini dijelovi naredbe razdvojeni su znakom |. Redoslijed prva tri atributa mora biti kako je
prikazano. Pri tome je atribut 'poster' opcionalan i može ga se preskočiti.

'width' i 'height' moraju biti brojevi.

'poster' mora biti ime datoteke bez navođenja direktorija ili relativnih staza poput '..'.

Nakon ovih atributa mora doći barem jedna specifikacija konkretne video datoteke i njezinog mime-tipa.
'subtitles' omogućava definiranje datoteke u kojoj se nalaze subtitlovi.

Primjer uporabe:

@#video#(width=320|height=240|poster=movie.jpg|src=movie.mp4#type=video/mp4|src=movie.ogg#type=video/ogg|subtitles=movie.en.vtt#language=en#label=English)

=============================================================================================
U js skriptama smještenim u jscustom direktorij mogu se pojavljivati sljedeće naredbe:
=============================================================================================

Naredba je strukturno vrlo slična naredbama koje se pišu u html dokumentima. Razlika je što
su bez razmaka smještene u komentar a prilikom aktiviranja čitav komentar nestaje i supstituira
se rezultatom izvođenja naredbe.

---------------------------------------------------------------------------------------------
/*@#import#(ime datoteke)*/

Služi za uključivanje druge datoteke iz istog direktorija (jscustom) u trenutnu skriptu
(događa se na poslužiteljskoj strani). Ova naredba skriptu ne interpretira za daljnje naredbe
već je doslovno umeće na mjesto naredbe. Očekuje se da je skripta zapisana koristeći
UTF-8 kodnu stranicu.

Ime datoteke može sadržavati varijable. Varijable se navode sintaksom ${ime}. Na raspolaganju su
dvije varijable:

uid - sadrži identifikator minilekcije
iid - sadrži identifikator itema zbog kojeg se skripta učitava

Ova naredba omogućava da imamo jednu skriptu koju koristi više različitih zadataka (itema)
i koji specifičnosti imaju zapisane u zasebnim datotekama koje se pri učitavanju kombiniraju
u jednu skriptu koja se potom izvodi.

Primjer:

/*@#import#(pattern${iid}.txt)*/

Primjer uporabe:

userData.elems = [/*@#import#(pattern${iid}.txt)*/];
---------------------------------------------------------------------------------------------
