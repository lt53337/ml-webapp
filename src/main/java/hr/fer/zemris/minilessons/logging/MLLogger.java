package hr.fer.zemris.minilessons.logging;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.util.StringUtil;

public class MLLogger {

	private static final IAccessLogger accessLogger;
	private static final IEduLogger eduLogger;
	
	private MLLogger() {
	}

	public static IAccessLogger getAccesslogger() {
		return accessLogger;
	}
	
	public static IEduLogger getEdulogger() {
		return eduLogger;
	}

	private static volatile boolean autoflusherOff = false;
	private static Object mutex = new Object();
	private static Thread autoflusherThread = null;
	
	public static void shutdownAutoflusher() {
		synchronized (mutex) {
			autoflusherOff = true;
			mutex.notifyAll();
		}
		if(autoflusherThread!=null) {
			autoflusherThread.interrupt();
		}
	}
	
	private static void pingFlusher() {
		synchronized (mutex) {
			mutex.notifyAll();
		}
	}

	private static class AFJob implements Runnable {
		@Override
		public void run() {
			while(true) {
				try {
					synchronized (mutex) {
						mutex.wait();
					}
				} catch(Exception ignorable) {
				}
				// Ako treba prekinuti periodičko flushanje...
				if(autoflusherOff) break;
				// Inače sam prestao čekati jer me je netko notificirao...
				// Stoga idem spavati 10 sekundi...
				try {
					Thread.sleep(10000);
				} catch(Exception ignorable) {
				}
				// Ako treba prekinuti periodičko flushanje...
				if(autoflusherOff) break;
				if(accessLogger != null) accessLogger.flush();
				if(eduLogger != null) eduLogger.flush();
			}
			System.out.println("Log flusher thread done...");
		}
	}
	
	static {
		Path accessLogDir = null;
		Path eduLogDir = null;

		IAccessLogger alogger = null;
		IEduLogger elogger = null;
		
		try {
			Properties prop = new Properties();
			InputStream is = DAOProvider.class.getClassLoader().getResourceAsStream("ml-basic.properties");
			if(is==null) {
				throw new IOException("File not found: ml-basic.properties. Could not initialize logging subsystem.");
			}
			try {
				prop.load(is);
			} finally {
				is.close();
			}
			String tmp = StringUtil.trim(prop.getProperty("ml.logging.accessLog.dir"));
			if(tmp!=null) {
				accessLogDir = Paths.get(tmp);
				if(!Files.isDirectory(accessLogDir)) {
					throw new IOException(tmp+" is not directory.");
				}
				alogger = new FileAccessLogger(accessLogDir);
			}
			tmp = StringUtil.trim(prop.getProperty("ml.logging.eduLog.dir"));
			if(tmp!=null) {
				eduLogDir = Paths.get(tmp);
				if(!Files.isDirectory(accessLogDir)) {
					throw new IOException(tmp+" is not directory.");
				}
				elogger = new FileEduLogger(eduLogDir);
			}
		} catch(IOException ex) {
			throw new RuntimeException("Could not initialize logging subsystem.", ex);
		}

		accessLogger = alogger != null ? alogger : new NullAccessLogger();
		eduLogger = elogger != null ? elogger : new NullEduLogger();
		
		autoflusherThread = new Thread(new AFJob(), "MLLogAutoFlusherThread");
		autoflusherThread.setDaemon(true);
		autoflusherThread.start();
	}
	
	private static class NullAccessLogger implements IAccessLogger {
		@Override
		public void log(String ip, String username, String path, String referer) {
		}
		
		@Override
		public void flush() {
		}
		
		@Override
		public void close() {
		}
	}
	
	private static class NullEduLogger implements IEduLogger {
		@Override
		public void flush() {
		}
		
		@Override
		public void close() {
		}
		
		@Override
		public void log(IEduLogPacket packet) {
		}
	}
	
	private static class FileAccessLogger implements IAccessLogger {
		private Path directory;
		private String dateKey;
		private BufferedWriter bw;
		private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		private SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd:HH:mm:ss");
		
		public FileAccessLogger(Path directory) {
			super();
			this.directory = directory;
		}
		
		private synchronized void check(Date now) throws IOException {
			String key = sdf.format(now);
			if(key.equals(dateKey)) return;
			if(bw!=null) {
				bw.close();
				bw = null;
			}
			dateKey = key;
			bw = Files.newBufferedWriter(directory.resolve("a_"+dateKey+".log"), StandardCharsets.UTF_8, StandardOpenOption.APPEND, StandardOpenOption.WRITE, StandardOpenOption.CREATE);
		}
		
		@Override
		public synchronized void flush() {
			if(bw==null) return;
			try {
				bw.flush();
			} catch(IOException ex) {
				ex.printStackTrace();
			}
		}
		
		@Override
		public synchronized void close() {
			if(bw==null) return;
			try {
				bw.close();
				bw = null;
				dateKey = null;
			} catch(IOException ex) {
				ex.printStackTrace();
			}
		}
		
		@Override
		public synchronized void log(String ip, String username, String path, String referer) {
			try {
				Date now = new Date();
				check(now);
				if(bw!=null) {
					bw.write(ip + " - " + escapeLog(username) + " - " + sdf2.format(now) + " - \"" + escapeLog(path) + "\" - \"" + escapeLog(referer) + "\"\n");
					pingFlusher();
				}
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}

		private String escapeLog(String text) {
			if(text==null || text.isEmpty()) return "";
			StringBuilder sb = new StringBuilder();
			char[] array = text.toCharArray();
			for(char c : array) {
				switch(c) {
				case '\\': sb.append("\\\\"); break;
				case ' ': sb.append("\\s"); break;
				case '\"': sb.append("\\q"); break;
				case '\r': sb.append("\\r"); break;
				case '\n': sb.append("\\n"); break;
				default: sb.append(c);
				}
			}
			return sb.toString();
		}
	}
	
	private static class FileEduLogger implements IEduLogger {
		private Path directory;
		private String dateKey;
		private DataOutputStream bw;
		private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		private SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd:HH:mm:ss");

		public FileEduLogger(Path directory) {
			super();
			this.directory = directory;
		}
		
		private synchronized void check(Date now) throws IOException {
			String key = sdf.format(now);
			if(key.equals(dateKey)) return;
			if(bw!=null) {
				bw.close();
				bw = null;
			}
			dateKey = key;
			bw = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(directory.resolve("a_"+dateKey+".log").toFile(), true)));
		}
		
		@Override
		public synchronized void flush() {
			if(bw==null) return;
			try {
				bw.flush();
			} catch(IOException ex) {
				ex.printStackTrace();
			}
		}
		
		@Override
		public synchronized void close() {
			if(bw==null) return;
			try {
				bw.close();
				bw = null;
				dateKey = null;
			} catch(IOException ex) {
				ex.printStackTrace();
			}
		}
		
		@Override
		public synchronized void log(IEduLogPacket packet) {
			try {
				Date now = new Date();
				check(now);
				if(bw!=null) {
					int id = packet.getIdentifier();
					byte[] content = packet.getContent();
					String dateStr = sdf2.format(now);

					// Write packet start marker:
					bw.writeByte(0xFE);
					bw.writeByte(0xEF);

					// Write packet ID:
					bw.writeInt(id);

					// Write single byte for length and then characters for date (UTF-8):
					byte[] dateBytes = dateStr.getBytes(StandardCharsets.UTF_8);
					bw.writeByte(dateBytes.length);
					bw.write(dateBytes);

					// Write single byte for username length and then characters for username (UTF-8):
					String username = packet.getUserName();
					if(username==null || username.equals("")) {
						bw.writeByte(0);
					} else {
						byte[] usernameBytes = username.getBytes(StandardCharsets.UTF_8);
						bw.writeByte(usernameBytes.length);
						bw.write(usernameBytes);
					}

					// Write 4-bytes for content length, then content.
					if(content==null) {
						bw.writeInt(0);
					} else {
						bw.writeInt(content.length);
						if(content.length > 0) {
							bw.write(content);
						}
					}
					pingFlusher();
				}
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
	}
}
