package hr.fer.zemris.minilessons.logging;

public interface IEduLogger {

	void log(IEduLogPacket packet);
	void flush();
	void close();
	
}
