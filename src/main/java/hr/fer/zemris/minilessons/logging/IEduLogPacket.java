package hr.fer.zemris.minilessons.logging;

public interface IEduLogPacket {
	int getIdentifier();
	byte[] getContent();
	String getUserName();
}
