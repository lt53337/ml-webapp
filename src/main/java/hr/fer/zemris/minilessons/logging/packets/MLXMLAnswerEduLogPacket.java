package hr.fer.zemris.minilessons.logging.packets;

import hr.fer.zemris.minilessons.logging.AbstractEduLogPacket;

public class MLXMLAnswerEduLogPacket extends AbstractEduLogPacket {

	private static final int PACKET_ID = 4;
	private byte[] content;
	
	public MLXMLAnswerEduLogPacket(String username, String minilessonUID, String itemID, String viewID, String keyID) {
		super(PACKET_ID, username);
		content = new PacketBuilder().writeString(minilessonUID).writeString(itemID).writeString(viewID).writeString(keyID).getContent();
	}

	@Override
	public byte[] getContent() {
		return content;
	}
}
