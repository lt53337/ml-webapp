package hr.fer.zemris.minilessons.logging.packets;

import hr.fer.zemris.minilessons.logging.AbstractEduLogPacket;

public class MLItemCompletedEduLogPacket extends AbstractEduLogPacket {

	private static final int PACKET_ID = 5;
	private byte[] content;
	
	public MLItemCompletedEduLogPacket(String username, String minilessonUID, String itemID) {
		super(PACKET_ID, username);
		content = new PacketBuilder().writeString(minilessonUID).writeString(itemID).getContent();
	}

	@Override
	public byte[] getContent() {
		return content;
	}
}
