package hr.fer.zemris.minilessons.logging;

public abstract class AbstractEduLogPacket implements IEduLogPacket {

	private int id;
	private String username;
	
	public AbstractEduLogPacket(int id, String username) {
		this.id = id;
		this.username = username;
	}

	@Override
	public int getIdentifier() {
		return id;
	}

	@Override
	public String getUserName() {
		return username;
	}

}
