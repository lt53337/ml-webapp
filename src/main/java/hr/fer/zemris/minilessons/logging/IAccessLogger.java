package hr.fer.zemris.minilessons.logging;

public interface IAccessLogger {

	void log(String ip, String username, String path, String referer);
	void flush();
	void close();
}
