package hr.fer.zemris.minilessons.locking;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class LockTreeNode {

	private String name;
	private int refCount;
	private ReadWriteLock rwlock;
	private Map<String, LockTreeNode> children;
	private LockTreeNode parent;
	
	public LockTreeNode(LockTreeNode parent, String name) {
		this.parent = parent;
		this.name = name;
		this.rwlock = new ReentrantReadWriteLock();
		this.refCount = 0;
		System.out.println(Thread.currentThread().getName() + ": Created LockTreeNode("+nodePath()+")");
	}

	public LockTreeNode getOrCreateChild(String name) {
		if(children==null) {
			children = new HashMap<>();
		}
		LockTreeNode n = children.get(name);
		if(n==null) {
			n = new LockTreeNode(this, name);
			children.put(name, n);
		}
		return n;
	}

	public LockTreeNode getChild(String name) {
		if(children==null) return null;
		return children.get(name);
	}
	
	public void incrementRefCount() {
		refCount++;
		System.out.println(Thread.currentThread().getName() + ": LockTreeNode("+nodePath()+").refcount = " + refCount);
	}
	
	public void decrementRefCount() {
		refCount--;
		System.out.println(Thread.currentThread().getName() + ": LockTreeNode("+nodePath()+").refcount = " + refCount);
		if(refCount < 1) {
			if(parent != null) {
				System.out.println(Thread.currentThread().getName() + ": Removed LockTreeNode("+nodePath()+")");
				parent.children.remove(name);
			}
		}
	}
	
	public void acquireReadLock() {
		rwlock.readLock().lock();
		System.out.println(Thread.currentThread().getName() + ": LockTreeNode("+nodePath()+"): got read lock");
	}
	
	public void releaseReadLock() {
		rwlock.readLock().unlock();
		System.out.println(Thread.currentThread().getName() + ": LockTreeNode("+nodePath()+"): released read lock");
	}
	
	public void acquireWriteLock() {
		rwlock.writeLock().lock();
		System.out.println(Thread.currentThread().getName() + ": LockTreeNode("+nodePath()+"): got write lock");
	}
	
	public void releaseWriteLock() {
		rwlock.writeLock().unlock();
		System.out.println(Thread.currentThread().getName() + ": LockTreeNode("+nodePath()+"): released write lock");
	}
	
	public String nodePath() {
		Stack<String> st = new Stack<>();
		LockTreeNode n = this;
		while(n != null) {
			st.push(n.name);
			n = n.parent;
		}
		StringBuilder sb = new StringBuilder();
		while(!st.isEmpty()) {
			sb.append("/").append(st.pop());
		}
		return sb.toString();
	}
}
