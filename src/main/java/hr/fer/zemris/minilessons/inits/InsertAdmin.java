package hr.fer.zemris.minilessons.inits;

import java.util.Date;
import java.util.HashSet;

import hr.fer.zemris.minilessons.AuthDomain;
import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.dao.DAOUtil;
import hr.fer.zemris.minilessons.dao.model.MLBasicPermission;
import hr.fer.zemris.minilessons.dao.model.UploadPolicy;
import hr.fer.zemris.minilessons.dao.model.User;
import hr.fer.zemris.minilessons.util.PasswordUtil;

/**
 * This is helper task which inserts admin into persistence storage, if admin is not present.
 * Be advised: username and password are simple and known, so this is not a production-system
 * task. Use it for private testing purposes only.
 * 
 * @author marcupic
 *
 */
public class InsertAdmin implements Runnable {

	@Override
	public void run() {
		DAOUtil.transactional(()->{
			User admin = DAOProvider.getDao().findUserByUsername("admin", AuthDomain.LOCAL.getKey());
			if(admin!=null) return admin;
			
			Date now = new Date();
			
			admin = new User();
			admin.setUsername("admin");
			admin.setAuthDomain(AuthDomain.LOCAL.getKey());
			admin.setEmail("");
			admin.setFirstName("Adam");
			admin.setLastName("Administrator");
			admin.setJmbag("#0000000001");
			admin.setUploadPolicy(UploadPolicy.ALLOWED);
			admin.setPermissions(new HashSet<>());
			admin.getPermissions().add(MLBasicPermission.ADMINISTRATION.getPermissonName());
			admin.setPasswordHash(PasswordUtil.encodePassword("pass"));
			admin.setCreatedOn(now);
			
			DAOProvider.getDao().saveUser(admin);
			
			return admin;
		});
	}
}
