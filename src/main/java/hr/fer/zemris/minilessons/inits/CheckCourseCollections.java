package hr.fer.zemris.minilessons.inits;

import hr.fer.zemris.minilessons.CourseCollections;
import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.dao.DAOUtil;
import hr.fer.zemris.minilessons.dao.model.CourseCollection;

/**
 * This is helper task which checks for and inserts default course list.
 * 
 * @author marcupic
 */
public class CheckCourseCollections implements Runnable {

	@Override
	public void run() {
		DAOUtil.transactional(()->{
			CourseCollection cc = DAOProvider.getDao().findCourseCollectionByID(CourseCollections.DEFAULT.getKey());
			if(cc==null) {
				cc = new CourseCollection();
				cc.setId(CourseCollections.DEFAULT.getKey());
				cc.getTitles().put("hr", "Pregled kolegija");
				cc.getTitles().put("en", "Course listing");
				DAOProvider.getDao().saveCourseCollection(cc);
			}
			return null;
		});
	}
	
}
