package hr.fer.zemris.minilessons.testspec.model;

import java.util.List;

public class TSMTestQuestion {

	private int index;
	private TestQuestionTarget target;
	private String questionUID;
	private String questionConfiguration;
	private FinishAcceptChecker finishAcceptChecker;
	private ProblemGrader problemGrader;
	private Condition scoreVisibilityChecker;
	private List<ProblemMessageProducer> messageProducers;
	private Condition problemEnabledChecker;

	public TSMTestQuestion(int index, TestQuestionTarget target, String questionUID, String questionConfiguration,
			FinishAcceptChecker finishAcceptChecker, ProblemGrader problemGrader, Condition scoreVisibilityChecker,
			List<ProblemMessageProducer> messageProducers, Condition problemEnabledChecker) {
		super();
		this.index = index;
		this.target = target;
		this.questionUID = questionUID;
		this.questionConfiguration = questionConfiguration;
		this.finishAcceptChecker = finishAcceptChecker;
		this.problemGrader = problemGrader;
		this.scoreVisibilityChecker = scoreVisibilityChecker;
		this.messageProducers = messageProducers;
		this.problemEnabledChecker = problemEnabledChecker;
	}
	
	public int getIndex() {
		return index;
	}
	public TestQuestionTarget getTarget() {
		return target;
	}
	public String getQuestionUID() {
		return questionUID;
	}
	public String getQuestionConfiguration() {
		return questionConfiguration;
	}
	public FinishAcceptChecker getFinishAcceptChecker() {
		return finishAcceptChecker;
	}
	public ProblemGrader getProblemGrader() {
		return problemGrader;
	}
	public Condition getScoreVisibilityChecker() {
		return scoreVisibilityChecker;
	}
	public List<ProblemMessageProducer> getMessageProducers() {
		return messageProducers;
	}
	public Condition getProblemEnabledChecker() {
		return problemEnabledChecker;
	}
}
