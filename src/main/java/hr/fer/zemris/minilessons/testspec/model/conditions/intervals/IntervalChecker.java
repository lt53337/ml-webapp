package hr.fer.zemris.minilessons.testspec.model.conditions.intervals;

/**
 * Model objekta koji provjerava je li predani broj u zadanom intervalu.
 * 
 * @author marcupic
 */
public interface IntervalChecker {

	boolean isInInterval(double value);
	
}
