package hr.fer.zemris.minilessons.testspec.model.translations;

import java.util.Optional;

import hr.fer.zemris.minilessons.localization.I18NText;
import hr.fer.zemris.minilessons.localization.I18NTexts;
import hr.fer.zemris.minilessons.testspec.model.TranslationProvider;

public abstract class AbstractTranslationProvider implements TranslationProvider {

	private static final String DEFAULT_LANG = "hr";
	
	protected Optional<I18NText> getTranslation(I18NTexts texts, String languageTag, boolean fallBackToDefault) {
		I18NText tr = texts.getTranslations().get(languageTag);
		if(tr != null) return Optional.of(tr);
		if(!fallBackToDefault) return Optional.empty();
		tr = texts.getTranslations().get(DEFAULT_LANG);
		if(tr != null) return Optional.of(tr);
		tr = texts.getTranslations().get(texts.getLanguages().iterator().next());
		return Optional.ofNullable(tr);
	}
	
}
