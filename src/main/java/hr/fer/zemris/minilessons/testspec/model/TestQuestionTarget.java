package hr.fer.zemris.minilessons.testspec.model;

public enum TestQuestionTarget {

	USER,
	GROUP,
	GROUP_USER
	
}
