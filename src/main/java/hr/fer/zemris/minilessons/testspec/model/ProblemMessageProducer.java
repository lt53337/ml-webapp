package hr.fer.zemris.minilessons.testspec.model;

import java.util.Optional;

import hr.fer.zemris.minilessons.localization.I18NText;

/**
 * Objekt koji za zadatak generira lokaliziranu poruku na
 * traženom jeziku (ili ako nema na traženom jeziku, bira neki drugi jezik), ili pak
 * ne vraća ništa, ako nema što za poručiti.
 * 
 * @author marcupic
 *
 */
public interface ProblemMessageProducer {

	Optional<I18NText> getText(CheckersEnvironment env, String languageTag);
	
}
