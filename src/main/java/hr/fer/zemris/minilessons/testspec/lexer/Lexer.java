package hr.fer.zemris.minilessons.testspec.lexer;

public class Lexer {

	private char[] data;
	private int current;
	private Token currentToken;
	
	public Lexer(String text) {
		data = text.toCharArray();
	}

	public Token nextToken() {
		if(currentToken!=null && currentToken.isEOF()) return currentToken;
		
		while(true) {
			while(current<data.length && data[current]<=' ') current++;
			if(current>=data.length) return updateToken(new Token(null, TokenType.EOF));
			if(current+1<data.length && data[current]=='/' && data[current+1]=='/') {
				current += 2;
				while(current<data.length && data[current]!='\n' && data[current]!='\r') current++;
				while(current<data.length && (data[current]=='\n' || data[current]=='\r')) current++;
				continue;
			}
			break;
		}
		
		if(data[current]=='@' && current+1<data.length && Character.isJavaIdentifierStart(data[current+1])) {
			current++;
			int st = current;
			current++;
			while(current<data.length && Character.isJavaIdentifierPart(data[current])) current++;
			return updateToken(new Token(new String(data, st, current-st), TokenType.MESSAGE_KEY));
		}
		if(Character.isJavaIdentifierStart(data[current])) {
			int st = current;
			current++;
			while(current<data.length && Character.isJavaIdentifierPart(data[current])) current++;
			return updateToken(new Token(new String(data, st, current-st), TokenType.IDENT));
		}
		
		if(oneOfSymbols(data[current])!=-1) {
			char c = data[current];
			current++;
			return updateToken(new Token(c, TokenType.SYMBOL));
		}
		
		if(Character.isDigit(data[current])) {
			int st = current;
			current++;
			while(current<data.length && Character.isDigit(data[current])) current++;
			if(current<data.length && data[current]=='.') {
				current++;
				while(current<data.length && Character.isDigit(data[current])) current++;
				return updateToken(new Token(Double.valueOf(new String(data, st, current-st)), TokenType.DOUBLE));
			}
			return updateToken(new Token(Integer.valueOf(new String(data, st, current-st)), TokenType.INTEGER));
		}
		
		if(current+2<data.length && data[current]=='\"' && data[current+1]=='\"' && data[current+2]=='\"') {
			current+=3;
			int st = current;
			while(current+2<data.length && (data[current]!='\"' || data[current+1]!='\"' || data[current+2]!='\"')) current++;
			if(current+2>=data.length) throw new LexerException("Unterminated raw string found.");
			String str = new String(data,st,current-st);
			current += 3;
			return updateToken(new Token(str, TokenType.STRING));
		}		
		
		if(data[current]=='\"' && current+1<data.length && data[current+1]!='\"') {
			current++; // TODO dovrši
			StringBuilder sb = new StringBuilder();
			boolean closed = false;
			while(current<data.length) {
				if(data[current]=='\"') {
					closed = true;
					current++;
					break;
				}
				if(data[current]=='\\') {
					current++;
					if(current>=data.length) throw new LexerException("Unexpected end of specification source (unterminated string).");
					switch(data[current]) {
					case '\\': sb.append('\\'); current++; break;
					case '\"': sb.append('\"'); current++; break;
					case '\r': sb.append('\r'); current++; break;
					case '\n': sb.append('\n'); current++; break;
					default: throw new LexerException("Invalid string escape sequence found: \\"+data[current]+".");
					}
					continue;
				}
				sb.append(data[current]);
				current++;
			}
			if(!closed) throw new LexerException("Unterminated string found.");
			return updateToken(new Token(sb.toString(), TokenType.STRING));
		}
		
		
		throw new LexerException("Unexpected character: '"+data[current]+"'.");
	}

	private static final char[] SYMBOLS = {'{', '}', '(', ')', '[', ']', ';', '#', '@', ':', ','};
	
	private int oneOfSymbols(char c) {
		for(int i = 0; i < SYMBOLS.length; i++) {
			if(SYMBOLS[i]==c) return i;
		}
		return -1;
	}

	public Token getToken() {
		return currentToken;
	}
	
	private Token updateToken(Token t) {
		currentToken = t;
		return t;
	}
}
