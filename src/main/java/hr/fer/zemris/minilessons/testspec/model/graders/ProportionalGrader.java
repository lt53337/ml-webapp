package hr.fer.zemris.minilessons.testspec.model.graders;

import hr.fer.zemris.minilessons.testspec.model.ProblemGrader;

public class ProportionalGrader implements ProblemGrader {

	private double factor;
	
	public ProportionalGrader(double factor) {
		this.factor = factor;
	}

	@Override
	public double calculateScore(boolean solved, double correctness) {
		return !solved ? 0 : correctness * factor;
	}

}
