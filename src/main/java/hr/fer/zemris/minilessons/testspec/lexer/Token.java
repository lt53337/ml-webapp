package hr.fer.zemris.minilessons.testspec.lexer;

public class Token {

	private Object value;
	private TokenType tokenType;
	
	public Token(Object value, TokenType tokenType) {
		super();
		this.value = value;
		this.tokenType = tokenType;
	}
	
	public boolean isEOF() {
		return tokenType==TokenType.EOF;
	}
	
	public boolean isSymbol(char c) {
		return tokenType==TokenType.SYMBOL && ((Character)value).charValue() == c;
	}
	
	public boolean isIdentifier(String ident) {
		return tokenType==TokenType.IDENT && ident.equals(value);
	}
	
	public boolean isIdentifier() {
		return tokenType==TokenType.IDENT;
	}
	
	public boolean isInteger() {
		return tokenType==TokenType.INTEGER;
	}
	
	public boolean isDouble() {
		return tokenType==TokenType.DOUBLE;
	}
	
	public boolean isNumber() {
		return tokenType==TokenType.DOUBLE || tokenType==TokenType.INTEGER;
	}
	
	public boolean isMessageKey() {
		return tokenType==TokenType.MESSAGE_KEY;
	}

	public boolean isString() {
		return tokenType==TokenType.STRING;
	}
	
	public String asString() {
		return value==null ? null : value.toString();
	}
	
	@Override
	public String toString() {
		return "["+tokenType+", "+value+(value==null?"":" ("+value.getClass().getName()+")")+"]";
	}
	
	public Number asNumber() {
		return value==null ? null : (Number)value;
	}
	
	public Integer asInteger() {
		return value==null ? null : (Integer)value;
	}
	
	public Double asDouble() {
		return value==null ? null : (Double)value;
	}
	
}
