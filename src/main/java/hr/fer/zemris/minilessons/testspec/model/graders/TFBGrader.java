package hr.fer.zemris.minilessons.testspec.model.graders;

import hr.fer.zemris.minilessons.testspec.model.ProblemGrader;

public class TFBGrader implements ProblemGrader {

	private double forCorrect;
	private double forIncorrect;
	private double forUnsolved;
	private double tolerance;

	public TFBGrader(double forCorrect, double forIncorrect, double forUnsolved, double tolerance) {
		super();
		this.forCorrect = forCorrect;
		this.forIncorrect = forIncorrect;
		this.forUnsolved = forUnsolved;
		this.tolerance = tolerance;
	}
	
	@Override
	public double calculateScore(boolean solved, double correctness) {
		if(!solved) return forUnsolved;
		return correctness < 1-tolerance ? forIncorrect : forCorrect;
	}

}
