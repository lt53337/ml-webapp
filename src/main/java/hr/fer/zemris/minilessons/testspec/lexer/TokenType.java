package hr.fer.zemris.minilessons.testspec.lexer;

public enum TokenType {
	EOF,
	IDENT,
	STRING,
	INTEGER,
	DOUBLE,
	SYMBOL,
	MESSAGE_KEY
}
