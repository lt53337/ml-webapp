package hr.fer.zemris.minilessons.testspec.model;

import java.util.Optional;

import hr.fer.zemris.minilessons.localization.I18NText;

public class ProblemMessageProducerImpl implements ProblemMessageProducer {

	private Condition condition;
	private TranslationProvider provider;
	
	public ProblemMessageProducerImpl(Condition condition, TranslationProvider provider) {
		this.condition = condition;
		this.provider = provider;
	}

	@Override
	public Optional<I18NText> getText(CheckersEnvironment env, String languageTag) {
		if(!condition.test(env)) return Optional.empty();
		return provider.getTranslation(languageTag, true);
	}
	
}
