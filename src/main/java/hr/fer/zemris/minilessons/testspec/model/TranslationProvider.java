package hr.fer.zemris.minilessons.testspec.model;

import java.util.Optional;

import hr.fer.zemris.minilessons.localization.I18NText;

/**
 * Objekt koji dohvaća poruku na traženom jeziku, odnosno na defaultnom
 * jeziku ako traženi ne postoji i to je dopušteno.
 * 
 * @author marcupic
 *
 */
public interface TranslationProvider {

	Optional<I18NText> getTranslation(String languageTag, boolean fallBackToDefault);
	
}
