package hr.fer.zemris.minilessons.testspec.model;

/**
 * Model provjerivača koji treba utvrditi kad korisnik želi završiti zadatak,
 * je li to dopušteno. Različite implementacije bi mogle primjerice vratiti
 * true samo ako je zadatak rješavan i mjera točnosti je barem 0.5 i slično.
 * 
 * @author marcupic
 */
public interface FinishAcceptChecker {

	boolean finishAcceptable(boolean solved, double correctness, boolean error);

	static FinishAcceptChecker ALWAYS = (solved,correctness,error)->true;
	static FinishAcceptChecker IF_SOLVED = (solved,correctness,error)->solved;
	static FinishAcceptChecker IF_CORRECT = (solved,correctness,error)->solved && correctness >= 0.99999;
	
}
