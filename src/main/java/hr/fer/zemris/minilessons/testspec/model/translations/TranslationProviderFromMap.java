package hr.fer.zemris.minilessons.testspec.model.translations;

import java.util.Map;
import java.util.Optional;

import hr.fer.zemris.minilessons.localization.I18NText;
import hr.fer.zemris.minilessons.localization.I18NTexts;

public class TranslationProviderFromMap extends AbstractTranslationProvider {

	private String key;
	private Map<String,I18NTexts> map;
	
	public TranslationProviderFromMap(String key, Map<String,I18NTexts> map) {
		this.key = key;
		this.map = map;
	}

	@Override
	public Optional<I18NText> getTranslation(String languageTag, boolean fallBackToDefault) {
		I18NTexts texts = map.get(key);
		if(texts==null) throw new RuntimeException("Looked for translation of " + key + " which does not exists.");
		return getTranslation(texts, languageTag, fallBackToDefault);
	}
	
}
