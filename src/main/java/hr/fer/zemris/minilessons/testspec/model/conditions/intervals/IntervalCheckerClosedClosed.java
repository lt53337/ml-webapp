package hr.fer.zemris.minilessons.testspec.model.conditions.intervals;

public class IntervalCheckerClosedClosed extends AbstractIntervalChecker {

	public IntervalCheckerClosedClosed(double lower, double upper) {
		super(lower, upper);
	}

	@Override
	public boolean isInInterval(double value) {
		return value >= lower && value <= upper;
	}

}
