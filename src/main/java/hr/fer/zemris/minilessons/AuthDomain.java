package hr.fer.zemris.minilessons;

/**
 * Autentifikacijske domene koje su podržane.
 * 
 * @author marcupic
 *
 */
public enum AuthDomain {

	/**
	 * Autentifikacija korisnika ide lokalno preko baze.
	 */
	LOCAL("local"),
	/**
	 * Autentifikacija korisnika ide preko Ferka.
	 */
	FERKO("ferko"),
	/**
	 * Autentifikacija korisnika ide preko javne registracije
	 * (implementacijski, to će vjerojatno opet ići lokalno preko baze, 
	 * ali želimo biti u mogućnosti razlikovati korisnike koji su dodani
	 * lokalno te koji su stvoreni preko javne registracije).
	 */
	PUBLIC("public");
	
	private String key;
	
	AuthDomain(String key) {
		this.key = key;
	}
	
	public String getKey() {
		return key;
	}
}
