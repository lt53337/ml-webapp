package hr.fer.zemris.minilessons.periodicals;

/**
 * Testni periodical koji ispisuje na zaslon svaki puta kada je pokrenut. Služi za potrebe testiranja.
 * 
 * @author marcupic
 *
 */
public class DummyPeriodical implements Runnable {

	public DummyPeriodical() {
	}

	@Override
	public void run() {
		System.out.println(" >>> DummyPeriodical: job executed.");
	}
}
