package hr.fer.zemris.minilessons.periodicals;

import java.util.Date;
import java.util.List;

import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.dao.DAOUtil;
import hr.fer.zemris.minilessons.dao.model.IntValueStat;
import hr.fer.zemris.minilessons.dao.model.Minilesson;
import hr.fer.zemris.minilessons.dao.model.MinilessonStats;
import hr.fer.zemris.minilessons.dao.model.MinilessonUserGrade;
import hr.fer.zemris.minilessons.dao.model.Wish;
import hr.fer.zemris.minilessons.dao.model.WishUserVote;

/**
 * Periodical koji ažurira statističke podatke o minilekcijama.
 * 
 * @author marcupic
 *
 */
public class MLGrades implements Runnable {

	public MLGrades() {
	}

	@Override
	public void run() {
		
		// Calculate minilesson statistics...
		// ----------------------------------
		List<String> list = DAOUtil.transactional(()->{
			return DAOProvider.getDao().findUnprocessedMinilessons();
		});

		list.forEach(minilessonID -> {

			DAOUtil.transactional(()->{
				Minilesson minilesson = DAOProvider.getDao().findMinilessonByID(minilessonID);
				MinilessonStats stats = DAOProvider.getDao().findMinilessonStats(minilesson);
				List<MinilessonUserGrade> mugList = DAOProvider.getDao().findMinilessonUserGrades(minilessonID);
				
				fillStats(
					stats.getQualityStatistics(),
					mugList.stream().filter(mug -> mug.getQualityGrade()>0).mapToInt(mug->mug.getQualityGrade()).sorted().toArray()
				);
				fillStats(
					stats.getDifficultyStatistics(),
					mugList.stream().filter(mug -> mug.getDifficultyGrade()>0).mapToInt(mug->mug.getDifficultyGrade()).sorted().toArray()
				);
				fillStats(
					stats.getDurationStatistics(),
					mugList.stream().filter(mug -> mug.getDuration()>0).mapToInt(mug->mug.getDuration()).sorted().toArray()
				);

				stats.setLastCalculatedOn(new Date());

				mugList.forEach(mug -> {
					if(mug.isUnprocessed()) {
						mug.setUnprocessed(false);
					}
				});
				
				return null;
			});
			
		});
		
		// Calculate wish statistics...
		// ----------------------------------
		List<Long> list2 = DAOUtil.transactional(()->{
			return DAOProvider.getDao().findUnprocessedWishes();
		});
		
		list2.forEach(wishID -> {
			DAOUtil.transactional(()->{
				Wish wish = DAOProvider.getDao().findWish(wishID);
				List<WishUserVote> votes = DAOProvider.getDao().findWishVotes(wishID);
				int[] stat = new int[]{0,0,0};
				for(WishUserVote wuv : votes) {
					switch(wuv.getVote()) {
					case -1: stat[0]++; break;
					case  0: stat[1]++; break;
					case  1: stat[2]++; break;
					}
					if(wuv.isUnprocessed()) {
						wuv.setUnprocessed(false);
					}
				}
				if(wish.getNegativeVotes()!=stat[0]) {
					wish.setNegativeVotes(stat[0]);
				}
				if(wish.getPositiveVotes()!=stat[2]) {
					wish.setPositiveVotes(stat[2]);
				}
				return null;
			});
		});
	}

	private void fillStats(IntValueStat stats, int[] data) {
		if(data.length==0) {
			stats.setAverage(0.0);
			stats.setMedian(0.0);
			stats.setStddev(0.0);
			stats.setN(0);
			return;
		}
		int sum = 0;
		for(int d : data) {
			sum += d;
		}
		double avg = sum / (double)data.length;
		
		double diff = 0;
		for(int d : data) {
			diff += (d-avg)*(d-avg);
		}
		
		diff = diff / data.length;
		
		int half = data.length/2;
		double med;
		if(data.length % 2 == 0) {
			// Ako je paran, ide prosjek srednja dva
			med = (data[half-1]+data[half])/2.0;
		} else {
			// Ako je neparan, onda ide onaj na polovici
			med = data[half];
		}
		
		stats.setAverage(avg);
		stats.setMedian(med);
		stats.setStddev(Math.sqrt(diff));
		stats.setN(data.length);
		
	}
	
}
