package hr.fer.zemris.minilessons.periodicals.periods;

import java.time.LocalDateTime;

public class SpecificMoment extends AbstractPeriodicalTimeSpec {
	private int h;
	private int m;
	private int s;
	
	@Override
	public boolean isFormatOK(String str) {
		if(str.length() != 8) return false;
		if(str.charAt(2)!=':' || str.charAt(5)!=':') return false;
		if(!digits(str,0,1) || !digits(str,3,4) || !digits(str,6,7)) return false;
		if(number(str,0,1)>23 || number(str,3,4)>59 || number(str,6,7)>59) return false;
		return true;
	}

	@Override
	public PeriodicalTimeSpec get(String str) {
		if(!isFormatOK(str)) throw new IllegalArgumentException("'"+str+"' is not properly formatted for PeriodicalTimeSpec.");
		SpecificMoment s = new SpecificMoment();
		s.h = number(str,0,1);
		s.m = number(str,3,4);
		s.s = number(str,6,7);
		return s;
	}

	@Override
	public LocalDateTime nextTime(LocalDateTime now) {
		LocalDateTime ldt = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), h, m, s);
		if(now.isAfter(ldt)) {
			ldt = now.plusDays(1);
		}
		return ldt;
	}
}