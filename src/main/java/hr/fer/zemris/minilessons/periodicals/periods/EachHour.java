package hr.fer.zemris.minilessons.periodicals.periods;

import java.time.LocalDateTime;

public class EachHour extends AbstractPeriodicalTimeSpec {
	private int m;
	private int s;
	
	@Override
	public boolean isFormatOK(String str) {
		if(str.length() != 7) return false;
		if(str.charAt(1)!=':' || str.charAt(4)!=':') return false;
		if(!digits(str,2,3) || !digits(str,5,6)) return false;
		if(number(str,2,3)>59 || number(str,5,6)>59) return false;
		return true;
	}

	@Override
	public PeriodicalTimeSpec get(String str) {
		if(!isFormatOK(str)) throw new IllegalArgumentException("'"+str+"' is not properly formatted for EachHour.");
		EachHour s = new EachHour();
		s.m = number(str,2,3);
		s.s = number(str,5,6);
		return s;
	}

	@Override
	public LocalDateTime nextTime(LocalDateTime now) {
		LocalDateTime ldt = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), now.getHour(), m, s);
		if(now.isAfter(ldt)) {
			ldt = now.plusHours(1);
		}
		return ldt;
	}
}