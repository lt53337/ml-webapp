package hr.fer.zemris.minilessons.localization;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class I18NTexts {

	private Map<String,I18NText> translations;
	private Set<String> languages;
	
	public I18NTexts(Map<String,I18NText> translations) {
		this.translations = Collections.unmodifiableMap(new HashMap<>(translations));
		this.languages = Collections.unmodifiableSet(new LinkedHashSet<>(translations.keySet()));
	}

	public Set<String> getLanguages() {
		return languages;
	}
	
	public Map<String, I18NText> getTranslations() {
		return translations;
	}
	
	public String getTranslation(String language) {
		return translations.get(language).getText();
	}

	@Override
	public String toString() {
		return translations.values().toString();
	}
}
