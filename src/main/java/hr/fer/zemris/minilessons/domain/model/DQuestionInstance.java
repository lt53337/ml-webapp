package hr.fer.zemris.minilessons.domain.model;

import java.util.Date;

import hr.fer.zemris.minilessons.dao.model.QIStatus;

public class DQuestionInstance implements Comparable<DQuestionInstance> {

	private Long id;
	private QIStatus status;
	private double correctness;
	private boolean solved;
	private String configName;
	private DUser user;
	private Date createdOn;
	private Date evaluatedOn;
	private String idprefix;

	public DQuestionInstance() {
	}

	public DQuestionInstance(Long id, QIStatus status, double correctness, boolean solved, String configName,
			Date createdOn, Date evaluatedOn) {
		super();
		this.id = id;
		this.status = status;
		this.correctness = correctness;
		this.solved = solved;
		this.configName = configName;
		this.createdOn = createdOn;
		this.evaluatedOn = evaluatedOn;
	}

	@Override
	public int compareTo(DQuestionInstance o) {
		if(this.createdOn==null) {
			if(o.createdOn != null) return -1;
		} else {
			if(o.createdOn == null) return 1;
		}
		if(this.createdOn!=null && o.createdOn!=null) {
			int r = this.createdOn.compareTo(o.createdOn);
			if(r != 0) return r;
		}
		return 0;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIdprefix() {
		return idprefix;
	}
	public void setIdprefix(String idprefix) {
		this.idprefix = idprefix;
	}
	
	public QIStatus getStatus() {
		return status;
	}

	public void setStatus(QIStatus status) {
		this.status = status;
	}

	public double getCorrectness() {
		return correctness;
	}

	public void setCorrectness(double correctness) {
		this.correctness = correctness;
	}

	public boolean isSolved() {
		return solved;
	}

	public void setSolved(boolean solved) {
		this.solved = solved;
	}

	public String getConfigName() {
		return configName;
	}

	public void setConfigName(String configName) {
		this.configName = configName;
	}

	public DUser getUser() {
		return user;
	}

	public void setUser(DUser user) {
		this.user = user;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getEvaluatedOn() {
		return evaluatedOn;
	}

	public void setEvaluatedOn(Date evaluatedOn) {
		this.evaluatedOn = evaluatedOn;
	}
	
	
}
