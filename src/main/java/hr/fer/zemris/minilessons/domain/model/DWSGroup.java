package hr.fer.zemris.minilessons.domain.model;

import java.util.List;
import java.util.stream.Collectors;

import hr.fer.zemris.minilessons.dao.model.WSGroup;

public class DWSGroup implements Comparable<DWSGroup> {

	private Long id;
	private String name;
	private String secret;
	private boolean fixed;
	private boolean active;
	private List<DUser> users;
	private boolean validated;

	public DWSGroup() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public boolean isFixed() {
		return fixed;
	}

	public void setFixed(boolean fixed) {
		this.fixed = fixed;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public List<DUser> getUsers() {
		return users;
	}
	public void setUsers(List<DUser> users) {
		this.users = users;
	}
	
	public boolean isValidated() {
		return validated;
	}
	public void setValidated(boolean validated) {
		this.validated = validated;
	}
	
	public static DWSGroup fromDAOModel(WSGroup g, boolean addUsersInName) {
		DWSGroup d = new DWSGroup();
		d.setValidated(g.isValidated());
		d.setActive(g.isActive());
		d.setFixed(g.isFixed());
		d.setId(g.getId());
		d.setSecret(g.getSecret());
		if(addUsersInName) {
			if(g.getUsers().isEmpty()) {
				d.setName(g.getName());
			} else {
				d.setName(
					g.getName() + " (" +
					g.getUsers().stream().map(u->DUser.fromUser(u, false)).sorted().map(u->u.getFirstName()+" "+u.getLastName()+" / "+u.getAuthDomain()).collect(Collectors.joining(", ")) +
					")"
				);
			}
		} else {
			d.setName(g.getName());
		}
		return d;
	}
	
	public static DWSGroup fromDAOModelWithUsers(WSGroup g) {
		DWSGroup d = new DWSGroup();
		d.setName(g.getName());
		d.setValidated(g.isValidated());
		d.setActive(g.isActive());
		d.setFixed(g.isFixed());
		d.setId(g.getId());
		d.setSecret(g.getSecret());
		List<DUser> users = g.getUsers().stream().map(u->DUser.fromUser(u, false)).sorted().collect(Collectors.toList());
		d.setUsers(users);
		return d;
	}

	@Override
	public int compareTo(DWSGroup o) {
		int r = this.name.compareTo(o.name);
		if(r!=0) return r;
		return this.id.compareTo(o.id);
	}
}
