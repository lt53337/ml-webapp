package hr.fer.zemris.minilessons.domain.model;

public class DWSActivity {

	private Long id;
	private String title;

	public DWSActivity() {
	}
	
	public DWSActivity(Long id, String title) {
		super();
		this.id = id;
		this.title = title;
	}
	
	public Long getId() {
		return id;
	}
	
	public String getTitle() {
		return title;
	}
}
