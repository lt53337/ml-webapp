/**
 * This package contains domain model objects. Domain model objects are objects which are passed
 * between Data-Access Layer and Service Layer. They are here to insulate the presentation layer
 * from data-storage specific details.
 */
package hr.fer.zemris.minilessons.domain.model;

