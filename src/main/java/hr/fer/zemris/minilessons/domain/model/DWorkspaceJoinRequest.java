package hr.fer.zemris.minilessons.domain.model;

import java.util.Date;

import hr.fer.zemris.minilessons.dao.model.WSJoinRequestStatus;
import hr.fer.zemris.minilessons.dao.model.WorkspaceJoinRequest;

public class DWorkspaceJoinRequest {

	private DWorkspace workspace;
	private DUser user;
	private Long id;
	private Date createdAt;
	private WSJoinRequestStatus status;
	private String message;
	private Date resolutionDate;
	
	public DWorkspaceJoinRequest() {
	}

	public static DWorkspaceJoinRequest fromDAOModel(WorkspaceJoinRequest req, DWorkspace dws) {
		DWorkspaceJoinRequest d = new DWorkspaceJoinRequest();
		d.setCreatedAt(req.getCreatedAt());
		d.setId(req.getId());
		d.setMessage(req.getMessage());
		d.setResolutionDate(req.getResolutionDate());
		d.setStatus(req.getStatus());
		d.setUser(DUser.fromUser(req.getUser(), false));
		d.setWorkspace(dws);
		return d;
	}
	
	public DWorkspace getWorkspace() {
		return workspace;
	}
	public void setWorkspace(DWorkspace workspace) {
		this.workspace = workspace;
	}

	public DUser getUser() {
		return user;
	}
	public void setUser(DUser user) {
		this.user = user;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public WSJoinRequestStatus getStatus() {
		return status;
	}
	public void setStatus(WSJoinRequestStatus status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public Date getResolutionDate() {
		return resolutionDate;
	}
	public void setResolutionDate(Date resolutionDate) {
		this.resolutionDate = resolutionDate;
	}

	
}
