package hr.fer.zemris.minilessons.domain.model;

import java.util.Date;

public class DXMLQDef {

	private String id;
	private String title;
	private Date uploadedOn;
	private Long userID;
	private String lastName;
	private String firstName;
	private String authDomain;
	
	public DXMLQDef() {
	}
	
	public DXMLQDef(String id, String title, Date uploadedOn, Long userID, String lastName, String firstName,
			String authDomain) {
		super();
		this.id = id;
		this.title = title;
		this.uploadedOn = uploadedOn;
		this.userID = userID;
		this.lastName = lastName;
		this.firstName = firstName;
		this.authDomain = authDomain;
	}

	@Override
	public String toString() {
		if(id==null) return "";
		return String.format("%1$s (%2$tF %2$tT, id=%3$s, %4$s %5$s / %6$s)", title, uploadedOn, id, firstName, lastName, authDomain);
	}

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public Date getUploadedOn() {
		return uploadedOn;
	}
	
	public void setUploadedOn(Date uploadedOn) {
		this.uploadedOn = uploadedOn;
	}
	
	public Long getUserID() {
		return userID;
	}

	public void setUserID(Long userID) {
		this.userID = userID;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getAuthDomain() {
		return authDomain;
	}

	public void setAuthDomain(String authDomain) {
		this.authDomain = authDomain;
	}
}
