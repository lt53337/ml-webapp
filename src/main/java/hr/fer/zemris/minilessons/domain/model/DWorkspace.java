package hr.fer.zemris.minilessons.domain.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hr.fer.zemris.minilessons.dao.model.WSActivity;
import hr.fer.zemris.minilessons.dao.model.Workspace;
import hr.fer.zemris.minilessons.dao.model.WorkspaceJoinPolicy;

public class DWorkspace {

	private Long id;
	private String title;
	private String description;
	private DUser owner;
	private WorkspaceJoinPolicy joinPolicy;
	private boolean archived;
	private Date createdAt;
	private List<WSActivity> activities;
	
	public DWorkspace() {
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public DUser getOwner() {
		return owner;
	}
	public void setOwner(DUser owner) {
		this.owner = owner;
	}

	public WorkspaceJoinPolicy getJoinPolicy() {
		return joinPolicy;
	}
	public void setJoinPolicy(WorkspaceJoinPolicy joinPolicy) {
		this.joinPolicy = joinPolicy;
	}

	public boolean isArchived() {
		return archived;
	}
	public void setArchived(boolean archived) {
		this.archived = archived;
	}

	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public List<WSActivity> getActivities() {
		return activities;
	}
	public void setActivities(List<WSActivity> activities) {
		this.activities = activities;
	}
	
	public static DWorkspace fromDAOModel(Workspace ws, String lang) {
		return fromDAOModel(ws, lang, false);
	}

	public static DWorkspace fromDAOModel(Workspace ws, String lang, boolean fillActivities) {
		DWorkspace dws = new DWorkspace();
		dws.setId(ws.getId());
		dws.setArchived(ws.isArchived());
		dws.setCreatedAt(ws.getCreatedAt());
		dws.setDescription(ws.getDescriptions().getOrDefault(lang, "???"));
		dws.setJoinPolicy(ws.getJoinPolicy());
		dws.setTitle(ws.getTitles().getOrDefault(lang, "???"));
		dws.setOwner(DUser.fromUser(ws.getOwner(), false));
		if(fillActivities) {
			dws.activities = new ArrayList<>(ws.getActivities());
			dws.activities.sort((a1,a2)->a1.getTitles().getOrDefault(lang, "???").compareTo(a2.getTitles().getOrDefault(lang, "???")));
		}
		return dws;
	}
}
