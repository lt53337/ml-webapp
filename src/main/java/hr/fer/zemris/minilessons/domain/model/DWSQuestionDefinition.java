package hr.fer.zemris.minilessons.domain.model;

import java.util.Date;
import java.util.List;

import hr.fer.zemris.minilessons.dao.model.WSQDWorkflow;
import hr.fer.zemris.minilessons.dao.model.WSQDWorkflowState;
import hr.fer.zemris.minilessons.dao.model.WorkspacePage;

public class DWSQuestionDefinition {
	private Long id;
	private String title;
	private double maxScore;
	private WSQDWorkflow workflow;
	private WSQDWorkflowState workflowCurrentState;
	private WorkspacePage subpage;
	private String groupingTitle;
	private String activityTitle;
	private boolean groupAssignment;
	private Date createdAt;
	private List<WSQDWorkflowState> nextStates;
	private DWSActivitySource source;
	
	public DWSQuestionDefinition(Long id, String title, Date createdAt, double maxScore, WSQDWorkflow workflow,
			WSQDWorkflowState workflowCurrentState, WorkspacePage subpage, String groupingTitle, String activityTitle,
			boolean groupAssignment) {
		super();
		this.id = id;
		this.title = title;
		this.createdAt = createdAt;
		this.maxScore = maxScore;
		this.workflow = workflow;
		this.workflowCurrentState = workflowCurrentState;
		this.subpage = subpage;
		this.groupingTitle = groupingTitle;
		this.activityTitle = activityTitle;
		this.groupAssignment = groupAssignment;
	}

	public DWSQuestionDefinition(Long id, String title, Date createdAt, double maxScore, WSQDWorkflow workflow,
			WSQDWorkflowState workflowCurrentState, WorkspacePage subpage,
			boolean groupAssignment, Long sourceid, Double sourceMaxScore, Boolean sourceInvalid) {
		super();
		this.id = id;
		this.title = title;
		this.createdAt = createdAt;
		this.maxScore = maxScore;
		this.workflow = workflow;
		this.workflowCurrentState = workflowCurrentState;
		this.subpage = subpage;
		this.groupAssignment = groupAssignment;
		if(sourceid != null) {
			this.source = new DWSActivitySource();
			this.source.setId(sourceid);
			this.source.setInvalid(sourceInvalid);
			this.source.setMaxScore(maxScore);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public DWSActivitySource getSource() {
		return source;
	}
	public void setSource(DWSActivitySource source) {
		this.source = source;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getCreatedAt() {
		return createdAt;
	}
	
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	public double getMaxScore() {
		return maxScore;
	}

	public void setMaxScore(double maxScore) {
		this.maxScore = maxScore;
	}

	public WSQDWorkflow getWorkflow() {
		return workflow;
	}

	public void setWorkflow(WSQDWorkflow workflow) {
		this.workflow = workflow;
	}

	public WSQDWorkflowState getWorkflowCurrentState() {
		return workflowCurrentState;
	}

	public void setWorkflowCurrentState(WSQDWorkflowState workflowCurrentState) {
		this.workflowCurrentState = workflowCurrentState;
	}

	public WorkspacePage getSubpage() {
		return subpage;
	}

	public void setSubpage(WorkspacePage subpage) {
		this.subpage = subpage;
	}

	public String getGroupingTitle() {
		return groupingTitle;
	}

	public void setGroupingTitle(String groupingTitle) {
		this.groupingTitle = groupingTitle;
	}

	public String getActivityTitle() {
		return activityTitle;
	}
	
	public void setActivityTitle(String activityTitle) {
		this.activityTitle = activityTitle;
	}
	
	public boolean isGroupAssignment() {
		return groupAssignment;
	}

	public void setGroupAssignment(boolean groupAssignment) {
		this.groupAssignment = groupAssignment;
	}
	
	public List<WSQDWorkflowState> getNextStates() {
		return nextStates;
	}
	public void setNextStates(List<WSQDWorkflowState> nextStates) {
		this.nextStates = nextStates;
	}
}
