package hr.fer.zemris.minilessons.domain.model;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

import hr.fer.zemris.minilessons.dao.model.IntValueStat;
import hr.fer.zemris.minilessons.dao.model.MLBasicPermission;
import hr.fer.zemris.minilessons.dao.model.Minilesson;
import hr.fer.zemris.minilessons.dao.model.MinilessonApproval;
import hr.fer.zemris.minilessons.dao.model.MinilessonStats;
import hr.fer.zemris.minilessons.dao.model.MinilessonStatus;
import hr.fer.zemris.minilessons.dao.model.MinilessonUserGrade;
import hr.fer.zemris.minilessons.service.MLCurrentContext;

/**
 * Domain model of minilesson.
 * 
 * @author marcupic
 */
public class DMinilesson {
	
	/**
	 * Minilesson primary key.
	 */
	private String id; // UUID
	/**
	 * Minilesson declared public ID.
	 */
	private String publicID;
	/**
	 * Minilesson version.
	 */
	private long version;
	/**
	 * Minilesson declared minor version.
	 */
	private long minorVersion;
	/**
	 * Minilesson title. Should be in apropriate language for current request which is being served.
	 */
	private String title;
	/**
	 * Statistical data on minilesson quality.
	 */
	private IntValueStat qualityStats;
	/**
	 * Statistical data on minilesson difficulty.
	 */
	private IntValueStat difficultyStats;
	/**
	 * Statistical data on minilesson duration.
	 */
	private IntValueStat durationStats;
	/**
	 * Date when statistics calculation occurred.
	 */
	private Date lastCalculatedOn;
	/**
	 * Is this minilesson visible?
	 */
	private boolean visible;
	/**
	 * Is this minilesson archived?
	 */
	private boolean archived;
	/**
	 * Approval status of this minilesson.
	 */
	private MinilessonApproval approval;
	/**
	 * Owner of this minilesson (user that uploaded it).
	 */
	private DUser owner;
	/**
	 * When was this minilesson uploaded.
	 */
	private Date uploadedOn;
	/**
	 * When was this minilesson last modified?
	 */
	private Date lastModifiedOn;
	/**
	 * Who approved this minilesson?
	 */
	private DUser approvedBy;
	/**
	 * When was this minilesson approved?
	 */
	private Date approvedOn;
	/**
	 * Set of language tags on which this minilesson is offered.
	 */
	private Set<String> languages = new HashSet<>();
	/**
	 * Set of all minilesson titles.
	 */
	private Set<DLocalizedString> titles = new HashSet<>();
	/**
	 * Set of all minilesson tags.
	 */
	private Set<DLocalizedString> tags = new HashSet<>();
	/**
	 * Set of all tags for current language.
	 */
	private Set<String> tagsForCurrentLanguage = new LinkedHashSet<>();
	/**
	 * User-given quality grade. Zero means: not graded. Grades can be from 1 (bad) to 5 (excellent).
	 */
	private int qualityGrade;
	/**
	 * User-given difficulty grade. Zero means: not graded. Grades can be from 1 (easy) to 5 (extremely hard).
	 */
	private int difficultyGrade;
	/**
	 * Time, in minutes, that user entered (this is user-perceived entry) as the time
	 * it took him to complete this minilesson. Used for statistics. 
	 */
	private int duration;
	/**
	 * Completion date.
	 */
	private Date completedOn;
	/**
	 * Opening date.
	 */
	private Date openedAt;
	/**
	 * Can current user manager approval?
	 */
	private boolean approvalManageable;
	/**
	 * Video URL info as obtained by configured video URL producer.
	 */
	private String videoURLInfo;
	
	/**
	 * Getter for primary key.
	 * 
	 * @return primary ke
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * Getter for declared public ID.
	 * 
	 * @return declared public ID
	 */
	public String getPublicID() {
		return publicID;
	}
	
	/**
	 * Getter for version.
	 * 
	 * @return version
	 */
	public long getVersion() {
		return version;
	}
	
	/**
	 * Getter for title.
	 * 
	 * @return title
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * Helper method to convert Minilesson DAO model object into this domain object.
	 * @param ml minilesson DAO object
	 * @param lang language for language-specific string retrieval
	 * @return domain minilesson object
	 */
	public static DMinilesson fromDAOModel(Minilesson ml, String lang) {
		DMinilesson dml = new DMinilesson();
		dml.id = ml.getId();
		dml.publicID = ml.getPublicID();
		dml.version = ml.getVersion();
		dml.minorVersion = ml.getMinorVersion();
		dml.title = ml.getTitles().getOrDefault(lang, "???");
		dml.visible = ml.isVisible();
		dml.archived = ml.isArchived();
		dml.approval = ml.getApproval();
		dml.owner = DUser.fromUser(ml.getOwner(), false);
		dml.uploadedOn = new Date(ml.getUploadedOn().getTime());
		dml.lastModifiedOn = ml.getLastModifiedOn()==null ? null : new Date(ml.getLastModifiedOn().getTime());
		dml.approvedBy = ml.getApprovedBy()==null ? null : DUser.fromUser(ml.getApprovedBy(), false);
		dml.approvedOn = ml.getApprovedOn()==null ? null : new Date(ml.getApprovedOn().getTime());
		dml.languages = new TreeSet<>(ml.getLanguages());
		dml.titles = new HashSet<>();
		ml.getTitles().forEach((l,t)->{dml.titles.add(new DLocalizedString(l, t));});
		dml.tags = new HashSet<>();
		ml.getTags().forEach(lt->{dml.tags.add(new DLocalizedString(lt.getLang(), lt.getText()));});
		dml.tagsForCurrentLanguage = new HashSet<>();
		ml.getTags().stream().filter(lt->lt.getLang().equals(lang)).forEach(lt->{dml.tagsForCurrentLanguage.add(lt.getText());});
		dml.setApprovalManageable(false);
		if(MLCurrentContext.getAuthUser()!=null) {
			Set<String> permissions = MLCurrentContext.getAuthUser().getPermissions();
			if(permissions.contains(MLBasicPermission.ADMINISTRATION.getPermissonName()) || permissions.contains(MLBasicPermission.CAN_APPROVE_MINILESSONS.getPermissonName())) {
				dml.setApprovalManageable(true);
			}
		}
		
		return dml;
	}

	/**
	 * Helper method to convert MinilessonStats DAO model object into this domain object.
	 * 
	 * @param mls statistical data with reference to minilesson object
	 * @param lang language for language-specific string retrieval
	 * @return domain minilesson object
	 */
	public static DMinilesson fromDAOModel(MinilessonStats mls, String lang) {
		DMinilesson dml = fromDAOModel(mls.getMinilesson(), lang);
		dml.qualityStats = mls.getQualityStatistics().copy();
		dml.difficultyStats = mls.getDifficultyStatistics().copy();
		dml.durationStats = mls.getDurationStatistics();
		dml.lastCalculatedOn = mls.getLastCalculatedOn();
		return dml;
	}

	/**
	 * Fill user grading data for this minilesson into existing domain object.
	 * 
	 * @param dml domain minilesson object
	 * @param mug statistical data
	 * @return
	 */
	public static DMinilesson fillWithUserGrades(DMinilesson dml, MinilessonUserGrade mug) {
		if(mug==null) {
			dml.qualityGrade = 0;
			dml.difficultyGrade = 0;
			dml.duration = 0;
		} else {
			dml.qualityGrade = mug.getQualityGrade();
			dml.difficultyGrade = mug.getDifficultyGrade();
			dml.duration = mug.getDuration();
		}
		return dml;
	}

	public static DMinilesson fillWithUserStatus(DMinilesson dml, MinilessonStatus ms) {
		if(ms==null) {
			dml.openedAt = null;
			dml.completedOn = null;
		} else {
			dml.openedAt = ms.getOpenedAt();
			dml.completedOn = ms.getCompletedOn();
		}
		return dml;
	}

	
	public long getMinorVersion() {
		return minorVersion;
	}

	public void setMinorVersion(long minorVersion) {
		this.minorVersion = minorVersion;
	}

	
	public IntValueStat getQualityStats() {
		return qualityStats;
	}

	public void setQualityStats(IntValueStat qualityStats) {
		this.qualityStats = qualityStats;
	}

	public IntValueStat getDifficultyStats() {
		return difficultyStats;
	}

	public void setDifficultyStats(IntValueStat difficultyStats) {
		this.difficultyStats = difficultyStats;
	}

	public IntValueStat getDurationStats() {
		return durationStats;
	}

	public void setDurationStats(IntValueStat durationStats) {
		this.durationStats = durationStats;
	}

	public Date getLastCalculatedOn() {
		return lastCalculatedOn;
	}

	public void setLastCalculatedOn(Date lastCalculatedOn) {
		this.lastCalculatedOn = lastCalculatedOn;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public boolean isArchived() {
		return archived;
	}

	public void setArchived(boolean archived) {
		this.archived = archived;
	}

	public MinilessonApproval getApproval() {
		return approval;
	}

	public void setApproval(MinilessonApproval approval) {
		this.approval = approval;
	}

	public DUser getOwner() {
		return owner;
	}

	public void setOwner(DUser owner) {
		this.owner = owner;
	}

	public Date getUploadedOn() {
		return uploadedOn;
	}

	public void setUploadedOn(Date uploadedOn) {
		this.uploadedOn = uploadedOn;
	}

	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public DUser getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(DUser approvedBy) {
		this.approvedBy = approvedBy;
	}

	public Date getApprovedOn() {
		return approvedOn;
	}

	public void setApprovedOn(Date approvedOn) {
		this.approvedOn = approvedOn;
	}

	public Set<String> getLanguages() {
		return languages;
	}

	public void setLanguages(Set<String> languages) {
		this.languages = languages;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setPublicID(String publicID) {
		this.publicID = publicID;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Set<DLocalizedString> getTitles() {
		return titles;
	}

	public void setTitles(Set<DLocalizedString> titles) {
		this.titles = titles;
	}

	public Set<DLocalizedString> getTags() {
		return tags;
	}

	public void setTags(Set<DLocalizedString> tags) {
		this.tags = tags;
	}

	public Set<String> getTagsForCurrentLanguage() {
		return tagsForCurrentLanguage;
	}

	public void setTagsForCurrentLanguage(Set<String> tagsForCurrentLanguage) {
		this.tagsForCurrentLanguage = tagsForCurrentLanguage;
	}

	public int getQualityGrade() {
		return qualityGrade;
	}

	public void setQualityGrade(int qualityGrade) {
		this.qualityGrade = qualityGrade;
	}

	public int getDifficultyGrade() {
		return difficultyGrade;
	}

	public void setDifficultyGrade(int difficultyGrade) {
		this.difficultyGrade = difficultyGrade;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public Date getCompletedOn() {
		return completedOn;
	}

	public void setCompletedOn(Date completedOn) {
		this.completedOn = completedOn;
	}

	public Date getOpenedAt() {
		return openedAt;
	}

	public void setOpenedAt(Date openedAt) {
		this.openedAt = openedAt;
	}
	
	public boolean isApprovalManageable() {
		return approvalManageable;
	}
	
	public void setApprovalManageable(boolean approvalManageable) {
		this.approvalManageable = approvalManageable;
	}
	
	public String getVideoURLInfo() {
		return videoURLInfo;
	}
	
	public void setVideoURLInfo(String videoURLInfo) {
		this.videoURLInfo = videoURLInfo;
	}
}
