package hr.fer.zemris.minilessons;

import java.util.Collection;

/**
 * Utility class for localization filtering.
 * 
 * @author marcupic
 *
 */
public class LangUtil {

	/**
	 * Default value which will be returned if string with some language tag is not found.
	 */
	private static final String DEFAULT_STRING_VALUE = "???";
	
	/**
	 * From a collection of localized strings retrieves the one with requested language tag.
	 *  
	 * @param col collection of localized strings
	 * @param lang language tag
	 * @return string for requested language tag or {@link #DEFAULT_STRING_VALUE} if col is <code>null</code> or string with requested tag is not present
	 */
	public static String findLocalization(Collection<? extends ILocalizedString> col, String lang) {
		if(col==null) return DEFAULT_STRING_VALUE;
		for(ILocalizedString ls : col) {
			if(ls.getLang().equals(lang)) return ls.getText();
		}
		return DEFAULT_STRING_VALUE;
	}
	
}
