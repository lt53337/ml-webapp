package hr.fer.zemris.minilessons.web.controllers;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.dao.model.Course;
import hr.fer.zemris.minilessons.dao.model.MLBasicPermission;
import hr.fer.zemris.minilessons.service.Authenticated;
import hr.fer.zemris.minilessons.service.DAOProvided;
import hr.fer.zemris.minilessons.service.MLCurrentContext;
import hr.fer.zemris.minilessons.service.Transactional;
import hr.fer.zemris.minilessons.util.StringUtil;
import hr.fer.zemris.minilessons.util.UUIDUtil;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal.Technology;
import hr.fer.zemris.minilessons.xml.coursedef.CourseDefFactory;

public class CourseImporter {

	@DAOProvided @Authenticated(redirectAllowed=true)
	public TemplateRetVal importForm(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		if(!MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName())) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		return new TemplateRetVal("courseimportform", Technology.THYMELEAF);
	}
	
	@DAOProvided @Authenticated(redirectAllowed=false) @Transactional
	public TemplateRetVal importAction(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		boolean canAdmin = MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());
		if(!canAdmin) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		String xml = StringUtil.trim(req.getParameter("xml"));
		if(xml==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		Course course = null;
		try {
			course = CourseDefFactory.fromText(xml);
		} catch(Exception ex) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		course.setActive(true);
		course.setId(UUIDUtil.nextUUID());
		course.getDetails().setModifiedOn(course.getCreatedOn());
		
		DAOProvider.getDao().saveCourse(course);
		
		resp.sendRedirect(req.getContextPath()+"/ml/home");
		
		return null;
	}

}
