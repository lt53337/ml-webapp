package hr.fer.zemris.minilessons.web.controllers;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.minilessons.dao.DAO;
import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.dao.model.JSQuestionDefinition;
import hr.fer.zemris.minilessons.service.DAOProvided;
import hr.fer.zemris.minilessons.service.Transactional;
import hr.fer.zemris.minilessons.web.WebUtil;
import hr.fer.zemris.minilessons.web.retvals.StreamRetVal;

public class QuestionFiles {

	// Vraća datoteku iz poddirektorija files u paketu pitanja
	@DAOProvided @Transactional
	public StreamRetVal process(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		
		DAO dao = DAOProvider.getDao();
		String qid = variables.get("qid");
		JSQuestionDefinition q = dao.findJSQuestionDefinitionByID(qid);
		if(q==null) {
			System.out.println("Nisam ga pronašao!");
			return null;
		}
		
		String fid = variables.get("fid");
		if(fid.indexOf('\\')!=-1 || fid.indexOf('/')!=-1 || fid.indexOf('\'')!=-1 || fid.indexOf('\"')!=-1) return null;
		
		Path filePath = dao.getRealQPathFor(qid, "files/"+fid);
		if(filePath==null) {
			return null;
		}

		if(!fid.toLowerCase().endsWith(".jnlp")) {
			return new StreamRetVal(filePath);
		}

		if(Files.size(filePath)>10240) {
			// Odbijamo u memoriju učitavati JNLP datoteke veće od 10k.
			return null;
		}
		
		StringBuffer sb = req.getRequestURL();
		int pos = sb.lastIndexOf("/");
		if(pos==-1) return null;
		sb.delete(pos+1, sb.length());
		
		String reqURL = sb.toString();

		System.out.println("Koristim stazu: ["+reqURL+"]");
		
		byte[] data = Files.readAllBytes(filePath);
		String dataStr = new String(data, StandardCharsets.UTF_8);
		String newDataStr = Pattern.compile("@ML\\(codebase\\)").matcher(dataStr).replaceAll(reqURL);

		return new StreamRetVal(newDataStr.getBytes(StandardCharsets.UTF_8), WebUtil.guessMime(fid), fid);
	}
	
}
