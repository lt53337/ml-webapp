package hr.fer.zemris.minilessons.web.video;

/**
 * This is Video URL generator sutable for development purposes. It expects that video
 * materials will be directly copied into <code>webapp-project/src/main/webapp/videos/</code>
 * when web-app is run so that servlet-container serves it. In production, servlet
 * container will typically be hidden behind web server and the system will be configured so
 * that web server serves video materials directly in order to bypass servlet container.
 * 
 * @author marcupic
 */
public class TestVideoURLProducer implements VideoURLProducer {

	/**
	 * Constructor. Does not use configuration parameter.
	 *  
	 * @param configuration not used but declared because this constructor signature is expected to exists
	 */
	public TestVideoURLProducer(String configuration) {
	}

	@Override
	public String produce(String contextPath, String uid, String videoFileName) {
		return contextPath+"/videos/"+videoFileName;
	}
	
	@Override
	public String info(String contextPath, String uid) {
		return contextPath+"/videos/ (webapp-project/src/main/webapp/videos/)";
	}
}
