package hr.fer.zemris.minilessons.web.controllers.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;

import javax.servlet.http.HttpServletRequest;

import hr.fer.zemris.minilessons.util.StringUtil;

public class AbstractWebForm {

	/**
	 * Map of fieldName to errorKey.
	 */
	protected Map<String, ErrorInfo> errors = new HashMap<>();
	protected Map<String, Object> stringValues = new HashMap<>();
	protected Map<String, Object> typedValues = new HashMap<>();
	private List<WebFormConstraint> elements = new ArrayList<>();
	
	public AbstractWebForm() {
	}

	public void fillFromData(Object obj) {
		for(WebFormConstraint c : elements) {
			Object value = c.getGetter().get(obj);
			if(value==null) {
				stringValues.put(c.getFieldName(), value);
			} else if(value instanceof Set<?>){
				Set<String> set = new LinkedHashSet<>();
				for(Object o : (Set<?>)value) {
					set.add(o.toString());
				}
				stringValues.put(c.getFieldName(), set);
			} else if(value instanceof List<?>){
				List<String> list = new ArrayList<>();
				for(Object o : (List<?>)value) {
					list.add(o.toString());
				}
				stringValues.put(c.getFieldName(), list);
			} else {
				stringValues.put(c.getFieldName(), value.toString());
			}
		}
	}
	
	public void fillDataObject(Object obj) {
		for(WebFormConstraint c : elements) {
			c.getSetter().set(obj, typedValues.get(c.getFieldName()));
		}
	}
	
	public void initBlank() {
		for(WebFormConstraint c : elements) {
			c.initBlank(stringValues);
		}
	}
	
	public void fillAndVerify(HttpServletRequest req) {
		errors.clear();
		for(WebFormConstraint c : elements) {
			c.readAndValidate(req, errors, stringValues, typedValues);
		}
	}

	public Object getStringValue(String fieldName) {
		return stringValues.get(fieldName);
	}
	
	public Object getTypedValue(String fieldName) {
		return typedValues.get(fieldName);
	}
	
	public boolean hasErrors() {
		return !errors.isEmpty();
	}
	
	public boolean hasError(String fieldName) {
		ErrorInfo ei = errors.get(fieldName);
		return ei!=null;
	}
	
	public ErrorInfo getError(String fieldName) {
		return errors.get(fieldName);
	}
	
	public String getErrorFieldID(String fieldName) {
		ErrorInfo ei = errors.get(fieldName);
		return ei==null ? "?" : ei.fieldID;
	}
	
	public String getErrorErrorID(String fieldName) {
		ErrorInfo ei = errors.get(fieldName);
		return ei==null ? "?" : ei.errorID;
	}
	
	protected <T extends Enum<?>> void addEnum(String fieldName, String fieldID, Getter getter, Setter setter, Class<T> clazz) {
		elements.add(new EnumValue<>(fieldName, fieldID, false, getter, setter, clazz));
	}
	
	protected void addInteger(String fieldName, String fieldID, Getter getter, Setter setter) {
		elements.add(new IntegerValue(fieldName, fieldID, false, getter, setter));
	}

	protected void addOptionalInteger(String fieldName, String fieldID, Getter getter, Setter setter) {
		elements.add(new IntegerValue(fieldName, fieldID, true, getter, setter));
	}

	protected void addLong(String fieldName, String fieldID, Getter getter, Setter setter) {
		elements.add(new LongValue(fieldName, fieldID, false, getter, setter));
	}

	protected void addOptionalLong(String fieldName, String fieldID, Getter getter, Setter setter) {
		elements.add(new LongValue(fieldName, fieldID, true, getter, setter));
	}

	protected void addStringSet(String fieldName, String fieldID, Getter getter, Setter setter, Set<String> controlSet) {
		elements.add(new StringSetValue(fieldName, fieldID, false, getter, setter, controlSet));
	}

	protected void addOptionalStringSet(String fieldName, String fieldID, Getter getter, Setter setter, Set<String> controlSet) {
		elements.add(new StringSetValue(fieldName, fieldID, true, getter, setter, controlSet));
	}

	protected void addString(String fieldName, String fieldID, Getter getter, Setter setter) {
		elements.add(new StringValue(fieldName, fieldID, false, getter, setter, null));
	}

	protected void addOptionalString(String fieldName, String fieldID, Getter getter, Setter setter) {
		elements.add(new StringValue(fieldName, fieldID, true, getter, setter, null));
	}

	protected void addString(String fieldName, String fieldID, Getter getter, Setter setter, Predicate<String> validator) {
		elements.add(new StringValue(fieldName, fieldID, false, getter, setter, validator));
	}

	protected void addOptionalString(String fieldName, String fieldID, Getter getter, Setter setter, Predicate<String> validator) {
		elements.add(new StringValue(fieldName, fieldID, true, getter, setter, validator));
	}

	protected void addString(String fieldName, String fieldID, Getter getter, Setter setter, Set<String> controlSet) {
		elements.add(new StringValue(fieldName, fieldID, false, getter, setter, s -> controlSet==null ? true : controlSet.contains(s)));
	}

	protected void addOptionalString(String fieldName, String fieldID, Getter getter, Setter setter, Set<String> controlSet) {
		elements.add(new StringValue(fieldName, fieldID, true, getter, setter, s -> controlSet==null ? true : controlSet.contains(s)));
	}
	
	protected void addBoolean(String fieldName, String fieldID, Getter getter, Setter setter) {
		elements.add(new BooleanValue(fieldName, fieldID, false, getter, setter));
	}

	protected void addOptionalBoolean(String fieldName, String fieldID, Getter getter, Setter setter) {
		elements.add(new BooleanValue(fieldName, fieldID, true, getter, setter));
	}

	public interface Getter {
		Object get(Object obj);
	}
	
	public interface Setter {
		void set(Object obj, Object value);
	}
	
	public static interface WebFormConstraint {
		String getFieldName();
		void initBlank(Map<String, Object> stringValues);
		String getFieldID();
		Getter getGetter();
		Setter getSetter();
		boolean readAndValidate(HttpServletRequest req, Map<String, ErrorInfo> errors, Map<String, Object> stringValues, Map<String, Object> typedValues);
	}

	abstract public static class AbstractWebFormConstraint implements WebFormConstraint {
		private String fieldName;
		private String fieldID;
		private Getter getter;
		private Setter setter;

		public AbstractWebFormConstraint(String fieldName, String fieldID, Getter getter, Setter setter) {
			super();
			this.fieldName = Objects.requireNonNull(fieldName);
			this.fieldID = Objects.requireNonNull(fieldID);
			this.getter = Objects.requireNonNull(getter);
			this.setter = Objects.requireNonNull(setter);
		}

		@Override
		public String getFieldName() {
			return fieldName;
		}
		
		@Override
		public String getFieldID() {
			return fieldID;
		}
		
		@Override
		public Getter getGetter() {
			return getter;
		}
		
		@Override
		public Setter getSetter() {
			return setter;
		}
	}

	public static class IntegerValue extends AbstractWebFormConstraint {
		private boolean optional;
		
		public IntegerValue(String fieldName, String fieldID, boolean optional, Getter getter, Setter setter) {
			super(fieldName, fieldID, getter, setter);
			this.optional = optional;
		}
		
		@Override
		public void initBlank(Map<String, Object> stringValues) {
			stringValues.put(getFieldName(), optional ? null : Integer.valueOf(0));
		}
		
		@Override
		public boolean readAndValidate(HttpServletRequest req, Map<String, ErrorInfo> errors, Map<String, Object> stringValues, Map<String, Object> typedValues) {
			String val = StringUtil.trim(req.getParameter(getFieldName()));
			stringValues.put(getFieldName(), val);
			if(val==null) {
				if(optional) {
					typedValues.put(getFieldName(), null);
					return true;
				}
				errors.put(getFieldName(), new ErrorInfo(getFieldID(), "requiredParameter"));
				return false;
			}
			Integer v = null;
			try {
				v = Integer.valueOf(val);
			} catch(Exception ex) {
				errors.put(getFieldName(), new ErrorInfo(getFieldID(), "invalidParameterFormat"));
				return false;
			}
			typedValues.put(getFieldName(), v);
			return true;
		}
	}
	
	public static class LongValue extends AbstractWebFormConstraint {
		private boolean optional;
		
		public LongValue(String fieldName, String fieldID, boolean optional, Getter getter, Setter setter) {
			super(fieldName, fieldID, getter, setter);
			this.optional = optional;
		}
		
		@Override
		public void initBlank(Map<String, Object> stringValues) {
			stringValues.put(getFieldName(), optional ? null : Long.valueOf(0));
		}
		
		@Override
		public boolean readAndValidate(HttpServletRequest req, Map<String, ErrorInfo> errors, Map<String, Object> stringValues, Map<String, Object> typedValues) {
			String val = StringUtil.trim(req.getParameter(getFieldName()));
			stringValues.put(getFieldName(), val);
			if(val==null) {
				if(optional) {
					typedValues.put(getFieldName(), null);
					return true;
				}
				errors.put(getFieldName(), new ErrorInfo(getFieldID(), "requiredParameter"));
				return false;
			}
			Long v = null;
			try {
				v = Long.valueOf(val);
			} catch(Exception ex) {
				errors.put(getFieldName(), new ErrorInfo(getFieldID(), "invalidParameterFormat"));
				return false;
			}
			typedValues.put(getFieldName(), v);
			return true;
		}
	}
	
	public static class StringValue extends AbstractWebFormConstraint {
		private boolean optional;
		private Predicate<String> validator;
		
		public StringValue(String fieldName, String fieldID, boolean optional, Getter getter, Setter setter, Predicate<String> validator) {
			super(fieldName, fieldID, getter, setter);
			this.optional = optional;
			this.validator = validator;
		}
		
		@Override
		public void initBlank(Map<String, Object> stringValues) {
			stringValues.put(getFieldName(), optional ? null : "");
		}
		
		@Override
		public boolean readAndValidate(HttpServletRequest req, Map<String, ErrorInfo> errors, Map<String, Object> stringValues, Map<String, Object> typedValues) {
			String val = StringUtil.trim(req.getParameter(getFieldName()));
			stringValues.put(getFieldName(), val);
			if(val==null) {
				if(optional) {
					typedValues.put(getFieldName(), null);
					return true;
				}
				errors.put(getFieldName(), new ErrorInfo(getFieldID(), "requiredParameter"));
				return false;
			}
			if(validator!=null && !validator.test(val)) {
				errors.put(getFieldName(), new ErrorInfo(getFieldID(), "invalidParameterFormat"));
				return false;
			}
			typedValues.put(getFieldName(), val);
			return true;
		}
	}

	public static class BooleanValue extends AbstractWebFormConstraint {
		private boolean optional;
		
		public BooleanValue(String fieldName, String fieldID, boolean optional, Getter getter, Setter setter) {
			super(fieldName, fieldID, getter, setter);
			this.optional = optional;
		}
		
		@Override
		public void initBlank(Map<String, Object> stringValues) {
			stringValues.put(getFieldName(), optional ? null : Boolean.FALSE);
		}
		
		@Override
		public boolean readAndValidate(HttpServletRequest req, Map<String, ErrorInfo> errors, Map<String, Object> stringValues, Map<String, Object> typedValues) {
			String val = StringUtil.trim(req.getParameter(getFieldName()));
			stringValues.put(getFieldName(), val);
			if(val==null) {
				typedValues.put(getFieldName(), Boolean.FALSE);
				return true;
			}
			if(val.equals("1") || val.equals("true") || val.equals("on")) {
				typedValues.put(getFieldName(), Boolean.TRUE);
				return true;
			} else {
				typedValues.put(getFieldName(), Boolean.FALSE);
				return true;
			}
		}
	}
	
	public static class EnumValue<T extends Enum<?>> extends AbstractWebFormConstraint {
		private boolean optional;
		private Class<T> clazz;
		
		public EnumValue(String fieldName, String fieldID, boolean optional, Getter getter, Setter setter, Class<T> clazz) {
			super(fieldName, fieldID, getter, setter);
			this.optional = optional;
			this.clazz = clazz;
		}
		
		@Override
		public void initBlank(Map<String, Object> stringValues) {
			stringValues.put(getFieldName(), optional ? null : clazz.getEnumConstants()[0].name());
		}
		
		@Override
		public boolean readAndValidate(HttpServletRequest req, Map<String, ErrorInfo> errors, Map<String, Object> stringValues, Map<String, Object> typedValues) {
			String val = StringUtil.trim(req.getParameter(getFieldName()));
			stringValues.put(getFieldName(), val);
			if(val==null) {
				if(optional) {
					typedValues.put(getFieldName(), null);
					return true;
				}
				errors.put(getFieldName(), new ErrorInfo(getFieldID(), "requiredParameter"));
				return false;
			}
			
			T[] legalValues = clazz.getEnumConstants();
			int index = -1;
			for(int i = 0; i < legalValues.length; i++) {
				if(legalValues[i].name().equals(val)) {
					index=i;
					break;
				}
			}
			
			if(index==-1) {
				errors.put(getFieldName(), new ErrorInfo(getFieldID(), "invalidParameterFormat"));
				return false;
			}
			
			typedValues.put(getFieldName(), legalValues[index]);
			return true;
		}
	}

	public static class StringSetValue extends AbstractWebFormConstraint {
		private boolean optional;
		private Set<String> controlSet;
		
		public StringSetValue(String fieldName, String fieldID, boolean optional, Getter getter, Setter setter, Set<String> controlSet) {
			super(fieldName, fieldID, getter, setter);
			this.optional = optional;
			this.controlSet = controlSet;
		}
		
		@Override
		public void initBlank(Map<String, Object> stringValues) {
			stringValues.put(getFieldName(), new HashSet<>());
		}
		
		@Override
		public boolean readAndValidate(HttpServletRequest req, Map<String, ErrorInfo> errors, Map<String, Object> stringValues, Map<String, Object> typedValues) {
			String[] values = req.getParameterValues(getFieldName());
			Set<String> set = new LinkedHashSet<>();
			if(values!=null) {
				for(String v : values) {
					v = StringUtil.trim(v);
					if(v!=null) set.add(v);
				}
			}
			stringValues.put(getFieldName(), set);
			if(set.isEmpty()) {
				if(optional) {
					typedValues.put(getFieldName(), set);
					return true;
				}
				errors.put(getFieldName(), new ErrorInfo(getFieldID(), "invalidParameterFormat"));
				return false;
			}
			
			if(controlSet!=null) {
				for(String v : set) {
					if(!controlSet.contains(v)) {
						errors.put(getFieldName(), new ErrorInfo(getFieldID(), "requiredParameter"));
						return false;
					}
				}
			}
			
			typedValues.put(getFieldName(), set);
			return true;
		}
	}

	
	/**
	 * Helper class for error description.
	 * 
	 * @author marcupic
	 *
	 */
	public static class ErrorInfo {
		/**
		 * This is key which localizes form-field name.
		 */
		private String fieldID;
		/**
		 * This is key which localizes error message that has one parameter.
		 */
		private String errorID;
		
		/**
		 * Constructor.
		 * @param fieldID field-name key
		 * @param errorID error message key
		 */
		public ErrorInfo(String fieldID, String errorID) {
			super();
			this.fieldID = fieldID;
			this.errorID = errorID;
		}
		
		/**
		 * Getter for key which localizes error message that has one parameter.
		 * @return key
		 */
		public String getErrorID() {
			return errorID;
		}
		
		/**
		 * Getter for key which localizes form-field name.
		 * @return ID
		 */
		public String getFieldID() {
			return fieldID;
		}
	}
}
