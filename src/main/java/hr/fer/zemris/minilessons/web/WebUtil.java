package hr.fer.zemris.minilessons.web;

import java.io.IOException;
import java.io.Writer;

/**
 * Helper class for web-based stuff.
 * 
 * @author marcupic
 *
 */
public class WebUtil {

	/**
	 * Based on given filename method extracts file extension and returns
	 * appropriate mime-type. Only a small subset is currently supported.
	 * Text based format will have indication that file encoding will be
	 * UTF-8.
	 * 
	 * @param name file name
	 * @return mime-type for given file name
	 */
	public static String guessMime(String name) {
		if(name==null || name.isEmpty()) return "application/octet-stream";
		int i = name.lastIndexOf('.');
		String ext = i==-1 ? name.toLowerCase() : name.substring(i+1).toLowerCase();
		if(ext.equals("gif")) return "image/gif";
		if(ext.equals("png")) return "image/png";
		if(ext.equals("jpg")) return "image/jpg";
		if(ext.equals("zip")) return "application/zip";
		if(ext.equals("7z")) return "application/x-7z-compressed";
		if(ext.equals("rar")) return "application/x-rar-compressed";
		if(ext.equals("xml")) return "application/xml";
		if(ext.equals("java")) return "text/x-java-source";
		if(ext.equals("class")) return "application/java";
		if(ext.equals("map")) return "application/json";
		if(ext.equals("jar")) return "application/java-archive";
		if(ext.equals("odt")) return "application/vnd.oasis.opendocument.text";
		if(ext.equals("odg")) return "application/vnd.oasis.opendocument.graphics";
		if(ext.equals("odp")) return "application/vnd.oasis.opendocument.presentation";
		if(ext.equals("doc")) return "application/msword";
		if(ext.equals("docx")) return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
		if(ext.equals("ps")) return "application/postscript";
		if(ext.equals("ppt")) return "application/vnd.ms-powerpoint";
		if(ext.equals("pptx")) return "application/vnd.openxmlformats-officedocument.presentationml.presentation";
		if(ext.equals("xls")) return "application/vnd.ms-excel";
		if(ext.equals("xlsx")) return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
		if(ext.equals("txt")) return "text/plain; charset=utf-8";
		if(ext.equals("html")) return "text/html; charset=utf-8";
		if(ext.equals("htm")) return "text/html; charset=utf-8";
		if(ext.equals("pdf")) return "application/pdf";
		if(ext.equals("js")) return "application/javascript";
		if(ext.equals("css")) return "text/css; charset=utf-8";
		if(ext.equals("svg")) return "image/svg+xml";
		if(ext.equals("jnlp")) return "application/x-java-jnlp-file";
		return "application/octet-stream";
	}

	/**
	 * Encodes given text as valid HTML text. Method encodes only the following characters:
	 * '&amp;', '&lt;', '&gt;', '&quot;' and '&#39;'.
	 * @param sb destination for encoded text
	 * @param value string to encode
	 */
	public static void htmlEncode(StringBuilder sb, String value) {
		if(value==null) return;
		char[] array = value.toCharArray();
		for(char c : array) {
			if(c=='&') {
				sb.append("&amp;");
			} else if(c=='<') {
				sb.append("&lt;");
			} else if(c=='>') {
				sb.append("&gt;");
			} else if(c=='"') {
				sb.append("&quot;");
			} else if(c=='\'') {
				sb.append("&#x27;");
			} else if(c=='/') {
				sb.append("&#x2F;");
			} else {
				sb.append(c);
			}
		}
	}

	/**
	 * Encodes given text as valid HTML text. Method encodes only the following characters:
	 * '&amp;', '&lt;', '&gt;', '&quot;' and '&#39;'.
	 * @param w destination for encoded text
	 * @param value string to encode
	 */
	public static void htmlEncode(Writer w, String value) throws IOException {
		if(value==null) return;
		char[] array = value.toCharArray();
		for(char c : array) {
			if(c=='&') {
				w.append("&amp;");
			} else if(c=='<') {
				w.append("&lt;");
			} else if(c=='>') {
				w.append("&gt;");
			} else if(c=='"') {
				w.append("&quot;");
			} else if(c=='\'') {
				w.append("&#x27;");
			} else if(c=='/') {
				w.append("&#x2F;");
			} else {
				w.append(c);
			}
		}
	}
	
	/**
	 * Encodes given text as valid JavaScript string. Method encodes only the following characters:
	 * \n, \r, \t, ", '.
	 * @param w destination for encoded text
	 * @param value string to encode
	 */
	public static void jsEncode(Writer w, String value) throws IOException {
		if(value==null) return;
		char[] array = value.toCharArray();
		for(char c : array) {
			if(c < 256) {
				if(Character.isLetter(c) || Character.isDigit(c) || c==' ' || c=='.' || c==':' || c==','  || c=='+' || c=='-') {
					w.append(c);
				} else {
					// dodaj hex:
					w.append("\\x");
					hexCode(w, c);
				}
			} else {
				w.append(c);
			}
		}
	}

	/**
	 * Encodes given text as valid JavaScript string. Method encodes only the following characters:
	 * \n, \r, \t, ", '.
	 * @param value string to encode
	 * @return encoded string
	 */
	public static String jsEncode(String value) {
		if(value==null) return "";
		StringBuilder sb = new StringBuilder();
		char[] array = value.toCharArray();
		for(char c : array) {
			if(c < 256) {
				if(Character.isLetter(c) || Character.isDigit(c)) {
					sb.append(c);
				} else {
					// dodaj hex:
					sb.append("\\x");
					hexCode(sb, c);
				}
			} else {
				sb.append(c);
			}
		}
		return sb.toString();
	}

	/**
	 * Encodes given text as valid HTML attribute string.
	 * See <a href="https://www.owasp.org/index.php/XSS_(Cross_Site_Scripting)_Prevention_Cheat_Sheet#XSS_Prevention_Rules">here</a>
	 * for details.
	 * @param w destination for encoded text
	 * @param value string to encode
	 */
	public static void htmlAttribEncode(Writer w, String value) throws IOException {
		if(value==null) return;
		char[] array = value.toCharArray();
		for(char c : array) {
			if(c < 256) {
				if(Character.isLetter(c) || Character.isDigit(c)) {
					w.append(c);
				} else {
					// dodaj hex:
					w.append("&#x");
					hexCode(w, c);
					w.append(";");
				}
			} else {
				w.append(c);
			}
		}
	}
	
	/**
	 * Encodes given text as valid HTML attribute string.
	 * See <a href="https://www.owasp.org/index.php/XSS_(Cross_Site_Scripting)_Prevention_Cheat_Sheet#XSS_Prevention_Rules">here</a>
	 * for details.
	 * @param sb destination for encoded text
	 * @param value string to encode
	 */
	public static void htmlAttribEncode(StringBuilder sb, String value) throws IOException {
		if(value==null) return;
		char[] array = value.toCharArray();
		for(char c : array) {
			if(c < 256) {
				if(Character.isLetter(c) || Character.isDigit(c)) {
					sb.append(c);
				} else {
					// dodaj hex:
					sb.append("&#x");
					hexCode(sb, c);
					sb.append(";");
				}
			} else {
				sb.append(c);
			}
		}
	}
	
	/**
	 * Generates two-letter hex code for given character. Call only if c<256.
	 * @param w writer for hex output
	 * @param c char to encode
	 * @throws IOException if exception while writing occurs
	 */
	private static final void hexCode(Writer w, char c) throws IOException {
		int upper = ((int)c & 0xF0) >> 4;
		if(upper<10) {
			w.append((char)('0'+upper));
		} else {
			w.append((char)('A'+upper-10));
		}
		int lower = (int)c & 0x0F;
		if(lower<10) {
			w.append((char)('0'+lower));
		} else {
			w.append((char)('A'+lower-10));
		}
	}
	
	/**
	 * Generates two-letter hex code for given character. Call only if c<256.
	 * @param sb StringBuilder for hex output
	 * @param c char to encode
	 */
	private static final void hexCode(StringBuilder sb, char c) {
		int upper = ((int)c & 0xF0) >> 4;
		if(upper<10) {
			sb.append((char)('0'+upper));
		} else {
			sb.append((char)('A'+upper-10));
		}
		int lower = (int)c & 0x0F;
		if(lower<10) {
			sb.append((char)('0'+lower));
		} else {
			sb.append((char)('A'+lower-10));
		}
	}
}
