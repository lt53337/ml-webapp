package hr.fer.zemris.minilessons.web.controllers;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.minilessons.dao.DAO;
import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.dao.model.Minilesson;
import hr.fer.zemris.minilessons.service.DAOProvided;
import hr.fer.zemris.minilessons.service.Transactional;
import hr.fer.zemris.minilessons.web.WebUtil;
import hr.fer.zemris.minilessons.web.retvals.StreamRetVal;

public class MinilessonFiles {

	// Vraća datoteku iz poddirektorija files u paketu minilekcije
	@DAOProvided @Transactional
	public StreamRetVal process(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		
		DAO dao = DAOProvider.getDao();
		String uid = variables.get("uid");
		Minilesson ml = dao.findMinilessonByID(uid);
		if(ml==null) {
			System.out.println("Nisam ga pronašao!");
			return null;
		}
		
		String fid = variables.get("fid");
		if(fid.indexOf('\\')!=-1 || fid.indexOf('/')!=-1 || fid.indexOf('\'')!=-1 || fid.indexOf('\"')!=-1) return null;
		
		Path filePath = dao.getRealPathFor(uid, "files/"+fid);
		if(filePath==null || !Files.exists(filePath)) {
			resp.sendError(404);
			return null;
		}

		if(!fid.toLowerCase().endsWith(".jnlp")) {
			FileTime ft = null;
			try {
				ft = Files.getLastModifiedTime(filePath);
			} catch(Exception ex) {
			}
			String resourceEtag = "et-"+ml.getVersion()+"-"+ml.getMinorVersion()+"-"+(ft != null ? ft.toMillis() : "");
			String sentEtag = dequote(req.getHeader("If-None-Match"));
			System.out.println("Serving resource: " + fid + " | " + "Resource ETag: " + resourceEtag + " | " + "Received ETag: " + sentEtag);
			if(sentEtag != null && resourceEtag.equals(sentEtag)) {
				resp.setStatus(304);
				return null;
			}
			try {
				return new StreamRetVal(filePath, resourceEtag);
			} catch(IOException ex) {
				resp.sendError(404);
			}
			return null;
		}

		if(Files.size(filePath)>10240) {
			// Odbijamo u memoriju učitavati JNLP datoteke veće od 10k.
			return null;
		}
		
		StringBuffer sb = req.getRequestURL();
		int pos = sb.lastIndexOf("/");
		if(pos==-1) return null;
		sb.delete(pos+1, sb.length());
		
		String reqURL = sb.toString();

		System.out.println("Koristim stazu: ["+reqURL+"]");
		
		byte[] data = Files.readAllBytes(filePath);
		String dataStr = new String(data, StandardCharsets.UTF_8);
		String newDataStr = Pattern.compile("@ML\\(codebase\\)").matcher(dataStr).replaceAll(reqURL);

		return new StreamRetVal(newDataStr.getBytes(StandardCharsets.UTF_8), WebUtil.guessMime(fid), fid);
	}

	private String dequote(String value) {
		if(value==null) return null;
		value = value.trim();
		int l = value.length();
		if(value.charAt(0)=='"' && value.charAt(l-1)=='"') return value.substring(1, l-1);
		return value;
	}
	
}
