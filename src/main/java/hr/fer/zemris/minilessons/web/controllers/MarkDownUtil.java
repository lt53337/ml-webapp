package hr.fer.zemris.minilessons.web.controllers;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.pegdown.PegDownProcessor;

import hr.fer.zemris.minilessons.dao.model.SupportedTextFormat;
import hr.fer.zemris.minilessons.service.Authenticated;
import hr.fer.zemris.minilessons.web.retvals.StreamRetVal;

/**
 * Implementation of utility for server-side markdown processing and sanitizing.

 * @author marcupic
 */
public class MarkDownUtil {

	/**
	 * Implementation of markdown support.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return <code>null</code>
	 * @throws IOException if redirection fails
	 */
	@Authenticated(attemptLogin=false)
	public StreamRetVal process(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		String text = req.getParameter("mdtext");
		
		JSONObject result = new JSONObject();
		
		result.put("text", convert(text));
		
		return new StreamRetVal(result.toString(), StandardCharsets.UTF_8, "application/json", null);
	}

	/**
	 * Implementation of text formatting support.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return <code>null</code>
	 * @throws IOException if redirection fails
	 */
	@Authenticated(attemptLogin=false)
	public StreamRetVal format(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		String text = req.getParameter("text");
		String format = req.getParameter("format");

		String converted = null;
		try {
			converted = convertText(text, format);
		} catch(UnsupportedTextFormatException ex) {
			JSONObject result = new JSONObject();
			result.put("error", true);
			return new StreamRetVal(result.toString(), StandardCharsets.UTF_8, "application/json", null);
		}
		
		JSONObject result = new JSONObject();
		
		result.put("text", converted);
		result.put("error", false);
		
		return new StreamRetVal(result.toString(), StandardCharsets.UTF_8, "application/json", null);
	}
	
	/**
	 * Utility method which converts and sanitizes markdown.
	 * 
	 * @param text markdown to convert
	 * @return sanitized html
	 */
	public static String convert(String text) {
		if(text!=null) {
			text = text.trim();
		} else {
			text = "";
		}
		if(text.isEmpty()) {
			return "";
		}

		PegDownProcessor pdp = new PegDownProcessor();
		String html = pdp.markdownToHtml(text);
		
		Whitelist whitelist = Whitelist.basic();
		
		html = Jsoup.clean(html, whitelist);
		
		return html;
	}
	
	/**
	 * Converts source text written in given format into HTML for display.
	 * 
	 * @param text text
	 * @param textFormat format
	 * @return HTML
	 * @throws UnsupportedTextFormatException if format was not given or supported
	 */
	public static String convertText(String text, String textFormat) {
		if(SupportedTextFormat.MARKDOWN.getFormatName().equals(textFormat)) {
			return convert(text);
		} else {
			throw new UnsupportedTextFormatException();
		}
	}
	
	static class UnsupportedTextFormatException extends RuntimeException {
		private static final long serialVersionUID = 1L;
	}
}
