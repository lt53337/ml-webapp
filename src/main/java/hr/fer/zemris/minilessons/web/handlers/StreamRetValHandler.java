package hr.fer.zemris.minilessons.web.handlers;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.minilessons.util.IOUtil;
import hr.fer.zemris.minilessons.web.mappings.ResponseHandler;
import hr.fer.zemris.minilessons.web.retvals.StreamRetVal;

/**
 * Response handler for {@link StreamRetVal} results.
 * 
 * @author marcupic
 *
 */
public class StreamRetValHandler implements ResponseHandler {
	
	@Override
	public void handle(HttpServletRequest req, HttpServletResponse resp, Object result) throws IOException {
		StreamRetVal res = (StreamRetVal)result;
		try {
			if(res.getStatus() != null) {
				if(res.getStatusMessage()==null) {
					resp.sendError(res.getStatus());
				} else {
					resp.sendError(res.getStatus(), res.getStatusMessage());
				}
				return;
			}

			String ct = res.getContentType();
			if(ct == null) {
				ct = "application/octet-stream";
			}
			resp.setContentType(ct);
			
			if(res.getEtag() != null) {
				resp.setHeader("Cache-Control", "public, max-age=3600");
				resp.setHeader("ETag", "\"" + res.getEtag() + "\"");
			}
			
			long len = res.getLength();
			if(len>=0) resp.setContentLength((int)len);
			
			String name = res.getName();
			if(name!=null) name = name.trim();
			if(name!=null && !name.isEmpty()) resp.setHeader("Content-Disposition", "attachment; filename=\""+name+"\"");

			if(len>0) IOUtil.streamCopy(res.getStream(), resp.getOutputStream(), len);
		} finally {
			try { res.getStream().close(); } catch(Exception ignorable) {}
		}
	}
}