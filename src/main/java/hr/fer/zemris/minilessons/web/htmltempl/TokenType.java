package hr.fer.zemris.minilessons.web.htmltempl;

public enum TokenType {
	EOF,
	STRING,
	COMMAND
}
