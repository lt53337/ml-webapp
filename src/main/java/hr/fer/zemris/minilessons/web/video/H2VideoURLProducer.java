package hr.fer.zemris.minilessons.web.video;

/**
 * <p>This is Video URL generator which stores video materials in predefined folder
 * which is then branched in two level subfolders, based on first and second
 * character of minilesson ID, before the final video directory which name is
 * equal to minilesson ID. For example, if minilesson ID is "adebe3c48e674863a6ba142b0bdf0a9f"
 * and videoName is "bunny.mp4" and configured root is /mlvideo/, file will be expected
 * to be reacheable at location:
 * <tt>/mlvideo/a/d/adebe3c48e674863a6ba142b0bdf0a9f/bunny.mp4</tt>
 * </p>
 * 
 * @author marcupic
 */
public class H2VideoURLProducer implements VideoURLProducer {

	/**
	 * URL prefix.
	 */
	private String prefix;
	
	/**
	 * Constructor. Parameter is configured root-prefix for URL. If the prefix does not end with /,
	 * this separator will be automatically added.
	 *  
	 * @param configuration root-prefix for URL
	 */
	public H2VideoURLProducer(String configuration) {
		if(configuration!=null) configuration = configuration.trim();
		if(configuration==null || configuration.isEmpty()) {
			prefix = "/";
		} else {
			prefix = configuration;
			if(!prefix.endsWith("/")) {
				prefix = prefix + "/";
			}
		}
	}

	@Override
	public String produce(String contextPath, String uid, String videoFileName) {
		return buildPath(uid)+videoFileName;
	}
	
	@Override
	public String info(String contextPath, String uid) {
		return buildPath(uid);
	}
	
	/**
	 * Private helper method for URL construction.
	 * 
	 * @param uid minilesson ID
	 * @return partial URL
	 */
	private String buildPath(String uid) {
		if(uid.length()<2) throw new RuntimeException("Minilesson ID is too short for configured video URL producer.");
		return prefix+Character.toLowerCase(uid.charAt(0))+"/"+Character.toLowerCase(uid.charAt(1))+"/"+uid+"/";
	}
}
