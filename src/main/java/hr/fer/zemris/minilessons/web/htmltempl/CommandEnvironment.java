package hr.fer.zemris.minilessons.web.htmltempl;

import java.io.Writer;

public class CommandEnvironment {

	private String commandName;
	private String commandArgs;
	private ContentProcessor processor;
	private Writer writer;

	public CommandEnvironment(String commandName, String commandArgs, ContentProcessor processor, Writer writer) {
		super();
		this.commandName = commandName;
		this.commandArgs = commandArgs;
		this.processor = processor;
		this.writer = writer;
	}

	public String getCommandArgs() {
		return commandArgs;
	}
	
	public String getCommandName() {
		return commandName;
	}
	
	public ContentProcessor getProcessor() {
		return processor;
	}
	
	public Writer getWriter() {
		return writer;
	}

}
