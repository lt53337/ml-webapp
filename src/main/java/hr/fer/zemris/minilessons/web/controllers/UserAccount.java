package hr.fer.zemris.minilessons.web.controllers;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.dao.model.MLBasicPermission;
import hr.fer.zemris.minilessons.dao.model.UploadPolicy;
import hr.fer.zemris.minilessons.dao.model.User;
import hr.fer.zemris.minilessons.service.Authenticated;
import hr.fer.zemris.minilessons.service.AuthenticationService;
import hr.fer.zemris.minilessons.service.DAOProvided;
import hr.fer.zemris.minilessons.service.MLCurrentContext;
import hr.fer.zemris.minilessons.service.Transactional;
import hr.fer.zemris.minilessons.util.PasswordUtil;
import hr.fer.zemris.minilessons.util.StringUtil;
import hr.fer.zemris.minilessons.web.controllers.forms.AbstractWebForm;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal.Technology;

/**
 * Implementation of user account management.

 * @author marcupic
 */
public class UserAccount {

	/**
	 * Implementation of search page.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return <code>null</code>
	 */
	@DAOProvided @Authenticated(redirectAllowed=true)
	public TemplateRetVal searchForm(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		if(!MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName())) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		return new TemplateRetVal("usersearch", Technology.THYMELEAF);
	}

	/**
	 * Implementation of search process.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return <code>null</code>
	 */
	@DAOProvided @Authenticated(redirectAllowed=true)
	public TemplateRetVal search(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		if(!MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName())) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		String reqUsername = StringUtil.trim(req.getParameter("username"));
		String reqAuthDomain = StringUtil.trim(req.getParameter("authDomain"));
		String reqJmbag = StringUtil.trim(req.getParameter("jmbag"));

		User user = null;
		if(reqUsername!=null && reqAuthDomain!=null) {
			user = DAOProvider.getDao().findUserByUsername(reqUsername, reqAuthDomain);
		} else if(reqJmbag!=null) {
			user = DAOProvider.getDao().findUserByJMBAG(reqJmbag);
		} else {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		if(user==null) {
			req.setAttribute("errorMsgKey", "noUserForQuery");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		resp.sendRedirect(req.getContextPath()+"/ml/admin/user/"+user.getId());
		
		return null;
	}
	
	/**
	 * Implementation of edit page.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return <code>null</code>
	 */
	@DAOProvided @Authenticated(redirectAllowed=true)
	public TemplateRetVal edit(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		String userID = StringUtil.trim(variables.get("id"));
		if(userID==null) {
			req.setAttribute("errorMsgKey", "missingParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		Long reqUserID = null;
		try {
			reqUserID = Long.valueOf(userID);
		} catch(Exception ex) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		boolean canAdmin = false;
		if(!MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName())) {
			if(!MLCurrentContext.getAuthUser().getId().equals(reqUserID)) {
				req.setAttribute("errorMsgKey", "noPermission");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
		} else {
			canAdmin = true;
		}
		User user = DAOProvider.getDao().findUserByID(reqUserID);
		if(user==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		fillCommonData(req, canAdmin);
		
		UserForm uf = new UserForm(canAdmin);
		uf.fillFromData(user);
		
		req.setAttribute("user", uf);
		
		if("1".equals(req.getParameter("msg"))) {
			req.setAttribute("ackMsg", 1);
		} else if("2".equals(req.getParameter("msg"))) {
			req.setAttribute("ackMsg", 2);
		}

		return new TemplateRetVal("userform", Technology.THYMELEAF);
	}

	/**
	 * Implementation of new user page.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return <code>null</code>
	 */
	@DAOProvided @Authenticated(redirectAllowed=true)
	public TemplateRetVal newUser(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		boolean canAdmin = false;
		if(!MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName())) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		} else {
			canAdmin = true;
		}

		fillCommonData(req, canAdmin);
		
		UserForm uf = new UserForm(canAdmin);
		uf.initBlank();
		
		req.setAttribute("user", uf);

		return new TemplateRetVal("userform", Technology.THYMELEAF);
	}

	/**
	 * Helper method for filling data shared among new/edit/save.
	 * 
	 * @param req request
	 * @param canAdmin can user admin
	 */
	private static void fillCommonData(HttpServletRequest req, boolean canAdmin) {
		Set<String> authDomains = AuthenticationService.getAuthenticationDomains();
		Set<String> permNames = new TreeSet<>();
		for(MLBasicPermission p : MLBasicPermission.values()) {
			permNames.add(p.getPermissonName());
		}
		Set<String> policyNames = new LinkedHashSet<>();
		for(UploadPolicy p : UploadPolicy.values()) {
			policyNames.add(p.name());
		}

		req.setAttribute("authDomains", authDomains);
		req.setAttribute("permNames", permNames);
		req.setAttribute("canAdmin", canAdmin);
		req.setAttribute("uploadPolicies", policyNames);
	}
	
	/**
	 * Implementation of save action for update/insert user.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return <code>null</code>
	 */
	@DAOProvided @Authenticated(redirectAllowed=true) @Transactional
	public TemplateRetVal save(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		boolean canAdmin = MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());

		fillCommonData(req, canAdmin);
		
		UserForm uf = new UserForm(canAdmin);
		uf.fillAndVerify(req);
		
		if(uf.hasErrors()) {
			req.setAttribute("user", uf);
			return new TemplateRetVal("userform", Technology.THYMELEAF);
		}

		Long id = (Long)uf.getTypedValue("id");
		
		if(!canAdmin && id==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		if(!canAdmin && !MLCurrentContext.getAuthUser().getId().equals(id)) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		User user = null;
		
		if(id!=null) {
			user = DAOProvider.getDao().findUserByID(id);
			if(user==null) {
				req.setAttribute("errorMsgKey", "invalidParameters");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
		} else {
			user = new User();
		}
		
		uf.fillDataObject(user);

		if(id==null) {
			user.setCreatedOn(new Date());
			DAOProvider.getDao().saveUser(user);
		}
		
		resp.sendRedirect(req.getContextPath()+"/ml/admin/user/"+user.getId()+"?msg="+(id==null ? 1 : 2));
		
		return null;
	}
	
	public static class UserForm extends AbstractWebForm {
		private boolean administrator;
		
		@SuppressWarnings("unchecked")
		public UserForm(boolean canAdmin) {
			this.administrator = canAdmin;
			addOptionalLong("id", "u.id", o -> ((User)o).getId(), (o,v)-> ((User)o).setId((Long)v));
			if(administrator) addString("firstName", "u.firstName", o -> ((User)o).getFirstName(), (o,v)-> ((User)o).setFirstName((String)v));
			if(administrator) addString("lastName", "u.lastName", o -> ((User)o).getLastName(), (o,v)-> ((User)o).setLastName((String)v));
			if(administrator) addString("username", "u.username", o -> ((User)o).getUsername(), (o,v)-> ((User)o).setUsername((String)v));
			if(administrator) addString("jmbag", "u.jmbag", o -> ((User)o).getJmbag(), (o,v)-> ((User)o).setJmbag((String)v));
			addString("email", "u.email", o -> ((User)o).getEmail(), (o,v)-> ((User)o).setEmail((String)v));
			addOptionalString("password", "u.password", o -> null, (o,v)-> {
				if(v!=null) ((User)o).setPasswordHash(PasswordUtil.encodePassword((String)v));
			});
			addOptionalString("password2", "u.password", o -> null, (o,v)-> {});
			if(administrator) addEnum("uploadPolicy", "u.uploadPolicy", o -> ((User)o).getUploadPolicy().toString(), (o,v)-> ((User)o).setUploadPolicy((UploadPolicy)v), UploadPolicy.class);
			if(administrator) addOptionalStringSet("permissions", "user.permissions", o -> new LinkedHashSet<>(((User)o).getPermissions()), (o,v)-> {
				((User)o).getPermissions().addAll((Set<String>)v);
				((User)o).getPermissions().retainAll((Set<String>)v);
			}, Arrays.stream(MLBasicPermission.values()).map(o->o.getPermissonName()).collect(Collectors.toSet()));
			if(administrator) addString("authDomain", "u.authDomain", o -> ((User)o).getAuthDomain(), (o,v)-> ((User)o).setAuthDomain((String)v), AuthenticationService.getAuthenticationDomains());
			if(administrator) addBoolean("accountDisabled", "u.accountDisabled", o -> ((User)o).isAccountDisabled(), (o,v)-> ((User)o).setAccountDisabled((Boolean)v));
		}

		public boolean isAdministrator() {
			return administrator;
		}
		
		@Override
		public void fillAndVerify(HttpServletRequest req) {
			super.fillAndVerify(req);
			int br = (stringValues.get("password")!=null ? 1 : 0) + (stringValues.get("password2")!=null ? 1 : 0); 
			if(br==1) {
				errors.put("password", new ErrorInfo("u.password", "twoPasswordsRequired"));
				return;
			} else if(br==2) {
				if(!stringValues.get("password").equals(stringValues.get("password2"))) {
					errors.put("password", new ErrorInfo("u.password", "twoPasswordsMismatch"));
					return;
				}
			}
			if(typedValues.get("id")==null) {
				if(br!=2) {
					errors.put("password", new ErrorInfo("u.password", "newAccountPasswordRequired"));
					return;
				}
			}
			
		}
	}
}
