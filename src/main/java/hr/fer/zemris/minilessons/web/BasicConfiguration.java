package hr.fer.zemris.minilessons.web;

import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.thymeleaf.TemplateEngine;

import hr.fer.zemris.minilessons.Configuration;

/**
 * This is implementation of {@link Configuration}.
 * 
 * @author marcupic
 *
 */
public class BasicConfiguration implements Configuration {

	/**
	 * Servlet context.
	 */
	private ServletContext servletContext;
	/**
	 * Request we are serving.
	 */
	private HttpServletRequest request;
	/**
	 * Locale which should be used for content generation.
	 */
	private Locale locale;
	/**
	 * Template engine which should be used for content generation.
	 */
	private TemplateEngine engine;
	
	/**
	 * Constructor.
	 * 
	 * @param servletContext servlet context
	 * @param request http request
	 */
	public BasicConfiguration(ServletContext servletContext, HttpServletRequest request) {
		super();
		this.servletContext = servletContext;
		this.request = request;
		String locale = this.request.getParameter("locale");
		if(locale != null) {
			locale = locale.trim();
			if(!locale.isEmpty()) {
				this.locale = Locale.forLanguageTag(locale);
				request.getSession().setAttribute("locale", locale);
			} else {
				locale = null;
			}
		}
		if(locale == null) {
			locale = (String)request.getSession().getAttribute("locale");
			if(locale != null) {
				locale = locale.trim();
				if(!locale.isEmpty()) {
					this.locale = Locale.forLanguageTag(locale);
				} else {
					locale = null;
				}
			}
		}
		if(locale == null) {
			locale = "hr";
			this.locale = Locale.forLanguageTag(locale);
		}
		this.engine = (TemplateEngine)this.servletContext.getAttribute(WebConstants.ML_TEMPLATE_ENGINE_KEY);
	}

	@Override
	public Locale getLocale() {
		return locale;
	}
	
	@Override
	public TemplateEngine getTemplateEngine() {
		return engine;
	}
}
