package hr.fer.zemris.minilessons.web.controllers;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.dao.model.ItemComment;
import hr.fer.zemris.minilessons.dao.model.ItemStatus;
import hr.fer.zemris.minilessons.dao.model.ItemThread;
import hr.fer.zemris.minilessons.dao.model.ItemThreadStatus;
import hr.fer.zemris.minilessons.dao.model.MLBasicPermission;
import hr.fer.zemris.minilessons.dao.model.Minilesson;
import hr.fer.zemris.minilessons.dao.model.SupportedTextFormat;
import hr.fer.zemris.minilessons.dao.model.User;
import hr.fer.zemris.minilessons.service.Authenticated;
import hr.fer.zemris.minilessons.service.DAOProvided;
import hr.fer.zemris.minilessons.service.MLCurrentContext;
import hr.fer.zemris.minilessons.service.Transactional;
import hr.fer.zemris.minilessons.util.StringUtil;
import hr.fer.zemris.minilessons.web.controllers.MarkDownUtil.UnsupportedTextFormatException;
import hr.fer.zemris.minilessons.web.retvals.StreamRetVal;

/**
 * Implementation of commenting minilesson items.

 * @author marcupic
 */
public class ItemCommentThreads {

	/**
	 * Implementation of threads listing for minilesson item.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return JSON result
	 */
	@DAOProvided @Authenticated(redirectAllowed=false,attemptLogin=false,optional=false)
	public StreamRetVal listThreads(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		
		String minilessonID = StringUtil.trim(variables.get("mid"));
		String itemID = StringUtil.trim(variables.get("iid"));

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		if(minilessonID==null || itemID==null) {
			return error();
		}

		User user = DAOProvider.getDao().findUserByID(MLCurrentContext.getAuthUser().getId());
		Minilesson minilesson = DAOProvider.getDao().findMinilessonByID(minilessonID);
		if(minilesson==null || user==null) {
			return error();
		}
		
		ItemStatus itStatus = DAOProvider.getDao().findItemStatus(minilesson, user, itemID);
		if(itStatus==null || itStatus.getOpenedOn()==null) {
			return error();
		}

		List<ItemThread> list = DAOProvider.getDao().listMinilessonItemThreads(minilessonID, itemID);

		JSONObject result = new JSONObject();
		result.put("error", false);
		
		JSONArray arr = new JSONArray();
		result.put("threads", arr);

		for(ItemThread it : list) {
			JSONObject obj = new JSONObject();
			obj.put("id", it.getId());
			obj.put("title", it.getTitle());
			obj.put("posted", sdf.format(it.getPostedOn()));
			obj.put("status", it.getStatus().name());
			obj.put("statusSetBy", userToJSON(it.getStatusSetBy()));
			obj.put("statusChangedOn", it.getStatusChangedOn()==null ? JSONObject.NULL : sdf.format(it.getStatusChangedOn()));
			arr.put(obj);
		}
		
		return new StreamRetVal(result.toString(), StandardCharsets.UTF_8, "application/json", null);
	}

	/**
	 * Implementation of threads listing for minilesson item.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return JSON result
	 */
	@DAOProvided @Authenticated(redirectAllowed=false,attemptLogin=false,optional=false)
	public StreamRetVal listThread(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		
		String minilessonID = StringUtil.trim(variables.get("mid"));
		String itemID = StringUtil.trim(variables.get("iid"));
		String threadIDStr = StringUtil.trim(variables.get("tid"));

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		if(minilessonID==null || itemID==null || threadIDStr==null) {
			return error();
		}

		User user = DAOProvider.getDao().findUserByID(MLCurrentContext.getAuthUser().getId());
		Minilesson minilesson = DAOProvider.getDao().findMinilessonByID(minilessonID);
		if(minilesson==null || user==null) {
			return error();
		}
		
		ItemStatus itStatus = DAOProvider.getDao().findItemStatus(minilesson, user, itemID);
		if(itStatus==null || itStatus.getOpenedOn()==null) {
			return error();
		}

		Long threadID = null;
		try {
			threadID = Long.valueOf(threadIDStr);
		} catch(Exception ex) {
			return error();
		}
		
		ItemThread itemThread = DAOProvider.getDao().findItemThread(threadID);
		if(itemThread==null || !itemThread.getMinilesson().getId().equals(minilessonID) || !itemThread.getItem().equals(itemID)) {
			return error();
		}
		
		List<ItemComment> list = DAOProvider.getDao().listItemComments(threadID);

		JSONObject result = new JSONObject();
		result.put("error", false);
		
		JSONArray arr = new JSONArray();
		result.put("comments", arr);

		boolean seeHidden = MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName()) || user.equals(minilesson.getOwner());
		boolean seeDeleted = MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());
		try {
			for(ItemComment it : list) {
				if(it.isHidden() && !seeHidden) continue;
				if(it.isDeleted() && !seeDeleted) continue;
				JSONObject obj = new JSONObject();
				obj.put("id", it.getId());
				obj.put("text", MarkDownUtil.convertText(it.getText(), it.getTextFormat()));
				obj.put("posted", sdf.format(it.getPostedOn()));
				obj.put("owner", userToJSON(it.getOwner()));
				obj.put("hidden", it.isHidden());
				arr.put(obj);
			}
		} catch(UnsupportedTextFormatException ex) {
			return error();
		}
		
		return new StreamRetVal(result.toString(), StandardCharsets.UTF_8, "application/json", null);
	}

	/**
	 * Implementation of adding new thread comment.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return JSON result
	 */
	@DAOProvided @Authenticated(redirectAllowed=false,attemptLogin=false,optional=false) @Transactional
	public StreamRetVal addComment(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {

		if(!MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.CAN_COMMENT_MLITEMS.getPermissonName()) && !MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName())) {
			return error();
		}

		String text = StringUtil.trim(req.getParameter("text"));
		if(text==null) return error();

		String minilessonID = StringUtil.trim(variables.get("mid"));
		String itemID = StringUtil.trim(variables.get("iid"));
		String threadIDStr = StringUtil.trim(variables.get("tid"));

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		if(minilessonID==null || itemID==null || threadIDStr==null) {
			return error();
		}

		User user = DAOProvider.getDao().findUserByID(MLCurrentContext.getAuthUser().getId());
		Minilesson minilesson = DAOProvider.getDao().findMinilessonByID(minilessonID);
		if(minilesson==null || user==null) {
			return error();
		}
		
		ItemStatus itStatus = DAOProvider.getDao().findItemStatus(minilesson, user, itemID);
		if(itStatus==null || itStatus.getOpenedOn()==null) {
			return error();
		}

		Long threadID = null;
		try {
			threadID = Long.valueOf(threadIDStr);
		} catch(Exception ex) {
			return error();
		}
		
		ItemThread itemThread = DAOProvider.getDao().findItemThread(threadID);
		if(itemThread==null || !itemThread.getMinilesson().getId().equals(minilessonID) || !itemThread.getItem().equals(itemID) || !isThreadLive(itemThread)) {
			return error();
		}

		String textFormat = SupportedTextFormat.MARKDOWN.getFormatName();

		ItemComment c = new ItemComment();
		c.setItemThread(itemThread);
		c.setOwner(user);
		c.setPostedOn(new Date());
		c.setText(text);
		c.setTextFormat(textFormat);
		DAOProvider.getDao().saveItemComment(c);

		JSONObject result = new JSONObject();
		result.put("error", false);
		
		JSONArray arr = new JSONArray();
		result.put("comments", arr);

		try {
			ItemComment it = c;
			JSONObject obj = new JSONObject();
			obj.put("id", it.getId());
			obj.put("text", MarkDownUtil.convertText(it.getText(), it.getTextFormat()));
			obj.put("posted", sdf.format(it.getPostedOn()));
			obj.put("owner", userToJSON(it.getOwner()));
			obj.put("hidden", it.isHidden());
			arr.put(obj);
		} catch(UnsupportedTextFormatException ex) {
			return error();
		}
		
		return new StreamRetVal(result.toString(), StandardCharsets.UTF_8, "application/json", null);
	}

	private boolean isThreadLive(ItemThread itemThread) {
		if(itemThread.getStatus()==ItemThreadStatus.ACTIVE) return true;
		if(itemThread.getStatus()==ItemThreadStatus.POSTPONED) return true;
		return false;
	}

	/**
	 * Implementation of adding new thread.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return JSON result
	 */
	@DAOProvided @Authenticated(redirectAllowed=false,attemptLogin=false,optional=false) @Transactional
	public StreamRetVal addThread(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {

		if(!MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.CAN_COMMENT_MLITEMS.getPermissonName()) && !MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName())) {
			return error();
		}

		String title = StringUtil.trim(req.getParameter("title"));
		if(title==null) return error();

		String text = StringUtil.trim(req.getParameter("text"));
		if(text==null) return error();

		boolean conversationPublic = "true".equals(StringUtil.trim(req.getParameter("convIsPublic")));

		String minilessonID = StringUtil.trim(variables.get("mid"));
		String itemID = StringUtil.trim(variables.get("iid"));

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		if(minilessonID==null || itemID==null) {
			return error();
		}

		User user = DAOProvider.getDao().findUserByID(MLCurrentContext.getAuthUser().getId());
		Minilesson minilesson = DAOProvider.getDao().findMinilessonByID(minilessonID);
		if(minilesson==null || user==null) {
			return error();
		}
		
		ItemStatus itStatus = DAOProvider.getDao().findItemStatus(minilesson, user, itemID);
		if(itStatus==null || itStatus.getOpenedOn()==null) {
			return error();
		}

		ItemThread thread = new ItemThread();
		thread.setConversationPublic(conversationPublic);
		thread.setItem(itemID);
		thread.setMinilesson(minilesson);
		thread.setOwner(user);
		thread.setPostedOn(new Date());
		thread.setStatus(ItemThreadStatus.ACTIVE);
		thread.setStatusChangedOn(null);
		thread.setStatusSetBy(null);
		thread.setTitle(title);
		DAOProvider.getDao().saveItemThread(thread);
		
		String textFormat = SupportedTextFormat.MARKDOWN.getFormatName();

		ItemComment c = new ItemComment();
		c.setItemThread(thread);
		c.setOwner(user);
		c.setPostedOn(thread.getPostedOn());
		c.setText(text);
		c.setTextFormat(textFormat);
		DAOProvider.getDao().saveItemComment(c);

		JSONObject result = new JSONObject();
		result.put("error", false);

		JSONArray arr = new JSONArray();
		result.put("threads", arr);

		JSONObject obj = new JSONObject();
		obj.put("id", thread.getId());
		obj.put("title", thread.getTitle());
		obj.put("posted", sdf.format(thread.getPostedOn()));
		obj.put("status", thread.getStatus().name());
		obj.put("statusSetBy", userToJSON(thread.getStatusSetBy()));
		obj.put("statusChangedOn", thread.getStatusChangedOn()==null ? JSONObject.NULL : sdf.format(thread.getStatusChangedOn()));
		arr.put(obj);

		arr = new JSONArray();
		result.put("comments", arr);

		try {
			ItemComment it = c;
			obj = new JSONObject();
			obj.put("id", it.getId());
			obj.put("text", MarkDownUtil.convertText(it.getText(), it.getTextFormat()));
			obj.put("posted", sdf.format(it.getPostedOn()));
			obj.put("owner", userToJSON(it.getOwner()));
			obj.put("hidden", it.isHidden());
			arr.put(obj);
		} catch(UnsupportedTextFormatException ex) {
			return error();
		}
		
		return new StreamRetVal(result.toString(), StandardCharsets.UTF_8, "application/json", null);
	}

	/**
	 * Implementation of changing visibility of item comment.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return JSON result
	 */
	@DAOProvided @Authenticated(redirectAllowed=false,attemptLogin=false,optional=false) @Transactional
	public StreamRetVal changeCommentVisibility(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {

		if(!MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.CAN_COMMENT_MLITEMS.getPermissonName()) && !MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName())) {
			return error();
		}

		String minilessonID = StringUtil.trim(variables.get("mid"));
		String itemID = StringUtil.trim(variables.get("iid"));
		String threadIDStr = StringUtil.trim(variables.get("tid"));
		String commentIDStr = StringUtil.trim(variables.get("cid"));
		String hiddenStr = StringUtil.trim(req.getParameter("hidden"));

		if(minilessonID==null || itemID==null || threadIDStr==null || commentIDStr==null || hiddenStr==null) {
			return error();
		}
		
		boolean hidden;
		if(hiddenStr.equals("true")) {
			hidden = true;
		} else if(hiddenStr.equals("false")) {
			hidden = false;
		} else {
			return error();
		}

		Long commentID = null;
		try {
			commentID = Long.valueOf(commentIDStr);
		} catch(Exception ex) {
			return error();
		}

		User user = DAOProvider.getDao().findUserByID(MLCurrentContext.getAuthUser().getId());
		Minilesson minilesson = DAOProvider.getDao().findMinilessonByID(minilessonID);
		if(minilesson==null || user==null) {
			return error();
		}
		
		ItemStatus itStatus = DAOProvider.getDao().findItemStatus(minilesson, user, itemID);
		if(itStatus==null || itStatus.getOpenedOn()==null) {
			return error();
		}

		Long threadID = null;
		try {
			threadID = Long.valueOf(threadIDStr);
		} catch(Exception ex) {
			return error();
		}
		
		ItemThread itemThread = DAOProvider.getDao().findItemThread(threadID);
		if(itemThread==null || !itemThread.getMinilesson().getId().equals(minilessonID) || !itemThread.getItem().equals(itemID) || !isThreadLive(itemThread)) {
			return error();
		}

		ItemComment comment = DAOProvider.getDao().findItemComment(commentID);
		if(comment==null || !comment.getItemThread().equals(itemThread) || comment.isDeleted()) {
			return error();
		}

		if(!MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName()) && !comment.getOwner().equals(user)) {
			return error();
		}

		if(comment.isHidden() != hidden) {
			comment.setHidden(hidden);
		}
		
		JSONObject result = new JSONObject();
		result.put("error", false);
		result.put("hidden", hidden);
		return new StreamRetVal(result.toString(), StandardCharsets.UTF_8, "application/json", null);
	}

	/**
	 * Implementation of changing deleted status of item comment.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return JSON result
	 */
	@DAOProvided @Authenticated(redirectAllowed=false,attemptLogin=false,optional=false) @Transactional
	public StreamRetVal changeCommentDeleted(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {

		if(!MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.CAN_COMMENT_MLITEMS.getPermissonName()) && !MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName())) {
			return error();
		}

		String minilessonID = StringUtil.trim(variables.get("mid"));
		String itemID = StringUtil.trim(variables.get("iid"));
		String threadIDStr = StringUtil.trim(variables.get("tid"));
		String commentIDStr = StringUtil.trim(variables.get("cid"));
		String deletedStr = StringUtil.trim(req.getParameter("deleted"));

		if(minilessonID==null || itemID==null || threadIDStr==null || commentIDStr==null || deletedStr==null) {
			return error();
		}
		
		boolean deleted;
		if(deletedStr.equals("true")) {
			deleted = true;
		} else if(deletedStr.equals("false")) {
			deleted = false;
		} else {
			return error();
		}

		Long commentID = null;
		try {
			commentID = Long.valueOf(commentIDStr);
		} catch(Exception ex) {
			return error();
		}

		User user = DAOProvider.getDao().findUserByID(MLCurrentContext.getAuthUser().getId());
		Minilesson minilesson = DAOProvider.getDao().findMinilessonByID(minilessonID);
		if(minilesson==null || user==null) {
			return error();
		}
		
		ItemStatus itStatus = DAOProvider.getDao().findItemStatus(minilesson, user, itemID);
		if(itStatus==null || itStatus.getOpenedOn()==null) {
			return error();
		}

		Long threadID = null;
		try {
			threadID = Long.valueOf(threadIDStr);
		} catch(Exception ex) {
			return error();
		}
		
		ItemThread itemThread = DAOProvider.getDao().findItemThread(threadID);
		if(itemThread==null || !itemThread.getMinilesson().getId().equals(minilessonID) || !itemThread.getItem().equals(itemID) || !isThreadLive(itemThread)) {
			return error();
		}

		ItemComment comment = DAOProvider.getDao().findItemComment(commentID);
		if(comment==null || !comment.getItemThread().equals(itemThread)) {
			return error();
		}

		if(!MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName()) && !comment.getOwner().equals(user)) {
			return error();
		}

		// Samo administrator može "od-obrisati" komentar...
		if(!deleted && !MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName())) {
			return error();
		}
		
		if(comment.isDeleted() != deleted) {
			comment.setDeleted(deleted);
		}
		
		JSONObject result = new JSONObject();
		result.put("error", false);
		result.put("deleted", deleted);
		return new StreamRetVal(result.toString(), StandardCharsets.UTF_8, "application/json", null);
	}
	
	/**
	 * Implementation of changing publicConversation status of thread.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return JSON result
	 */
	@DAOProvided @Authenticated(redirectAllowed=false,attemptLogin=false,optional=false) @Transactional
	public StreamRetVal changeThreadPublic(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {

		if(!MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.CAN_COMMENT_MLITEMS.getPermissonName()) && !MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName())) {
			return error();
		}

		String minilessonID = StringUtil.trim(variables.get("mid"));
		String itemID = StringUtil.trim(variables.get("iid"));
		String threadIDStr = StringUtil.trim(variables.get("tid"));
		String isPublicStr = StringUtil.trim(req.getParameter("isPublic"));

		if(minilessonID==null || itemID==null || threadIDStr==null || isPublicStr==null) {
			return error();
		}
		
		boolean isPublic;
		if(isPublicStr.equals("true")) {
			isPublic = true;
		} else if(isPublicStr.equals("false")) {
			isPublic = false;
		} else {
			return error();
		}

		User user = DAOProvider.getDao().findUserByID(MLCurrentContext.getAuthUser().getId());
		Minilesson minilesson = DAOProvider.getDao().findMinilessonByID(minilessonID);
		if(minilesson==null || user==null) {
			return error();
		}
		
		ItemStatus itStatus = DAOProvider.getDao().findItemStatus(minilesson, user, itemID);
		if(itStatus==null || itStatus.getOpenedOn()==null) {
			return error();
		}

		Long threadID = null;
		try {
			threadID = Long.valueOf(threadIDStr);
		} catch(Exception ex) {
			return error();
		}
		
		ItemThread itemThread = DAOProvider.getDao().findItemThread(threadID);
		if(itemThread==null || !itemThread.getMinilesson().getId().equals(minilessonID) || !itemThread.getItem().equals(itemID)) {
			return error();
		}

		if(!MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName()) && !itemThread.getOwner().equals(user)) {
			return error();
		}

		// Samo administrator ovo može mijenjati za zatvorene threadove
		if(!isThreadLive(itemThread) && !MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName())) {
			return error();
		}
		
		if(isPublic != itemThread.isConversationPublic()) {
			itemThread.setConversationPublic(isPublic);
		}
		
		JSONObject result = new JSONObject();
		result.put("error", false);
		result.put("conversationPublic", isPublic);
		return new StreamRetVal(result.toString(), StandardCharsets.UTF_8, "application/json", null);
	}

	/**
	 * Helper method.
	 * 
	 * @return JSON stream
	 */
	private StreamRetVal error() {
		JSONObject result = new JSONObject();
		result.put("error", true);
		return new StreamRetVal(result.toString(), StandardCharsets.UTF_8, "application/json", null);
	}

	/**
	 * Helper method.
	 * 
	 * @param user User DAO model
	 * @return JSON object with some of user data
	 */
	private Object userToJSON(User user) {
		if(user==null) return JSONObject.NULL;
		JSONObject obj = new JSONObject();
		obj.put("id", user.getId());
		obj.put("firstName", user.getFirstName());
		obj.put("lastName", user.getLastName());
		obj.put("authDomain", user.getAuthDomain());
		return obj;
	}
}
