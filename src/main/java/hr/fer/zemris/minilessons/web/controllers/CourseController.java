package hr.fer.zemris.minilessons.web.controllers;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.dao.model.CommonQuestionDefinition;
import hr.fer.zemris.minilessons.dao.model.Course;
import hr.fer.zemris.minilessons.dao.model.MLBasicPermission;
import hr.fer.zemris.minilessons.dao.model.Minilesson;
import hr.fer.zemris.minilessons.domain.model.DCourse;
import hr.fer.zemris.minilessons.service.Authenticated;
import hr.fer.zemris.minilessons.service.DAOProvided;
import hr.fer.zemris.minilessons.service.MLCurrentContext;
import hr.fer.zemris.minilessons.service.MLDeployer;
import hr.fer.zemris.minilessons.service.QDeployer;
import hr.fer.zemris.minilessons.service.Transactional;
import hr.fer.zemris.minilessons.util.StringUtil;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal.Technology;

public class CourseController {

	@DAOProvided @Authenticated(optional=true, redirectAllowed=true)
	public TemplateRetVal show(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		String id = StringUtil.trim(variables.get("cid"));
		if(id==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());
		Course c = DAOProvider.getDao().findCourseByID(id);
		if(c==null ) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		String lang = MLCurrentContext.getLocale().getLanguage();
		
		List<Node> nodeList = null;
		try {
			Parser p = new Parser(c.getDetails().getContent().getOrDefault(lang, "???"));
			nodeList = p.nodeList;
		} catch(Exception ex) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		AppRequestContext apctx = new AppRequestContext(variables.get("apctx"));
		
		HTMLGenerator gen = new HTMLGenerator(lang, req.getContextPath(), c.getMinilessons(), c.getQuestions(), apctx.newWith("c", c.getId()));
		nodeList.forEach(n->n.accept(gen));
		req.setAttribute("canAdmin", canAdmin);
		req.setAttribute("course", DCourse.from(c, lang));
		req.setAttribute("courseHTML", gen.sb.toString());
		req.setAttribute("apctx", apctx);
		return new TemplateRetVal("course", Technology.THYMELEAF);
	}

	@DAOProvided @Authenticated(optional=false, redirectAllowed=true)
	public TemplateRetVal showEdit(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		String id = StringUtil.trim(variables.get("cid"));
		if(id==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());
		if(!canAdmin) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		String lang = MLCurrentContext.getLocale().getLanguage();
		Course c = DAOProvider.getDao().findCourseByID(id);
		if(c==null ) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		AppRequestContext apctx = new AppRequestContext(variables.get("apctx"));
		req.setAttribute("apctx", apctx);
		
		req.setAttribute("course", DCourse.from(c, lang));
		List<Map.Entry<String, String>> translations = new ArrayList<>(new HashMap<>(c.getDetails().getContent()).entrySet());
		translations.sort((e1,e2)->e1.getKey().compareTo(e2.getKey()));
		req.setAttribute("translations", translations);
		return new TemplateRetVal("courseedit", Technology.THYMELEAF);
	}

	public static class KeyValue {
		private String key;
		private String value;
		public KeyValue(String key, String value) {
			super();
			this.key = key;
			this.value = value;
		}
		public String getKey() {
			return key;
		}
		public String getValue() {
			return value;
		}
	}
	
	@DAOProvided @Authenticated(optional=false, redirectAllowed=false) @Transactional
	public TemplateRetVal performEdit(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		String id = StringUtil.trim(variables.get("cid"));
		if(id==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());
		if(!canAdmin) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		Course c = DAOProvider.getDao().findCourseByID(id);
		if(c==null ) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		String translenStr = StringUtil.trim(req.getParameter("translen"));
		Integer transLen = null;
		if(translenStr != null) {
			try {
				transLen = Integer.valueOf(translenStr);
			} catch(Exception ignorable) {
			}
		}
		if(transLen==null || transLen<0) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		List<KeyValue> entries = new ArrayList<>();
		List<String> errors = new ArrayList<>();
		Set<String> langs = new HashSet<>();
		for(int i = 0; i <= transLen; i++) {
			String lang = StringUtil.trim(req.getParameter("lang"+i));
			String trans = StringUtil.trim(req.getParameter("trans"+i));
			if(lang==null && trans==null) continue;
			if(lang==null) {
				errors.add("You can not have translation without having the language defined.");
			} else {
				if(!langs.add(lang)) {
					errors.add("Language "+lang+" is defined multiple times.");
				}
			}
			entries.add(new KeyValue(lang, trans==null ? "" : trans));
		}
		
		String lang = MLCurrentContext.getLocale().getLanguage();
		req.setAttribute("course", DCourse.from(c, lang));

		Set<String> allMLPublicIDs = new HashSet<>();
		Set<String> allQPublicIDs = new HashSet<>();
		Map<String, Set<String>> qConfigs = new HashMap<>();
		entries.forEach(e->{
			try {
				Parser p = new Parser(e.value);
				PublicIDExtractor ex = new PublicIDExtractor();
				p.nodeList.forEach(n->n.accept(ex));
				allMLPublicIDs.addAll(ex.mlids);
				allQPublicIDs.addAll(ex.qids);
				ex.qconfigs.entrySet().forEach(ee->{
					if(qConfigs.containsKey(ee.getKey())) {
						qConfigs.get(ee.getKey()).addAll(ee.getValue());
					} else {
						qConfigs.put(ee.getKey(), ee.getValue());
					}
				});
			} catch(Exception ex) {
				errors.add(ex.getMessage());
			}
		});

		Set<Minilesson> mlset = new HashSet<>();
		if(errors.isEmpty()) {
			allMLPublicIDs.forEach(publicID->{
				Minilesson ml = DAOProvider.getDao().findNewestVisibleMinilesson(publicID);
				if(ml==null) {
					errors.add("Minilesson with publicID="+publicID+" either does not exists or does not have visible nonarchived version.");
				} else {
					mlset.add(ml);
				}
			});
		}

		Set<CommonQuestionDefinition> qset = new HashSet<>();
		if(errors.isEmpty()) {
			allQPublicIDs.forEach(publicID->{
				CommonQuestionDefinition qd = DAOProvider.getDao().findNewestVisibleQuestionDefinition(publicID);
				if(qd==null) {
					errors.add("Question definition with publicID="+publicID+" either does not exists or does not have visible nonarchived version.");
				} else {
					qset.add(qd);
					Set<String> reqConfigs = qConfigs.get(qd.getPublicID());
					if(reqConfigs != null) {
						Set<String> missing = new LinkedHashSet<>(reqConfigs);
						missing.removeAll(qd.getConfigVariability().keySet());
						if(!missing.isEmpty()) {
							errors.add("Question definition with publicID="+publicID+" does not have all required configurations: "+missing+".");
						}
					}
				}
			});
		}

		AppRequestContext apctx = new AppRequestContext(variables.get("apctx"));
		req.setAttribute("apctx", apctx);

		if(!errors.isEmpty()) {
			req.setAttribute("errors", errors);
			req.setAttribute("translations", entries);
			return new TemplateRetVal("courseedit", Technology.THYMELEAF);
		}
		
		c.getDetails().getContent().keySet().removeIf(k->!langs.contains(k));
		entries.forEach(e->c.getDetails().getContent().put(e.getKey(), e.getValue()));
		c.getMinilessons().addAll(mlset);
		c.getMinilessons().retainAll(mlset);
		
		c.getQuestions().addAll(qset);
		c.getQuestions().retainAll(qset);
		
		resp.sendRedirect(req.getContextPath()+"/ml/course/"+apctx.getContextData()+"/"+URLEncoder.encode(c.getId(), "UTF-8"));
		return null;
	}

	private static interface NodeVisitor {
		void visit(TextNode node);
		void visit(MLTitleNode node);
		void visit(MLURLNode node);
		void visit(MLQTitleNode node);
		void visit(MLQURLNode node);
	}

	private static class HTMLGenerator implements NodeVisitor {
		private StringBuilder sb = new StringBuilder();
		private String lang;
		private Map<String,Minilesson> mlMap;
		private Map<String,CommonQuestionDefinition> qMap;
		private String contextPath;
		private AppRequestContext apctx;
		
		public HTMLGenerator(String lang, String contextPath, Set<Minilesson> mlset, Set<CommonQuestionDefinition> qset, AppRequestContext apctx) {
			super();
			this.lang = lang;
			this.contextPath = contextPath;
			this.apctx = apctx;
			mlMap = new HashMap<>();
			mlset.forEach(ml->mlMap.put(ml.getPublicID(), ml));
			qMap = new HashMap<>();
			qset.forEach(q->qMap.put(q.getPublicID(), q));
		}
		@Override
		public void visit(MLTitleNode node) {
			Minilesson ml = mlMap.get(node.publicID);
			if(ml==null) {
				sb.append("???");
				return;
			}
			sb.append(ml.getTitles().getOrDefault(lang, "????"));
		}
		@Override
		public void visit(MLURLNode node) {
			Minilesson ml = mlMap.get(node.publicID);
			if(ml==null) {
				sb.append("#");
				return;
			}
			sb.append(contextPath).append("/ml/minilessons/").append(apctx.getContextData()).append("/ml/").append(ml.getId());
			//sb.append(contextPath).append("/ml/minilesson/").append(apctx.getContextData()).append("/").append(ml.getId()).append("/item");
		}
		@Override
		public void visit(MLQTitleNode node) {
			CommonQuestionDefinition qd = qMap.get(node.publicID);
			if(qd==null) {
				sb.append("???");
				return;
			}
			sb.append(qd.getTitles().getOrDefault(lang, "????"));
		}
		@Override
		public void visit(MLQURLNode node) {
			CommonQuestionDefinition qd = qMap.get(node.publicID);
			if(qd==null) {
				sb.append("#");
				return;
			}
			sb.append(contextPath).append("/ml/questions/").append(apctx.getContextData()).append("/details/").append(qd.getId());
			if(node.config!=null && !node.config.isEmpty()) {
				sb.append("/").append(node.config);
			}
		}
		@Override
		public void visit(TextNode node) {
			sb.append(node.text);
		}
	}

	private static class PublicIDExtractor implements NodeVisitor {
		private Set<String> mlids = new HashSet<>();
		private Set<String> qids = new HashSet<>();
		private Map<String,Set<String>> qconfigs = new HashMap<>();
		
		@Override
		public void visit(MLTitleNode node) {
			mlids.add(node.publicID);
		}
		@Override
		public void visit(MLURLNode node) {
			mlids.add(node.publicID);
		}
		@Override
		public void visit(MLQTitleNode node) {
			qids.add(node.publicID);
			if(node.config != null && !node.config.isEmpty()) {
				if(!qconfigs.containsKey(node.publicID)) {
					qconfigs.put(node.publicID, new HashSet<>());
				}
				qconfigs.get(node.publicID).add(node.config);
			}
		}
		@Override
		public void visit(MLQURLNode node) {
			qids.add(node.publicID);
			if(node.config != null && !node.config.isEmpty()) {
				if(!qconfigs.containsKey(node.publicID)) {
					qconfigs.put(node.publicID, new HashSet<>());
				}
				qconfigs.get(node.publicID).add(node.config);
			}
		}
		@Override
		public void visit(TextNode node) {
		}
	}
	
	private abstract static class Node {
		public abstract void accept(NodeVisitor v);
	}
	
	private static class TextNode extends Node {
		private String text;

		public TextNode(String text) {
			super();
			this.text = text;
		}

		@Override
		public void accept(NodeVisitor v) {
			v.visit(this);
		}
	}
	
	private static class MLTitleNode extends Node {
		private String publicID;

		public MLTitleNode(String publicID) {
			super();
			this.publicID = publicID;
		}

		@Override
		public void accept(NodeVisitor v) {
			v.visit(this);
		}
		
	}
	
	private static class MLQTitleNode extends Node {
		private String publicID;
		private String config;

		public MLQTitleNode(String publicID, String config) {
			super();
			this.publicID = publicID;
			this.config = config;
		}

		@Override
		public void accept(NodeVisitor v) {
			v.visit(this);
		}
		
	}
	
	private static class MLURLNode extends Node {
		private String publicID;

		public MLURLNode(String publicID) {
			super();
			this.publicID = publicID;
		}

		@Override
		public void accept(NodeVisitor v) {
			v.visit(this);
		}
		
	}

	private static class MLQURLNode extends Node {
		private String publicID;
		private String config;

		public MLQURLNode(String publicID, String config) {
			super();
			this.publicID = publicID;
			this.config = config;
		}

		@Override
		public void accept(NodeVisitor v) {
			v.visit(this);
		}
		
	}

	public static class Parser {
		private List<Node> nodeList = new ArrayList<>();
		
		public Parser(String text) {
			if(text!=null) {
				parse(text);
			}
		}

		private void parse(String text) {
			int poc = 0;
			while(poc < text.length()) {
				int index = text.indexOf("@@[[", poc);
				if(index > poc) {
					nodeList.add(new TextNode(text.substring(poc, index)));
					poc = index;
				} else if(index==-1) {
					nodeList.add(new TextNode(text.substring(poc)));
					break;
				}
				// Imam početak taga...
				int endIndex = text.indexOf("]]@@", index+4);
				if(endIndex==-1) {
					throw new RuntimeException("Unterminated instruction found.");
				}
				poc = endIndex+4;
				String tag = text.substring(index+4, endIndex).trim();
				int colon = tag.indexOf(':');
				if(colon==-1) {
					parseNoColonTag(tag);
				} else {
					parseTag(tag.substring(0, colon).trim(), tag.substring(colon+1).trim());
				}
			}
		}

		private void parseTag(String name, String arg) {
			if("q-url".equals(name)) {
				int pipe = arg.indexOf('|');
				String qid = pipe == -1 ? arg : arg.substring(0, pipe).trim();
				String config = pipe == -1 ? "" : arg.substring(pipe+1).trim();
				if(!config.isEmpty()) {
					if(!QTester.checkConfigurationName(config) && !QTester.checkXMLConfigurationName(config)) {
						throw new RuntimeException("Question configuration is invalid.");
					}
				}
				QDeployer.checkPublicID(qid);
				nodeList.add(new MLQURLNode(qid,config));
				return;
			}
			if("q-title".equals(name)) {
				int pipe = arg.indexOf('|');
				String qid = pipe == -1 ? arg : arg.substring(0, pipe).trim();
				String config = pipe == -1 ? "" : arg.substring(pipe+1).trim();
				if(!config.isEmpty()) {
					if(!QTester.checkConfigurationName(config) && !QTester.checkXMLConfigurationName(config)) {
						throw new RuntimeException("Question configuration is invalid.");
					}
				}
				QDeployer.checkPublicID(qid);
				nodeList.add(new MLQTitleNode(qid,config));
				return;
			}
			if("ml-url".equals(name)) {
				String pid = StringUtil.trim(arg);
				if(pid==null) {
					throw new RuntimeException("Invalid minilesson publicID found: 'null'.");
				}
				try {
					MLDeployer.checkPublicID(pid);
				} catch(Exception ex) {
					throw new RuntimeException("Invalid minilesson publicID found: '"+pid+"'.");
				}
				nodeList.add(new MLURLNode(pid));
				return;
			}
			if("ml-title".equals(name)) {
				String pid = StringUtil.trim(arg);
				if(pid==null) {
					throw new RuntimeException("Invalid minilesson publicID found: 'null'.");
				}
				try {
					MLDeployer.checkPublicID(pid);
				} catch(Exception ex) {
					throw new RuntimeException("Invalid minilesson publicID found: '"+pid+"'.");
				}
				nodeList.add(new MLTitleNode(pid));
				return;
			}
			
			throw new RuntimeException("Unknown instruction found: '"+name+"'.");
		}

		private void parseNoColonTag(String tag) {
			throw new RuntimeException("Unknown instruction found: '"+tag+"'.");
		}
	}
}
