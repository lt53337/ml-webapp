package hr.fer.zemris.minilessons.web.controllers;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.dao.model.Course;
import hr.fer.zemris.minilessons.dao.model.CourseCollection;
import hr.fer.zemris.minilessons.dao.model.MLBasicPermission;
import hr.fer.zemris.minilessons.domain.model.DCourse;
import hr.fer.zemris.minilessons.domain.model.DCourseCollection;
import hr.fer.zemris.minilessons.service.Authenticated;
import hr.fer.zemris.minilessons.service.DAOProvided;
import hr.fer.zemris.minilessons.service.MLCurrentContext;
import hr.fer.zemris.minilessons.service.Transactional;
import hr.fer.zemris.minilessons.util.StringUtil;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal.Technology;

public class CourseCollections {

	@DAOProvided @Authenticated(optional=true, redirectAllowed=true)
	public TemplateRetVal showDefault(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		return show(req, resp, variables, hr.fer.zemris.minilessons.CourseCollections.DEFAULT.getKey());
	}

	@DAOProvided @Authenticated(optional=true, redirectAllowed=true)
	public TemplateRetVal showSpecified(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		String id = StringUtil.trim(variables.get("ccid"));
		if(id==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		return show(req, resp, variables, id);
	}

	private TemplateRetVal show(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables, String ccID) {
		String lang = MLCurrentContext.getLocale().getLanguage();
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());
		ccID = StringUtil.trim(ccID);
		if(ccID==null) ccID = hr.fer.zemris.minilessons.CourseCollections.DEFAULT.getKey();
		CourseCollection cc = DAOProvider.getDao().findCourseCollectionByID(ccID);
		if(cc==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		req.setAttribute("canAdmin", canAdmin);
		
		DCourseCollection dcc = new DCourseCollection();
		dcc.setId(cc.getId());
		dcc.setTitle(cc.getTitles().getOrDefault(lang, "???"));
		req.setAttribute("courseCollection", dcc);

		List<DCourse> clist = cc.getCourses().stream().map(c->DCourse.from(c, lang)).collect(Collectors.toList());
		req.setAttribute("courses", clist);
		
		return new TemplateRetVal("coursecollection", Technology.THYMELEAF);
	}

	@DAOProvided @Authenticated(optional=false, redirectAllowed=true)
	public TemplateRetVal showAdminList(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		String ccID = StringUtil.trim(variables.get("ccid"));
		if(ccID==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());
		if(!canAdmin) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		String lang = MLCurrentContext.getLocale().getLanguage();
		CourseCollection cc = DAOProvider.getDao().findCourseCollectionByID(ccID);
		if(cc==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		DCourseCollection dcc = new DCourseCollection();
		dcc.setId(cc.getId());
		dcc.setTitle(cc.getTitles().getOrDefault(lang, "???"));
		req.setAttribute("courseCollection", dcc);

		List<DCourse> clist = cc.getCourses().stream().map(c->DCourse.from(c, lang)).collect(Collectors.toList());
		req.setAttribute("courses", clist);

		List<Course> allCourses = DAOProvider.getDao().listAllCourses();
		Set<Course> alreadyIn = new HashSet<>(cc.getCourses());
		allCourses.removeIf(alreadyIn::contains);

		List<DCourse> allclist = allCourses.stream().map(c->DCourse.from(c, lang)).collect(Collectors.toList());
		req.setAttribute("allcourses", allclist);

		return new TemplateRetVal("coursecollectionform", Technology.THYMELEAF);
	}

	@DAOProvided @Authenticated(optional=false, redirectAllowed=true) @Transactional
	public TemplateRetVal updateAdminList(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		String ccID = StringUtil.trim(variables.get("ccid"));
		if(ccID==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());
		if(!canAdmin) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		CourseCollection cc = DAOProvider.getDao().findCourseCollectionByID(ccID);
		if(cc==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		String[] cids = req.getParameterValues("cidlist");

		List<Course> list = new ArrayList<>();
		if(cids != null) {
			boolean errors = false;
			for(String s : cids) {
				s = StringUtil.trim(s);
				if(s==null) {
					errors = true;
					break;
				}
				Course c = DAOProvider.getDao().findCourseByID(s);
				if(c==null) {
					errors = true;
					break;
				}
				list.add(c);
			}
			if(errors) {
				req.setAttribute("errorMsgKey", "invalidParameters");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
		}

		if(!cc.getCourses().equals(list)) {
			cc.getCourses().clear();
			cc.getCourses().addAll(list);
		}

		resp.sendRedirect(req.getContextPath()+"/ml/courses/show/"+URLEncoder.encode(cc.getId(), "UTF-8"));
		return null;
	}
}
