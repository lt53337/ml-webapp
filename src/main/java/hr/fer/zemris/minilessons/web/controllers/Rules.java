package hr.fer.zemris.minilessons.web.controllers;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal.Technology;

/**
 * Implementation of rules functionality.

 * @author marcupic
 */
public class Rules {

	/**
	 * Implementation of rules.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return <code>null</code>
	 * @throws IOException if redirection fails
	 */
	public TemplateRetVal rules(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		return new TemplateRetVal("rules",Technology.THYMELEAF);
	}

}
