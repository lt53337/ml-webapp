package hr.fer.zemris.minilessons.web.htmltempl.commands;

import java.io.IOException;
import java.util.function.UnaryOperator;

import hr.fer.zemris.minilessons.service.MLCurrentContext;
import hr.fer.zemris.minilessons.util.VideoSpec;
import hr.fer.zemris.minilessons.util.VideoSpecParser;
import hr.fer.zemris.minilessons.util.VideoSrcSpec;
import hr.fer.zemris.minilessons.util.VideoSubtitleSpec;
import hr.fer.zemris.minilessons.web.htmltempl.CommandEnvironment;
import hr.fer.zemris.minilessons.web.htmltempl.ContentProcessorUtil;
import hr.fer.zemris.minilessons.web.htmltempl.HtmlTemplateCommand;
import hr.fer.zemris.minilessons.web.video.VideoURLProducer;

public class VideoCmd implements HtmlTemplateCommand {

	private String contextPath;
	private String uid;

	public VideoCmd(String contextPath, String uid) {
		super();
		this.contextPath = contextPath;
		this.uid = uid;
	}

	@Override
	public void execute(CommandEnvironment env) throws IOException {
		VideoSpec vs = null;
		try {
			vs = VideoSpecParser.parse(env.getCommandArgs());
		} catch(Exception ex) {
			throw new RuntimeException("Exception while parsing video command '" + env.getCommandArgs() + "'.");
		}
		
		// Checks...
		if(vs.getPosterFileName()!=null && !ContentProcessorUtil.checkFileName(vs.getPosterFileName())) {
			throw new RuntimeException("Poster file '" + vs.getPosterFileName() + "' contains illegal characters.");
		}
		for(VideoSrcSpec src : vs.getSources()) {
			if(src.getFileName()!=null && !ContentProcessorUtil.checkFileName(src.getFileName())) {
				throw new RuntimeException("Video source file '" + src.getFileName() + "' contains illegal characters.");
			}
		}
		for(VideoSubtitleSpec sub : vs.getSubtitles()) {
			if(sub.getFileName()!=null && !ContentProcessorUtil.checkFileName(sub.getFileName())) {
				throw new RuntimeException("Video source file '" + sub.getFileName() + "' contains illegal characters.");
			}
		}
		final VideoURLProducer urlProducerBase = MLCurrentContext.getVideoURLProducer();
		UnaryOperator<String> urlProducer = f -> urlProducerBase.produce(contextPath, uid, f);
		
		// OK, generate...
		env.getWriter().append("<video width=\"").append(Integer.toString(vs.getWidth())).append("\" height=\"").append(Integer.toString(vs.getHeight())).append("\"");
		if(vs.getPosterFileName()!=null) {
			env.getWriter().append(" poster=\"").append(urlProducer.apply(vs.getPosterFileName())).append("\"");
		}
		env.getWriter().append(" controls>\n");
		for(VideoSrcSpec src : vs.getSources()) {
			env.getWriter().append("  <source src='").append(urlProducer.apply(src.getFileName())).append("' type='").append(src.getType()).append("'>\n");
		}
		for(VideoSubtitleSpec sub : vs.getSubtitles()) {
			env.getWriter().append("  <track kind='subtitles' src='").append(urlProducer.apply(sub.getFileName())).append("' srclang='").append(sub.getLanguage()).append("' label='").append(sub.getLabel()).append("'>\n");
		}
		env.getWriter().append("  Your browser does not support the video tag.\n");
		env.getWriter().append("</video>");
	}

}
