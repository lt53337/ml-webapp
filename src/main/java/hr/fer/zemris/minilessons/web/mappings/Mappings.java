package hr.fer.zemris.minilessons.web.mappings;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class represents ordered collection of mappings. 
 * Method {@link #match(String, HttpServletRequest, HttpServletResponse, HttpMethod, ResultCallback)}
 * goes through mappings from first registered. First mapping which matches the request will
 * execute specified controller.
 *  
 * @author marcupic
 *
 */
public class Mappings {

	/**
	 * List of mappings.
	 */
	private List<Mapping> mappings = new ArrayList<>();

	/**
	 * Add mapping at the end of mappings.
	 * 
	 * @param mapping mapping to add
	 * @return this mappings object in order to support method chaining
	 */
	public Mappings add(Mapping mapping) {
		mappings.add(mapping);
		return this;
	}
	
	/**
	 * Find first matching mapping for given URL, execute its controller and return result.
	 * 
	 * @param url url to analyze
	 * @param req request
	 * @param resp response
	 * @param httpMethod http method of request
	 * @param action object to notify with result which controller produced
	 * @return <code>true</code> if url was matched, <code>false</code> otherwise
	 * @throws IOException
	 * @throws ServletException
	 */
	public boolean match(String url, HttpServletRequest req, HttpServletResponse resp, HttpMethod httpMethod, ResultCallback action) throws IOException, ServletException {
		String[] pathParts = null;
		if(url!=null) {
			url = url.trim();
			if(url.startsWith("/")) {
				pathParts = url.substring(1).split("/");
			}
		}

		for(Mapping m : mappings) {
			Map<String,String> matchingResult = m.matches(pathParts, httpMethod);
			if(matchingResult!=null) {
				Object result = m.invoke(req, resp, matchingResult);
				if(result!=null) {
					action.accept(result);
				}
				return true;
			}
		}

		return false;
	}
}
