package hr.fer.zemris.minilessons.web.htmltempl.commands;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

import hr.fer.zemris.minilessons.web.WebUtil;
import hr.fer.zemris.minilessons.web.htmltempl.CommandEnvironment;
import hr.fer.zemris.minilessons.web.htmltempl.HtmlTemplateCommand;
import hr.fer.zemris.minilessons.xmlmodel.XMLBranch;
import hr.fer.zemris.minilessons.xmlmodel.XMLString;

public class BranchTitleCmd implements HtmlTemplateCommand {
	
	private Map<String, XMLBranch> branches;
	private String lang;
	
	public BranchTitleCmd(Map<String, XMLBranch> branches, String lang) {
		this.branches = branches;
		this.lang = lang;
	}

	@Override
	public void execute(CommandEnvironment env) throws IOException {
		String bid = env.getCommandArgs();
		XMLBranch branch = branches.get(bid);
		if(branch==null) {
			throw new RuntimeException("Requested branch not found: " + bid);
		}
		Optional<XMLString> title = branch.getTitle().stream().filter(s -> s.getLang().equals(lang)).findAny();
		if(title.isPresent()) {
			StringBuilder sb = new StringBuilder();
			WebUtil.htmlEncode(sb, title.get().getText());
			env.getWriter().append(sb);
		} else {
			env.getWriter().append("???");
		}
	}

}
