package hr.fer.zemris.minilessons.web.controllers;

import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import hr.fer.zemris.minilessons.MapVariableMapping;
import hr.fer.zemris.minilessons.dao.DAO;
import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.dao.model.QIStatus;
import hr.fer.zemris.minilessons.dao.model.QuestionControlResult;
import hr.fer.zemris.minilessons.dao.model.JSQuestionDefinition;
import hr.fer.zemris.minilessons.dao.model.JSQuestionInstance;
import hr.fer.zemris.minilessons.dao.model.User;
import hr.fer.zemris.minilessons.dao.model.WSQuestionInstance;
import hr.fer.zemris.minilessons.domain.model.DQuestion;
import hr.fer.zemris.minilessons.service.Authenticated;
import hr.fer.zemris.minilessons.service.DAOProvided;
import hr.fer.zemris.minilessons.service.MLCurrentContext;
import hr.fer.zemris.minilessons.service.Transactional;
import hr.fer.zemris.minilessons.util.StringUtil;
import hr.fer.zemris.minilessons.web.WebUtil;
import hr.fer.zemris.minilessons.web.controllers.QuestionsController.SS_ACTION;
import hr.fer.zemris.minilessons.web.htmltempl.ContentProcessor;
import hr.fer.zemris.minilessons.web.htmltempl.HtmlTemplateCommand;
import hr.fer.zemris.minilessons.web.htmltempl.commands.CommonIncludeCmd;
import hr.fer.zemris.minilessons.web.htmltempl.commands.EchoCmd;
import hr.fer.zemris.minilessons.web.htmltempl.commands.FileCmd;
import hr.fer.zemris.minilessons.web.htmltempl.commands.I18NCmd;
import hr.fer.zemris.minilessons.web.htmltempl.commands.PageIncludeCmd;
import hr.fer.zemris.minilessons.web.htmltempl.commands.RawTextCmd;
import hr.fer.zemris.minilessons.web.htmltempl.commands.RegisterPageScriptCmd;
import hr.fer.zemris.minilessons.web.retvals.StreamRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal.Technology;

public class QTester {

	public static interface ScriptLog {
		void log(Object obj);
	}
	
	public static class QTesterScriptLog implements ScriptLog {
		@Override
		public void log(Object obj) {
			if(obj==null) {
				System.out.println("[CONSOLE:LOG] null");
			} else {
				System.out.println("[CONSOLE:LOG] " + obj.toString());
			}
		}
	}
	
	@DAOProvided @Transactional @Authenticated(redirectAllowed=true)
	public Object list(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		String lang = MLCurrentContext.getLocale().getLanguage();
		DAO dao = DAOProvider.getDao();

		List<DQuestion> questions = dao.findVisibleJSQuestionDefinitions().stream().map(q->DQuestion.fromDAOModel(q, lang)).collect(Collectors.toList());
		req.setAttribute("mlquestions", questions);
		
		return new TemplateRetVal("testqlist", Technology.THYMELEAF);
	}

	@DAOProvided @Transactional @Authenticated(redirectAllowed=true)
	public StreamRetVal qupdate(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		return questionProcessUpdate(req, resp, variables, false);
	}
	
	@DAOProvided @Transactional @Authenticated(redirectAllowed=true)
	public StreamRetVal qupdatefinish(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		return questionProcessUpdate(req, resp, variables, true);
	}
	
	@SuppressWarnings("restriction")
	private static ScriptEngine getScriptEngine() {
		jdk.nashorn.api.scripting.NashornScriptEngineFactory nsf = new jdk.nashorn.api.scripting.NashornScriptEngineFactory();
		ScriptEngine engine = nsf.getScriptEngine("--language=es6");
		engine.put("console", new QTesterScriptLog());
		return engine;
	}
	
	public static void saveJSQuestionInstanceState(HttpServletRequest req, DAO dao, JSQuestionInstance qi) {
		String lang = MLCurrentContext.getLocale().getLanguage();
		String qfdata = req.getParameter("qstates");
		if(qfdata!=null) qfdata = qfdata.trim();
		if(qfdata==null || qfdata.isEmpty()) {
			throw new RuntimeException("No state found to save.");
		}
		
		ScriptEngine jsengine1 = getScriptEngine();
		Invocable invocable1 = (Invocable)jsengine1;

		try {
			StringBuilder sb = new StringBuilder();
			sb.append("var qstate = [];\n");
			sb.append("function setQState(s) { qstate = JSON.parse(s); }\n");
			sb.append("function getPIID(n) { return qstate[n].piid; }\n");
			sb.append("function getQuestionState(n) { return JSON.stringify(qstate[n].qs); }\n");
			sb.append("function getN() { return qstate.length; } \n");
			
			jsengine1.eval(sb.toString());
			invocable1.invokeFunction("setQState", qfdata);
		} catch (NoSuchMethodException | ScriptException e) {
			throw new RuntimeException("Could not read state.", e);
		}
		
		int n = 0;
		try {
			n = ((Number)invocable1.invokeFunction("getN")).intValue();
		} catch (Exception e) {
			throw new RuntimeException("Could not read state.", e);
		}
		
		if(n!=1) {
			throw new RuntimeException("Could not read state.");
		}

		String qstate = null;
		Long piid = null;
		try {
			piid = Long.valueOf(invocable1.invokeFunction("getPIID", 0).toString());
		} catch (Exception e) {
			throw new RuntimeException("Could not read state.", e);
		}
		try {
			qstate = invocable1.invokeFunction("getQuestionState", 0).toString();
		} catch (Exception e) {
			throw new RuntimeException("Could not read state.", e);
		}

		if(!qi.getId().equals(piid) || qi.getStatus()!=QIStatus.SOLVABLE) {
			throw new RuntimeException("Invalid problem state.");
		}

		String serverScript = null;
		try {
			serverScript = prepareBasicScriptProcessor(lang, dao, qi.getQuestion().getId(), null, null).load("main.js");
		} catch(Exception ex) {
			throw new RuntimeException("Could not prepare question script.", ex);
		}
		
		String script = "var userData = {};\n\n" + serverScript + "\n\n" + scriptFooter;
		
		ScriptEngine jsengine = getScriptEngine();
		Invocable invocable = (Invocable)jsengine;
		
		try {
			jsengine.eval(script);
		} catch (ScriptException e) {
			throw new RuntimeException("Could not evaluate question script.", e);
		}
		try {
			invocable.invokeFunction("__fw_setUserData", qi.getStoredState());
		} catch (NoSuchMethodException | ScriptException e) {
			throw new RuntimeException("Could not set stored data.", e);
		}
		try {
			invocable.invokeFunction("__fw_importQuestionState", qstate);
		} catch (NoSuchMethodException | ScriptException e) {
			throw new RuntimeException("Could not import obtained data.", e);
		}
		
		String userData = null;
		try {
			userData = invocable.invokeFunction("__fw_getUserData").toString();
		} catch (NoSuchMethodException | ScriptException e) {
			throw new RuntimeException("Could not read new problem state.", e);
		}
		qi.setStoredState(userData);
	}
	
	public static void evaluateJSQuestionInstanceState(HttpServletRequest req, DAO dao, JSQuestionInstance qi) {
		String lang = MLCurrentContext.getLocale().getLanguage();
		
		String serverScript = null;
		try {
			serverScript = prepareBasicScriptProcessor(lang, dao, qi.getQuestion().getId(), null, null).load("main.js");
		} catch(Exception ex) {
			throw new RuntimeException("Could not prepare question script.", ex);
		}
		
		String script = "var userData = {};\n\n" + serverScript + "\n\n" + scriptFooter;
		
		ScriptEngine jsengine = getScriptEngine();
		Invocable invocable = (Invocable)jsengine;
		
		try {
			jsengine.eval(script);
		} catch (ScriptException e) {
			throw new RuntimeException("Could not evaluate question script.", e);
		}
		try {
			invocable.invokeFunction("__fw_setUserData", qi.getStoredState());
		} catch (NoSuchMethodException | ScriptException e) {
			throw new RuntimeException("Could not set stored data.", e);
		}

		try {
			invocable.invokeFunction("__fw_questionEvaluate");
		} catch (NoSuchMethodException | ScriptException e) {
			qi.setStatus(QIStatus.ERROR);
			qi.setEvaluatedOn(new Date());
			throw new RuntimeException("Could not evaluate question.", e);
		}
		
		Double correctness = null;
		try {
			correctness = ((Number)invocable.invokeFunction("__fw_questionEvaluateCorrectness")).doubleValue();
		} catch (Exception e) {
			qi.setStatus(QIStatus.ERROR);
			qi.setEvaluatedOn(new Date());
			throw new RuntimeException("Could not evaluate question: unable to read correctness.", e);
		}
		
		boolean solved = false;
		try {
			Object obj = invocable.invokeFunction("__fw_questionEvaluateSolved");
			solved = ((Boolean)obj).booleanValue();
		} catch (NoSuchMethodException | ScriptException e) {
			qi.setStatus(QIStatus.ERROR);
			qi.setEvaluatedOn(new Date());
			throw new RuntimeException("Could not evaluate question: unable to read solved status.", e);
		}
		
		String userData = null;
		try {
			userData = invocable.invokeFunction("__fw_getUserData").toString();
		} catch (NoSuchMethodException | ScriptException e) {
			qi.setStatus(QIStatus.ERROR);
			qi.setEvaluatedOn(new Date());
			throw new RuntimeException("Could not evaluate question: unable to read state data after evaluation.", e);
		}
		qi.setStoredState(userData);

		System.out.println("Correctness = " + correctness);
		System.out.println("Solved = " + solved);
		
		qi.setStatus(QIStatus.EVALUATED);
		qi.setEvaluatedOn(new Date());
		qi.setCorrectness(correctness);
		qi.setSolved(solved);
	}
	
	
	private StreamRetVal questionProcessUpdate(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables, boolean evaluate) throws IOException {
		String lang = MLCurrentContext.getLocale().getLanguage();
		DAO dao = DAOProvider.getDao();

		String qfdata = req.getParameter("qstates");
		if(qfdata!=null) qfdata = qfdata.trim();
		if(qfdata==null || qfdata.isEmpty()) {
			System.out.println("Nisam ga pronašao!");
			return null;
		}
		
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());

		//ScriptEngineManager scEngMan = new ScriptEngineManager();
		ScriptEngine jsengine1 = getScriptEngine(); //scEngMan.getEngineByName("nashorn");
		Invocable invocable1 = (Invocable)jsengine1;

		try {
			StringBuilder sb = new StringBuilder();
			sb.append("var qstate = [];\n");
			sb.append("function setQState(s) { qstate = JSON.parse(s); }\n");
			sb.append("function getPIID(n) { return qstate[n].piid; }\n");
			sb.append("function getQuestionState(n) { return JSON.stringify(qstate[n].qs); }\n");
			sb.append("function getN() { return qstate.length; } \n");
			
			jsengine1.eval(sb.toString());
			invocable1.invokeFunction("setQState", qfdata);
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}
		
		int n = 0;
		try {
			n = ((Number)invocable1.invokeFunction("getN")).intValue();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
		if(n!=1) {
			System.out.println("Nisam ga pronašao!");
			return null;
		}

		String qstate = null;
		Long piid = null;
		try {
			piid = Long.valueOf(invocable1.invokeFunction("getPIID", 0).toString());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		try {
			qstate = invocable1.invokeFunction("getQuestionState", 0).toString();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		JSQuestionInstance qi = dao.findJSQuestionInstanceByID(piid);
		if(qi==null || !qi.getUser().equals(user) || qi.getStatus()!=QIStatus.SOLVABLE) {
			System.out.println("Nisam ga pronašao!");
			return null;
		}

		String serverScript = null;
		try {
			//serverScript = new String(Files.readAllBytes(dao.getRealQPathFor(qi.getQuestion().getId(), "scripts/main.js")), StandardCharsets.UTF_8);
			serverScript = prepareBasicScriptProcessor(lang, dao, qi.getQuestion().getId(), null, null).load("main.js");
		} catch(Exception ex) {
			ex.printStackTrace();
			return null;
		}
		
		Map<String, String> translations = constructLocalizationMap(lang, dao, qi.getQuestion().getId(), "scripts/translations_"+lang+".properties", "scripts/translations.properties", "common/translations_"+lang+".properties", "common/translations.properties");
		String serverSideLocalizationObject = createLocalizationObject(translations);
		
		String script = "var userData = {};\n\nvar i18n = "+serverSideLocalizationObject+";\n\n" + serverScript + "\n\n" + scriptFooter;
		
		ScriptEngine jsengine = getScriptEngine(); //scEngMan.getEngineByName("nashorn");
		Invocable invocable = (Invocable)jsengine;
		
		//Bindings bindings = jsengine.createBindings();
		//jsengine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);
		try {
			jsengine.eval(script);
		} catch (ScriptException e) {
			e.printStackTrace();
			return null;
		}
		try {
			invocable.invokeFunction("__fw_setUserData", qi.getStoredState());
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}
		try {
			invocable.invokeFunction("__fw_importQuestionState", qstate);
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}
		
		String userData = null;
		try {
			userData = invocable.invokeFunction("__fw_getUserData").toString();
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}
		qi.setStoredState(userData);

		if(evaluate) {
			try {
				invocable.invokeFunction("__fw_questionEvaluate");
			} catch (NoSuchMethodException | ScriptException e) {
				e.printStackTrace();
				return null;
			}
			
			Double correctness = null;
			try {
				correctness = ((Number)invocable.invokeFunction("__fw_questionEvaluateCorrectness")).doubleValue();
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
			
			boolean solved = false;
			try {
				Object obj = invocable.invokeFunction("__fw_questionEvaluateSolved");
				solved = ((Boolean)obj).booleanValue();
			} catch (NoSuchMethodException | ScriptException e) {
				e.printStackTrace();
				return null;
			}
			
			System.out.println("Correctness = " + correctness);
			System.out.println("Solved = " + solved);
			
			qi.setStatus(QIStatus.EVALUATED);
			qi.setEvaluatedOn(new Date());
			qi.setCorrectness(correctness);
			qi.setSolved(solved);
		}

		String userDataQuestionStr;
		String computedPropertiesStr;
		try {
			userDataQuestionStr = ((CharSequence)invocable.invokeFunction("__fw_getUserDataQuestion")).toString();
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}
		try {
			computedPropertiesStr = ((CharSequence)invocable.invokeFunction("__fw_getComputedProperties")).toString();
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}
		//System.out.println("udq = " + userDataQuestionStr);
		//System.out.println("cp = " + computedPropertiesStr);
		Map<String,Object> infoMap = new HashMap<>();
		addFromJSONToMap(infoMap, userDataQuestionStr);
		addFromJSONToMap(infoMap, computedPropertiesStr);
		
		Set<String> registeredPageScripts = new LinkedHashSet<>();
		String qHtml = null;
		try {
			ContentProcessor cp = prepareBasicContentProcessor(lang, dao, qi.getQuestion().getId(), req.getContextPath(), null, infoMap, translations);
			qHtml = cp.load("question-"+lang+".html");
			registeredPageScripts.addAll(cp.getRegisteredPageScripts());
		} catch(Exception ex) {
			ex.printStackTrace();
			return null;
		}
		String qScript = null;
		try {
			ContentProcessor cp = prepareBasicContentProcessor(lang, dao, qi.getQuestion().getId(), req.getContextPath(), null, infoMap, translations);
			qScript = cp.load("script.js");
			registeredPageScripts.addAll(cp.getRegisteredPageScripts());
		} catch(Exception ex) {
			ex.printStackTrace();
			return null;
		}

		String commonState = null;
		String emptyState = null;
		String userState = null;
		String correctState = null;
		try {
			commonState = invocable.invokeFunction("__fw_exportCommonState").toString();
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}
		try {
			emptyState = invocable.invokeFunction("__fw_exportEmptyState").toString();
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}
		try {
			userState = invocable.invokeFunction("__fw_exportUserState", "USER").toString();
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}

		if(qi.getStatus()!=QIStatus.SOLVABLE) {
			try {
				correctState = invocable.invokeFunction("__fw_exportUserState", "CORRECT").toString();
			} catch (NoSuchMethodException | ScriptException e) {
				e.printStackTrace();
				return null;
			}
		}
		
		return createQuestionPage(req, qi, qHtml, qScript, userData, commonState, emptyState, userState, correctState, registeredPageScripts);
	}

	/**
	 * Based on given localization files which are treated as UTF-8 encoded property files, method constructs
	 * JavaScript map object of form <code>{key1: value1, key2: value2, ...}</code>  where all keys and values
	 * are strings. Keys are added from first file to last file, with key being added only if already not present;
	 * this way, earlier translations will have precedence over later ones.
	 * 
	 * @param lang language tag
	 * @param dao dao object
	 * @param qid question id
	 * @param files property files to scan
	 * @return javascript map object
	 */
	private static String createLocalizationObject(String lang, DAO dao, String qid, String...files) {
		Map<String, String> map = new HashMap<>();
		for(String file : files) {
			Path p = dao.getRealQPathFor(qid, file);
			if(Files.isRegularFile(p) && Files.isReadable(p)) {
				Properties prop = new Properties();
				try(Reader r = Files.newBufferedReader(p)) {
					prop.load(r);
				} catch(Exception e) {
					e.printStackTrace();
				}
				prop.forEach((k,v)->{
					String key = k.toString();
					if(!map.containsKey(key)) {
						map.put(key, v.toString());
					}
				});
			}
		}
		
		return map.entrySet().stream().map(e->"\""+WebUtil.jsEncode(e.getKey())+"\": \""+WebUtil.jsEncode(e.getValue())+"\"").collect(Collectors.joining(",", "{", "}"));
	}

	/**
	 * Based on given localization files which are treated as UTF-8 encoded property files, method constructs
	 * a map object of form <code>{key1: value1, key2: value2, ...}</code>  where all keys and values
	 * are strings. Keys are added from first file to last file, with key being added only if already not present;
	 * this way, earlier translations will have precedence over later ones.
	 * 
	 * @param lang language tag
	 * @param dao dao object
	 * @param qid question id
	 * @param files property files to scan
	 * @return map object
	 */
	private static Map<String, String> constructLocalizationMap(String lang, DAO dao, String qid, String...files) {
		Map<String, String> map = new HashMap<>();
		for(String file : files) {
			Path p = dao.getRealQPathFor(qid, file);
			if(Files.isRegularFile(p) && Files.isReadable(p)) {
				Properties prop = new Properties();
				try(Reader r = Files.newBufferedReader(p)) {
					prop.load(r);
				} catch(Exception e) {
					e.printStackTrace();
				}
				prop.forEach((k,v)->{
					String key = k.toString();
					if(!map.containsKey(key)) {
						map.put(key, v.toString());
					}
				});
			}
		}
		
		return map;
	}

	/**
	 * Based on given localization map method constructs
	 * JavaScript map object of form <code>{key1: value1, key2: value2, ...}</code>  where all keys and values
	 * are strings.
	 * 
	 * @param translations a localization map
	 * @return javascript map object
	 */
	private static String createLocalizationObject(Map<String, String> translations) {
		return translations.entrySet().stream().map(e->"\""+WebUtil.jsEncode(e.getKey())+"\": \""+WebUtil.jsEncode(e.getValue())+"\"").collect(Collectors.joining(",", "{", "}"));
	}

	public static final Pattern PATTERN_Q_CONFIG_NAME = Pattern.compile("^[a-zA-Z][-a-zA-Z0-9_]*$");
	public static final Pattern PATTERN_Q_XML_CONFIG_NAME = Pattern.compile("^[1-9][0-9]*$");
	
	public static boolean checkConfigurationName(String configName) {
		if(configName==null) return false;
		return configName.length()<=20 && PATTERN_Q_CONFIG_NAME.matcher(configName).matches();
	}

	public static boolean checkXMLConfigurationName(String configName) {
		if(configName==null) return false;
		return configName.length()<=20 && PATTERN_Q_XML_CONFIG_NAME.matcher(configName).matches();
	}
	
//	public static JSQuestionInstance createJSQuestionInstance(DAO dao, JSQuestionDefinition qd, User user, String config) {
//		String lang = MLCurrentContext.getLocale().getLanguage();
//
//		if(config!=null) {
//			if(!checkConfigurationName(config)) {
//				throw new RuntimeException("Invalid configuration name.");
//			}
//		} else {
//			config = "default";
//		}
//		
//		JSQuestionInstance qi = new JSQuestionInstance();
//		qi.setQuestion(qd);
//		qi.setCreatedOn(new Date());
//		qi.setStatus(QIStatus.SOLVABLE);
//		qi.setUser(user);
//		qi.setConfigName(config);
//		
//		String serverScript = null;
//		try {
//			serverScript = prepareBasicScriptProcessor(lang, dao, qd.getId(), null, null).load("main.js");
//		} catch(Exception ex) {
//			ex.printStackTrace();
//			return null;
//		}
//		
//		String configScript;
//		try {
//			configScript = new String(Files.readAllBytes(dao.getRealQPathFor(qd.getId(), "scripts/config-"+config+".json")), StandardCharsets.UTF_8);
//		} catch(IOException ex) {
//			throw new RuntimeException("Could not read requested configuration.", ex);
//		}
//		
//		String script = "var userData = {};\n\n" + serverScript + "\n\n" + scriptFooter;
//		
//		ScriptEngine jsengine = getScriptEngine();
//		Invocable invocable = (Invocable)jsengine;
//		
//		try {
//			jsengine.eval(script);
//		} catch (ScriptException e) {
//			e.printStackTrace();
//			return null;
//		}
//		try {
//			invocable.invokeFunction("__fw_setUserData", "{}");
//		} catch (NoSuchMethodException | ScriptException e) {
//			e.printStackTrace();
//			return null;
//		}
//		try {
//			invocable.invokeFunction("__fw_questionInitialize", configScript);
//		} catch (NoSuchMethodException | ScriptException e) {
//			e.printStackTrace();
//			return null;
//		}
//
//		String userData = null;
//		try {
//			userData = invocable.invokeFunction("__fw_getUserData").toString();
//		} catch (NoSuchMethodException | ScriptException e) {
//			e.printStackTrace();
//			return null;
//		}
//		qi.setStoredState(userData);
//		dao.saveJSQuestionInstance(qi);
//	
//		return qi;
//	}

	public static JSQuestionInstance createJSQuestionInstance(DAO dao, JSQuestionDefinition qd, User user, String config) {
		List<JSQuestionInstance> questions = createMultipleJSQuestionInstances(dao, qd, Arrays.asList(user), config);
		return questions.get(0);
	}
	
	public static List<JSQuestionInstance> createMultipleJSQuestionInstances(DAO dao, JSQuestionDefinition qd, List<User> users, String config) {
		String lang = MLCurrentContext.getLocale().getLanguage();

		if(config!=null) {
			if(!checkConfigurationName(config)) {
				throw new RuntimeException("Invalid configuration name.");
			}
		} else {
			config = "default";
		}
		
		String serverScript = null;
		try {
			serverScript = prepareBasicScriptProcessor(lang, dao, qd.getId(), null, null).load("main.js");
		} catch(Exception ex) {
			ex.printStackTrace();
			return null;
		}
		
		String configScript;
		try {
			configScript = new String(Files.readAllBytes(dao.getRealQPathFor(qd.getId(), "scripts/config-"+config+".json")), StandardCharsets.UTF_8);
		} catch(IOException ex) {
			throw new RuntimeException("Could not read requested configuration.", ex);
		}
		
		String serverSideLocalizationObject = createLocalizationObject(lang, dao, qd.getId(), "scripts/translations_"+lang+".properties", "scripts/translations.properties", "common/translations_"+lang+".properties", "common/translations.properties");
		
		String apiCorrections = "";
		if(qd.getApiLevel()==1) {
			apiCorrections = "function multipleQuestionsInitialize(questionConfig, numberOfInstances) {\n"+
					"  questionInstances = [];\n"+
					"  for(var i = 0; i < numberOfInstances; i++) {\n"+
					"    userData = {};\n"+
					"    questionInitialize(questionConfig);\n"+
					"    questionInstances.push(userData);\n"+
					"  }\n"+
					"}\n\n";
		}
		
		String script = "var userData = {};\n\nvar i18n = "+serverSideLocalizationObject+";\n\n" + serverScript + "\n\n" + apiCorrections + scriptFooter;

		//String script = "var userData = {};\n\n" + serverScript + "\n\n" + scriptFooter;
		
		ScriptEngine jsengine = getScriptEngine();
		Invocable invocable = (Invocable)jsengine;
		
		try {
			jsengine.eval(script);
		} catch (ScriptException e) {
			e.printStackTrace();
			return null;
		}

		try {
			invocable.invokeFunction("__fw_setUserData", "{}");
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}
		try {
			invocable.invokeFunction("__fw_multipleQuestionsInitialize", configScript, users.size());
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}
		List<JSQuestionInstance> instances = new ArrayList<>();
		for(int i = 0, n = users.size(); i < n; i++) {
			User user = users.get(i);
			JSQuestionInstance qi = new JSQuestionInstance();
			qi.setQuestion(qd);
			qi.setCreatedOn(new Date());
			qi.setStatus(QIStatus.SOLVABLE);
			qi.setUser(user);
			qi.setConfigName(config);
			
			String userData = null;
			try {
				userData = invocable.invokeFunction("__fw_getQuestionUserData",i).toString();
			} catch (NoSuchMethodException | ScriptException e) {
				e.printStackTrace();
				return null;
			}
			qi.setStoredState(userData);
			instances.add(qi);
		}
		for(JSQuestionInstance qi : instances) {
			dao.saveJSQuestionInstance(qi);
		}
		
		return instances;
	}

	@DAOProvided @Transactional @Authenticated(redirectAllowed=true)
	public StreamRetVal newTest(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		String lang = MLCurrentContext.getLocale().getLanguage();
		DAO dao = DAOProvider.getDao();

		String conf = StringUtil.trim(variables.get("conf"));
		if(conf!=null) {
			if(!checkConfigurationName(conf)) {
				System.out.println("Nisam ga pronašao!");
				return null;
			}
		} else {
			conf = "default";
		}
		
		String q = StringUtil.trim(variables.get("q"));
		if(q==null) {
			System.out.println("Nisam ga pronašao!");
			return null;
		}

		JSQuestionDefinition jSQuestionDefinition = dao.findJSQuestionDefinitionByID(q);
		if(jSQuestionDefinition==null) {
			System.out.println("Nisam ga pronašao!");
			return null;
		}

		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());

		JSQuestionInstance jSQuestionInstance = new JSQuestionInstance();
		jSQuestionInstance.setQuestion(jSQuestionDefinition);
		jSQuestionInstance.setCreatedOn(new Date());
		jSQuestionInstance.setStatus(QIStatus.SOLVABLE);
		jSQuestionInstance.setUser(user);
		
		String serverScript = null;
		try {
			//serverScript = new String(Files.readAllBytes(dao.getRealQPathFor(question.getId(), "scripts/main.js")), StandardCharsets.UTF_8);
			serverScript = prepareBasicScriptProcessor(lang, dao, jSQuestionDefinition.getId(), null, null).load("main.js");
		} catch(Exception ex) {
			ex.printStackTrace();
			return null;
		}
		
		String configScript = new String(Files.readAllBytes(dao.getRealQPathFor(jSQuestionDefinition.getId(), "scripts/config-"+conf+".json")), StandardCharsets.UTF_8);
		Map<String, String> translations = constructLocalizationMap(lang, dao, jSQuestionInstance.getQuestion().getId(), "scripts/translations_"+lang+".properties", "scripts/translations.properties", "common/translations_"+lang+".properties", "common/translations.properties");
		
		//String script = "var userData = {};\n\n" + serverScript + "\n\n" + scriptFooter;
		String serverSideLocalizationObject = createLocalizationObject(translations);
		
		String apiCorrections = "";
		if(jSQuestionDefinition.getApiLevel()==1) {
			apiCorrections = "function multipleQuestionsInitialize(questionConfig, numberOfInstances) {\n"+
					"  questionInstances = [];\n"+
					"  for(var i = 0; i < numberOfInstances; i++) {\n"+
					"    userData = {};\n"+
					"    questionInitialize(questionConfig);\n"+
					"    questionInstances.push(userData);\n"+
					"  }\n"+
					"}\n\n";
		}
		
		String script = "var userData = {};\n\nvar i18n = "+serverSideLocalizationObject+";\n\n" + serverScript + "\n\n" + apiCorrections + scriptFooter;

		//ScriptEngineManager scEngMan = new ScriptEngineManager();
		ScriptEngine jsengine = getScriptEngine(); //scEngMan.getEngineByName("nashorn");
		Invocable invocable = (Invocable)jsengine;
		
		//Bindings bindings = jsengine.createBindings();
		//jsengine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);
		try {
			jsengine.eval(script);
		} catch (ScriptException e) {
			e.printStackTrace();
			return null;
		}
		try {
			invocable.invokeFunction("__fw_setUserData", "{}");
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}
		try {
			invocable.invokeFunction("__fw_questionInitialize", configScript);
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}

		String userDataQuestionStr;
		String computedPropertiesStr;
		try {
			userDataQuestionStr = ((CharSequence)invocable.invokeFunction("__fw_getUserDataQuestion")).toString();
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}
		try {
			computedPropertiesStr = ((CharSequence)invocable.invokeFunction("__fw_getComputedProperties")).toString();
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}
		//System.out.println("udq = " + userDataQuestionStr);
		//System.out.println("cp = " + computedPropertiesStr);
		Map<String,Object> infoMap = new HashMap<>();
		addFromJSONToMap(infoMap, userDataQuestionStr);
		addFromJSONToMap(infoMap, computedPropertiesStr);

		Set<String> registeredPageScripts = new LinkedHashSet<>();
		String qHtml = null;
		try {
			ContentProcessor cp = prepareBasicContentProcessor(lang, dao, q, req.getContextPath(), null, infoMap, translations);
			qHtml = cp.load("question-"+lang+".html");
			registeredPageScripts.addAll(cp.getRegisteredPageScripts());
		} catch(Exception ex) {
			ex.printStackTrace();
			return null;
		}
		String qScript = null;
		try {
			ContentProcessor cp = prepareBasicContentProcessor(lang, dao, q, req.getContextPath(), null, infoMap, translations);
			qScript = cp.load("script.js");
			registeredPageScripts.addAll(cp.getRegisteredPageScripts());
		} catch(Exception ex) {
			ex.printStackTrace();
			return null;
		}
		
		String userData = null;
		String commonState = null;
		String emptyState = null;
		String userState = null;
		try {
			commonState = invocable.invokeFunction("__fw_exportCommonState").toString();
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}
		try {
			emptyState = invocable.invokeFunction("__fw_exportEmptyState").toString();
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}
		try {
			userState = invocable.invokeFunction("__fw_exportUserState", "USER").toString();
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}
		try {
			userData = invocable.invokeFunction("__fw_getUserData").toString();
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}
		jSQuestionInstance.setStoredState(userData);
		dao.saveJSQuestionInstance(jSQuestionInstance);
		
		return createQuestionPage(req, jSQuestionInstance, qHtml, qScript, userData, commonState, emptyState, userState, null, registeredPageScripts);
	}

	private static void addFromJSONToMap(Map<String, Object> infoMap, String str) {
		JSONObject obj = new JSONObject(str);
		for(String key : obj.keySet()) {
			Object value = obj.get(key);
			infoMap.put(key, value);
			//System.out.println("Pronašao: ("+key+","+value+")");
		}
	}

	public static Object renderForSolvingJS(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables,
			AppRequestContext apctx, DAO dao, User user, JSQuestionInstance jsqi, WSQuestionInstance wsqi,
			JSQuestionInstance jsqic, WSQuestionInstance wsqic, boolean showEvalButton, SS_ACTION action) {
		
		JSQuestionInstance qi = action==SS_ACTION.SHOW_FOR_VERIFY ? jsqic : jsqi;
		
		String lang = MLCurrentContext.getLocale().getLanguage();
		
		String serverScript = null;
		try {
			serverScript = prepareBasicScriptProcessor(lang, dao, qi.getQuestion().getId(), null, null).load("main.js");
		} catch(Exception ex) {
			throw new RuntimeException("Could not prepare question script.", ex);
		}
		
		//String script = "var userData = {};\n\n" + serverScript + "\n\n" + scriptFooter;
		String serverSideLocalizationObject = createLocalizationObject(lang, dao, qi.getQuestion().getId(), "scripts/translations_"+lang+".properties", "scripts/translations.properties", "common/translations_"+lang+".properties", "common/translations.properties");
		String script = "var userData = {};\n\nvar i18n = "+serverSideLocalizationObject+";\n\n" + serverScript + "\n\n" + scriptFooter;
		
		ScriptEngine jsengine = getScriptEngine();
		Invocable invocable = (Invocable)jsengine;
		
		try {
			jsengine.eval(script);
		} catch (ScriptException e) {
			throw new RuntimeException("Could not evaluate question script.", e);
		}
		try {
			invocable.invokeFunction("__fw_setUserData", qi.getStoredState());
		} catch (NoSuchMethodException | ScriptException e) {
			throw new RuntimeException("Could not set stored data.", e);
		}

		String userDataQuestionStr;
		String computedPropertiesStr;
		try {
			userDataQuestionStr = ((CharSequence)invocable.invokeFunction("__fw_getUserDataQuestion")).toString();
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}
		try {
			computedPropertiesStr = ((CharSequence)invocable.invokeFunction("__fw_getComputedProperties")).toString();
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}
		//System.out.println("udq = " + userDataQuestionStr);
		//System.out.println("cp = " + computedPropertiesStr);
		Map<String,Object> infoMap = new HashMap<>();
		addFromJSONToMap(infoMap, userDataQuestionStr);
		addFromJSONToMap(infoMap, computedPropertiesStr);

		Map<String,String> translations = constructLocalizationMap(lang, dao, qi.getQuestion().getId(), "pages/translations_"+lang+".properties", "pages/translations.properties", "common/translations_"+lang+".properties", "common/translations.properties");

		Set<String> registeredPageScripts = new LinkedHashSet<>();
		String qHtml = null;
		try {
			ContentProcessor cp = prepareBasicContentProcessor(lang, dao, qi.getQuestion().getId(), req.getContextPath(), null, infoMap, translations);
			qHtml = cp.load("question-"+lang+".html");
			registeredPageScripts.addAll(cp.getRegisteredPageScripts());
		} catch(Exception ex) {
			ex.printStackTrace();
			return null;
		}
		String qScript = null;
		try {
			ContentProcessor cp = prepareBasicContentProcessor(lang, dao, qi.getQuestion().getId(), req.getContextPath(), null, infoMap, translations);
			qScript = cp.load("script.js");
			registeredPageScripts.addAll(cp.getRegisteredPageScripts());
		} catch(Exception ex) {
			ex.printStackTrace();
			return null;
		}

		String clientSideLocalizationObject = createLocalizationObject(translations);

		String userData = null;
		String commonState = null;
		String emptyState = null;
		String userState = null;
		String correctState = null;
		
		try {
			commonState = invocable.invokeFunction("__fw_exportCommonState").toString();
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}
		try {
			emptyState = invocable.invokeFunction("__fw_exportEmptyState").toString();
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}
		try {
			userState = invocable.invokeFunction("__fw_exportUserState", "USER").toString();
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}
		if(qi.getStatus()==QIStatus.EVALUATED) {
			try {
				correctState = invocable.invokeFunction("__fw_exportUserState", "CORRECT").toString();
			} catch (NoSuchMethodException | ScriptException e) {
				e.printStackTrace();
				return null;
			}
		}
		
		
		return createQuestionPageEx(req, qHtml, qScript, userData, commonState, emptyState, userState, correctState, apctx, dao, user, jsqi, wsqi, jsqic, wsqic, showEvalButton, action, clientSideLocalizationObject, registeredPageScripts);
	}

	private static StreamRetVal createQuestionPageEx(HttpServletRequest req, String qHtml,
			String qScript, String userData, String commonState, String emptyState, String userState, String correctState,
			AppRequestContext apctx, DAO dao, User user, JSQuestionInstance jsqi, WSQuestionInstance wsqi,
			JSQuestionInstance jsqic, WSQuestionInstance wsqic, boolean showEvalButton, SS_ACTION action, String clientSideLocalizationObject, Set<String> registeredPageScripts) {
		//System.out.println("userdata = " + userData);
		//System.out.println("userstate = " + userState);
		//System.out.println("commonstate = " + commonState);
		//System.out.println("emptystate = " + emptyState);

		JSQuestionInstance qi = action==SS_ACTION.SHOW_FOR_VERIFY ? jsqic : jsqi;

		StringBuilder page = new StringBuilder(1024);
		
		page.append("<!DOCTYPE html><html>\n<head>\n<meta charset=\"utf-8\">\n");
		
		page.append("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
		page.append("    \n");
		page.append("    <link rel=\"stylesheet\" href=\"//ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/themes/base/jquery-ui.css\">\n");
		page.append("    <script src=\"//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js\"></script>\n");
		page.append("    <script src=\"//ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js\"></script>\n");
		page.append("    \n");
		page.append("    <link rel=\"stylesheet\" href=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">\n");
		page.append("    <script src=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>\n");
		page.append("    <link rel=\"stylesheet\" type=\"text/css\" href=\"").append(req.getContextPath()).append("/css/common.css\">\n");
		page.append("    \n");
		page.append("    <link rel=\"stylesheet\" href=\"//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/default.min.css\">\n");
		page.append("    <script src=\"//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js\"></script>\n");
		page.append("    <script src=\"//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/languages/vhdl.min.js\"></script>\n");
		page.append("    \n");
		page.append("    <script type=\"text/javascript\" async src=\"//cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML\"></script>\n");
		page.append("    <script type=\"text/javascript\">hljs.initHighlightingOnLoad();</script>\n");
		if(registeredPageScripts != null) {
			for(String scrurl : registeredPageScripts) {
				page.append("    <script type=\"text/javascript\" src=\"").append(scrurl).append("\"></script>\n");
			}
		}
		page.append("<script>\n");

		boolean showReadOnly = action==SS_ACTION.SHOW_FOR_INSPECT || action==SS_ACTION.SHOW_FOR_VERIFY || qi.getStatus()!=QIStatus.SOLVABLE;
		boolean showCorrect = action==SS_ACTION.SHOW_FOR_INSPECT && qi.getStatus()==QIStatus.EVALUATED && correctState!=null;
		
		page.append("    var model = [\n");
		page.append("      {\n");
		page.append("        problemInstanceID: \"").append(qi.getId()).append("\",\n");
		page.append("        readOnly: ").append(showReadOnly?"true":"false").append(",\n");
		page.append("        localID: \'QLP0\',\n");
		page.append("        commonState: ").append(commonState).append(",\n");
		page.append("        userState: ").append(userState).append(",\n");
		page.append("        emptyState: ").append(emptyState).append(",\n");
		page.append("        i18n: ").append(clientSideLocalizationObject).append("\n");
		page.append("      }\n");
		if(showCorrect) {
			page.append("      ,{\n");
			page.append("        problemInstanceID: \"").append(qi.getId()).append("\",\n");
			page.append("        readOnly: true,\n");
			page.append("        localID: \'QLP1\',\n");
			page.append("        commonState: ").append(commonState).append(",\n");
			page.append("        userState: ").append(correctState).append(",\n");
			page.append("        emptyState: ").append(emptyState).append(",\n");
			page.append("        i18n: ").append(clientSideLocalizationObject).append("\n");
			page.append("      }\n");
		}
		page.append("    ];\n");

		page.append("\n");
		page.append("    var modelMap = {\n");
		page.append("      \"QLP0\": 0\n");
		if(correctState!=null) {
			page.append("      ,\"QLP1\": 1\n");
		}
		page.append("    };\n");
		page.append("\n");
		page.append("    var modelExportedObjects = {}\n");
		page.append("\n");
		page.append("    function qExportGlobalObject(qid,oid,obj) {\n");
		page.append("     if(!modelExportedObjects.hasOwnProperty(qid)) modelExportedObjects[qid] = {};\n");
		page.append("     modelExportedObjects[qid][oid] = obj;\n");
		page.append("    }\n");
		page.append("\n");
		page.append("    function qGlobalObject(qid,oid) {\n");
		page.append("     return modelExportedObjects[qid][oid];\n");
		page.append("    }\n");
		page.append("\n");
		page.append("    function resetQuestion(qid) {\n");
		page.append("      var i = modelMap[qid];\n");
		page.append("      if(model[i].readOnly) return;\n");
		page.append("      model[i].api.resetState(model[i].commonState, model[i].emptyState);\n");
		page.append("    }\n");
		page.append("\n");
		page.append("    function revertQuestion(qid) {\n");
		page.append("      var i = modelMap[qid];\n");
		page.append("      if(model[i].readOnly) return;\n");
		page.append("      model[i].api.revertState(model[i].commonState, model[i].userState);\n");
		page.append("    }\n");
		page.append("\n");
		page.append("    function initQPage() {\n");
		page.append("      for(var i = 0; i < model.length; i++) {\n");
		page.append("        model[i].api.initQuestion(model[i].commonState, model[i].userState);\n");
		page.append("      }\n");
		page.append("    }\n");
		page.append("\n");
		page.append("    function simulateTake() {\n");
		page.append("      var res = [];\n");
		page.append("      for(var i = 0; i < model.length; i++) {\n");
		page.append("        if(model[i].readOnly) continue;\n");
		page.append("        res.push({piid: model[i].problemInstanceID, qs: model[i].api.takeState()});\n");
		page.append("      }\n");
		page.append("      document.getElementById(\"ispis\").innerHTML = JSON.stringify(res);\n");
		page.append("    }\n");
		page.append("\n");
		page.append("    function sendUpdate() {\n");
		page.append("      var res = [];\n");
		page.append("      for(var i = 0; i < model.length; i++) {\n");
		page.append("        if(model[i].readOnly) continue;\n");
		page.append("        res.push({piid: model[i].problemInstanceID, qs: model[i].api.takeState()});\n");
		page.append("      }\n");
		page.append("      document.getElementById(\"qfsubmitdata\").value = JSON.stringify(res);\n");
		page.append("      document.getElementById(\"qfsubmit\").submit();\n");
		page.append("    }\n");
		page.append("\n");
		page.append("    function sendUpdateF() {\n");
		page.append("      var res = [];\n");
		page.append("      for(var i = 0; i < model.length; i++) {\n");
		page.append("        if(model[i].readOnly) continue;\n");
		page.append("        res.push({piid: model[i].problemInstanceID, qs: model[i].api.takeState()});\n");
		page.append("      }\n");
		page.append("      document.getElementById(\"qfsubmitdata2\").value = JSON.stringify(res);\n");
		page.append("      document.getElementById(\"qfsubmit2\").submit();\n");
		page.append("    }\n");
		page.append("\n");
		page.append("    function qI18n(text, data) {\n");
		page.append("      var r = /(?:\\{{3}\\s*)((\\d)+)(?:\\s*\\}{3})/g;\n");
		page.append("      return text.replace(r, function(m,p1) { if(!data) return m; var n = parseInt(p1)-1; if(n<0 || n>=data.length) return m; return data[n];});\n");
		page.append("    }\n");
		page.append("    function qlrToggle(btn,id1,id2) { if(btn.value=='Prikaži točno rješenje') {\n btn.value='Prikaži Vaše rješenje';\n } else {\n btn.value ='Prikaži točno rješenje';\n }\n $('#'+id1).toggle();\n $('#'+id2).toggle();\n }\n");
		
		page.append("</script>\n");

		page.append("<style>\n");
		page.append("  DIV.QBoxMainNavig { padding: 5px; margin: 5px; }\n");
		page.append("  DIV.QBoxFrame { border: 1px solid #aaaaff; margin: 5px; }\n");
		page.append("  DIV.QBoxHead { padding: 5px; border-bottom: 1px dashed #aaaaaa; }\n");
		page.append("  DIV.QBoxBody { padding: 5px; }\n");
		page.append("  DIV.QBoxBasicButtons { float: right; }\n");
		page.append("</style>\n");

		page.append("</head>\n<body>\n");

		if(action==SS_ACTION.SHOW_FOR_VERIFY) {
			if(qi.getStatus()==QIStatus.ERROR) {
				page.append("<div>Vrednovanje zadatka je rezultiralo pogreškom. Označite da zadatak nije rješavan.<br><hr></div>\n");
			} else if(qi.getStatus()!=QIStatus.EVALUATED) {
				return null;
			}
		}

		int index2 = -1;
		if(action==SS_ACTION.SHOW_FOR_VERIFY) {
			index2 = wsqi.getControlResult()==null ? -1 : (wsqi.getControlResult()==QuestionControlResult.CORRECT ? 0 : (wsqi.getControlResult()==QuestionControlResult.INCORRECT ? 1 : 2));
			page.append("\n<script>\nvar selectedOptionIndex = ").append(index2).append(";\nfunction qtoggle(e,i) {\n   if(i==selectedOptionIndex) {\n      selectedOptionIndex=-1; qRemoveClass(e,'qsel'); qAddClass(e,'qnosel');\n   } else if(selectedOptionIndex==-1) {\n      selectedOptionIndex=i; qRemoveClass(e,'qnosel'); qAddClass(e,'qsel');\n   } else {\n      var e2=document.getElementById('qid'+selectedOptionIndex); qRemoveClass(e2,'qsel'); qAddClass(e2,'qnosel'); selectedOptionIndex=i; qRemoveClass(e,'qnosel'); qAddClass(e,'qsel');\n   }\n   console.log('selektirana opcija je: '+selectedOptionIndex);\n}\nfunction qAddClass(e,c) {\n    e.className += ' '+c;\n}\nfunction qRemoveClass(e,c) {\n   e.className = c=='qsel' ? e.className.replace( /(?:^|\\s)qsel(?!\\S)/g , '' ) : e.className.replace( /(?:^|\\s)qnosel(?!\\S)/g , '' );\n}</script>");
		}
		
		page.append("<div class=\"container-fluid\">\n");
		page.append("<div class=\"row\">\n");
		page.append("<div class=\"col-md-9\">\n");

		boolean readOnly = correctState!=null;
		
		page.append("<div class=\"QBoxFrame\">\n");
		page.append("<div class=\"QBoxHead\">\n");
		page.append("<div class=\"QBoxBasicButtons\">\n");
		if(!readOnly) {
			page.append(" <input class=\"btn btn-warning btn-sm\" type=\"button\" value=\"Resetiraj\" onclick=\"resetQuestion('QLP0')\">\n");
			page.append(" <input class=\"btn btn-warning btn-sm\" type=\"button\" value=\"Povrati\" onclick=\"revertQuestion('QLP0')\">\n");
		} else {
			page.append(" <input class=\"btn btn-warning btn-sm\" type=\"button\" value=\"Prikaži točno rješenje\" onclick=\"qlrToggle(this,'QLR0','QLR1');\">\n");
		}
		page.append("</div>\n");
		page.append("<div><b>Zadatak 1</b> ");
		if(correctState!=null) {
			page.append("| Rješavano: ").append(qi.isSolved()).append(", mjera točnosti: ").append(qi.getCorrectness()).append("\n");
		}
		page.append("</div>\n");
		page.append("</div>\n");
		page.append("<div class=\"QBoxBody\" id=\"QLR0\">\n");
		page.append(qHtml.replaceAll("\\$\\{\\{\\{qid\\}\\}\\}", "QLP0"));
		page.append("</div>\n");
		
		if(correctState!=null) {
			page.append("<div class=\"QBoxBody\" style=\"display: none;\" id=\"QLR1\">\n");
			page.append(qHtml.replaceAll("\\$\\{\\{\\{qid\\}\\}\\}", "QLP1"));
			page.append("</div>\n");
		}
		
		page.append("</div>\n");
		page.append("<script>\n");
		page.append("model[0].api = (function(questionLocalID, readOnly, i18n){\n");
		page.append(qScript);
		page.append("})(model[0].localID, model[0].readOnly, model[0].i18n);\n");
		page.append("</script>\n");

		if(correctState!=null) {
			page.append("<script>\n");
			page.append("model[1].api = (function(questionLocalID, readOnly, i18n){\n");
			page.append(qScript);
			page.append("})(model[1].localID, model[1].readOnly, model[1].i18n);\n");
			page.append("</script>\n");
		}
		
		page.append("</div><!-- col-md-9 -->\n");
		page.append("</div><!-- row -->\n");

		if(action==SS_ACTION.SHOW_FOR_VERIFY) {
			page.append("<div><hr></div>");
			page.append("<div>");
			if(wsqic.getUser()!=null) {
				page.append("Pred Vama je zadatak koji je dobio korisnik ");
				WebUtil.htmlEncode(page, wsqic.getUser().getFirstName());
				page.append(" ");
				WebUtil.htmlEncode(page, wsqic.getUser().getLastName());
				page.append(". Provjerite je li njegovo rješenje točno. Odluku unesite putem gumbi koji su prikazani ispod.");
			} else {
				page.append("Pred Vama je zadatak koji je dobila grupa ");
				WebUtil.htmlEncode(page, wsqic.getGroup().getName());
				page.append(". Provjerite je li njihovo rješenje točno. Odluku unesite putem gumbi koji su prikazani ispod.");
			}
			page.append("</div>");
			String[] opcije = {"Rješenje je točno", "Rješenje nije točno", "Zadatak nije riješen"};
			for(int i = 0; i < opcije.length; i++) {
				page.append("<div");
				page.append(" id=\"qid").append(i).append("\"");
				page.append(" onclick=\"qtoggle(this,").append(i).append(");\"");
				page.append(" style=\"text-align: center; font-size: 2em; padding: 5px; width: 100%; margin-top: 5px; margin-bottom: 5px;\" class=\"").append(i==index2 ? "qsel":"qnosel").append("\">").append(opcije[i]).append("</div>\n");
			}
		}

		if(action==SS_ACTION.SHOW_FOR_INSPECT && wsqi!=null && wsqi.getToControl() != null) {
			page.append("<hr>");
			page.append("<div style=\"text-align: center;\">");
			page.append("Dodatno, trebali ste provjeriti je li rješenje studenta koje Vam je sustav prikazao točno, netočno ili nerješavano. ");
			if(wsqi.getControlResult()==null) {
				page.append("Na to niste odgovorili. ");
			} else if(wsqi.getControlResult()==QuestionControlResult.CORRECT) {
				page.append("Odgovorili ste da je rješenje točno. ");
			} else if(wsqi.getControlResult()==QuestionControlResult.INCORRECT) {
				page.append("Odgovorili ste da je rješenje netočno. ");
			} else if(wsqi.getControlResult()==QuestionControlResult.NOTSOLVED) {
				page.append("Odgovorili ste da zadatak nije riješen. ");
			};
			if(wsqi.getControllerCorrect()==null) {
				page.append("Je li Vaš odgovor točan, u ovom trenutku nije poznato.");
			} else if(wsqi.getControllerCorrect().booleanValue()==true) {
				page.append("I to je točan odgovor.");
			} else if(wsqi.getControllerCorrect().booleanValue()==false) {
				page.append("To nije točan odgovor.");
			}
			page.append("</div>");
			page.append("<hr>");
		}

		if(action==SS_ACTION.SHOW_FOR_SOLVING) {
			if(correctState==null) {
				page.append("<div class=\"row\">\n");
				page.append("<div class=\"col-md-9\">\n");
				
				page.append("<div class=\"QBoxMainNavig\">\n");

				page.append("<input class=\"btn btn-default\" type=\"button\" value=\"Save\" onclick=\"sendUpdate();\">\n");
				if(!showEvalButton) {
				} else {
					page.append("<input class=\"btn btn-danger\" type=\"button\" value=\"Save and finish\" onclick=\"sendUpdateF()\">\n");
				}
				
				//page.append("<input class=\"btn btn-info\" type=\"button\" value=\"SimulirajDohvat\" onclick=\"simulateTake()\">\n");
				//page.append("\n");
				//page.append("<div id=\"ispis\"></div>\n");
	
				page.append("<form id=\"qfsubmit\" action=\"").append(req.getContextPath()).append("/ml/questions/").append(apctx.getContextData()).append("/solving/jsi-").append(jsqi.getId()).append("\" method=\"post\">\n");
				page.append("<input id=\"qfsubmitdata\" type=\"hidden\" name=\"qstates\">\n");
				page.append("<input id=\"qaction\" type=\"hidden\" name=\"action\" value=\"save\">\n");
				page.append("</form>\n");
				page.append("<form id=\"qfsubmit2\" action=\"").append(req.getContextPath()).append("/ml/questions/").append(apctx.getContextData()).append("/solving/jsi-").append(jsqi.getId()).append("\" method=\"post\">\n");
				page.append("<input id=\"qfsubmitdata2\" type=\"hidden\" name=\"qstates\">\n");
				page.append("<input id=\"qaction2\" type=\"hidden\" name=\"action\" value=\"saveeval\">\n");
				page.append("</form>\n");
				page.append("</div>\n");
				
				page.append("</div><!-- col-md-9 -->\n");
				page.append("</div><!-- row -->\n");
			}
		}

		if(action==SS_ACTION.SHOW_FOR_INSPECT) {
			if(correctState!=null) {
				page.append("<div class=\"row\">\n");
				page.append("<div class=\"col-md-9\">\n");
				
				page.append("<button onclick=\"document.getElementById('qform4').submit(); return false;\" class='btn btn-warning'>Povratak</button>");
				
				page.append("</div><!-- col-md-9 -->\n");
				page.append("</div><!-- row -->\n");
			}
			if(wsqi==null) {
				page.append("\n<form action=\"").append(req.getContextPath()).append("/ml/questions/").append(apctx.getContextData()).append("/details/").append(jsqi.getQuestion().getId()).append("\" method=\"get\" id=\"qform4\"></form>");
			} else {
				page.append("\n<form action=\"").append(req.getContextPath()).append("/ml/workspaces/").append(wsqi.getDefinition().getWorkspace().getId()).append("/room/").append(wsqi.getDefinition().getSubpage().getNumID()).append("\" method=\"get\" id=\"qform4\"><input type='hidden' name='op' value='idle'></form>");
			}
		}
		
		if(action==SS_ACTION.SHOW_FOR_VERIFY) {
			page.append("<div class=\"row\">\n");
			page.append("<div class=\"col-md-9\">\n");

			page.append("<button onclick=\"document.getElementById('qhid3').value=selectedOptionIndex; document.getElementById('qfsubmit3').submit(); return false;\" class='btn btn-warning'>Pohrani</button>"); 

			page.append("<form id=\"qfsubmit3\" action=\"").append(req.getContextPath()).append("/ml/questions/").append(apctx.getContextData()).append("/solving/jsi-").append(jsqi.getId()).append("\" method=\"post\">\n");
			page.append("<input id=\"qhid3\" type=\"hidden\" name=\"data\">\n");
			page.append("<input id=\"qaction3\" type=\"hidden\" name=\"action\" value=\"saveverify\">\n");
			page.append("</form>\n");
			
			page.append("</div><!-- col-md-9 -->\n");
			page.append("</div><!-- row -->\n");
		}
		
		page.append("</div><!-- container-fluid -->\n");

		page.append("<script>\n");
		page.append("  initQPage();\n");
		page.append("</script>\n");

		page.append("</body>\n</html>\n");
		
		return new StreamRetVal(page.toString(), StandardCharsets.UTF_8, "text/html; charset=utf-8", null);
	}	
	private static StreamRetVal createQuestionPage(HttpServletRequest req, JSQuestionInstance jSQuestionInstance, String qHtml,
			String qScript, String userData, String commonState, String emptyState, String userState, String correctState, Set<String> registeredPageScripts) {
		//System.out.println("userdata = " + userData);
		//System.out.println("userstate = " + userState);
		//System.out.println("commonstate = " + commonState);
		//System.out.println("emptystate = " + emptyState);

		
		StringBuilder page = new StringBuilder(1024);
		
		page.append("<!DOCTYPE html><html>\n<head>\n<meta charset=\"utf-8\">\n");
		
		page.append("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
		page.append("    \n");
		page.append("    <link rel=\"stylesheet\" href=\"//ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/themes/base/jquery-ui.css\">\n");
		page.append("    <script src=\"//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js\"></script>\n");
		page.append("    <script src=\"//ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js\"></script>\n");
		page.append("    \n");
		page.append("    <link rel=\"stylesheet\" href=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">\n");
		page.append("    <script src=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>\n");
		page.append("    <link rel=\"stylesheet\" type=\"text/css\" href=\"").append(req.getContextPath()).append("/css/common.css\">\n");
		page.append("    \n");
		page.append("    <link rel=\"stylesheet\" href=\"//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/default.min.css\">\n");
		page.append("    <script src=\"//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js\"></script>\n");
		page.append("    <script src=\"//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/languages/vhdl.min.js\"></script>\n");
		page.append("    \n");
		page.append("    <script type=\"text/javascript\" async src=\"//cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML\"></script>\n");
		page.append("    <script type=\"text/javascript\">hljs.initHighlightingOnLoad();</script>\n");
		if(registeredPageScripts != null) {
			for(String scrurl : registeredPageScripts) {
				page.append("    <script type=\"text/javascript\" src=\"").append(scrurl).append("\"></script>\n");
			}
		}

		page.append("<script>\n");

		page.append("    var model = [\n");
		page.append("      {\n");
		page.append("        problemInstanceID: \"").append(jSQuestionInstance.getId()).append("\",\n");
		page.append("        readOnly: ").append(correctState==null?"false":"true").append(",\n");
		page.append("        localID: \'QLP0\',\n");
		page.append("        commonState: ").append(commonState).append(",\n");
		page.append("        userState: ").append(userState).append(", \n");
		page.append("        emptyState: ").append(emptyState).append("\n");
		page.append("      }\n");
		if(correctState!=null) {
			page.append("      ,{\n");
			page.append("        problemInstanceID: \"").append(jSQuestionInstance.getId()).append("\",\n");
			page.append("        readOnly: true,\n");
			page.append("        localID: \'QLP1\',\n");
			page.append("        commonState: ").append(commonState).append(",\n");
			page.append("        userState: ").append(correctState).append(", \n");
			page.append("        emptyState: ").append(emptyState).append("\n");
			page.append("      }\n");
		}
		page.append("    ];\n");

		page.append("\n");
		page.append("    var modelMap = {\n");
		page.append("      \"QLP0\": 0\n");
		if(correctState!=null) {
			page.append("      ,\"QLP1\": 1\n");
		}
		page.append("    };\n");
		page.append("\n");
		page.append("    var modelExportedObjects = {}\n");
		page.append("\n");
		page.append("    function qExportGlobalObject(qid,oid,obj) {\n");
		page.append("     if(!modelExportedObjects.hasOwnProperty(qid)) modelExportedObjects[qid] = {};\n");
		page.append("     modelExportedObjects[qid][oid] = obj;\n");
		page.append("    }\n");
		page.append("\n");
		page.append("    function qGlobalObject(qid,oid) {\n");
		page.append("     return modelExportedObjects[qid][oid];\n");
		page.append("    }\n");
		page.append("\n");
		page.append("    function resetQuestion(qid) {\n");
		page.append("      var i = modelMap[qid];\n");
		page.append("      if(model[i].readOnly) return;\n");
		page.append("      model[i].api.resetState(model[i].commonState, model[i].emptyState);\n");
		page.append("    }\n");
		page.append("\n");
		page.append("    function revertQuestion(qid) {\n");
		page.append("      var i = modelMap[qid];\n");
		page.append("      if(model[i].readOnly) return;\n");
		page.append("      model[i].api.revertState(model[i].commonState, model[i].userState);\n");
		page.append("    }\n");
		page.append("\n");
		page.append("    function initQPage() {\n");
		page.append("      for(var i = 0; i < model.length; i++) {\n");
		page.append("        model[i].api.initQuestion(model[i].commonState, model[i].userState);\n");
		page.append("      }\n");
		page.append("    }\n");
		page.append("\n");
		page.append("    function simulateTake() {\n");
		page.append("      var res = [];\n");
		page.append("      for(var i = 0; i < model.length; i++) {\n");
		page.append("        if(model[i].readOnly) continue;\n");
		page.append("        res.push({piid: model[i].problemInstanceID, qs: model[i].api.takeState()});\n");
		page.append("      }\n");
		page.append("      document.getElementById(\"ispis\").innerHTML = JSON.stringify(res);\n");
		page.append("    }\n");
		page.append("\n");
		page.append("    function sendUpdate() {\n");
		page.append("      var res = [];\n");
		page.append("      for(var i = 0; i < model.length; i++) {\n");
		page.append("        if(model[i].readOnly) continue;\n");
		page.append("        res.push({piid: model[i].problemInstanceID, qs: model[i].api.takeState()});\n");
		page.append("      }\n");
		page.append("      document.getElementById(\"qfsubmitdata\").value = JSON.stringify(res);\n");
		page.append("      document.getElementById(\"qfsubmit\").submit();\n");
		page.append("    }\n");
		page.append("\n");
		page.append("    function sendUpdateF() {\n");
		page.append("      var res = [];\n");
		page.append("      for(var i = 0; i < model.length; i++) {\n");
		page.append("        if(model[i].readOnly) continue;\n");
		page.append("        res.push({piid: model[i].problemInstanceID, qs: model[i].api.takeState()});\n");
		page.append("      }\n");
		page.append("      document.getElementById(\"qfsubmitdata2\").value = JSON.stringify(res);\n");
		page.append("      document.getElementById(\"qfsubmit2\").submit();\n");
		page.append("    }\n");
		page.append("\n");
		page.append("    function qI18n(text, data) {\n");
		page.append("      var r = /(?:\\{{3}\\s*)((\\d)+)(?:\\s*\\}{3})/g;\n");
		page.append("      return text.replace(r, function(m,p1) { if(!data) return m; var n = parseInt(p1)-1; if(n<0 || n>=data.length) return m; return data[n];});\n");
		page.append("    }\n");
		page.append("    function qlrToggle(btn,id1,id2) { if(btn.value=='Prikaži točno rješenje') {\n btn.value='Prikaži Vaše rješenje';\n } else {\n btn.value ='Prikaži točno rješenje';\n }\n $('#'+id1).toggle();\n $('#'+id2).toggle();\n }\n");
		
		page.append("</script>\n");

		page.append("<style>\n");
		page.append("  DIV.QBoxMainNavig { padding: 5px; margin: 5px; }\n");
		page.append("  DIV.QBoxFrame { border: 1px solid #aaaaff; margin: 5px; }\n");
		page.append("  DIV.QBoxHead { padding: 5px; border-bottom: 1px dashed #aaaaaa; }\n");
		page.append("  DIV.QBoxBody { padding: 5px; }\n");
		page.append("  DIV.QBoxBasicButtons { float: right; }\n");
		page.append("</style>\n");

		page.append("</head>\n<body>\n");

		page.append("<div class=\"container-fluid\">\n");
		page.append("<div class=\"row\">\n");
		page.append("<div class=\"col-md-9\">\n");

		boolean readOnly = correctState!=null;
		
		page.append("<div class=\"QBoxFrame\">\n");
		page.append("<div class=\"QBoxHead\">\n");
		page.append("<div class=\"QBoxBasicButtons\">\n");
		if(!readOnly) {
			page.append(" <input class=\"btn btn-warning btn-sm\" type=\"button\" value=\"Resetiraj\" onclick=\"resetQuestion('QLP0')\">\n");
			page.append(" <input class=\"btn btn-warning btn-sm\" type=\"button\" value=\"Povrati\" onclick=\"revertQuestion('QLP0')\">\n");
		} else {
			page.append(" <input class=\"btn btn-warning btn-sm\" type=\"button\" value=\"Prikaži točno rješenje\" onclick=\"qlrToggle(this,'QLR0','QLR1');\">\n");
		}
		page.append("</div>\n");
		page.append("<div><b>Zadatak 1</b> ");
		if(correctState!=null) {
			page.append("| Rješavano: ").append(jSQuestionInstance.isSolved()).append(", mjera točnosti: ").append(jSQuestionInstance.getCorrectness()).append("\n");
		}
		page.append("</div>\n");
		page.append("</div>\n");
		page.append("<div class=\"QBoxBody\" id=\"QLR0\">\n");
		page.append(qHtml.replaceAll("\\$\\{\\{\\{qid\\}\\}\\}", "QLP0"));
		page.append("</div>\n");
		
		if(correctState!=null) {
			page.append("<div class=\"QBoxBody\" style=\"display: none;\" id=\"QLR1\">\n");
			page.append(qHtml.replaceAll("\\$\\{\\{\\{qid\\}\\}\\}", "QLP1"));
			page.append("</div>\n");
		}
		
		page.append("</div>\n");
		page.append("<script>\n");
		page.append("model[0].api = (function(questionLocalID, readOnly){\n");
		page.append(qScript);
		page.append("})(model[0].localID, model[0].readOnly);\n");
		page.append("</script>\n");

		if(correctState!=null) {
			page.append("<script>\n");
			page.append("model[1].api = (function(questionLocalID, readOnly){\n");
			page.append(qScript);
			page.append("})(model[1].localID, model[1].readOnly);\n");
			page.append("</script>\n");
		}
		
		page.append("</div><!-- col-md-9 -->\n");
		page.append("</div><!-- row -->\n");

		if(correctState==null) {
			page.append("<div class=\"row\">\n");
			page.append("<div class=\"col-md-9\">\n");
			
			page.append("<div class=\"QBoxMainNavig\">\n");
	
			page.append("<input class=\"btn btn-default\" type=\"button\" value=\"Save\" onclick=\"sendUpdate()\">\n");
			page.append("<input class=\"btn btn-danger\" type=\"button\" value=\"Save and finish\" onclick=\"sendUpdateF()\">\n");

			page.append("<input class=\"btn btn-info\" type=\"button\" value=\"SimulirajDohvat\" onclick=\"simulateTake()\">\n");
			page.append("\n");
			page.append("<div id=\"ispis\"></div>\n");

			page.append("<form id=\"qfsubmit\" action=\"").append(req.getContextPath()).append("/ml/test/question/qupdate\" method=\"post\">\n");
			page.append("<input id=\"qfsubmitdata\" type=\"hidden\" name=\"qstates\">\n");
			page.append("</form>\n");
			page.append("<form id=\"qfsubmit2\" action=\"").append(req.getContextPath()).append("/ml/test/question/qupdatef\" method=\"post\">\n");
			page.append("<input id=\"qfsubmitdata2\" type=\"hidden\" name=\"qstates\">\n");
			page.append("</form>\n");
			page.append("</div>\n");
			
			page.append("</div><!-- col-md-9 -->\n");
			page.append("</div><!-- row -->\n");
		}
		
		if(correctState!=null) {
			page.append("<div class=\"row\">\n");
			page.append("<div class=\"col-md-9\">\n");
			
			page.append("<div><a href=\"").append(req.getContextPath()).append("/ml/test/question/list\">Povratak na popis zadataka</a></div>\n");
			
			page.append("</div><!-- col-md-9 -->\n");
			page.append("</div><!-- row -->\n");
		}

		page.append("</div><!-- container-fluid -->\n");

		page.append("<script>\n");
		page.append("  initQPage();\n");
		page.append("</script>\n");

		page.append("</body>\n</html>\n");
		
		return new StreamRetVal(page.toString(), StandardCharsets.UTF_8, "text/html; charset=utf-8", null);
	}
	
	public static ContentProcessor prepareBasicContentProcessor(String lang, DAO dao, String qid, String contextPath, Map<String, String> varMap, Map<String, Object> infoMap, Map<String,String> translations) {
		if(varMap==null) varMap = new HashMap<>();
		if(infoMap==null) infoMap = new HashMap<>();
		Map<String, HtmlTemplateCommand> commands = new HashMap<>();
		commands.put("file", new FileCmd(() -> contextPath+"/ml/question/"+qid+"/file/"));
		commands.put("file-url", new FileCmd(() -> contextPath+"/ml/question/"+qid+"/file/"));
		commands.put("include", new PageIncludeCmd());
		commands.put("page-include", new PageIncludeCmd());
		commands.put("register-page-script", new RegisterPageScriptCmd(() -> contextPath+"/ml/question/"+qid+"/file/"));
		commands.put("common-include", new CommonIncludeCmd());
		commands.put("raw-text", new RawTextCmd(new MapVariableMapping(varMap)));
		commands.put("echoIfTrue", new EchoCmd(Boolean.TRUE, new MapVariableMapping(infoMap)));
		commands.put("echoIfFalse", new EchoCmd(Boolean.FALSE, new MapVariableMapping(infoMap)));
		commands.put("echo", new EchoCmd(null, new MapVariableMapping(infoMap)));
		commands.put("i18n", new I18NCmd(translations));
		return new ContentProcessor(path->dao.getRealQPathFor(qid, path), c->commands.containsKey(c), commands, "pages");
	}
	
	public static ContentProcessor prepareBasicScriptProcessor(String lang, DAO dao, String qid, Map<String, String> varMap, Map<String, Object> infoMap) {
		if(varMap==null) varMap = new HashMap<>();
		if(infoMap==null) infoMap = new HashMap<>();
		Map<String, HtmlTemplateCommand> commands = new HashMap<>();
		commands.put("include", new PageIncludeCmd());
		commands.put("common-include", new CommonIncludeCmd());
		commands.put("raw-text", new RawTextCmd(new MapVariableMapping(varMap)));
		commands.put("echoIfTrue", new EchoCmd(Boolean.TRUE, new MapVariableMapping(infoMap)));
		commands.put("echoIfFalse", new EchoCmd(Boolean.FALSE, new MapVariableMapping(infoMap)));
		commands.put("echo", new EchoCmd(null, new MapVariableMapping(infoMap)));
		return new ContentProcessor(path->dao.getRealQPathFor(qid, path), c->commands.containsKey(c), commands, "scripts");
	}
	
	private static final String scriptFooter = "// Pomoćne metode: kasnije prebaciti u framework:\n"+
			"// -------------------------------------------------------------------------------------------------\n"+
			"function __fw_setUserData(str) {\n"+
			"  userData = JSON.parse(str);\n"+
			"}\n"+
			"\n"+
			"function __fw_getUserData() {\n"+
			"  return JSON.stringify(userData);\n"+
			"}\n"+
			"\n"+
			"function __fw_getUserDataQuestion() {\n"+
			"  return JSON.stringify(userData.question);\n"+
			"}\n"+
			"\n"+
			"function __fw_getUserDataQuestionState() {\n"+
			"  return JSON.stringify(userData.questionState);\n"+
			"}\n"+
			"\n"+
			"function __fw_getUserDataCorrectQuestionState() {\n"+
			"  return JSON.stringify(userData.correctQuestionState);\n"+
			"}\n"+
			"\n"+
			"function __fw_questionInitialize(questionConfig) {\n"+
			"  questionInitialize(JSON.parse(questionConfig));\n"+
			"}\n"+
			"\n"+
			"function __fw_multipleQuestionsInitialize(questionConfig,numberOfInstances) {\n"+
			"  multipleQuestionsInitialize(JSON.parse(questionConfig), numberOfInstances);\n"+
			"}\n"+
			"\n"+
			"function __fw_getQuestionUserData(instanceNumber) {\n"+
			"  return JSON.stringify(questionInstances[instanceNumber]);\n"+
			"}\n"+
			"\n"+
			"function __fw_importQuestionState(data) {\n"+
			"  importQuestionState(JSON.parse(data));\n"+
			"}\n"+
			"\n"+
			"function __fw_exportEmptyState() {\n"+
			"  return JSON.stringify(exportEmptyState());\n"+
			"}\n"+
			"\n"+
			"function __fw_exportUserState(stateType) {\n"+
			"  return JSON.stringify(exportUserState(stateType));\n"+
			"}\n"+
			"\n"+
			"function __fw_exportCommonState() {\n"+
			"  return JSON.stringify(exportCommonState());\n"+
			"}\n"+
			"\n"+
			"var __fw_qevalres = {};\n"+
			"\n"+
			"function __fw_questionEvaluate() {\n"+
			"  __fw_qevalres = questionEvaluate();\n"+
			"}\n"+
			"\n"+
			"function __fw_questionEvaluateCorrectness() {\n"+
			"  return __fw_qevalres.correctness;\n"+
			"}\n"+
			"\n"+
			"function __fw_getComputedProperties() {\n"+
			"  return JSON.stringify(getComputedProperties());\n"+
			"}\n"+
			"\n"+
			"function __fw_questionEvaluateSolved() {\n"+
			"  return __fw_qevalres.solved;\n"+
			"}\n";
	
}
