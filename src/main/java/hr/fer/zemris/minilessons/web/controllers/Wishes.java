package hr.fer.zemris.minilessons.web.controllers;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.dao.model.Wish;
import hr.fer.zemris.minilessons.dao.model.WishStatus;
import hr.fer.zemris.minilessons.dao.model.WishUserVote;
import hr.fer.zemris.minilessons.dao.model.WishWithUserVote;
import hr.fer.zemris.minilessons.domain.model.DWish;
import hr.fer.zemris.minilessons.service.Authenticated;
import hr.fer.zemris.minilessons.service.DAOProvided;
import hr.fer.zemris.minilessons.service.MLCurrentContext;
import hr.fer.zemris.minilessons.service.Transactional;
import hr.fer.zemris.minilessons.web.retvals.StreamRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal.Technology;

/**
 * Implementation of wish-list functionality.

 * @author marcupic
 */
public class Wishes {

	/**
	 * Implementation of wish list browsing.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return <code>null</code>
	 */
	@DAOProvided @Transactional @Authenticated(optional=true)
	public TemplateRetVal showWishes(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		
		List<DWish> dlist = null;
		
		if(MLCurrentContext.getAuthUser()==null) {
			List<Wish> wlist = DAOProvider.getDao().listAllWishes();
			dlist = wlist.stream().map(w->DWish.fromWish(w)).collect(Collectors.toList());
		} else {
			List<WishWithUserVote> wlist = DAOProvider.getDao().listAllWishesEx(MLCurrentContext.getAuthUser().getId());
			dlist = wlist.stream().map(w->DWish.fromWish(w.getWish()).fillUserVote(w.getVote())).collect(Collectors.toList());
		}
		
		req.setAttribute("wishes", dlist);
		return new TemplateRetVal("wishlist",Technology.THYMELEAF);
	}

	@DAOProvided @Transactional @Authenticated(redirectAllowed=false,attemptLogin=false)
	public StreamRetVal saveWishVote(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		String wishIDStr = req.getParameter("wid");
		String voteStr = req.getParameter("vote");
		
		Long wishID = null;
		int vote = 0;
		
		if("0".equals(voteStr)) {
			vote = 0;
		} else if("1".equals(voteStr)) {
			vote = 1;
		} else if("-1".equals(voteStr)) {
			vote = -1;
		} else {
			return new StreamRetVal("{\"errorCode\": 1}", StandardCharsets.UTF_8, "application/json", null);
		}

		if(wishIDStr == null) {
			return new StreamRetVal("{\"errorCode\": 1}", StandardCharsets.UTF_8, "application/json", null);
		}
		try {
			wishID = Long.valueOf(wishIDStr);
		} catch(Exception ex) {
			return new StreamRetVal("{\"errorCode\": 1}", StandardCharsets.UTF_8, "application/json", null);
		}
		
		Wish wish = DAOProvider.getDao().findWish(wishID);
		if(wish==null) {
			return new StreamRetVal("{\"errorCode\": 1}", StandardCharsets.UTF_8, "application/json", null);
		}
		
		WishUserVote wuv = DAOProvider.getDao().findWishUserVote(wishID, MLCurrentContext.getAuthUser().getId());

		if(wuv==null) {
			wuv = new WishUserVote();
			wuv.setWish(wish);
			wuv.setUser(DAOProvider.getDao().findUserByID(MLCurrentContext.getAuthUser().getId()));
			wuv.setUnprocessed(true);
			wuv.setVote(vote);
			DAOProvider.getDao().saveWishUserVote(wuv);
		} else {
			if(wuv.getVote() != vote) {
				wuv.setVote(vote);
				wuv.setUnprocessed(true);
			}
		}
		
		return new StreamRetVal("{\"errorCode\": 0, \"vote\": "+vote+"}", StandardCharsets.UTF_8, "application/json", null);
	}
	
	
	/**
	 * Implementation of adding new wish.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return <code>null</code>
	 */
	@DAOProvided @Transactional @Authenticated(redirectAllowed=true)
	public TemplateRetVal showNewWishEditor(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		return new TemplateRetVal("wish",Technology.THYMELEAF);
	}

	/**
	 * Implementation of saving new wish.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return <code>null</code>
	 */
	@DAOProvided @Transactional @Authenticated(redirectAllowed=false)
	public StreamRetVal saveNewWish(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		String area = trimDenull(req.getParameter("area"));
		String topic = trimDenull(req.getParameter("topic"));
		String description = trimDenull(req.getParameter("description"));

		if(area.isEmpty()) {
			return new StreamRetVal("{\"errorCode\": 1}", StandardCharsets.UTF_8, "application/json", null);
		}
		if(topic.isEmpty()) {
			return new StreamRetVal("{\"errorCode\": 2}", StandardCharsets.UTF_8, "application/json", null);
		}
		if(description.isEmpty()) {
			return new StreamRetVal("{\"errorCode\": 3}", StandardCharsets.UTF_8, "application/json", null);
		}

		Wish wish = new Wish();
		wish.setArea(area);
		wish.setTopic(topic);
		wish.setDescription(description);
		wish.setOpenedAt(new Date());
		wish.setOpenedBy(DAOProvider.getDao().findUserByID(MLCurrentContext.getAuthUser().getId()));
		wish.setStatus(WishStatus.OPEN);
		
		DAOProvider.getDao().saveNewWish(wish);
		
		return new StreamRetVal("{\"errorCode\": 0}", StandardCharsets.UTF_8, "application/json", null);
	}

	private String trimDenull(String parameter) {
		if(parameter==null) return "";
		return parameter.trim();
	}
}
