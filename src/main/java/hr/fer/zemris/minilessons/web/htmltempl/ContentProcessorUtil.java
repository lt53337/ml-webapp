package hr.fer.zemris.minilessons.web.htmltempl;

public class ContentProcessorUtil {

	// Legalna imena datoteka kada ih uključujemo kroz naredbe:  (slovo|broj|tocka|minus)+
	public static boolean checkFileName(String commandArgs) {
		if(commandArgs==null || commandArgs.isEmpty()) return false;
		if(commandArgs.equals(".") || commandArgs.equals("..")) return false;
		for(char c : commandArgs.toCharArray()) {
			if(Character.isLetter(c)) continue;
			if(Character.isDigit(c)) continue;
			if(c=='-' || c=='.') continue;
			return false;
		}
		return true;
	}

}
