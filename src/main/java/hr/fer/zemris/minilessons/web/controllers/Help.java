package hr.fer.zemris.minilessons.web.controllers;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.minilessons.service.MLCurrentContext;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal.Technology;

/**
 * Implementation of help subsystem.

 * @author marcupic
 */
public class Help {

	/**
	 * Implementation of page.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return <code>null</code>
	 */
	public TemplateRetVal helpPage(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		String helpID = variables.get("hid");
		if(helpID!=null) {
			helpID = helpID.trim();
		}
		if(helpID==null || helpID.isEmpty()) {
			req.setAttribute("errorMsgKey", "missingParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		long id = 0;
		try {
			id = Long.parseLong(helpID);
		} catch(Exception ex) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		String lang = MLCurrentContext.getLocale().getLanguage();
		String key = String.format("%04d", id);
		return new TemplateRetVal("help/help"+key+"_"+lang, Technology.THYMELEAF);
	}

}
