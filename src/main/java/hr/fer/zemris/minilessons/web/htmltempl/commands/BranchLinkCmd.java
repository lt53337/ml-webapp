package hr.fer.zemris.minilessons.web.htmltempl.commands;

import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

import hr.fer.zemris.minilessons.web.htmltempl.CommandEnvironment;
import hr.fer.zemris.minilessons.web.htmltempl.HtmlTemplateCommand;
import hr.fer.zemris.minilessons.xmlmodel.XMLBranch;
import hr.fer.zemris.minilessons.xmlmodel.XMLItem;

public class BranchLinkCmd implements HtmlTemplateCommand {
	
	private Map<String, XMLBranch> branches;
	private Map<String,XMLItem> branchParent;
	private Set<String> visibleBranchIDs;
	private Supplier<String> urlSupplier;
	
	public BranchLinkCmd(Supplier<String> urlSupplier, Map<String, XMLBranch> branches, Map<String,XMLItem> branchParent, Set<String> visibleBranchIDs) {
		this.branches = branches;
		this.branchParent = branchParent;
		this.visibleBranchIDs = visibleBranchIDs;
		this.urlSupplier = urlSupplier;
	}

	@Override
	public void execute(CommandEnvironment env) throws IOException {
		String bid = env.getCommandArgs();
		if(!visibleBranchIDs.contains(bid)) {
			throw new RuntimeException("Requested branch " + bid + " is not visible from this item!");
		}
		XMLBranch branch = branches.get(bid);
		if(branch==null) {
			throw new RuntimeException("Requested branch not found: " + bid);
		}
		XMLItem branchParentItem = branchParent.get(bid);
		if(branchParentItem==null) {
			throw new RuntimeException("Branch parent for requested branch not found: " + bid);
		}
		env.getWriter().append(urlSupplier.get()).append(branchParentItem.getId()).append("/start");
	}

}
