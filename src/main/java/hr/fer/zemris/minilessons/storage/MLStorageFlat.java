package hr.fer.zemris.minilessons.storage;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import hr.fer.zemris.minilessons.FileUtil;
import hr.fer.zemris.minilessons.dao.DAOProvider;

public class MLStorageFlat implements MLStorage {

	private Path root;
	
	public MLStorageFlat() {
		try {
			Properties prop = new Properties();
			InputStream is = DAOProvider.class.getClassLoader().getResourceAsStream("ml-storage-flat.properties");
			if(is==null) {
				throw new IOException("File not found: ml-storage-flat. Could not initialize storage engine.");
			}
			try {
				prop.load(is);
			} finally {
				is.close();
			}
			String dbi = prop.getProperty("ml.storage.flat.root");
			if(dbi==null || dbi.isEmpty()) {
				throw new IOException("ml.storage.flat.root is not set.");
			}
			root = Paths.get(dbi);
			if(!Files.isDirectory(root)) {
				throw new RuntimeException("Directory "+root+" specified for storage is not present.");
			}
			if(!Files.isReadable(root)) {
				throw new RuntimeException("Directory "+root+" specified for storage could not be read.");
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Path> allMinilessonRoots() throws IOException {
		List<Path> list = new ArrayList<>();
		try(DirectoryStream<Path> ds = Files.newDirectoryStream(root)) {
			for(Path p : ds) {
				String name = p.getFileName().toString();
				if(name.equals(".") || name.equals("..")) continue;
				if(!Files.isDirectory(p)) {
					continue;
				}
				if(Files.exists(p.resolve("descriptor.xml"))) {
					list.add(p);
				}
			}
		}
		return list;
	}
	
	@Override
	public Path getRealPathFor(String minilessonKey, String path) {
		if(path==null) return root.resolve(minilessonKey);
		return root.resolve(minilessonKey).resolve(path);
	}
	
	@Override
	public void persistMinilessonPackage(String minilessonID, Path srcDir) throws IOException {
		FileUtil.copyTree(srcDir, root.resolve(minilessonID));
	}
	
	@Override
	public void updateMinilessonPackage(String minilessonID, Path srcDir) throws IOException {
		// TODO: ovo nije dobra implementacija prema ugovoru sučelja; popraviti
		FileUtil.copyTree(srcDir, root.resolve(minilessonID));
	}
}
