package hr.fer.zemris.minilessons.storage;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of frozen storage: minilessons are detected during boot time.
 * 
 * @author marcupic
 *
 */
public class MLStorageFrozen implements MLStorage {

	private Map<String,Path> rootMap = new HashMap<>();
	
	public MLStorageFrozen() {
	}

	@Override
	public List<Path> allMinilessonRoots() throws IOException {
		return new ArrayList<>(rootMap.values());
	}
	
	@Override
	public Path getRealPathFor(String minilessonKey, String path) {
		if(path==null) return null;
		return rootMap.get(minilessonKey).resolve(path);
	}

	@Override
	public void persistMinilessonPackage(String minilessonID, Path srcDir) throws IOException {
		System.out.println("[MLStorageFrozen] Warning: minilesson "+minilessonID+" deployed in original directory "+srcDir+". If this directory is deleted, minilesson won't be available! This mode is FOR TESTING ONLY. Do not use in production!");
		rootMap.put(minilessonID, srcDir);
	}

	@Override
	public void updateMinilessonPackage(String minilessonID, Path srcDir) throws IOException {
		throw new IOException("Package update is not supported by this storage.");
	}
}
