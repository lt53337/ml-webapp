package hr.fer.zemris.minilessons;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Collection;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.security.NoTypePermission;
import com.thoughtworks.xstream.security.NullPermission;
import com.thoughtworks.xstream.security.PrimitiveTypePermission;

import hr.fer.zemris.minilessons.xmlmodel.XMLAbout;
import hr.fer.zemris.minilessons.xmlmodel.XMLAuthor;
import hr.fer.zemris.minilessons.xmlmodel.XMLBranch;
import hr.fer.zemris.minilessons.xmlmodel.XMLFile;
import hr.fer.zemris.minilessons.xmlmodel.XMLLanguage;
import hr.fer.zemris.minilessons.xmlmodel.XMLMinilesson;
import hr.fer.zemris.minilessons.xmlmodel.XMLOffer;
import hr.fer.zemris.minilessons.xmlmodel.XMLOption;
import hr.fer.zemris.minilessons.xmlmodel.XMLPage;
import hr.fer.zemris.minilessons.xmlmodel.XMLQuestion;
import hr.fer.zemris.minilessons.xmlmodel.XMLString;
import hr.fer.zemris.minilessons.xmlmodel.XMLView;

/**
 * Utility class for construction of parsers for parsing <code>descriptor.xml</code>
 * as well as for parsing.
 * 
 * @author marcupic
 *
 */
public class DescriptorParserUtil {

	/**
	 * Parse <code>descriptor.xml</code> which is made available as provided input stream and returns
	 * {@link XMLMinilesson} object which represents the content of provided <code>descriptor.xml</code>.
	 * 
	 * @param is input stream from which the <code>descriptor.xml</code> can be read
	 * @return object which represents the read and parsed file
	 */
	public static XMLMinilesson parseInputStream(InputStream is) {
		try {
			return (XMLMinilesson)getParser().fromXML(new InputStreamReader(new BufferedInputStream(is), StandardCharsets.UTF_8));
		} finally {
			try { is.close(); } catch(Exception ignorable) {}
		}
	}
	
	/**
	 * Creates and returns a new parser for processing of <code>descriptor.xml</code>.
	 * 
	 * @return new parser
	 */
	private static XStream getParser() {
		XStream xs = new XStream(new StaxDriver());
		
		xs.addPermission(NoTypePermission.NONE);
		xs.addPermission(NullPermission.NULL);
		xs.addPermission(PrimitiveTypePermission.PRIMITIVES);
		xs.allowTypeHierarchy(Collection.class);
		xs.allowTypes(new Class[] {String.class});
		xs.allowTypesByWildcard(new String[] {XMLMinilesson.class.getPackage().getName()+".*"});

		xs.alias("id", String.class);
		
		xs.alias("minilesson", XMLMinilesson.class);
		xs.useAttributeFor(XMLMinilesson.class, "uid");
		xs.useAttributeFor(XMLMinilesson.class, "version");
		xs.useAttributeFor(XMLMinilesson.class, "minorVersion");
		xs.addImplicitCollection(XMLMinilesson.class, "items");

		xs.alias("branch", XMLBranch.class);
		xs.useAttributeFor(XMLBranch.class, "id");
		xs.addImplicitCollection(XMLBranch.class, "title");

		xs.alias("language", XMLLanguage.class);
		xs.useAttributeFor(XMLLanguage.class, "lang");

		xs.alias("about", XMLAbout.class);

		xs.alias("string", XMLString.class);
		xs.useAttributeFor(XMLString.class, "text");
		xs.useAttributeFor(XMLString.class, "lang");

		xs.alias("author", XMLAuthor.class);
		xs.useAttributeFor(XMLAuthor.class, "firstName");
		xs.useAttributeFor(XMLAuthor.class, "lastName");
		xs.useAttributeFor(XMLAuthor.class, "email");

		xs.alias("option", XMLOption.class);
		xs.useAttributeFor(XMLOption.class, "key");
		xs.useAttributeFor(XMLOption.class, "inherit");
		xs.addImplicitCollection(XMLOption.class, "translations");

		xs.alias("page", XMLPage.class);
		xs.useAttributeFor(XMLPage.class, "id");
		xs.useAttributeFor(XMLPage.class, "branchID");
		xs.addImplicitCollection(XMLPage.class, "files", "file", XMLFile.class);
		
		xs.alias("file", XMLFile.class);
		xs.useAttributeFor(XMLFile.class, "name");
		xs.useAttributeFor(XMLFile.class, "lang");
		xs.useAttributeFor(XMLFile.class, "format");

		xs.alias("question", XMLQuestion.class);
		xs.useAttributeFor(XMLQuestion.class, "id");
		xs.useAttributeFor(XMLQuestion.class, "type");
		xs.useAttributeFor(XMLQuestion.class, "handler");
		xs.useAttributeFor(XMLQuestion.class, "branchID");

		xs.alias("view", XMLView.class);
		xs.useAttributeFor(XMLView.class, "name");
		xs.useAttributeFor(XMLView.class, "renderPageId");
		xs.addImplicitCollection(XMLView.class, "offers");

		xs.alias("offer", XMLOffer.class);
		xs.useAttributeFor(XMLOffer.class, "key");
		xs.useAttributeFor(XMLOffer.class, "action");
		xs.useAttributeFor(XMLOffer.class, "completed");

		return xs;
	}
}
