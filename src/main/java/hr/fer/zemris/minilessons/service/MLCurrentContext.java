package hr.fer.zemris.minilessons.service;

import java.util.Locale;

import hr.fer.zemris.minilessons.dao.model.MLBasicPermission;
import hr.fer.zemris.minilessons.dao.model.UploadPolicy;
import hr.fer.zemris.minilessons.web.video.VideoURLProducer;

public class MLCurrentContext {

	private static final ThreadLocal<Data> data = new ThreadLocal<>();
	
	private MLCurrentContext() {
	}

	public static AuthUser getAuthUser() {
		Data obj = data.get();
		return data==null ? null : obj.authUser;
	}

	public static void setAuthUser(AuthUser user) {
		Data obj = getOrCreate();
		obj.authUser = user;
	}

	public static VideoURLProducer getVideoURLProducer() {
		Data obj = data.get();
		return data==null ? null : obj.videoURLProducer;
	}
	
	public static void setVideoURLProducer(VideoURLProducer producer) {
		Data obj = getOrCreate();
		obj.videoURLProducer = producer;
	}
	
	public static Locale getLocale() {
		Data obj = data.get();
		return data==null ? null : obj.locale;
	}

	public static void setLocale(Locale locale) {
		Data obj = getOrCreate();
		obj.locale = locale;
	}

	private static Data getOrCreate() {
		Data obj = data.get();
		if(obj==null) {
			obj = new Data();
			data.set(obj);
		}
		return obj;
	}

	public static void clear() {
		data.remove();
	}
	
	private static class Data {
		private AuthUser authUser;
		private Locale locale;
		private VideoURLProducer videoURLProducer;
	}
	
	public static boolean canManageMinilessons() {
		return getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName()) || getAuthUser().getUploadPolicy()!=UploadPolicy.DENIED;
	}
}
