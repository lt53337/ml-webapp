package hr.fer.zemris.minilessons.service;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import hr.fer.zemris.minilessons.AuthDomain;
import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.dao.model.MLBasicPermission;
import hr.fer.zemris.minilessons.dao.model.UploadPolicy;

/**
 * This is implementation of authentication service for minilessons web-app. Method {@link #initialize()}
 * must be called prior to any other authentication-related methods.
 * 
 * @author marcupic
 *
 */
public class AuthenticationService {

	/**
	 * What is the default authentication domain (is user does not provide any).
	 */
	private static String defaultAuthDomain;
	/**
	 * Map of registered authentication domains.
	 */
	private static Map<String, AuthenticationServiceProvider> providers;
	/**
	 * Set of authentication domain names (keys).
	 */
	private static Set<String> authenticationDomains;
	/**
	 * Is this service initialized?
	 */
	private static boolean initialized;
	/**
	 * Is public registration of users enabled?
	 */
	private static boolean registrationEnabled;
	/**
	 * What is upload policy for newly registered users?
	 */
	private static UploadPolicy regUsersUploadPolicy;
	/**
	 * What are permissions of newly registered users?
	 */
	private static Set<String> regUsersPermissions;
	
	/**
	 * Private constructor. We do not want any instances of this class.
	 */
	private AuthenticationService() {
	}
	
	/**
	 * Getter for default domain.
	 * 
	 * @return default domain
	 * @throws RuntimeException if service was not initialized
	 */
	public static String getDefaultAuthDomain() {
		if(!initialized) throw new RuntimeException("Authentication service was not initialized!");
		return defaultAuthDomain;
	}

	/**
	 * Getter for authentication service provider for given authentication domain.
	 * 
	 * @param authDomain authentication domain
	 * @return authentication service provider or <code>null</code> if there is no provider for requested domain
	 * @throws RuntimeException if service was not initialized
	 */
	public static AuthenticationServiceProvider getProvider(String authDomain) {
		if(!initialized) throw new RuntimeException("Authentication service was not initialized!");
		return providers.get(authDomain);
	}
	
	/**
	 * Initializer. Must be explicitly called before any other authentication-related methods.
	 */
	public static void initialize() {
		if(initialized) return;
		initialized = true;
		
		String configKey = null;
		String defaultProvKey = null;
		Properties prop = new Properties();
		try {
			InputStream is = DAOProvider.class.getClassLoader().getResourceAsStream("ml-authentication.properties");
			if(is==null) {
				throw new IOException("File not found: ml-authentication.properties. Could not start authentication service.");
			}
			try {
				prop.load(is);
			} finally {
				is.close();
			}
			configKey = prop.getProperty("ml.auth.providers");
			if(configKey==null) {
				throw new RuntimeException("ml.auth.providers not found. Authentication service will not be started.");
			}
			defaultProvKey = prop.getProperty("ml.auth.default");
			if(defaultProvKey!=null) {
				defaultProvKey = defaultProvKey.trim();
			}
			if(defaultProvKey==null) {
				throw new RuntimeException("ml.auth.default not found. Authentication service will not be started.");
			}
		} catch(IOException ex) {
			throw new RuntimeException(ex);
		}

		if(!prop.containsKey("ml.auth.registration.enabled")) {
			throw new RuntimeException("ml.auth.registration.enabled not found. Authentication service will not be started.");
		}
		if("true".equals(prop.get("ml.auth.registration.enabled").toString().trim())) {
			registrationEnabled = true;
		} else if("false".equals(prop.get("ml.auth.registration.enabled").toString().trim())) {
			registrationEnabled = false;
		} else {
			throw new RuntimeException("ml.auth.registration.enabled is set to invalid value ("+prop.get("ml.auth.registration.enabled").toString()+"); only 'true' and 'false' are allowed. Authentication service will not be started.");
		}

		setRegUserUploadPolicy(prop, "ml.auth.registration.uploadPolicy");
		setRegUserPermissions(prop, "ml.auth.registration.permissions");
		
		Map<String, AuthenticationServiceProvider> provMap = new HashMap<>();
		
		String[] providerNames = configKey.split(" ");
		for(String providerName : providerNames) {
			if(providerName.isEmpty()) continue;
			String provStem = "ml.auth.provider."+providerName+".";
			int provStemLength = provStem.length();
			String provClassName = prop.getProperty(provStem+"className");
			if(provClassName!=null) provClassName = provClassName.trim();
			if(provClassName==null || provClassName.isEmpty()) {
				throw new RuntimeException("Class name for authentication provider " + providerName + " is not found. Authentication service will not be started.");
			}
			Set<String> keys = prop.keySet().stream().map(o->o.toString()).filter(s->s.startsWith(provStem)).collect(Collectors.toSet());
			Map<String,String> map = new HashMap<>();
			keys.forEach(k->{
				map.put(k.substring(provStemLength), prop.getProperty(k));
			});
			
			try {
				AuthenticationServiceProvider prov = (AuthenticationServiceProvider)AuthenticationService.class.getClassLoader().loadClass(provClassName).getConstructor(Map.class).newInstance(map);
				provMap.put(providerName, prov);
				if(defaultProvKey.equals(providerName)) {
					defaultAuthDomain = providerName;
				}
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException
					| ClassNotFoundException e) {
				throw new RuntimeException("Could not initialize authentication provider "+providerName+".", e);
			}
		}
		
		if(registrationEnabled && !provMap.containsKey(AuthDomain.PUBLIC.getKey())) {
			throw new RuntimeException("Since public registration is enabled, provider named '"+AuthDomain.PUBLIC.getKey()+"' must be configured, but it was not. Authentication service will not be started.");
		}
		
		if(!provMap.containsKey(AuthDomain.LOCAL.getKey())) {
			throw new RuntimeException("Provider named '"+AuthDomain.LOCAL.getKey()+"' must be configured, but it was not. Authentication service will not be started.");
		}

		if(provMap.isEmpty()) {
			throw new RuntimeException("No authentication service providers were specified. Authentication service will not be started.");
		}
		
		if(defaultAuthDomain==null) {
			throw new RuntimeException("Default authentication service provider was not among the loaded. Authentication service will not be started.");
		}
		
		AuthenticationService.providers = provMap;
		AuthenticationService.authenticationDomains = Collections.unmodifiableSet(new TreeSet<>(provMap.keySet()));
	}

	/**
	 * Helper method which reads and sets upload policy for newly registered users from properties object.
	 * @param prop configuration
	 * @param policyKey key name holding configuration parameter
	 */
	private static void setRegUserUploadPolicy(Properties prop, String policyKey) {
		String policyName = prop.getProperty(policyKey);
		if(policyName!=null) policyName = policyName.trim();
		if(policyName==null) {
			throw new RuntimeException(policyKey+" is not set. Authentication service will not be started.");
		}
		if(policyName.isEmpty()) {
			throw new RuntimeException(policyKey+" is not set. Authentication service will not be started.");
		}
		try {
			regUsersUploadPolicy = UploadPolicy.valueOf(policyName);
			System.out.println("Upload policy for newly registered users will be "+regUsersUploadPolicy+".");
		} catch(Exception ex) {
			throw new RuntimeException(policyKey+" is set to invalid value ("+policyName+"). Authentication service will not be started.");
		}
	}

	/**
	 * Helper method which reads and sets a set of permissions which will be given to newly registered users.
	 * @param prop configuration
	 * @param key key name holding configuration parameter
	 */
	private static void setRegUserPermissions(Properties prop, String key) {
		String permissionsStr = prop.getProperty(key);
		if(permissionsStr!=null) permissionsStr = permissionsStr.trim();
		if(permissionsStr==null) {
			throw new RuntimeException(key+" is not set. Authentication service will not be started.");
		}
		if(permissionsStr.isEmpty()) {
			regUsersPermissions = Collections.emptySet();
			System.out.println("Newly registered users will have following permisions: none.");
			return;
		}
		Set<MLBasicPermission> bpset = new LinkedHashSet<>();
		Set<String> permSet = new LinkedHashSet<>();
		for(String permName : permissionsStr.split(",")) {
			permName = permName.trim();
			if(permName.isEmpty()) continue;
			MLBasicPermission perm = null;
			try {
				perm = MLBasicPermission.valueOf(permName);
			} catch(Exception ex) {
				throw new RuntimeException(key+" is set to invalid value; '"+permName+"' is not valid permission name. Authentication service will not be started.");
			}
			bpset.add(perm);
			permSet.add(perm.getPermissonName());
		}
		regUsersPermissions = Collections.unmodifiableSet(permSet);
		System.out.println("Newly registered users will have following permisions: "+bpset+".");
	}

	/**
	 * Is public registration of new users enabled?
	 * 
	 * @return <code>true</code> if it is, <code>false</code> otherwise
	 */
	public static boolean isRegistrationEnabled() {
		return registrationEnabled;
	}

	/**
	 * Getter for default policy for newly registered users.
	 * 
	 * @return upload policy
	 */
	public static UploadPolicy getRegUsersUploadPolicy() {
		return regUsersUploadPolicy;
	}

	/**
	 * Getter for permission set for newly registered users.
	 * 
	 * @return set of permissions
	 */
	public static Set<String> getRegUsersPermissions() {
		return regUsersPermissions;
	}
	
	/**
	 * Getter for set of authentication domains.
	 * 
	 * @return set
	 */
	public static Set<String> getAuthenticationDomains() {
		return authenticationDomains;
	}
}
