package hr.fer.zemris.minilessons.service;

import java.util.Objects;

/**
 * This exception can be called from controller to signal the framework that its method
 * should be called again, but after the locking has been performed.
 *
 * @author marcupic
 */
public class CallAfterLockException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	/**
	 * Path to use for locking.
	 */
	private String[] lockPath;

	/**
	 * Constructor.
	 * 
	 * @param lockPath path to use for locking
	 */
	public CallAfterLockException(String[] lockPath) {
		super();
		this.lockPath = Objects.requireNonNull(lockPath, "Locking path must not be null!");
	}
	
	/**
	 * Getter for path which must be used for locking.
	 * 
	 * @return path
	 */
	public String[] getLockPath() {
		return lockPath;
	}
}
