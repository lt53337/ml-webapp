/**
 * 
 */
package hr.fer.zemris.minilessons.service;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target(METHOD)
/**
 * Označava da je metodu legalno pozvati samo ako trenutno postoji autentificirani korisnik.
 * U tom slučaju, informacije o korisniku bit će dostupne u kontekstu {@link MLCurrentContext}.
 * 
 * @author marcupic
 */
public @interface Authenticated {
	/**
	 * If URL is intercepted and suspended pending user's authentication, can user be automatically
	 * redirected to intercepted URL after successful authentication? Default is <code>false</code>.
	 * 
	 * @return <code>true</code> if redirection is allowed, <code>false</code> otherwise
	 */
	public boolean redirectAllowed() default false;
	/**
	 * <p>If set to <code>true</code>, then authentication is not hard prerequisite. Instead, if user
	 * is authenticated, {@link AuthUser} object will be loaded and made available. If user is not
	 * authenticated, nothing will be loaded.</p>
	 * 
	 * <p>If this option is set to <code>false</code> (which is default), then request must be authenticated.
	 * If it is not, user will be redirected to login page.</p>
	 * 
	 * @return <code>true</code> if optional, <code>false</code> otherwise
	 */
	public boolean optional() default false;
	/**
	 * If set to <code>true</code>, an attempt to authenticate user should be made (this is default);
	 * e.g. by displaying login-page to user.
	 * If set to <code>false</code>, appropriate error status will be sent to user.
	 * 
	 * @return <code>true</code> if login attempt is allowed, <code>false</code> to immediately send error code to user
	 */
	public boolean attemptLogin() default true;
}
