package hr.fer.zemris.minilessons.util;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import org.apache.commons.codec.binary.Hex;

/**
 * Helper utility for working with passwords.
 * 
 * @author marcupic
 *
 */
public class PasswordUtil {

	/**
	 * Calculate a SHA-256 hash of given password and returns hex-encoded hash as string.
	 * @param password
	 * @return
	 */
	public static String encodePassword(String password) {
		if(password==null) {
			password = "";
		}
		MessageDigest md = null;
		
		try {
			md = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("Could not obtain SHA-256 digester.", e);
		}
		
		md.update(password.getBytes(StandardCharsets.UTF_8));
		return Hex.encodeHexString(md.digest());
	}

	/**
	 * Letters which can be present in password.
	 */
	private final static String letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,_/()*=+-"; 
	
	/**
	 * Generates new random password.
	 * 
	 * @return generated password
	 */
	public static String generateRandomPassword() {
		Random rnd = new Random();
		int n = 15 + rnd.nextInt(10);
		char[] data = new char[n];
		int llen = letters.length();
		for(int i = 0; i < n; i++) {
			data[i] = letters.charAt(rnd.nextInt(llen));
		}
		String pwd = new String(data);
		return pwd;
	}
}