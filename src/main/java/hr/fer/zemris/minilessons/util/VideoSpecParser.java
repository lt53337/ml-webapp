package hr.fer.zemris.minilessons.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * <p>Parser of video tag specification. Here is example of supported format and parsers usage.</p>
 * 
 * <pre><code>String text = "width=320|height=240|poster=movie.jpg|src=movie.mp4#type=video/mp4|src=movie.ogg#type=video/ogg|subtitles=movie.en.vtt#language=en#label=English";
 * 
 * VideoSpec vs = VideoSpecParser.parse(text);</code></pre>
 * 
 * <p>More detailed specification will (hopefully) be written at some later time.</p>
 * 
 * @author marcupic
 */
public class VideoSpecParser {

	/**
	 * Parse given video specification and return {@link VideoSpec} object.
	 * 
	 * @param text video specification
	 * @return object representing given specification; in case of errors, {@link RuntimeException} will be thrown
	 */
	public static VideoSpec parse(String text) {
		String[] elems = text.split("\\|");
		if(elems.length < 2) throw new RuntimeException("Invalid video specification.");
		for(int i = 0; i < elems.length; i++) {
			elems[i] = elems[i].trim();
		}
		if(!elems[0].startsWith("width=") || !elems[1].startsWith("height=")) {
			throw new RuntimeException("Invalid video specification. First two attributes must be width and height, in that order.");
		}

		int w = 0;
		int h = 0;
		List<VideoSrcSpec> sources = new ArrayList<>();
		List<VideoSubtitleSpec> subtitles = new ArrayList<>();
		String poster = null;
		w = Integer.parseInt(elems[0].substring(6));
		h = Integer.parseInt(elems[1].substring(7));
		
		int start = 2;
		if(start < elems.length && elems[start].startsWith("poster=")) {
			poster = elems[start].substring(7);
			start++;
		}
		
		for(int i = start; i < elems.length; i++) {
			if(elems[i].isEmpty()) continue;
			String[] elems2 = elems[i].split("#");
			KeyValues kvs = KeyValues.fromStringArray(elems2);
			if(kvs.hasKey("src") && kvs.hasKey("type")) {
				if(kvs.size()!=2) throw new RuntimeException("Invalid video specification: "+elems[i]+".");
				sources.add(new VideoSrcSpec(kvs.forKey("src").value, kvs.forKey("type").value));
				continue;
			}
			if(kvs.hasKey("subtitles")) {
				int expectedLen = 1;
				if(kvs.hasKey("language")) expectedLen++;
				if(kvs.hasKey("label")) expectedLen++;
				if(kvs.size() != expectedLen) throw new RuntimeException("Invalid video specification: "+elems[i]+".");
				KeyValue lang = kvs.forKey("language");
				KeyValue label = kvs.forKey("label");
				subtitles.add(new VideoSubtitleSpec(kvs.forKey("subtitles").value, lang.value, label.value));
				continue;
			}
			throw new  RuntimeException("Invalid video specification: "+elems[i]+".");
		}
		
		if(sources.isEmpty()) {
			throw new RuntimeException("Invalid video specification: '" + text + "'. No sources were given.");
		}
		
		return new VideoSpec(w, h, poster, sources, subtitles);
	}

	/**
	 * An iterable implemenation of KeyValue collection.
	 * 
	 * @author marcupic
	 *
	 */
	private static class KeyValues implements Iterable<KeyValue> {
		
		/**
		 * Array for key-value storage.
		 */
		private KeyValue[] pairs;
		
		/**
		 * Constructor. It is expected that each string is of form key=value; spaces will be trimmed from
		 * key and value. If format is not followed, {@link RuntimeException} will be thrown.
		 * 
		 * @param elems an array of string representation of key-value pairs; must not be <code>null</code>
		 * @return key-values collection
		 */
		private static KeyValues fromStringArray(String[] elems) {
			KeyValues kvs = new KeyValues();
			kvs.pairs = new KeyValue[elems.length];
			for(int i = 0; i < elems.length; i++) {
				int pos = elems[i].indexOf('=');
				if(pos==-1) {
					throw new RuntimeException("KeyValue specification is invalid: ["+elems[i]+"].");
				}
				kvs.pairs[i] = new KeyValue(elems[i].substring(0, pos).trim(), elems[i].substring(pos+1).trim());
			}
			return kvs;
		}

		/**
		 * Finds and returns {@link KeyValue} for given <code>key</code>.
		 * 
		 * @param key key which is being searched
		 * @return KeyValue for specified key or <code>null</code> if such one is not present
		 */
		private KeyValue forKey(String key) {
			for(KeyValue kv : pairs) {
				if(kv.key.equals(key)) return kv;
			}
			return null;
		}

		/**
		 * Checks if {@link KeyValue} for given <code>key</code> exists in this collection. 
		 * @param key key to look for
		 * @return <code>true</code> if such {@link KeyValue} exists in this collection, <code>false</code> otherwise
		 */
		public boolean hasKey(String key) {
			return forKey(key)!=null;
		}
		
		/**
		 * Getter for number of key-value pairs in this collection.
		 * @return collection size
		 */
		public int size() {
			return pairs.length;
		}
		
		@Override
		public Iterator<KeyValue> iterator() {
			return new Iterator<VideoSpecParser.KeyValue>() {
				int current = 0;
				@Override
				public boolean hasNext() {
					return current < pairs.length;
				}
				@Override
				public KeyValue next() {
					if(!hasNext()) throw new NoSuchElementException();
					return pairs[current++];
				}
			};
		}
	}
	
	/**
	 * Private class modelling key-value pair.
	 * 
	 * @author marcupic
	 *
	 */
	private static class KeyValue {
		/**
		 * Key.
		 */
		private String key;
		/**
		 * Value.
		 */
		private String value;
		/**
		 * Constructor.
		 * 
		 * @param key key
		 * @param value value
		 */
		public KeyValue(String key, String value) {
			super();
			this.key = key;
			this.value = value;
		}
	}
}
