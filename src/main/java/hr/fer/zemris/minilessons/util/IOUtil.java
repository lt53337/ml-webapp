package hr.fer.zemris.minilessons.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * A utility class for IO operations.
 * 
 * @author marcupic
 */
public class IOUtil {

	/**
	 * Copy content of input stream into output stream. If <code>limit</code> is non-negative,
	 * only that many bytes are read from input stream and copied into output stream. If 
	 * <code>limit</code> is negative, all available data from input stream are read and
	 * copied (until EOF). Both streams will remain open after this method is done.
	 * 
	 * @param is input stream with data
	 * @param os output stream in which data must be copied
	 * @param limit optional limit on number od bytes to read and copy; can be -1
	 * @throws IOException in case of errors
	 */
	public static void streamCopy(InputStream is, OutputStream os, long limit) throws IOException {
		byte[] buf = new byte[4096];
		long sent = 0;
		while(true) {
			int toRead = buf.length;
			if(limit>=0) {
				toRead = (int)Math.min(toRead, limit-sent);
				if(toRead<=0) break;
			}
			int r = is.read(buf, 0, toRead);
			if(r<1) break;
			os.write(buf, 0, r);
			sent += r;
		}
	}
}
