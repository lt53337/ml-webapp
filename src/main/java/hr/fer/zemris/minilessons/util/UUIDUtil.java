package hr.fer.zemris.minilessons.util;

import java.util.UUID;

/**
 * Helper class for UUID generation.
 * 
 * @author marcupic
 *
 */
public class UUIDUtil {

	/**
	 * Generate new random-based UUID and return its string
	 * encoding. All dashes are deleted.
	 * 
	 * @return new uuid
	 * @see UUID
	 */
	public static String nextUUID() {
		UUID uuid = UUID.randomUUID();
		return uuid.toString().replace("-", "");
	}
}
