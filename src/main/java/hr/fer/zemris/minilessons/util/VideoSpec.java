package hr.fer.zemris.minilessons.util;

import java.util.List;

/**
 * This class represents a single video specification with multiple formats and subtitles.
 * 
 * @author marcupic
 *
 */
public class VideoSpec {
	/**
	 * Video image width.
	 */
	private int width;
	/**
	 * Video image height.
	 */
	private int height;
	/**
	 * Poster file to use for video. Will be interpreted as relative to same folder in which
	 * video files are stored.
	 */
	private String posterFileName;
	/**
	 * List of video-source specifications.
	 */
	private List<VideoSrcSpec> sources;
	/**
	 * List of video-subtitles specifications.
	 */
	private List<VideoSubtitleSpec> subtitles;

	/**
	 * Constructor.
	 * 
	 * @param width width of video image
	 * @param height height of video image
	 * @param posterFileName path to file with poster
	 * @param sources video-sources
	 * @param subtitles video-subtitles
	 */
	public VideoSpec(int width, int height, String posterFileName, List<VideoSrcSpec> sources,
			List<VideoSubtitleSpec> subtitles) {
		super();
		this.width = width;
		this.height = height;
		this.posterFileName = posterFileName;
		this.sources = sources;
		this.subtitles = subtitles;
	}
	
	/**
	 * Getter for video image width.
	 * @return width
	 */
	public int getWidth() {
		return width;
	}
	
	/**
	 * Getter for video image height.
	 * @return height
	 */
	public int getHeight() {
		return height;
	}
	
	/**
	 * Getter for video poster file path.
	 * @return path to poster file
	 */
	public String getPosterFileName() {
		return posterFileName;
	}
	
	/**
	 * Getter for list of video-source specifications.
	 * @return video sources
	 */
	public List<VideoSrcSpec> getSources() {
		return sources;
	}
	
	/**
	 * Getter for list of subtitles. 
	 * @return subtitles
	 */
	public List<VideoSubtitleSpec> getSubtitles() {
		return subtitles;
	}
}
