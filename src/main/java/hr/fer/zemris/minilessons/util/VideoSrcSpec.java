package hr.fer.zemris.minilessons.util;

/**
 * This class represents a single video source. Video-source is specified by
 * filename and mime-type from which a format can be determined.
 * 
 * @author marcupic
 */
public class VideoSrcSpec {

	/**
	 * File name of video file.
	 */
	private String fileName;
	/**
	 * Mime-type of video.
	 */
	private String type;
	
	/**
	 * Constructor.
	 * 
	 * @param fileName file name
	 * @param type mime-type
	 */
	public VideoSrcSpec(String fileName, String type) {
		super();
		this.fileName = fileName;
		this.type = type;
	}
	
	/**
	 * Getter for filename.
	 * 
	 * @return filename
	 */
	public String getFileName() {
		return fileName;
	}
	
	/**
	 * Getter for mime-type.
	 * 
	 * @return mime-type
	 */
	public String getType() {
		return type;
	}
}
