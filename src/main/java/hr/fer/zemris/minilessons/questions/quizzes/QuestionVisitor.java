package hr.fer.zemris.minilessons.questions.quizzes;

public interface QuestionVisitor {
	void visit(AbcSingleQuestion question);
	void visit(TextQuestion question);
}
