package hr.fer.zemris.minilessons.questions.quizzes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import hr.fer.zemris.minilessons.localization.I18NText;
import hr.fer.zemris.minilessons.localization.I18NTexts;

public class TextQuestionGroup extends QuestionGroupBase {

	private List<TextQuestion> questions;
	
	public TextQuestionGroup(String publicID, int version, int minorVersion, Set<String> supportedLanguages, I18NTexts title, List<TextQuestion> questions, Set<I18NText> tags) {
		super(publicID, version, minorVersion, supportedLanguages, title, tags);
		this.questions = Collections.unmodifiableList(new ArrayList<>(questions));
	}

	@Override
	public int size() {
		return questions.size();
	}
	
	@Override
	public TextQuestion getQuestion(int index) {
		return questions.get(index);
	}
	
	@Override
	public void accept(QuestionGroupVisitor v) {
		v.visit(this);
	}
	
	@Override
	public SupportedXMLQuestions getQuestionType() {
		return SupportedXMLQuestions.TEXTUAL;
	}
	
	@Override
	public TextQuestion getForID(String id) {
		for(TextQuestion q : questions) {
			if(q.getId().equals(id)) return q;
		}
		return null;
	}

}
