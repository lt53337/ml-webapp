package hr.fer.zemris.minilessons.questions.quizzes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import hr.fer.zemris.minilessons.localization.I18NTexts;

public class AbcSingleQuestion extends AbstractQuestion {

	private I18NTexts text;
	private List<AbcSingleAnswer> answers;
	
	public AbcSingleQuestion(String id, I18NTexts text, List<AbcSingleAnswer> answers) {
		super(id);
		if(answers==null || answers.isEmpty() || answers.size()<2) {
			throw new IllegalArgumentException("AbcSingleQuestion answers must be nonempty collection (it must have at least two answers: one incorrect and one correct).");
		}
		long num = answers.stream().filter(a->a.isCorrect()).count();
		if(num > 1) {
			throw new IllegalArgumentException("AbcSingleQuestion answers can not have more than one correct answer.");
		}
		if(num == 0) {
			throw new IllegalArgumentException("AbcSingleQuestion answers must have one correct answer (non was given).");
		}
		this.text = Objects.requireNonNull(text);
		this.answers = Collections.unmodifiableList(new ArrayList<>(answers));
	}

	public List<AbcSingleAnswer> getAnswers() {
		return answers;
	}
	
	public I18NTexts getText() {
		return text;
	}
	
	@Override
	public void accept(QuestionVisitor v) {
		v.visit(this);
	}
}
