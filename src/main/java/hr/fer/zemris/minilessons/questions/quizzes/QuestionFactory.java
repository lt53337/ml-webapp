package hr.fer.zemris.minilessons.questions.quizzes;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import hr.fer.zemris.minilessons.localization.I18NText;
import hr.fer.zemris.minilessons.localization.I18NTexts;
import hr.fer.zemris.minilessons.service.QDeployer;

public class QuestionFactory {

	private static final String JAXP_SCHEMA_LANGUAGE = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
	private static final String JAXP_SCHEMA_SOURCE = "http://java.sun.com/xml/jaxp/properties/schemaSource";
	private static final String W3C_XML_SCHEMA = "http://www.w3.org/2001/XMLSchema";
	
	private static class MyErrorHandler implements ErrorHandler {
		private List<String> messages = new ArrayList<>();
		
		private String getParseExceptionInfo(SAXParseException spe) {
	        String systemId = spe.getSystemId();
	        if (systemId == null) {
	            systemId = "null";
	        }

	        String info = "URI=" + systemId + " Line=" + spe.getLineNumber() +
	                      ": " + spe.getMessage();
	        return info;
	    }
		@Override
		public void warning(SAXParseException exception) throws SAXException {
			messages.add("Warning: " + getParseExceptionInfo(exception));
		}
		@Override
		public void error(SAXParseException exception) throws SAXException {
			messages.add("Error: " + getParseExceptionInfo(exception));
		}
		@Override
		public void fatalError(SAXParseException exception) throws SAXException {
			messages.add("Fatal: " + getParseExceptionInfo(exception));
		}
		public List<String> getMessages() {
			return messages;
		}
	}

	private static class MyEntityResolver implements EntityResolver {
		@Override
		public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
			if(systemId != null && systemId.endsWith("/common-types.xsd")) {
				return new InputSource(QuestionFactory.class.getResourceAsStream("common-types.xsd"));
			}
			return null;
		}
	}
	
	public static QuestionGroupBase fromText(String text) {
		return fromReader(new StringReader(text));
	}
	
	public static QuestionGroupBase fromReader(Reader reader) {
		return fromInputSource(new InputSource(reader));
	}
	
	private static QuestionGroupBase fromInputSource(InputSource src) {
		MyErrorHandler errorHandler = new MyErrorHandler();
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
			dbf.setValidating(true);
			dbf.setCoalescing(true);
			dbf.setExpandEntityReferences(true);
			dbf.setIgnoringComments(true);
			dbf.setIgnoringElementContentWhitespace(true);
			dbf.setAttribute(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA);
			dbf.setAttribute(JAXP_SCHEMA_SOURCE, new String[] {"common-types.xsd"});
			
			DocumentBuilder db = dbf.newDocumentBuilder();
			db.setErrorHandler(errorHandler);
			db.setEntityResolver(new MyEntityResolver());
			Document doc = db.parse(src);
			QuestionGroupBase qgb = build(doc.getFirstChild());
			if(errorHandler.getMessages().isEmpty()) {
				return qgb;
			}
		} catch(Exception ex) {
			throw new RuntimeException("Could not parse question group. " + ex.getMessage(), ex);
		}
		throw new RuntimeException("Could not parse question group. " + errorHandler.getMessages());
	}

	private static QuestionGroupBase build(Node root) {
		String publicID = root.getAttributes().getNamedItem("uid").getNodeValue();
		int version = Integer.parseInt(root.getAttributes().getNamedItem("version").getNodeValue());
		int minorVersion = Integer.parseInt(root.getAttributes().getNamedItem("minorVersion").getNodeValue());
		QDeployer.checkPublicID(publicID);
		
		NodeList children = root.getChildNodes();
		Set<String> languages = buildLanguages(children.item(0));
		I18NTexts title = buildText(children.item(1), languages);
		int startIndex = 2;
		Set<I18NText> tags = null;
		if(children.getLength()>=3 && children.item(2).getNodeName().equals("tags")) {
			tags = buildTags(children.item(2),languages);
			startIndex++;
		}
		if(root.getNodeName().equals("abc-single")) {
			List<AbcSingleQuestion> questions = new ArrayList<>();
			for(int i = startIndex, n = children.getLength(); i < n; i++) {
				Node qNode = children.item(i);
				questions.add(buildAbcSingleQuestion(qNode, languages));
			}			
			return new AbcSingleQuestionGroup(publicID, version, minorVersion, languages, title, questions, tags);
		} else if(root.getNodeName().equals("textual")) {
			List<TextQuestion> questions = new ArrayList<>();
			for(int i = startIndex, n = children.getLength(); i < n; i++) {
				Node qNode = children.item(i);
				questions.add(buildTextQuestion(qNode, languages));
			}			
			return new TextQuestionGroup(publicID, version, minorVersion, languages, title, questions, tags);
		}
		throw new RuntimeException("Unexpected tag found: " + root.getNodeName());
	}

	private static AbcSingleQuestion buildAbcSingleQuestion(Node qNode, Set<String> languages) {
		String id = qNode.getAttributes().getNamedItem("qid").getNodeValue();
		NodeList children = qNode.getChildNodes();
		I18NTexts text = buildText(children.item(0), languages);
		List<AbcSingleAnswer> answers = new ArrayList<>();
		for(int i = 1, n = children.getLength(); i < n; i++) {
			Node aNode = children.item(i);
			AbcSingleAnswer answer = buildAbcSingleAnswer(aNode, languages);
			answers.add(answer);
		}
		
		return new AbcSingleQuestion(id, text, answers);
	}

	private static TextQuestion buildTextQuestion(Node qNode, Set<String> languages) {
		String id = qNode.getAttributes().getNamedItem("qid").getNodeValue();
		NodeList children = qNode.getChildNodes();
		I18NTexts text = buildText(children.item(0), languages);
		List<TextAnswer> answers = new ArrayList<>();
		for(int i = 1, n = children.getLength(); i < n; i++) {
			Node aNode = children.item(i);
			TextAnswer answer = buildTextAnswer(aNode, languages);
			answers.add(answer);
		}		
		return new TextQuestion(id, text, answers);
	}

	private static AbcSingleAnswer buildAbcSingleAnswer(Node node, Set<String> languages) {
		String correct = node.getAttributes().getNamedItem("correct").getNodeValue();
		I18NTexts translations = buildText(node, languages);
		return new AbcSingleAnswer(correct.equals("true"), translations);
	}

	private static TextAnswer buildTextAnswer(Node node, Set<String> languages) {
		String caseSensitive = node.getAttributes().getNamedItem("case-sensitive").getNodeValue();
		I18NTexts translations = buildText(node, languages);
		return new TextAnswer(caseSensitive.equals("true"), translations);
	}

	private static I18NTexts buildText(Node item, Set<String> languages) {
		NodeList children = item.getChildNodes();
		Set<String> foundLangs = new HashSet<>();
		Map<String,I18NText> translations = new LinkedHashMap<>();
		
		for(int i = 0, n = children.getLength(); i < n; i++) {
			Node strNode = children.item(i);
			Node langAttr = strNode.getAttributes().getNamedItem("lang");
			if(langAttr==null) throw new RuntimeException("Tag str missing lang attribute.");
			String lang = langAttr.getNodeValue();
			if(!languages.contains(lang)) {
				throw new RuntimeException("Found lang="+lang+" which is not in declared supported languages.");
			}
			if(translations.containsKey(lang)) {
				throw new RuntimeException("Found multiple translations for lang="+lang+".");
			}
			foundLangs.add(lang);
			String text = strNode.getFirstChild().getNodeValue();
			translations.put(lang, new I18NText(text, lang));
		}
		
		if(!languages.equals(foundLangs)) {
			throw new RuntimeException("Set of declared supported languages "+languages+" and provided translations "+foundLangs+" are mismatched.");
		}
		return new I18NTexts(translations);
	}

	private static Set<I18NText> buildTags(Node item, Set<String> languages) {
		NodeList children = item.getChildNodes();
		Set<I18NText> tags = new LinkedHashSet<>();
		for(int i = 0, n = children.getLength(); i < n; i++) {
			Node strNode = children.item(i);
			Node langAttr = strNode.getAttributes().getNamedItem("lang");
			if(langAttr==null) throw new RuntimeException("Tag str missing lang attribute.");
			String lang = langAttr.getNodeValue();
			if(!languages.contains(lang)) {
				throw new RuntimeException("Found lang="+lang+" which is not in declared supported languages.");
			}
			String text = strNode.getFirstChild().getNodeValue();
			if(!tags.add(new I18NText(text, lang))) {
				throw new RuntimeException("Found duplicate tag.");
			}
		}
		return tags;
	}

	private static Set<String> buildLanguages(Node item) {
		Set<String> set = new LinkedHashSet<>();
		if(!item.getNodeName().equals("supported-languages")) throw new RuntimeException("supported-languages expected.");
		NodeList children = item.getChildNodes();
		for(int i = 0, n = children.getLength(); i < n; i++) {
			Node langNode = children.item(i);
			Node langAttr = langNode.getAttributes().getNamedItem("name");
			if(langAttr==null) throw new RuntimeException("Tag language missing name attribute.");
			set.add(langAttr.getNodeValue());
		}
		return set;
	}
}
