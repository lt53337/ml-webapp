package hr.fer.zemris.minilessons.questions.quizzes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import hr.fer.zemris.minilessons.localization.I18NText;
import hr.fer.zemris.minilessons.localization.I18NTexts;

public class AbcSingleQuestionGroup extends QuestionGroupBase {

	private List<AbcSingleQuestion> questions;
	
	public AbcSingleQuestionGroup(String publicID, int version, int minorVersion, Set<String> supportedLanguages, I18NTexts title, List<AbcSingleQuestion> questions, Set<I18NText> tags) {
		super(publicID, version, minorVersion, supportedLanguages, title, tags);
		this.questions = Collections.unmodifiableList(new ArrayList<>(questions));
	}

	@Override
	public int size() {
		return questions.size();
	}
	
	@Override
	public AbcSingleQuestion getQuestion(int index) {
		return questions.get(index);
	}
	
	@Override
	public void accept(QuestionGroupVisitor v) {
		v.visit(this);
	}
	
	@Override
	public SupportedXMLQuestions getQuestionType() {
		return SupportedXMLQuestions.ABCSINGLE;
	}
	
	@Override
	public AbcSingleQuestion getForID(String id) {
		for(AbcSingleQuestion q : questions) {
			if(q.getId().equals(id)) return q;
		}
		return null;
	}
}
