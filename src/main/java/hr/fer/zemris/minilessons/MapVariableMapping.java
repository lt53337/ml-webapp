package hr.fer.zemris.minilessons;

import java.util.Map;

public class MapVariableMapping implements VariableMapping {

	private Map<String,?> map;
	
	public MapVariableMapping(Map<String,?> map) {
		this.map = map;
	}

	@Override
	public boolean containsKey(String key) {
		return map.containsKey(key);
	}

	@Override
	public Object get(String key) {
		return map.get(key);
	}

}
