package hr.fer.zemris.minilessons.dao.model;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="jsqinstances")
@Cacheable(false)
public class JSQuestionInstance extends CommonQuestionInstance {

	@ManyToOne(optional=false,fetch=FetchType.LAZY)
	private JSQuestionDefinition question;
	
	@Lob
	@Column(length=1024*32,nullable=false)
	private String storedState;

	
	public JSQuestionInstance() {
	}

	public JSQuestionDefinition getQuestion() {
		return question;
	}
	public void setQuestion(JSQuestionDefinition question) {
		this.question = question;
	}

	public String getStoredState() {
		return storedState;
	}
	public void setStoredState(String storedState) {
		this.storedState = storedState;
	}
	
}
