package hr.fer.zemris.minilessons.dao.model;

/**
 * Enumeration of text formats whose rendering is 
 * supported by this system. 
 * 
 * @author marcupic
 */
public enum SupportedTextFormat {
	MARKDOWN("markdown");
	
	private String formatName;
	
	SupportedTextFormat(String formatName) {
		this.formatName = formatName;
	}
	
	public String getFormatName() {
		return formatName;
	}
}
