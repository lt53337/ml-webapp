package hr.fer.zemris.minilessons.dao.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

@Entity
@Table(name="wish")
public class Wish {

	/**
	 * ID.
	 */
	@Id @GeneratedValue
	private Long id;
	
	/**
	 * Area: for example, course name.
	 */
	@Column(length=100, nullable=false)
	private String area;

	/**
	 * Topic: for example, some topic which is covered by course. 
	 */
	@Column(length=100, nullable=false)
	private String topic;

	/**
	 * Detailed description of wish.
	 */
	@Column(length=1024*10, nullable=false)
	private String description;
	
	/**
	 * When was this wish created.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private Date openedAt;

	/**
	 * Who created it?
	 */
	@ManyToOne
	@JoinColumn(nullable=false)
	private User openedBy;

	@ManyToOne
	private User statusChangedBy;

	@Temporal(TemporalType.TIMESTAMP)
	private Date statusChangedOn;
	
	/**
	 * What minilessons were created as a result of this wish? Or is wish is for something which already exists, which minilessons can be consulted?
	 */
	@ManyToMany(fetch=FetchType.LAZY)
	private Set<Minilesson> minilessons = new HashSet<>();
	
	/**
	 * If this wish is duplicate of another, which one?
	 */
	@ManyToOne
	private Wish duplicateOf;
	
	/**
	 * When status was changed (e.g. to WONTIMPL), here will be additional explanation.
	 */
	@Column(length=2048)
	private String statusText;
	
	/**
	 * What is status of this wish.
	 */
	@Enumerated(EnumType.STRING)
	@Column(nullable=false,length=10)
	private WishStatus status;
	
	/**
	 * Optimistic lock version.
	 */
	@Version
	private long olversion;
	
	/**
	 * Number of positive votes.
	 */
	private int positiveVotes;
	
	/**
	 * Number of negative votes.
	 */
	private int negativeVotes;
	
	public Wish() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getOpenedAt() {
		return openedAt;
	}

	public void setOpenedAt(Date openedAt) {
		this.openedAt = openedAt;
	}

	public User getOpenedBy() {
		return openedBy;
	}

	public void setOpenedBy(User openedBy) {
		this.openedBy = openedBy;
	}

	public User getStatusChangedBy() {
		return statusChangedBy;
	}

	public void setStatusChangedBy(User statusChangedBy) {
		this.statusChangedBy = statusChangedBy;
	}

	public Date getStatusChangedOn() {
		return statusChangedOn;
	}

	public void setStatusChangedOn(Date statusChangedOn) {
		this.statusChangedOn = statusChangedOn;
	}

	public Set<Minilesson> getMinilessons() {
		return minilessons;
	}

	public void setMinilessons(Set<Minilesson> minilessons) {
		this.minilessons = minilessons;
	}

	public Wish getDuplicateOf() {
		return duplicateOf;
	}

	public void setDuplicateOf(Wish duplicateOf) {
		this.duplicateOf = duplicateOf;
	}

	public String getStatusText() {
		return statusText;
	}

	public void setStatusText(String statusText) {
		this.statusText = statusText;
	}

	public long getOlversion() {
		return olversion;
	}
	
	public void setOlversion(long olversion) {
		this.olversion = olversion;
	}
	
	public WishStatus getStatus() {
		return status;
	}
	
	public void setStatus(WishStatus status) {
		this.status = status;
	}

	public int getPositiveVotes() {
		return positiveVotes;
	}
	
	public void setPositiveVotes(int positiveVotes) {
		this.positiveVotes = positiveVotes;
	}
	
	public int getNegativeVotes() {
		return negativeVotes;
	}
	
	public void setNegativeVotes(int negativeVotes) {
		this.negativeVotes = negativeVotes;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Wish))
			return false;
		Wish other = (Wish) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
}
