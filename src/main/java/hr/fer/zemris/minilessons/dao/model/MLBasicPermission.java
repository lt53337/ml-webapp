package hr.fer.zemris.minilessons.dao.model;

/**
 * Basic permissions.
 * 
 * @author marcupic
 *
 */
public enum MLBasicPermission {
	/**
	 * User can perform all administration tasks.
	 */
	ADMINISTRATION("admin"),
	/**
	 * User can review and approve minilessons.
	 */
	CAN_APPROVE_MINILESSONS("can-approve"),
	/**
	 * User can manage wish list (mark wishes as closed,
	 * mark wishes as duplicates, etc.)
	 */
	CAN_MANAGE_WISH_LIST("manage-wish-list"),
	/**
	 * User can add new wish on wish list.
	 */
	CAN_ADD_ON_WISH_LIST("add-on-wish-list"),
	/**
	 * User can grade minilesson.
	 */
	CAN_GRADE_MINILESSONS("can-grade"),
	/**
	 * User can vote for existing wishes.
	 */
	CAN_VOTE_FOR_WISH("can-vote-wish"),
	/**
	 * User can write comments on minilesson items
	 */
	CAN_COMMENT_MLITEMS("can-comment-ml"),
	/**
	 * User can write comments on minilesson items
	 */
	CAN_CREATE_WORKSPACE("can-create-ws")
	;
	
	/**
	 * Permission name.
	 */
	private final String permissonName;
	
	MLBasicPermission(String permissonName) {
		this.permissonName = permissonName;
	}

	public String getPermissonName() {
		return permissonName;
	}
}
