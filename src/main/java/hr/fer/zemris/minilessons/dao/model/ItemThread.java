package hr.fer.zemris.minilessons.dao.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

@Entity
@Table(name="item_threads",indexes={
	@Index(columnList="minilesson_id,item", unique=false)
})
public class ItemThread {

	/**
	 * ID.
	 */
	@Id @GeneratedValue
	private Long id;
	
	/**
	 * User who posted this comment.
	 */
	@ManyToOne
	@JoinColumn(name="owner_id", nullable=false)
	private User owner;
	
	/**
	 * What minilesson this comment belongs to.
	 */
	@ManyToOne
	@JoinColumn(name="minilesson_id", nullable=false)
	private Minilesson minilesson;

	/**
	 * For which item is this comment?
	 */
	@Column(length=10,nullable=false)
	private String item;
	
	/**
	 * When was this comment posted?
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private Date postedOn;

	/**
	 * Status of this conversation thread.
	 */
	@Enumerated(EnumType.STRING)
	@Column(nullable=false,length=10)
	private ItemThreadStatus status;

	/**
	 * Which user made last status change?
	 * Can be <code>null</code> for newly
	 * created threads.
	 */
	@ManyToOne
	@JoinColumn
	private User statusSetBy;
	
	/**
	 * When was last status change made?
	 * Can be <code>null</code> for newly
	 * created threads.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	private Date statusChangedOn;

	/**
	 * Optimistic-lock version.
	 */
	@Version
	private long olversion;

	/**
	 * Title for this conversation.
	 */
	@Column(length=200,nullable=false)
	private String title;
	
	/**
	 * Is this conversation public?
	 */
	private boolean conversationPublic;
	
	public ItemThread() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public Minilesson getMinilesson() {
		return minilesson;
	}

	public void setMinilesson(Minilesson minilesson) {
		this.minilesson = minilesson;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public Date getPostedOn() {
		return postedOn;
	}

	public void setPostedOn(Date postedOn) {
		this.postedOn = postedOn;
	}

	public ItemThreadStatus getStatus() {
		return status;
	}

	public void setStatus(ItemThreadStatus status) {
		this.status = status;
	}

	public User getStatusSetBy() {
		return statusSetBy;
	}

	public void setStatusSetBy(User statusSetBy) {
		this.statusSetBy = statusSetBy;
	}

	public Date getStatusChangedOn() {
		return statusChangedOn;
	}

	public void setStatusChangedOn(Date statusChangedOn) {
		this.statusChangedOn = statusChangedOn;
	}

	public long getOlversion() {
		return olversion;
	}

	public void setOlversion(long olversion) {
		this.olversion = olversion;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isConversationPublic() {
		return conversationPublic;
	}

	public void setConversationPublic(boolean conversationPublic) {
		this.conversationPublic = conversationPublic;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ItemThread))
			return false;
		ItemThread other = (ItemThread) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
