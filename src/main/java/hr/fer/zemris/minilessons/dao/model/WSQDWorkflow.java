package hr.fer.zemris.minilessons.dao.model;

public enum WSQDWorkflow {

	WF1("UNINITIALIZED-AUTO"),
	WF2("UNINITIALIZED-AUTOREPEAT"),
	WF3("UNINITIALIZED-OPEN-INSPECT-CLOSED"),
	WF4("UNINITIALIZED-OPEN-VERIFY-INSPECT-CLOSED"),
	WF5("UNINITIALIZED-AUTO_WITH_SUBQUESTION");
	
	private String title;

	private WSQDWorkflow(String title) {
		this.title = title;
	}
	
	public String getTitle() {
		return title;
	}
}
