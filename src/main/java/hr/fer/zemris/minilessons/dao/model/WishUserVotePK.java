package hr.fer.zemris.minilessons.dao.model;

import java.io.Serializable;

/**
 * This class exists only to serve as primary key for JPA-based persistence layer.
 * 
 * @author marcupic
 *
 */
public class WishUserVotePK implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Wish ID.
	 */
	private Long wish;
	/**
	 * User ID.
	 */
	private Long user;
	
	public WishUserVotePK() {
	}

	public WishUserVotePK(Long wish, Long user) {
		super();
		this.wish = wish;
		this.user = user;
	}

	public Long getWish() {
		return wish;
	}

	public Long getUser() {
		return user;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		result = prime * result + ((wish == null) ? 0 : wish.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof WishUserVotePK))
			return false;
		WishUserVotePK other = (WishUserVotePK) obj;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		if (wish == null) {
			if (other.wish != null)
				return false;
		} else if (!wish.equals(other.wish))
			return false;
		return true;
	}
}
