package hr.fer.zemris.minilessons.dao;

/**
 * Interface which specifies a way of talking with DAO provider.
 * 
 * @author marcupic
 */
public interface DAOManager {
	/**
	 * Open connection to provider.
	 */
	void open();
	/**
	 * Close connection to provider.
	 */
	void close();
	/**
	 * Begin transaction. Valid only if connection is open.
	 */
	void beginTransaction();
	/**
	 * Commit transaction. Valid only if connection is open.
	 */
	void commitTransaction();
	/**
	 * Rollback transaction. Valid only if connection is open.
	 */
	void rollbackTransaction();
}
