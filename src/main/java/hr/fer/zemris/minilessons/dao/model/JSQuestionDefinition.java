package hr.fer.zemris.minilessons.dao.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("JS")
public class JSQuestionDefinition extends CommonQuestionDefinition {

	@Column(nullable=true)
	private Integer apiLevel;
	
	public JSQuestionDefinition() {
	}

	public Integer getApiLevel() {
		return apiLevel;
	}
	
	public void setApiLevel(Integer apiLevel) {
		this.apiLevel = apiLevel;
	}
}
