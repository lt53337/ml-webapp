package hr.fer.zemris.minilessons.dao.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="wsactscores")
public class WSActivityScore {

	@EmbeddedId
	private WSActivityScorePK id;
	
	@ManyToOne(fetch=FetchType.LAZY) @MapsId("sourceid")
	@JoinColumn(nullable=false)
	private WSActivitySource source;

	@ManyToOne(fetch=FetchType.LAZY) @MapsId("userid")
	@JoinColumn(nullable=false)
	private User user;
	
	private double score;
	
	private boolean pending;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private Date assignedAt;

	public WSActivityScore() {
	}

	public WSActivityScorePK getId() {
		return id;
	}

	public void setId(WSActivityScorePK id) {
		this.id = id;
	}

	public WSActivitySource getSource() {
		return source;
	}

	public void setSource(WSActivitySource source) {
		this.source = source;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public boolean isPending() {
		return pending;
	}

	public void setPending(boolean pending) {
		this.pending = pending;
	}

	public Date getAssignedAt() {
		return assignedAt;
	}

	public void setAssignedAt(Date assignedAt) {
		this.assignedAt = assignedAt;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof WSActivityScore))
			return false;
		WSActivityScore other = (WSActivityScore) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
