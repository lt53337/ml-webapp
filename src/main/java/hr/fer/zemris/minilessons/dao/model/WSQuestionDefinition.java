package hr.fer.zemris.minilessons.dao.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="wsquestion_defs")
public class WSQuestionDefinition {
	
	@Id @GeneratedValue
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(nullable=false)
	private Workspace workspace;
	
	@Enumerated(EnumType.STRING)
	@Column(length=10)
	private WorkspacePage subpage;
	
	/**
	 * Localized titles.
	 */
	@ElementCollection(fetch=FetchType.EAGER)
	@MapKeyColumn(name="lang", length=10)
	@Column(name="text", length=230)
	@CollectionTable(name="wsqd_titles",joinColumns={@JoinColumn(name="wsqd_id")})
	private Map<String, String> titles = new HashMap<>();

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(nullable=true)
	private WSGrouping grouping;
	
	private boolean groupAssignment;
	
	private boolean acceptIncorrectSolution;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(nullable=true)
	private WSActivity activity;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(nullable=true)
	private WSActivitySource source;
	
	@Column(length=60, name="qdef", nullable=false)
	private String questionDefinition;

	@Column(length=40, name="qdefconf")
	private String questionDefinitionConfig;
	
	@Column(length=60, name="additqdef", nullable=true)
	private String additionalQuestionDefinition;
	
	@Column(length=250, name="additqdefconf", nullable=true)
	private String additionalQuestionDefinitionConfigs;
	
	private boolean additionalGroupAssignment;

	@Column(length=10, name="workflow", nullable=false)
	@Enumerated(EnumType.STRING)
	private WSQDWorkflow workflow;
	
	@Column(length=10, name="wfstate", nullable=false)
	@Enumerated(EnumType.STRING)
	private WSQDWorkflowState workflowCurrentState;
	
	private double maxScore;

	private double maxAdditionalScore;
	
	private double maxControlScore;

	@Temporal(TemporalType.TIMESTAMP)
	private Date solvableSince;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date solvableUntil;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date additionalSolvableUntil;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private Date createdAt;
	
	public WSQuestionDefinition() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreatedAt() {
		return createdAt;
	}
	
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	public Workspace getWorkspace() {
		return workspace;
	}

	public void setWorkspace(Workspace workspace) {
		this.workspace = workspace;
	}

	public WorkspacePage getSubpage() {
		return subpage;
	}

	public void setSubpage(WorkspacePage subpage) {
		this.subpage = subpage;
	}

	public Map<String, String> getTitles() {
		return titles;
	}

	public void setTitles(Map<String, String> titles) {
		this.titles = titles;
	}

	public WSGrouping getGrouping() {
		return grouping;
	}

	public void setGrouping(WSGrouping grouping) {
		this.grouping = grouping;
	}

	public boolean isGroupAssignment() {
		return groupAssignment;
	}

	public void setGroupAssignment(boolean groupAssignment) {
		this.groupAssignment = groupAssignment;
	}

	public boolean isAcceptIncorrectSolution() {
		return acceptIncorrectSolution;
	}

	public void setAcceptIncorrectSolution(boolean acceptIncorrectSolution) {
		this.acceptIncorrectSolution = acceptIncorrectSolution;
	}

	public WSActivity getActivity() {
		return activity;
	}

	public void setActivity(WSActivity activity) {
		this.activity = activity;
	}

	public WSActivitySource getSource() {
		return source;
	}

	public void setSource(WSActivitySource source) {
		this.source = source;
	}

	public String getQuestionDefinition() {
		return questionDefinition;
	}

	public void setQuestionDefinition(String questionDefinition) {
		this.questionDefinition = questionDefinition;
	}

	public String getQuestionDefinitionConfig() {
		return questionDefinitionConfig;
	}

	public void setQuestionDefinitionConfig(String questionDefinitionConfig) {
		this.questionDefinitionConfig = questionDefinitionConfig;
	}

	public String getAdditionalQuestionDefinition() {
		return additionalQuestionDefinition;
	}

	public void setAdditionalQuestionDefinition(String additionalQuestionDefinition) {
		this.additionalQuestionDefinition = additionalQuestionDefinition;
	}

	public String getAdditionalQuestionDefinitionConfigs() {
		return additionalQuestionDefinitionConfigs;
	}

	public void setAdditionalQuestionDefinitionConfigs(String additionalQuestionDefinitionConfigs) {
		this.additionalQuestionDefinitionConfigs = additionalQuestionDefinitionConfigs;
	}

	public boolean isAdditionalGroupAssignment() {
		return additionalGroupAssignment;
	}

	public void setAdditionalGroupAssignment(boolean additionalGroupAssignment) {
		this.additionalGroupAssignment = additionalGroupAssignment;
	}

	public WSQDWorkflow getWorkflow() {
		return workflow;
	}

	public void setWorkflow(WSQDWorkflow workflow) {
		this.workflow = workflow;
	}

	public WSQDWorkflowState getWorkflowCurrentState() {
		return workflowCurrentState;
	}

	public void setWorkflowCurrentState(WSQDWorkflowState workflowCurrentState) {
		this.workflowCurrentState = workflowCurrentState;
	}

	public double getMaxScore() {
		return maxScore;
	}

	public void setMaxScore(double maxScore) {
		this.maxScore = maxScore;
	}

	public double getMaxAdditionalScore() {
		return maxAdditionalScore;
	}

	public void setMaxAdditionalScore(double maxAdditionalScore) {
		this.maxAdditionalScore = maxAdditionalScore;
	}

	public double getMaxControlScore() {
		return maxControlScore;
	}

	public void setMaxControlScore(double maxControlScore) {
		this.maxControlScore = maxControlScore;
	}

	public Date getSolvableSince() {
		return solvableSince;
	}

	public void setSolvableSince(Date solvableSince) {
		this.solvableSince = solvableSince;
	}

	public Date getSolvableUntil() {
		return solvableUntil;
	}

	public void setSolvableUntil(Date solvableUntil) {
		this.solvableUntil = solvableUntil;
	}

	public Date getAdditionalSolvableUntil() {
		return additionalSolvableUntil;
	}

	public void setAdditionalSolvableUntil(Date additionalSolvableUntil) {
		this.additionalSolvableUntil = additionalSolvableUntil;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof WSQuestionDefinition))
			return false;
		WSQuestionDefinition other = (WSQuestionDefinition) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
