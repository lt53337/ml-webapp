package hr.fer.zemris.minilessons.dao.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class WSActivityScorePK implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Column(name="sourceid")
	private Long sourceid;
	
	@Column(name="userid")
	private Long userid;
	
	public WSActivityScorePK(Long userid, Long sourceid) {
		super();
		this.userid = userid;
		this.sourceid = sourceid;
	}
	
	public WSActivityScorePK() {
	}

	public Long getUserid() {
		return userid;
	}

	public void setUserid(Long userid) {
		this.userid = userid;
	}

	public Long getSourceid() {
		return sourceid;
	}

	public void setSourceid(Long sourceid) {
		this.sourceid = sourceid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((sourceid == null) ? 0 : sourceid.hashCode());
		result = prime * result + ((userid == null) ? 0 : userid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof WSActivityScorePK))
			return false;
		WSActivityScorePK other = (WSActivityScorePK) obj;
		if (sourceid == null) {
			if (other.sourceid != null)
				return false;
		} else if (!sourceid.equals(other.sourceid))
			return false;
		if (userid == null) {
			if (other.userid != null)
				return false;
		} else if (!userid.equals(other.userid))
			return false;
		return true;
	}
}
