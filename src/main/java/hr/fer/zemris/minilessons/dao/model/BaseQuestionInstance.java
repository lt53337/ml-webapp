package hr.fer.zemris.minilessons.dao.model;

public interface BaseQuestionInstance {
	Long getId();
	String getContext();
	void setContext(String context);
}
