package hr.fer.zemris.minilessons.dao.model;

/**
 * Question instance status.
 * 
 * @author marcupic
 *
 */
public enum QIStatus {

	/**
	 * If question instance can still be solved by user.
	 */
	SOLVABLE,
	/**
	 * If question instance was evaluated.
	 */
	EVALUATED,
	/**
	 * If an error occured during question evaluation.
	 */
	ERROR;
	
}
