package hr.fer.zemris.minilessons.dao.model;

/**
 * JSQuestionDefinition approval status.
 * 
 * @author marcupic
 */
public enum QuestionApproval {
	/**
	 * JSQuestionDefinition is waiting for approval. It is unavailable for regular usage.
	 */
	WAITING,
	/**
	 * JSQuestionDefinition is waiting for approval, but is restricted. It is available for regular usage
	 * but will have visible indication that it is not verified yet.
	 */
	SOFT_WAITING,
	/**
	 * JSQuestionDefinition is rejected.
	 */
	REJECTED,
	/**
	 * JSQuestionDefinition is approved for regular usage.
	 */
	APPROVED
}
