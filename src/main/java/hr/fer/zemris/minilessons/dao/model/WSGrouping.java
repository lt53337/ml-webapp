package hr.fer.zemris.minilessons.dao.model;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Cacheable;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name="ws_groupings")
@Cacheable(true)
@Cache(usage=CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class WSGrouping {

	/**
	 * ID.
	 */
	@Id @GeneratedValue
	private Long id;
	
	/**
	 * Naziv grupiranja.
	 */
	@ElementCollection(fetch=FetchType.EAGER)
	@MapKeyColumn(name="lang", length=10)
	@Column(name="text", length=100)
	@CollectionTable(name="wsgrouping_titles",joinColumns={@JoinColumn(name="wsgr_id")})
	private Map<String,String> titles = new HashMap<>();
	
	/**
	 * Minimalni broj članova unutar grupe.
	 */
	private Integer minMembers;

	/**
	 * Maksimalni broj članova unutar grupe.
	 */
	private Integer maxMembers;
	
	/**
	 * Je li uređivanje članstva od studenata omogućeno. 
	 */
	private boolean managementEnabled;

	@ManyToOne
	@JoinColumn(nullable=false)
	private Workspace workspace;
	
	public WSGrouping() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Workspace getWorkspace() {
		return workspace;
	}
	
	public void setWorkspace(Workspace workspace) {
		this.workspace = workspace;
	}
	
	public Map<String, String> getTitles() {
		return titles;
	}
	public void setTitles(Map<String, String> titles) {
		this.titles = titles;
	}
	
	public Integer getMinMembers() {
		return minMembers;
	}

	public void setMinMembers(Integer minMembers) {
		this.minMembers = minMembers;
	}

	public Integer getMaxMembers() {
		return maxMembers;
	}

	public void setMaxMembers(Integer maxMembers) {
		this.maxMembers = maxMembers;
	}

	public boolean isManagementEnabled() {
		return managementEnabled;
	}

	public void setManagementEnabled(boolean managementEnabled) {
		this.managementEnabled = managementEnabled;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof WSGrouping))
			return false;
		WSGrouping other = (WSGrouping) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
}
