package hr.fer.zemris.minilessons.dao.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
public class UserCourseRoleKey implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Column(name="user_id")
	private Long user_id;
	
	@Column(name="course_id", length=32, nullable=false)
	private String course_id;
	
	@Enumerated(EnumType.STRING)
	@Column(name="role",length=20)
	private CourseRole role;
	
	public UserCourseRoleKey() {
	}

	
	public Long getUser_id() {
		return user_id;
	}

	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}


	public String getCourse_id() {
		return course_id;
	}

	public void setCourse_id(String course_id) {
		this.course_id = course_id;
	}


	public CourseRole getRole() {
		return role;
	}
	public void setRole(CourseRole role) {
		this.role = role;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((course_id == null) ? 0 : course_id.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		result = prime * result + ((user_id == null) ? 0 : user_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof UserCourseRoleKey))
			return false;
		UserCourseRoleKey other = (UserCourseRoleKey) obj;
		if (course_id == null) {
			if (other.course_id != null)
				return false;
		} else if (!course_id.equals(other.course_id))
			return false;
		if (role != other.role)
			return false;
		if (user_id == null) {
			if (other.user_id != null)
				return false;
		} else if (!user_id.equals(other.user_id))
			return false;
		return true;
	}
}