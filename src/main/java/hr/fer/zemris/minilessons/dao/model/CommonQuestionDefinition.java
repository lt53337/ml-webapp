package hr.fer.zemris.minilessons.dao.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

@Entity
@Table(name="question_defs")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="discr", discriminatorType=DiscriminatorType.STRING, length=4)
public abstract class CommonQuestionDefinition {

	@Id
	@Column(length=32)
	private String id; // will be of UUID-format

	/**
	 * Public QuestionDefinition ID (alternate key when used with version), as
	 * declared with attribute <code>uid</code> in <code>question</code>-tag.
	 */
	@Column(length=200,unique=false,nullable=false)
	private String publicID;
	/**
	 * QuestionDefinition declared version.
	 */
	private long version;
	/**
	 * QuestionDefinition declared minor version.
	 */
	private long minorVersion;

	/**
	 * Is this question visible?
	 */
	private boolean visible;
	/**
	 * Is this question archived?
	 */
	private boolean archived;

	/**
	 * Is this question public?
	 */
	private boolean publicQuestion = true;

	/**
	 * Owner of this question (user that uploaded it).
	 */
	@ManyToOne
	@JoinColumn(nullable=false)
	private User owner;
	/**
	 * When was this question uploaded.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private Date uploadedOn;

	/**
	 * Approval status of this question.
	 */
	@Enumerated(EnumType.STRING)
	@Column(length=20,nullable=false)
	private QuestionApproval approval;
	/**
	 * Who approved this question?
	 */
	@ManyToOne
	@JoinColumn(nullable=true)
	private User approvedBy;
	/**
	 * When was this question approved?
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=true)
	private Date approvedOn;

	/**
	 * Localized titles of question.
	 */
	@ElementCollection(fetch=FetchType.EAGER)
	@MapKeyColumn(name="lang", length=10)
	@Column(name="text", length=230)
	@CollectionTable(name="q_titles",joinColumns={@JoinColumn(name="q_id")})
	private Map<String, String> titles = new HashMap<>();
	/**
	 * Localized tags of question.
	 */
	@ElementCollection(fetch=FetchType.EAGER)
	@CollectionTable(name="q_tags",joinColumns={@JoinColumn(name="q_id")})
	@OrderColumn(name="idx")
	private List<LocalizedString> tags = new ArrayList<>();
	/**
	 * Set of language tags on which this question is offered.
	 */
	@ElementCollection(fetch=FetchType.EAGER)
	@CollectionTable(name="q_langs",joinColumns={@JoinColumn(name="q_id")})
	@Column(length=10, name="lang")
	private Set<String> languages = new HashSet<>();

	/**
	 * Question configuration variabilities.
	 */
	@ElementCollection(fetch=FetchType.EAGER)
	@MapKeyColumn(name="config", length=30)
	@Column(name="variability", length=10)
	@Enumerated(EnumType.STRING)
	@CollectionTable(name="q_configs",joinColumns={@JoinColumn(name="q_id")})
	private Map<String, QuestionConfigVariability> configVariability = new HashMap<>();
	
	@Enumerated(EnumType.STRING)
	@Column(nullable=false, length=5)
	private QuestionType questionType;
	
	@Version
	private int olversion;
	
	public CommonQuestionDefinition() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isPublicQuestion() {
		return publicQuestion;
	}
	public void setPublicQuestion(boolean publicQuestion) {
		this.publicQuestion = publicQuestion;
	}
	
	public String getPublicID() {
		return publicID;
	}
	public void setPublicID(String publicID) {
		this.publicID = publicID;
	}

	public long getVersion() {
		return version;
	}
	public void setVersion(long version) {
		this.version = version;
	}

	public long getMinorVersion() {
		return minorVersion;
	}
	public void setMinorVersion(long minorVersion) {
		this.minorVersion = minorVersion;
	}

	public boolean isVisible() {
		return visible;
	}
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public boolean isArchived() {
		return archived;
	}
	public void setArchived(boolean archived) {
		this.archived = archived;
	}

	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}

	public Date getUploadedOn() {
		return uploadedOn;
	}
	public void setUploadedOn(Date uploadedOn) {
		this.uploadedOn = uploadedOn;
	}

	public Map<String, String> getTitles() {
		return titles;
	}
	public void setTitles(Map<String, String> titles) {
		this.titles = titles;
	}

	public List<LocalizedString> getTags() {
		return tags;
	}
	public void setTags(List<LocalizedString> tags) {
		this.tags = tags;
	}
	
	public Set<String> getLanguages() {
		return languages;
	}
	public void setLanguages(Set<String> languages) {
		this.languages = languages;
	}

	public int getOlversion() {
		return olversion;
	}
	public void setOlversion(int olversion) {
		this.olversion = olversion;
	}

	public QuestionApproval getApproval() {
		return approval;
	}
	public void setApproval(QuestionApproval approval) {
		this.approval = approval;
	}

	public User getApprovedBy() {
		return approvedBy;
	}
	public void setApprovedBy(User approvedBy) {
		this.approvedBy = approvedBy;
	}

	public Date getApprovedOn() {
		return approvedOn;
	}
	public void setApprovedOn(Date approvedOn) {
		this.approvedOn = approvedOn;
	}

	public Map<String, QuestionConfigVariability> getConfigVariability() {
		return configVariability;
	}
	public void setConfigVariability(Map<String, QuestionConfigVariability> configVariability) {
		this.configVariability = configVariability;
	}
	
	public QuestionType getQuestionType() {
		return questionType;
	}
	public void setQuestionType(QuestionType questionType) {
		this.questionType = questionType;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof CommonQuestionDefinition))
			return false;
		CommonQuestionDefinition other = (CommonQuestionDefinition) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	/**
	 * Public comparator which compares questions by version.minorversion. Ordering performed by
	 * this comparator will have question with latest (biggest) version and then minorversion
	 * as first element.
	 */
	public static final Comparator<CommonQuestionDefinition> CMP_BY_VERSION = (q1,q2)->{
		int res = -Long.compare(q1.getVersion(), q2.getVersion());
		if(res != 0) return res;
		return -Long.compare(q1.getMinorVersion(), q2.getMinorVersion());
	};
}
