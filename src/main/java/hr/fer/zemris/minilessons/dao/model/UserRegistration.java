package hr.fer.zemris.minilessons.dao.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * User registration request.
 * 
 * @author marcupic
 */
@Entity
@Table(name="registrations",indexes={@Index(columnList="dateKey,requestIP")})
public class UserRegistration {

	/**
	 * When was this request submitted?
	 */
	@Temporal(TemporalType.TIMESTAMP)
	private Date requestDate;

	/**
	 * For efficient indexing purposes, additional field which stores registration date only as string.
	 */
	@Column(length=10,nullable=false)
	private String dateKey;
	
	/**
	 * From which IP address was this registration requested?
	 */
	@Column(length=45, nullable=false)
	private String requestIP;
	
	/**
	 * Primary key.
	 */
	@Id @GeneratedValue
	private Long id;
	
	/**
	 * First name.
	 */
	@Column(length=40,nullable=false,unique=false)
	private String firstName;
	
	/**
	 * Last name.
	 */
	@Column(length=60,nullable=false,unique=false)
	private String lastName;
	
	/**
	 * Username.
	 */
	@Column(length=40,nullable=false,unique=false)
	private String username;

	/**
	 * E-mail.
	 */
	@Column(length=100,nullable=false,unique=false)
	private String email;
	
	/**
	 * Hash of password.
	 */
	@Column(length=64,nullable=false,unique=false)
	private String passwordHash;
	
	/**
	 * When was this account activated?
	 */
	@Temporal(TemporalType.TIMESTAMP)
	private Date activationDate;

	/**
	 * Activation key to be used for this account activation.
	 */
	@Column(length=64,nullable=false,unique=false)
	private String activationKey;
	
	public UserRegistration() {
	}

	/**
	 * Getter for primary key.
	 * 
	 * @return primary key
	 */
	public Long getId() {
		return id;
	}
	
	/**
	 * Setter for primary key.
	 * 
	 * @param id primary key
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * Getter for first name.
	 * 
	 * @return first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Setter for fist name.
	 * 
	 * @param firstName first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Getter for last name.
	 * 
	 * @return last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Setter for last name.
	 * 
	 * @param lastName last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Getter for username.
	 * 
	 * @return username
	 */
	public String getUsername() {
		return username;
	}

	/**	

	 * Setter for username.
	 * 
	 * @param username username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Getter for e-mail.
	 * 
	 * @return e-mail
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Setter for e-mail.
	 * 
	 * @param email e-mail
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Getter for password hash.
	 * 
	 * @return password hash
	 */
	public String getPasswordHash() {
		return passwordHash;
	}
	/**
	 * Setter for password hash.
	 * 
	 * @param passwordHash password hash
	 */
	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	/**
	 * Getter for the date when the request was submitted.
	 * 
	 * @return date
	 */
	public Date getRequestDate() {
		return requestDate;
	}

	/**
	 * Setter for submission date.
	 * 
	 * @param requestDate date
	 */
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	/**
	 * Getter for the IP address from which a request was made.
	 * 
	 * @return ip address
	 */
	public String getRequestIP() {
		return requestIP;
	}

	/**
	 * Setter for request IP.
	 * 
	 * @param requestIP ip address
	 */
	public void setRequestIP(String requestIP) {
		this.requestIP = requestIP;
	}

	/**
	 * Getter for the date when this request was confirmed and activated.
	 * 
	 * @return activation date or <code>null</code> if this request was not activated
	 */
	public Date getActivationDate() {
		return activationDate;
	}

	/**
	 * Setter for activation date.
	 * 
	 * @param activationDate date
	 */
	public void setActivationDate(Date activationDate) {
		this.activationDate = activationDate;
	}	
	
	/**
	 * Getter for activation key.
	 * 
	 * @return activation key
	 */
	public String getActivationKey() {
		return activationKey;
	}
	
	/**
	 * Setter for activation key.
	 * 
	 * @param activationKey activation key
	 */
	public void setActivationKey(String activationKey) {
		this.activationKey = activationKey;
	}

	/**
	 * Getter for datekey.
	 * @return dateKey
	 */
	public String getDateKey() {
		return dateKey;
	}
	
	/**
	 * Setter for dateKey.
	 * @param dateKey dateKey
	 */
	public void setDateKey(String dateKey) {
		this.dateKey = dateKey;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof UserRegistration))
			return false;
		UserRegistration other = (UserRegistration) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
