package hr.fer.zemris.minilessons.dao.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import hr.fer.zemris.minilessons.ILocalizedString;

/**
 * This class represents localized text.
 * 
 * @author marcupic
 *
 */
@Embeddable
public class LocalizedString implements ILocalizedString {

	/**
	 * Language tag.
	 */
	@Column(length=10,nullable=false,unique=false)
	private String lang;
	/**
	 * Localized text.
	 */
	@Column(length=230,nullable=false,unique=false)
	private String text;
	
	/**
	 * Constructor.
	 */
	public LocalizedString() {
	}

	/**
	 * Constructor.
	 * 
	 * @param lang language tag
	 * @param text localized text
	 */
	public LocalizedString(String lang, String text) {
		super();
		this.lang = lang;
		this.text = text;
	}

	@Override
	public String getLang() {
		return lang;
	}
	
	@Override
	public String getText() {
		return text;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((lang == null) ? 0 : lang.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof LocalizedString))
			return false;
		LocalizedString other = (LocalizedString) obj;
		if (lang == null) {
			if (other.lang != null)
				return false;
		} else if (!lang.equals(other.lang))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "["+lang+"] /"+text+"/";
	}
}
