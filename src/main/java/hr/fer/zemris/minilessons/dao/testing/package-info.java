/**
 * Package contains prototype implementation of DAO which was used during development
 * to locally run web-application. All data is stored in memory and lost when web-app
 * is restarted.
 */
package hr.fer.zemris.minilessons.dao.testing;

