package hr.fer.zemris.minilessons.dao.jpa;

import javax.persistence.EntityManager;

import hr.fer.zemris.minilessons.dao.DAOException;
import hr.fer.zemris.minilessons.dao.DAOManager;

public class DAOManagerImpl implements DAOManager {

	@Override
	public void open() {
		if(JPAData.getEntityManager() != null) {
			throw new DAOException("Nested DAO provider opening is not allowed.");
		}
		JPAData.setEntityManager(JPAData.getEmf().createEntityManager());
	}

	@Override
	public void close() {
		EntityManager em = JPAData.getEntityManager(); 
		if(em != null) {
			em.close();
			JPAData.removeEntityManager();
		}
	}

	@Override
	public void beginTransaction() {
		EntityManager em = JPAData.getEntityManager(); 
		if(em == null) {
			throw new DAOException("Transaction can not be opened: DAO provider is not open.");
		}
		em.getTransaction().begin();
	}

	@Override
	public void commitTransaction() {
		EntityManager em = JPAData.getEntityManager(); 
		if(em == null) {
			throw new DAOException("Transaction can not be opened: DAO provider is not open.");
		}
		em.getTransaction().commit();
	}

	@Override
	public void rollbackTransaction() {
		EntityManager em = JPAData.getEntityManager(); 
		if(em == null) {
			throw new DAOException("Transaction can not be opened: DAO provider is not open.");
		}
		em.getTransaction().rollback();
	}

}
