package hr.fer.zemris.minilessons.dao.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Cacheable;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * Model of user.
 * 
 * @author marcupic
 */
@Entity
@Table(name="users",uniqueConstraints={@UniqueConstraint(columnNames={"username","authDomain"})})
@Cacheable(true)
public class User implements Comparable<User> {
	
	/**
	 * Primary key.
	 */
	@Id @GeneratedValue
	private Long id;
	/**
	 * First name.
	 */
	@Column(length=40,nullable=false,unique=false)
	private String firstName;
	/**
	 * Last name.
	 */
	@Column(length=60,nullable=false,unique=false)
	private String lastName;
	/**
	 * Username.
	 */
	@Column(length=40,nullable=false,unique=false)
	private String username;
	/**
	 * JMBAG.
	 */
	@Column(length=15,nullable=false,unique=true)
	private String jmbag;
	/**
	 * E-mail.
	 */
	@Column(length=100,nullable=true,unique=false)
	private String email;
	/**
	 * Hash of password.
	 */
	@Column(length=64,nullable=false,unique=false)
	private String passwordHash;
	/**
	 * Authentication domain: who is responsible for checking username and password.
	 */
	@Column(length=20,nullable=false,unique=false)
	private String authDomain;
	/**
	 * Upload policy for this user.
	 */
	@Column(length=20,nullable=false,unique=false)
	@Enumerated(EnumType.STRING)
	private UploadPolicy uploadPolicy;
	/**
	 * Permissions. Values are {@link MLBasicPermission#getPermissonName()}.
	 */
	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="user_permissions")
	@Column(length=25)
	private Set<String> permissions = new HashSet<>();
	/**
	 * When was this user created.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private Date createdOn;
	
	/**
	 * Is this account disabled?
	 */
	private boolean accountDisabled;
	
	/**
	 * Constructor.
	 */
	public User() {
	}

	/**
	 * Getter for primary key.
	 * 
	 * @return primary key
	 */
	public Long getId() {
		return id;
	}
	
	/**
	 * Setter for primary key.
	 * 
	 * @param id primary key
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * Getter for first name.
	 * 
	 * @return first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Setter for fist name.
	 * 
	 * @param firstName first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Getter for last name.
	 * 
	 * @return last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Setter for last name.
	 * 
	 * @param lastName last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Getter for username.
	 * 
	 * @return username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Setter for username.
	 * 
	 * @param username username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Getter for jmbag.
	 * 
	 * @return jmbag
	 */
	public String getJmbag() {
		return jmbag;
	}

	/**
	 * Setter for jmbag.
	 * 
	 * @param jmbag jmbag
	 */
	public void setJmbag(String jmbag) {
		this.jmbag = jmbag;
	}

	/**
	 * Getter for e-mail.
	 * 
	 * @return e-mail
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Setter for e-mail.
	 * 
	 * @param email e-mail
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Getter for password hash.
	 * 
	 * @return password hash
	 */
	public String getPasswordHash() {
		return passwordHash;
	}
	/**
	 * Setter for password hash.
	 * 
	 * @param passwordHash password hash
	 */
	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}
	
	/**
	 * Getter for authentication service.
	 * 
	 * @return authentication service
	 */
	public String getAuthDomain() {
		return authDomain;
	}
	/**
	 * Setter for authentication service.
	 * 
	 * @param authDomain authentication service
	 */
	public void setAuthDomain(String authDomain) {
		this.authDomain = authDomain;
	}

	/**
	 * Getter for upload policy.
	 * 
	 * @return upload policy
	 */
	public UploadPolicy getUploadPolicy() {
		return uploadPolicy;
	}
	/**
	 * Setter for upload policy.
	 * 
	 * @param uploadPolicy upload policy
	 */
	public void setUploadPolicy(UploadPolicy uploadPolicy) {
		this.uploadPolicy = uploadPolicy;
	}

	/**
	 * Getter for permissions. For legal values see {@link MLBasicPermission}.
	 * 
	 * @return permissions
	 */
	public Set<String> getPermissions() {
		return permissions;
	}

	/**
	 * Setter for permissions.
	 * 
	 * @param permissions permissions
	 */
	public void setPermissions(Set<String> permissions) {
		this.permissions = permissions;
	}

	/**
	 * Getter for date when account was created in this system.
	 * 
	 * @return date
	 */
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * Setter for account creation date.
	 * 
	 * @param created account creation date
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	/**
	 * Getter for account disabled.
	 * 
	 * @return is account disabled?
	 */
	public boolean isAccountDisabled() {
		return accountDisabled;
	}

	/**
	 * Setter for account disabled.
	 * 
	 * @param accountDisabled account disabled
	 */
	public void setAccountDisabled(boolean accountDisabled) {
		this.accountDisabled = accountDisabled;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	public int compareTo(User o) {
		int r = this.lastName.compareTo(o.lastName);
		if(r != 0) return r;
		r = this.firstName.compareTo(o.firstName);
		if(r != 0) return r;
		r = this.jmbag.compareTo(o.jmbag);
		if(r != 0) return r;
		return this.id.compareTo(o.id);
	}
}
