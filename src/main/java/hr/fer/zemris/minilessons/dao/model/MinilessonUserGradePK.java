package hr.fer.zemris.minilessons.dao.model;

import java.io.Serializable;

/**
 * This class exists only to serve as primary key for JPA-based persistence layer.
 * 
 * @author marcupic
 *
 */
public class MinilessonUserGradePK implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Minilesson primary key.
	 */
	private String minilesson;
	/**
	 * User primary key.
	 */
	private Long user;

	/**
	 * Constructor.
	 */
	public MinilessonUserGradePK() {
	}

	/**
	 * Constructor.
	 * 
	 * @param minilesson Minilesson primary key.
	 * @param user user primary key.
	 */
	public MinilessonUserGradePK(String minilesson, Long user) {
		super();
		this.minilesson = minilesson;
		this.user = user;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((minilesson == null) ? 0 : minilesson.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof MinilessonUserGradePK))
			return false;
		MinilessonUserGradePK other = (MinilessonUserGradePK) obj;
		if (minilesson == null) {
			if (other.minilesson != null)
				return false;
		} else if (!minilesson.equals(other.minilesson))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	/**
	 * Getter for minilesson primary key.
	 * 
	 * @return minilesson primary key
	 */
	public String getMinilesson() {
		return minilesson;
	}

	/**
	 * Getter for user primary key.
	 * 
	 * @return user primary key
	 */
	public Long getUser() {
		return user;
	}
}
