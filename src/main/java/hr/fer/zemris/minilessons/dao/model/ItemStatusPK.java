package hr.fer.zemris.minilessons.dao.model;

import java.io.Serializable;

/**
 * This class exists only to serve as primary key for JPA-based persistence layer.
 * 
 * @author marcupic
 *
 */
public class ItemStatusPK implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Primary key of minilesson.
	 */
	private String minilesson;
	/**
	 * Primary key of user.
	 */
	private Long user;
	/**
	 * Item ID.
	 */
	private String itemID;

	/**
	 * Constructor.
	 */
	public ItemStatusPK() {
	}

	/**
	 * Constructor.
	 * 
	 * @param minilesson primary key of minilesson
	 * @param user primary key of user
	 * @param itemID item ID
	 */
	public ItemStatusPK(String minilesson, Long user, String itemID) {
		super();
		this.minilesson = minilesson;
		this.user = user;
		this.itemID = itemID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((itemID == null) ? 0 : itemID.hashCode());
		result = prime * result + ((minilesson == null) ? 0 : minilesson.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ItemStatusPK))
			return false;
		ItemStatusPK other = (ItemStatusPK) obj;
		if (itemID == null) {
			if (other.itemID != null)
				return false;
		} else if (!itemID.equals(other.itemID))
			return false;
		if (minilesson == null) {
			if (other.minilesson != null)
				return false;
		} else if (!minilesson.equals(other.minilesson))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	/**
	 * Getter for minilesson primary key.
	 * 
	 * @return minilesson primary key
	 */
	public String getMinilesson() {
		return minilesson;
	}

	/**
	 * Getter for user primary key.
	 * 
	 * @return user primary key
	 */
	public Long getUser() {
		return user;
	}
	
	/**
	 * Getter for item ID.
	 * @return item ID
	 */
	public String getItemID() {
		return itemID;
	}
}
