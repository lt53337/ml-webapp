package hr.fer.zemris.minilessons.dao.model;

/**
 * Question Configuration Variability: if we create multiple instances of the same question configuration,
 * what can we expect to get?
 * 
 * @author marcupic
 */
public enum QuestionConfigVariability {

	/**
	 * No variability is supported: all new instances will be the same. 
	 */
	NONE,
	/**
	 * Low variability is supported: a few of the new instances will be different. 
	 */
	LOW,
	/**
	 * Medium variability is supported: more than a few of the new instances will be different. 
	 */
	MEDIUM,
	/**
	 * High variability is supported: a chance that the new instances will be the same is rather low. 
	 */
	HIGH;
	
}
