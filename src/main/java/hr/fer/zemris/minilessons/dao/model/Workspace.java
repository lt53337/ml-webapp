package hr.fer.zemris.minilessons.dao.model;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.Cacheable;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name="workspaces")
@Cacheable(true)
@Cache(usage=CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Workspace {

	/**
	 * Id.
	 */
	@Id @GeneratedValue
	private Long id;

	/**
	 * Localized titles of course.
	 */
	@ElementCollection(fetch=FetchType.EAGER)
	@MapKeyColumn(name="lang", length=10)
	@Column(name="text", length=230)
	@CollectionTable(name="ws_titles",joinColumns={@JoinColumn(name="ws_id")})
	private Map<String,String> titles = new HashMap<>();

	/**
	 * Localized descriptions of course.
	 */
	@ElementCollection(fetch=FetchType.EAGER)
	@MapKeyColumn(name="lang", length=10)
	@Column(name="text", length=4096)
	@CollectionTable(name="ws_descriptions",joinColumns={@JoinColumn(name="ws_id")})
	private Map<String,String> descriptions = new HashMap<>();

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(nullable=false)
	private User owner;

	@ManyToMany
	@JoinTable(
		name="ws_users", 
		joinColumns=@JoinColumn(name="ws_id", referencedColumnName="id"),
		inverseJoinColumns=@JoinColumn(name="u_id", referencedColumnName="id")
	)
	private Set<User> users = new HashSet<>();

	@Enumerated(EnumType.STRING)
	@Column(length=10)
	private WorkspaceJoinPolicy joinPolicy;
	
	private boolean archived;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private Date createdAt;

	@OneToMany(fetch=FetchType.LAZY,mappedBy="workspace")
	@Cache(usage=CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<WSGrouping> groupings = new HashSet<>();
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="workspace")
	@Cache(usage=CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<WSActivity> activities = new HashSet<>();
	
	public Workspace() {
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public Set<WSActivity> getActivities() {
		return activities;
	}
	public void setActivities(Set<WSActivity> activities) {
		this.activities = activities;
	}
	
	public Set<WSGrouping> getGroupings() {
		return groupings;
	}
	public void setGroupings(Set<WSGrouping> groupings) {
		this.groupings = groupings;
	}
	
	public Map<String, String> getTitles() {
		return titles;
	}
	public void setTitles(Map<String, String> titles) {
		this.titles = titles;
	}

	public Map<String, String> getDescriptions() {
		return descriptions;
	}
	public void setDescriptions(Map<String, String> descriptions) {
		this.descriptions = descriptions;
	}

	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}

	public Set<User> getUsers() {
		return users;
	}
	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public WorkspaceJoinPolicy getJoinPolicy() {
		return joinPolicy;
	}
	public void setJoinPolicy(WorkspaceJoinPolicy joinPolicy) {
		this.joinPolicy = joinPolicy;
	}

	public boolean isArchived() {
		return archived;
	}
	public void setArchived(boolean archived) {
		this.archived = archived;
	}

	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Workspace))
			return false;
		Workspace other = (Workspace) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
