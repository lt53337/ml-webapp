package hr.fer.zemris.minilessons.dao;

public interface DAOBoot {

	void startup();
	
	void destroy();
	
}
