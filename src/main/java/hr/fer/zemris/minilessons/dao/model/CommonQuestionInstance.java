package hr.fer.zemris.minilessons.dao.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@MappedSuperclass
public abstract class CommonQuestionInstance {

	@Id @GeneratedValue
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(length=10)
	private QIStatus status;

	private double correctness;

	@Column(length=60, nullable=true)
	private String context;

	private boolean solved;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(nullable=true)
	private User user;
	
	@Column(length=20, nullable=false)
	private String configName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=true)
	private Date evaluatedOn;

	public CommonQuestionInstance() {
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public QIStatus getStatus() {
		return status;
	}
	public void setStatus(QIStatus status) {
		this.status = status;
	}

	public double getCorrectness() {
		return correctness;
	}
	public void setCorrectness(double correctness) {
		this.correctness = correctness;
	}

	public String getContext() {
		return context;
	}
	public void setContext(String context) {
		this.context = context;
	}

	public boolean isSolved() {
		return solved;
	}
	public void setSolved(boolean solved) {
		this.solved = solved;
	}

	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	public String getConfigName() {
		return configName;
	}
	public void setConfigName(String configName) {
		this.configName = configName;
	}

	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	public Date getEvaluatedOn() {
		return evaluatedOn;
	}
	public void setEvaluatedOn(Date evaluatedOn) {
		this.evaluatedOn = evaluatedOn;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof CommonQuestionInstance))
			return false;
		CommonQuestionInstance other = (CommonQuestionInstance) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}

