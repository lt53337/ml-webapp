package hr.fer.zemris.minilessons.dao.model;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="xmlquestion_insts")
@Cacheable(false)
public class XMLQuestionInstance extends CommonQuestionInstance implements BaseQuestionInstance {

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(nullable=false)
	private XMLQuestionDefinition question;

	@Lob @Column(length=8096)
	private String qData;
	
	@Lob @Column(length=8096)
	private String uData;

	public XMLQuestionInstance() {
	}

	public XMLQuestionDefinition getQuestion() {
		return question;
	}
	public void setQuestion(XMLQuestionDefinition question) {
		this.question = question;
	}
	
	public String getqData() {
		return qData;
	}

	public void setqData(String qData) {
		this.qData = qData;
	}

	public String getuData() {
		return uData;
	}

	public void setuData(String uData) {
		this.uData = uData;
	}

}
