package hr.fer.zemris.minilessons.dao;

import java.util.concurrent.Callable;

/**
 * Helper class for DAO usage.
 * 
 * @author marcupic
 *
 */
public class DAOUtil {

	/**
	 * Method executes given work under DAO transaction.
	 * 
	 * @param work work to execute
	 * @return work execution result
	 */
	public static <T> T transactional(Callable<T> work) {
		T t = null;
		if(work==null) return t;
		try {
			DAOProvider.getDAOManager().open();
			boolean transactionOpened = false;
			try {
				DAOProvider.getDAOManager().beginTransaction();
				transactionOpened = true;
				t = work.call();
				DAOProvider.getDAOManager().commitTransaction();
			} catch(Exception e) {
				if(transactionOpened) {
					DAOProvider.getDAOManager().rollbackTransaction();
				}
				throw new DAOException(e);
			}
		} finally {
			DAOProvider.getDAOManager().close();
		}
		return t;
	}
}
