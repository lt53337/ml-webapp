package hr.fer.zemris.minilessons.dao.model;

public enum WorkspacePage {

	QUIZ("Quizzes","1"),
	PROJ("Projects","2"),
	PRAC("Practice","3");
	
	private String title;
	private String numID;
	
	private WorkspacePage(String title, String numID) {
		this.title = title;
		this.numID = numID;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getNumID() {
		return numID;
	}
	
	public static WorkspacePage fromNumID(String id) {
		if("1".equals(id)) return QUIZ;
		if("2".equals(id)) return PROJ;
		if("3".equals(id)) return PRAC;
		throw new IllegalArgumentException("ID '"+id+"' is not valid specifier for WorkspacePage.");
	}
}
