package hr.fer.zemris.minilessons.dao.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.MapKeyColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

@Entity
@Table(name="course_details")
public class CourseDetails {

	/**
	 * Course Details ID (primary key).
	 * Will be set to the same value as is Course instance.
	 */
	@Id
	@Column(length=32)
	private String id;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedOn;

	@Lob
	@ElementCollection(fetch=FetchType.EAGER)
	@MapKeyColumn(name="lang", length=10)
	@Column(name="text", length=1024*16, nullable=false)
	@CollectionTable(name="c_details",joinColumns={@JoinColumn(name="c_id")})
	private Map<String,String> content = new HashMap<>();

	@Version
	private int version;
	
	public CourseDetails() {
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Map<String, String> getContent() {
		return content;
	}
	public void setContent(Map<String, String> content) {
		this.content = content;
	}

	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof CourseDetails))
			return false;
		CourseDetails other = (CourseDetails) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
