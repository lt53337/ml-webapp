package hr.fer.zemris.minilessons.dao.model;

public enum WSJoinRequestStatus {

	/**
	 * If request is still not processed.
	 */
	OPEN,
	/**
	 * If request was granted.
	 */
	ACCEPTED,
	/**
	 * If request was rejected.
	 */
	REJECTED,
	/**
	 * If user of request has been banned.
	 */
	BANNED
}
