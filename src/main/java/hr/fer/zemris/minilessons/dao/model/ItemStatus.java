package hr.fer.zemris.minilessons.dao.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * User status of item from minilesson. Objects of this class are used to record the progress
 * user made in minilesson.
 * 
 * @author marcupic
 *
 */
@Entity
@IdClass(ItemStatusPK.class)
@Table(name="item_statuses",indexes={
	@Index(columnList="minilesson_id, user_id", unique=false)
})
public class ItemStatus {

	/**
	 * Minilesson for which progress is recorded.
	 */
	@Id
	@ManyToOne
	private Minilesson minilesson;
	/**
	 * User whose progress is recorded.
	 */
	@Id
	@ManyToOne
	private User user;
	/**
	 * ID of item which progress is recorded.
	 */
	@Id
	@Column(length=10)
	private String itemID;
	
	/**
	 * When was this item opened?
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private Date openedOn;
	/**
	 * When was this item completed?
	 */
	@Temporal(TemporalType.TIMESTAMP)
	private Date completedOn;
	/**
	 * Additional data which is tracked with this item for this user.
	 */
	@Column(length=1024*10)
	private String userData;
	
	/**
	 * For items which need to distinct first opening, flag which can
	 * be used to track if item was initialized or not.
	 */
	private boolean initialized;
	
	/**
	 * Constructor.
	 */
	public ItemStatus() {
	}

	/**
	 * Constructor.
	 * 
	 * @param minilesson minilesson
	 * @param user user
	 * @param itemID item ID
	 */
	public ItemStatus(Minilesson minilesson, User user, String itemID) {
		super();
		this.minilesson = minilesson;
		this.user = user;
		this.itemID = itemID;
	}


	/**
	 * Getter for Minilesson for which progress is recorded.
	 * @return minilesson
	 */
	public Minilesson getMinilesson() {
		return minilesson;
	}

	/**
	 * Setter for Minilesson for which progress is recorded.
	 * @param minilesson minilesson
	 */
	public void setMinilesson(Minilesson minilesson) {
		this.minilesson = minilesson;
	}

	/**
	 * Getter for user whose progress is recorded.
	 * @return user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Setter for user whose progress is recorded.
	 * 
	 * @param user user
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * Getter for item ID which progress is recorded.
	 * 
	 * @return item ID
	 */
	public String getItemID() {
		return itemID;
	}

	/**
	 * Setter for item ID which progress is recorded.
	 * 
	 * @param itemID item id
	 */
	public void setItemID(String itemID) {
		this.itemID = itemID;
	}

	/**
	 * Getter for completion date and time.
	 * 
	 * @return completion date and time is item was completed or <code>null</code> otherwise
	 */
	public Date getCompletedOn() {
		return completedOn;
	}

	/**
	 * Setter for completion date and time.
	 * 
	 * @param completedOn date and time when item was completed by user
	 */
	public void setCompletedOn(Date completedOn) {
		this.completedOn = completedOn;
	}

	/**
	 * Getter for additional data which is tracked for user and this item. 
	 * @return additional data is any was set or <code>null</code> otherwise
	 */
	public String getUserData() {
		return userData;
	}

	/**
	 * Setter for additional data which is tracked for user and this item.
	 * @param userData data to persist
	 */
	public void setUserData(String userData) {
		this.userData = userData;
	}

	/**
	 * Getter for the date when user first opened this item.
	 * @return date
	 */
	public Date getOpenedOn() {
		return openedOn;
	}
	
	/**
	 * Setter for the date when user first opened this item.
	 * @param openedOn date
	 */
	public void setOpenedOn(Date openedOn) {
		this.openedOn = openedOn;
	}
	
	/**
	 * Getter for initialized status. <code>false</code> by default.
	 * 
	 * @return initialized
	 */
	public boolean isInitialized() {
		return initialized;
	}
	
	/**
	 * Setter for initialized flag.
	 * @param initialized initialized flag
	 */
	public void setInitialized(boolean initialized) {
		this.initialized = initialized;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((itemID == null) ? 0 : itemID.hashCode());
		result = prime * result + ((minilesson == null) ? 0 : minilesson.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemStatus other = (ItemStatus) obj;
		if (itemID == null) {
			if (other.itemID != null)
				return false;
		} else if (!itemID.equals(other.itemID))
			return false;
		if (minilesson == null) {
			if (other.minilesson != null)
				return false;
		} else if (!minilesson.equals(other.minilesson))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}
	
}
