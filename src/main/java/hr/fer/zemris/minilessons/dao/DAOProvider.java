package hr.fer.zemris.minilessons.dao;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;

import hr.fer.zemris.minilessons.qstorage.QStorage;
import hr.fer.zemris.minilessons.storage.MLStorage;

/**
 * Binding singleton which returns implementation of data-access layer.
 * 
 * @author marcupic
 *
 */
public class DAOProvider {
	
	/**
	 * Cached object.
	 */
	private static DAO dao;
	/**
	 * Cached object.
	 */
	private static DAOManager daoManager;
	
	static {
		try {
			Properties prop = new Properties();
			InputStream is = DAOProvider.class.getClassLoader().getResourceAsStream("dao.properties");
			if(is==null) {
				throw new IOException("File not found: dao.properties. Could not initialize DAO layer.");
			}
			try {
				prop.load(is);
			} finally {
				is.close();
			}
			String stor = prop.getProperty("ml.storage.impl");
			String qstor = prop.getProperty("ml.qstorage.impl");
			String dmi = prop.getProperty("dao.manager.impl");
			String di = prop.getProperty("dao.impl");
			if(stor!=null) stor = stor.trim();
			if(qstor!=null) qstor = qstor.trim();
			if(dmi!=null) dmi = dmi.trim();
			if(di!=null) di = di.trim();
			if(stor==null || stor.isEmpty()) {
				throw new IOException("ml.storage.impl is not set.");
			}
			if(qstor==null || qstor.isEmpty()) {
				throw new IOException("ml.qstorage.impl is not set.");
			}
			if(dmi==null || dmi.isEmpty()) {
				throw new IOException("dao.manager.impl is not set.");
			}
			if(di==null || di.isEmpty()) {
				throw new IOException("dao.impl is not set.");
			}
			MLStorage storage = (MLStorage)DAOProvider.class.getClassLoader().loadClass(stor).newInstance();
			QStorage qstorage = (QStorage)DAOProvider.class.getClassLoader().loadClass(qstor).newInstance();
			dao = (DAO)DAOProvider.class.getClassLoader().loadClass(di).getConstructor(MLStorage.class,QStorage.class).newInstance(storage,qstorage);
			daoManager = (DAOManager)DAOProvider.class.getClassLoader().loadClass(dmi).newInstance();
		} catch (IOException | InstantiationException | IllegalAccessException | ClassNotFoundException | NoSuchMethodException | InvocationTargetException e) {
			System.out.println("XXXXXXXXXXXXXXXXXXX" + e.getMessage());
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Getter for DAO object.
	 * 
	 * @return DAO object
	 */
	public static DAO getDao() {
		return dao;
	}
	
	/**
	 * Getter for DAO Manager object.
	 * 
	 * @return DAO Manager
	 */
	public static DAOManager getDAOManager() {
		return daoManager;
	}
	
}
