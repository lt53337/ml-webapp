package hr.fer.zemris.minilessons.dao.testing;

import hr.fer.zemris.minilessons.dao.DAOManager;

public class DAOManagerImpl implements DAOManager {

	@Override
	public void open() {
		// No Op.
	}

	@Override
	public void close() {
		// No Op.
	}

	@Override
	public void beginTransaction() {
		// No Op.
	}

	@Override
	public void commitTransaction() {
		// No Op.
	}

	@Override
	public void rollbackTransaction() {
		// No Op.
	}

}
