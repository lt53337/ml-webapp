package hr.fer.zemris.minilessons.dao.testing;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalLong;
import java.util.stream.Collectors;

import hr.fer.zemris.minilessons.dao.DAO;
import hr.fer.zemris.minilessons.dao.DAOException;
import hr.fer.zemris.minilessons.dao.model.CommonQuestionDefinition;
import hr.fer.zemris.minilessons.dao.model.Course;
import hr.fer.zemris.minilessons.dao.model.CourseCollection;
import hr.fer.zemris.minilessons.dao.model.ItemComment;
import hr.fer.zemris.minilessons.dao.model.ItemStatus;
import hr.fer.zemris.minilessons.dao.model.ItemStatusPK;
import hr.fer.zemris.minilessons.dao.model.ItemThread;
import hr.fer.zemris.minilessons.dao.model.JSQuestionDefinition;
import hr.fer.zemris.minilessons.dao.model.JSQuestionInstance;
import hr.fer.zemris.minilessons.dao.model.MLStatWithUserGrade;
import hr.fer.zemris.minilessons.dao.model.MLUserStat;
import hr.fer.zemris.minilessons.dao.model.Minilesson;
import hr.fer.zemris.minilessons.dao.model.MinilessonApproval;
import hr.fer.zemris.minilessons.dao.model.MinilessonStats;
import hr.fer.zemris.minilessons.dao.model.MinilessonStatus;
import hr.fer.zemris.minilessons.dao.model.MinilessonStatusPK;
import hr.fer.zemris.minilessons.dao.model.MinilessonUserGrade;
import hr.fer.zemris.minilessons.dao.model.MinilessonUserSharedData;
import hr.fer.zemris.minilessons.dao.model.User;
import hr.fer.zemris.minilessons.dao.model.UserCourseRole;
import hr.fer.zemris.minilessons.dao.model.UserRegistration;
import hr.fer.zemris.minilessons.dao.model.WSActivity;
import hr.fer.zemris.minilessons.dao.model.WSActivityScore;
import hr.fer.zemris.minilessons.dao.model.WSActivityScorePK;
import hr.fer.zemris.minilessons.dao.model.WSActivitySource;
import hr.fer.zemris.minilessons.dao.model.WSGroup;
import hr.fer.zemris.minilessons.dao.model.WSGrouping;
import hr.fer.zemris.minilessons.dao.model.WSJoinRequestStatus;
import hr.fer.zemris.minilessons.dao.model.WSQDWorkflowState;
import hr.fer.zemris.minilessons.dao.model.WSQuestionDefinition;
import hr.fer.zemris.minilessons.dao.model.WSQuestionInstance;
import hr.fer.zemris.minilessons.dao.model.Wish;
import hr.fer.zemris.minilessons.dao.model.WishUserVote;
import hr.fer.zemris.minilessons.dao.model.WishWithUserVote;
import hr.fer.zemris.minilessons.dao.model.Workspace;
import hr.fer.zemris.minilessons.dao.model.WorkspaceJoinPolicy;
import hr.fer.zemris.minilessons.dao.model.WorkspaceJoinRequest;
import hr.fer.zemris.minilessons.dao.model.WorkspacePage;
import hr.fer.zemris.minilessons.dao.model.XMLQuestionDefinition;
import hr.fer.zemris.minilessons.dao.model.XMLQuestionDefinitionAttachment;
import hr.fer.zemris.minilessons.dao.model.XMLQuestionInstance;
import hr.fer.zemris.minilessons.domain.model.DQuestionInstance;
import hr.fer.zemris.minilessons.domain.model.DWSQuestionDefinition;
import hr.fer.zemris.minilessons.domain.model.DXMLQDef;
import hr.fer.zemris.minilessons.qstorage.QStorage;
import hr.fer.zemris.minilessons.storage.MLStorage;

/**
 * <p>Prototype implementation of DAO which holds all data in memory. Nothing is persisted. When web-app is
 * restarted, all data is lost. This implementation tries to locate <code>testdao.properties</code> in classpath
 * and if found, it reads <code>scan-path</code> property. This property should point to a directory containing
 * minilessons (one subflolder for each minilesson). If this property is not found, implementation scans <code>minilessons</code>
 * folder in current directory (is such folder is present).</p>
 * 
 * <p>This DAO will automatically add a single user with primary key 1 and with help of http filter will automatically log-in this
 * user. Additionally, user specific data will be stored in users http session, allowing that the same user works with this web-app
 * from two different browsers (and hence two sessions) in isolation.</p>
 * 
 * @author marcupic
 */
public class DAOImpl implements DAO {
	
	/**
	 * A map which maps questionInstance ID (primary key) to question instance model DAO object.
	 */
	private Map<Long, JSQuestionInstance> jSQuestionInstances = new HashMap<>();
	/**
	 * A map which maps question ID (primary key) to parsed question model DAO object.
	 */
	private Map<String, CommonQuestionDefinition> commonqdefs = new HashMap<>();
	/**
	 * A map which maps minilesson ID (primary key) to parsed minilesson model DAO object.
	 */
	private Map<String, Minilesson> minilessons = new HashMap<>();
	/**
	 * A map for holding minilesson statistics.
	 */
	private Map<Long, MinilessonStats> minilessonStatsMap = new HashMap<>();
	/**
	 * A map which maps user ID to user model DAO object.
	 */
	private Map<Long, User> users = new HashMap<>();
	/**
	 * A map which maps user ID to minilesson user shared data model DAO object.
	 */
	private Map<Long, MinilessonUserSharedData> mlUsersShareData = new HashMap<>();

	private MLStorage storage;
	private QStorage qstorage;
	
	/**
	 * Constructor.
	 */
	public DAOImpl(MLStorage storage, QStorage qstorage) {
		this.storage = storage;
		this.qstorage = qstorage;
	}

	@Override
	public MLStorage getMLStorage() {
		return storage;
	}

	@Override
	public QStorage getQStorage() {
		return qstorage;
	}
	
	@Override
	public Path getRealPathFor(String minilessonID, String path) {
		return storage.getRealPathFor(minilessonID, path);
	}
	
	@Override
	public Path getRealQPathFor(String questionID, String path) {
		return qstorage.getRealPathFor(questionID, path);
	}
	
	@Override
	public User findUserByID(long id) {
		return users.get(id);
	}
	
	@Override
	public Minilesson findNewestVisibleMinilesson(String publicID) {
		List<Minilesson> list = minilessons.values().stream().filter(m->m.getPublicID().equals(publicID) && m.isVisible() && !m.isArchived()).collect(Collectors.toList());
		if(list.isEmpty()) return null;
		if(list.size()>1) {
			list.sort((o1,o2)->{int r = Long.compare(o2.getVersion(), o1.getVersion()); if(r!=0) return r;  return Long.compare(o2.getMinorVersion(), o1.getMinorVersion()); });
		}
		return list.get(0);
	}

	@Override
	public CommonQuestionDefinition findNewestVisibleQuestionDefinition(String publicID) {
		List<CommonQuestionDefinition> list = commonqdefs.values().stream().filter(m->m.getPublicID().equals(publicID) && m.isVisible() && !m.isArchived() && m.isPublicQuestion()).collect(Collectors.toList());
		if(list.isEmpty()) return null;
		if(list.size()>1) {
			list.sort((o1,o2)->{int r = Long.compare(o2.getVersion(), o1.getVersion()); if(r!=0) return r;  return Long.compare(o2.getMinorVersion(), o1.getMinorVersion()); });
		}
		return list.get(0);
	}
	
	@Override
	public User findUserByUsername(String username, String authDomain) {
		Optional<User> user = users.values().stream().filter(u->u.getUsername().equals(username) && u.getAuthDomain().equals(authDomain)).findAny();
		return user.isPresent() ? user.get() : null;
	}

	@Override
	public User findUserByJMBAG(String jmbag) {
		Optional<User> user = users.values().stream().filter(u->u.getJmbag().equals(jmbag)).findAny();
		return user.isPresent() ? user.get() : null;
	}

	@Override
	public void saveUser(User user) {
		if(user.getId()!=null) throw new RuntimeException("saveUser must be called only for new users which do not have id.");
		OptionalLong maxOpt = users.keySet().stream().mapToLong(u -> u.longValue()).max();
		
		long max = maxOpt.isPresent() ? maxOpt.getAsLong() : 0L;
		user.setId(max+1);
		
		users.put(user.getId(), user);
	}

	@Override
	public void saveMinilessonUserSharedData(MinilessonUserSharedData sharedData) {
		if(sharedData.getId()!=null) throw new RuntimeException("saveMinilessonUserSharedData must be called only for new users which do not have id.");
		OptionalLong maxOpt = mlUsersShareData.keySet().stream().mapToLong(u -> u.longValue()).max();
		
		long max = maxOpt.isPresent() ? maxOpt.getAsLong() : 0L;
		sharedData.setId(max+1);
		
		mlUsersShareData.put(sharedData.getId(), sharedData);
	}
	
	@Override
	public List<Minilesson> listMinilessons() {
		return new ArrayList<>(minilessons.values());
	}

	@Override
	public Minilesson findMinilessonByID(String id) {
		return minilessons.get(id);
	}

	@Override
	public List<Minilesson> listAllMinilessonsForOwner(User user) {
		return minilessons.values().stream().filter(m->m.getOwner().equals(user) && !m.isArchived()).collect(Collectors.toList());
	}

	@Override
	public List<Minilesson> listAllMinilessonVersions(String publicID) {
		return minilessons.values().stream().filter(m->m.getPublicID().equals(publicID)).collect(Collectors.toList());
	}
	
	@Override
	public List<JSQuestionDefinition> listAllJSQuestionDefinitionVersions(String publicID) {
		return commonqdefs.values().stream().filter(m->(m instanceof JSQuestionDefinition) && m.getPublicID().equals(publicID)).map(m->(JSQuestionDefinition)m).collect(Collectors.toList());
	}
	
	@Override
	public List<XMLQuestionDefinition> listAllXMLQuestionDefinitionVersions(String publicID) {
		return commonqdefs.values().stream().filter(m->(m instanceof XMLQuestionDefinition) && m.getPublicID().equals(publicID)).map(m->(XMLQuestionDefinition)m).collect(Collectors.toList());
	}
	
	@Override
	public ItemStatus findItemStatus(Minilesson minilesson, User user, String itemID) {
		Map<ItemStatusPK, ItemStatus> map = privGetItemStatusMap();
		ItemStatusPK key = new ItemStatusPK(minilesson.getId(), user.getId(), itemID);
		return map.get(key);
	}
	
	@Override
	public void saveItemStatus(ItemStatus itemStatus) {
		Map<ItemStatusPK, ItemStatus> map = privGetItemStatusMap();
		ItemStatusPK key = new ItemStatusPK(itemStatus.getMinilesson().getId(), itemStatus.getUser().getId(), itemStatus.getItemID());
		map.put(key, itemStatus);
	}
	
	@Override
	public List<String> listCompletedItems(Minilesson minilesson, User user) {
		Map<ItemStatusPK, ItemStatus> map = privGetItemStatusMap();
		return map.values().stream().filter(it -> it.getMinilesson().equals(minilesson) && it.getUser().equals(user) && it.getCompletedOn()!=null).map(it -> it.getItemID()).collect(Collectors.toList());
	}

	@Override
	public MinilessonStatus findMinilessonStatus(Minilesson minilesson, User user) {
		Map<MinilessonStatusPK, MinilessonStatus> map = privGetMinilessonStatusMap();
		MinilessonStatusPK key = new MinilessonStatusPK(minilesson.getId(), user.getId());
		return map.get(key);
	}

	@Override
	public void saveMinilessonStatus(MinilessonStatus minilessonStatus) {
		Map<MinilessonStatusPK, MinilessonStatus> map = privGetMinilessonStatusMap();
		MinilessonStatusPK key = new MinilessonStatusPK(minilessonStatus.getMinilesson().getId(), minilessonStatus.getUser().getId());
		map.put(key, minilessonStatus);
	}
	
	/**
	 * Helper method which retrieves ItemStatus map from users http session. It is expected that {@link TestingDAOData} will be able to provide
	 * users http session object. 
	 * 
	 * @return ItemStatus map from users session
	 */
	private Map<ItemStatusPK, ItemStatus> privGetItemStatusMap() {
		@SuppressWarnings("unchecked")
		Map<ItemStatusPK, ItemStatus> map = (Map<ItemStatusPK, ItemStatus>)TestingDAOData.get().getAttribute("tdd-itemstatmap");
		if(map == null) {
			map = new HashMap<>();
			TestingDAOData.get().setAttribute("tdd-itemstatmap", map);
		}
		return map;
	}
	
	/**
	 * Helper method which retrieves MinilessonStatus map from users http session. It is expected that {@link TestingDAOData} will be able to provide
	 * users http session object. 
	 * 
	 * @return MinilessonStatus map from users session
	 */
	private Map<MinilessonStatusPK, MinilessonStatus> privGetMinilessonStatusMap() {
		@SuppressWarnings("unchecked")
		Map<MinilessonStatusPK, MinilessonStatus> map = (Map<MinilessonStatusPK, MinilessonStatus>)TestingDAOData.get().getAttribute("tdd-mlstatmap");
		if(map == null) {
			map = new HashMap<>();
			TestingDAOData.get().setAttribute("tdd-mlstatmap", map);
		}
		return map;
	}
	
	@Override
	public void saveMinilesson(Minilesson minilesson) {
		minilessons.put(minilesson.getId(), minilesson);
	}
	
	@Override
	public void saveJSQuestionDefinition(JSQuestionDefinition jSQuestionDefinition) {
		commonqdefs.put(jSQuestionDefinition.getId(), jSQuestionDefinition);
	}
	
	@Override
	public void saveMinilessonStats(MinilessonStats minilessonStats) {
		if(minilessonStats.getId()!=null) throw new RuntimeException("saveMinilessonStats must be called only for new minilessonStats which do not have id.");
		OptionalLong maxOpt = minilessonStatsMap.keySet().stream().mapToLong(s -> s.longValue()).max();
		
		long max = maxOpt.isPresent() ? maxOpt.getAsLong() : 0L;
		minilessonStats.setId(max+1);
		
		minilessonStatsMap.put(minilessonStats.getId(), minilessonStats);
	};

	@Override
	public List<MinilessonStats> listAllVisibleMLStats() {
		return minilessonStatsMap.values().stream().filter(s->{
			return !s.getMinilesson().isArchived() && s.getMinilesson().isVisible() && 
					(s.getMinilesson().getApproval()==MinilessonApproval.APPROVED || s.getMinilesson().getApproval()==MinilessonApproval.SOFT_WAITING);
		}).sorted((s1,s2)->s1.getMinilesson().getUploadedOn().compareTo(s2.getMinilesson().getUploadedOn())).collect(Collectors.toList());
	}

	@Override
	public List<MinilessonStats> listMLStatsForCourseMinilessons(Course course) {
		return minilessonStatsMap.values().stream().filter(s->{
			return course.getMinilessons().contains(s.getMinilesson()) && !s.getMinilesson().isArchived() && s.getMinilesson().isVisible() && 
					(s.getMinilesson().getApproval()==MinilessonApproval.APPROVED || s.getMinilesson().getApproval()==MinilessonApproval.SOFT_WAITING);
		}).sorted((s1,s2)->s1.getMinilesson().getUploadedOn().compareTo(s2.getMinilesson().getUploadedOn())).collect(Collectors.toList());
	}
	
	@Override
	public List<MinilessonStats> listMLStatsForMinilesson(Minilesson ml) {
		return minilessonStatsMap.values().stream().filter(s->{
			return ml.equals(s.getMinilesson()) && !s.getMinilesson().isArchived() && s.getMinilesson().isVisible() && 
					(s.getMinilesson().getApproval()==MinilessonApproval.APPROVED || s.getMinilesson().getApproval()==MinilessonApproval.SOFT_WAITING);
		}).sorted((s1,s2)->s1.getMinilesson().getUploadedOn().compareTo(s2.getMinilesson().getUploadedOn())).collect(Collectors.toList());
	}
	
	@Override
	public List<MinilessonStats> listAllVisibleMLStatsForOwner(User owner) {
		return minilessonStatsMap.values().stream().filter(s->{
			return s.getMinilesson().getOwner().equals(owner) && !s.getMinilesson().isArchived() && s.getMinilesson().isVisible() && 
					(s.getMinilesson().getApproval()==MinilessonApproval.APPROVED || s.getMinilesson().getApproval()==MinilessonApproval.SOFT_WAITING);
		}).sorted((s1,s2)->s1.getMinilesson().getUploadedOn().compareTo(s2.getMinilesson().getUploadedOn())).collect(Collectors.toList());
	}
	
	@Override
	public List<String> findUnprocessedMinilessons() {
		// Not supported in testing mode: always returns empty list.
		return new ArrayList<>();
	}
	
	@Override
	public List<MinilessonUserGrade> findMinilessonUserGrades(Long userID) {
		return null;
	}
	
	@Override
	public MinilessonUserGrade findMinilessonUserGrades(Minilesson minilesson, User user) {
		// Not supported in testing mode: always returns null.
		return null;
	}

	@Override
	public List<MinilessonUserGrade> findMinilessonUserGrades(String minilessonID) {
		// Not supported in testing mode: always returns empty list.
		return new ArrayList<>();
	}
	
	@Override
	public List<MLStatWithUserGrade> listAllVisibleMLStatsEx(Long userID) {
		User user = findUserByID(userID);
		List<MinilessonStats> list1 = listAllVisibleMLStats();
		List<MLStatWithUserGrade> result = new ArrayList<>();
		list1.forEach(ms->result.add(new MLStatWithUserGrade(ms, null, findMinilessonStatus(ms.getMinilesson(), user))));
		return result;
	}
	
	@Override
	public List<MLStatWithUserGrade> listMLStatsExForCourseMinilessons(Long userID, Course course) {
		User user = findUserByID(userID);
		List<MinilessonStats> list1 = listAllVisibleMLStats().stream().filter(s->course.getMinilessons().contains(s.getMinilesson())).collect(Collectors.toList());
		List<MLStatWithUserGrade> result = new ArrayList<>();
		list1.forEach(ms->result.add(new MLStatWithUserGrade(ms, null, findMinilessonStatus(ms.getMinilesson(), user))));
		return result;
	}
	
	@Override
	public List<MLStatWithUserGrade> listMLStatsExForMinilesson(Long userID, Minilesson ml) {
		User user = findUserByID(userID);
		List<MinilessonStats> list1 = listAllVisibleMLStats().stream().filter(s->ml.equals(s.getMinilesson())).collect(Collectors.toList());
		List<MLStatWithUserGrade> result = new ArrayList<>();
		list1.forEach(ms->result.add(new MLStatWithUserGrade(ms, null, findMinilessonStatus(ms.getMinilesson(), user))));
		return result;
	}
	
	@Override
	public void saveMinilessonUserGrade(MinilessonUserGrade mug) {
		throw new DAOException("Saving of MinilessonUserGrade is not supported by this implementation.");
	}
	
	@Override
	public MinilessonStats findMinilessonStats(Minilesson minilesson) {
		Optional<MinilessonStats> opt = minilessonStatsMap.values().stream().filter(s->s.getMinilesson().equals(minilesson)).findAny();
		return opt.isPresent() ? opt.get() : null;
	}
	
	@Override
	public List<Long> findUnprocessedWishes() {
		// Not supported in testing mode: always returns empty list.
		return new ArrayList<>();
	}
	
	@Override
	public List<WishUserVote> findWishVotes(Long wishID) {
		// Not supported in testing mode: always returns empty list.
		return new ArrayList<>();
	}
	
	@Override
	public Wish findWish(Long id) {
		// Not supported in testing mode: always returns null.
		return null;
	}
	
	@Override
	public void saveNewWish(Wish wish) {
		throw new DAOException("Saving of Wish is not supported by this implementation.");
	}
	
	@Override
	public List<Wish> listAllWishes() {
		// Not supported in testing mode: always returns empty list.
		return new ArrayList<>();
	}
	
	@Override
	public List<WishWithUserVote> listAllWishesEx(Long userID) {
		// Not supported in testing mode: always returns empty list.
		return new ArrayList<>();
	}
	
	@Override
	public WishUserVote findWishUserVote(Long wishID, Long userID) {
		// Not supported in testing mode: always returns null.
		return null;
	}
	
	@Override
	public void saveWishUserVote(WishUserVote wuv) {
		throw new DAOException("Saving of WishUserVote is not supported by this implementation.");
	}
	
	@Override
	public void saveUserRegistration(UserRegistration ureg) {
		throw new DAOException("Saving of UserRegistration is not supported by this implementation.");
	}
	
	@Override
	public int countRegistrationsForDate(String date) {
		return 0;
	}
	
	@Override
	public int countRegistrationsForDate(String date, String ip) {
		return 0;
	}
	
	@Override
	public UserRegistration findUserRegistration(Long id) {
		return null;
	}
	
	@Override
	public List<MLUserStat> getUserMinilessonBasicStats(String minilessonID) {
		return Collections.emptyList();
	}
	
	@Override
	public ItemThread findItemThread(Long threadID) {
		// Not supported in testing mode: always returns null.
		return null;
	}
	
	@Override
	public List<ItemComment> listItemComments(Long threadID) {
		// Not supported in testing mode: always returns empty list.
		return Collections.emptyList();
	}
	
	@Override
	public List<ItemThread> listMinilessonItemThreads(String minilessonID, String itemID) {
		// Not supported in testing mode: always returns empty list.
		return Collections.emptyList();
	}
	
	@Override
	public void saveItemThread(ItemThread thread) {
		throw new DAOException("Saving of ItemThread is not supported by this implementation.");
	}
	
	@Override
	public void saveItemComment(ItemComment itemComment) {
		throw new DAOException("Saving of ItemComment is not supported by this implementation.");
	}
	
	@Override
	public ItemComment findItemComment(Long commentID) {
		// Not supported in testing mode: always returns null.
		return null;
	}
	
	@Override
	public void saveJSQuestionInstance(JSQuestionInstance jSQuestionInstance) {
		if(jSQuestionInstance.getId()==null) {
			Optional<Long> lastID = jSQuestionInstances.keySet().stream().max(Long::compare);
			Long newID = lastID.isPresent() ? lastID.get()+1 : 1L;
			jSQuestionInstance.setId(newID);
		}
		jSQuestionInstances.put(jSQuestionInstance.getId(), jSQuestionInstance);
	}
	
	@Override
	public JSQuestionDefinition findJSQuestionDefinitionByID(String id) {
		return (JSQuestionDefinition)commonqdefs.get(id);
	}

	@Override
	public CommonQuestionDefinition findCommonQuestionDefinitionByID(String id) {
		return commonqdefs.get(id);
	}
	
	@Override
	public JSQuestionInstance findJSQuestionInstanceByID(Long id) {
		return jSQuestionInstances.get(id);
	}
	
	@Override
	public List<JSQuestionDefinition> findVisibleJSQuestionDefinitions() {
		return commonqdefs.values().stream().filter(q->(q instanceof JSQuestionDefinition) && !q.isArchived() && q.isVisible()).map(q->(JSQuestionDefinition)q).collect(Collectors.toList());
	}
	
	@Override
	public List<JSQuestionDefinition> findVisiblePublicJSQuestionDefinitions(User owner) {
		return commonqdefs.values().stream().filter(q -> (q instanceof JSQuestionDefinition) && !q.isArchived() && q.isVisible() && (q.isPublicQuestion() || q.getOwner().equals(owner))).map(q->(JSQuestionDefinition)q).collect(Collectors.toList());
	}
	
	@Override
	public List<CommonQuestionDefinition> findVisiblePublicCommonQuestionDefinitions(User owner) {
		return commonqdefs.values().stream().filter(q -> !q.isArchived() && q.isVisible() && (q.isPublicQuestion() || q.getOwner().equals(owner))).collect(Collectors.toList());
	}

	private Map<String, Course> courseMap = new HashMap<>();
	private Map<String, CourseCollection> courseCollectionMap = new HashMap<>();
	private List<UserCourseRole> ucrList = new ArrayList<>();
	
	@Override
	public void saveCourse(Course course) {
		courseMap.put(course.getId(), course);
	}
	
	@Override
	public void saveCourseCollection(CourseCollection cc) {
		courseCollectionMap.put(cc.getId(), cc);
	}
	
	@Override
	public void saveUserCourseRole(UserCourseRole ucr) {
		Optional<UserCourseRole> o = ucrList.stream().filter(ucr::equals).findAny();
		if(!o.isPresent()) {
			ucrList.add(ucr);
			return;
		}
		throw new RuntimeException("UserCourseRole already saved.");
	}
	
	@Override
	public List<CourseCollection> findAllCourseCollections() {
		return new ArrayList<>(courseCollectionMap.values());
	}
	
	@Override
	public Course findCourseByID(String id) {
		return courseMap.get(id);
	}
	
	@Override
	public CourseCollection findCourseCollectionByID(String id) {
		return courseCollectionMap.get(id);
	}
	
	@Override
	public List<UserCourseRole> findUserCourseRoleFor(Course course) {
		return ucrList.stream().filter(ucr->ucr.getCourse().equals(course)).collect(Collectors.toList());
	}
	
	@Override
	public List<UserCourseRole> findUserCourseRoleFor(User user) {
		return ucrList.stream().filter(ucr->ucr.getUser().equals(user)).collect(Collectors.toList());
	}
	
	@Override
	public List<Course> listAllCourses() {
		return new ArrayList<>(courseMap.values());
	}
	
	private Map<Long,Workspace> workspaces = new HashMap<>();
	private Map<Long,WorkspaceJoinRequest> workspaceJoinRequests = new HashMap<>(); 
	
	@Override
	public void saveWorkspace(Workspace ws) {
		if(ws.getId()==null) {
			Optional<Long> lastID = workspaces.keySet().stream().max(Long::compare);
			Long newID = lastID.isPresent() ? lastID.get()+1 : 1L;
			ws.setId(newID);
		}
		workspaces.put(ws.getId(), ws);
	}

	@Override
	public void saveWorkspaceJoinRequest(WorkspaceJoinRequest req) {
		if(req.getId()==null) {
			Optional<Long> lastID = workspaceJoinRequests.keySet().stream().max(Long::compare);
			Long newID = lastID.isPresent() ? lastID.get()+1 : 1L;
			req.setId(newID);
		}
		workspaceJoinRequests.put(req.getId(), req);
	}
	
	@Override
	public Workspace findWorkspaceByID(Long id) {
		return workspaces.get(id);
	}
	
	@Override
	public WorkspaceJoinRequest findWorkspaceJoinRequestByID(Long id) {
		return workspaceJoinRequests.get(id);
	}
	
	@Override
	public List<Workspace> findJoinableWorkspaces(User user) {
		return workspaces.values().stream().filter(ws->!ws.getUsers().contains(user) && ws.getJoinPolicy()!=WorkspaceJoinPolicy.CLOSED).collect(Collectors.toList());
	}
	
	@Override
	public List<Workspace> findUserWorkspaces(User user) {
		return workspaces.values().stream().filter(ws->ws.getUsers().contains(user) || ws.getOwner().equals(user)).collect(Collectors.toList());
	}
	
	@Override
	public List<WorkspaceJoinRequest> findWorkspaceJoinRequests(Workspace ws, User user) {
		return workspaceJoinRequests.values().stream().filter(req->req.getWorkspace().equals(ws) && req.getUser().equals(user)).collect(Collectors.toList());
	}
	
	@Override
	public void removeWorkspaceJoinRequest(WorkspaceJoinRequest req) {
		workspaceJoinRequests.remove(req.getId());
	}
	
	@Override
	public List<WorkspaceJoinRequest> findUnsolvedWorkspaceJoinRequests(Workspace ws) {
		return workspaceJoinRequests.values().stream()
				.filter(r->r.getWorkspace().equals(ws) && r.getStatus()==WSJoinRequestStatus.OPEN)
				.collect(Collectors.toList());
	}
	
	private Map<Long,WSGroup> wsgroups = new HashMap<>();
	private Map<Long,WSGrouping> wsgrouping = new HashMap<>();
	
	@Override
	public void saveWSGroup(WSGroup gr) {
		if(gr.getId()==null) {
			Optional<Long> lastID = wsgroups.keySet().stream().max(Long::compare);
			Long newID = lastID.isPresent() ? lastID.get()+1 : 1L;
			gr.setId(newID);
		}
		wsgroups.put(gr.getId(), gr);
	}
	
	@Override
	public void saveWSGrouping(WSGrouping wsg) {
		if(wsg.getId()==null) {
			Optional<Long> lastID = wsgrouping.keySet().stream().max(Long::compare);
			Long newID = lastID.isPresent() ? lastID.get()+1 : 1L;
			wsg.setId(newID);
		}
		wsgrouping.put(wsg.getId(), wsg);
	}
	
	@Override
	public WSGroup findWSGroupByID(Long id) {
		return wsgroups.get(id);
	}
	
	@Override
	public WSGrouping findWSGroupingByID(Long id) {
		return wsgrouping.get(id);
	}
	
	@Override
	public List<WSGrouping> findGroupingsForWorkspace(Workspace ws) {
		return wsgrouping.values().stream().filter(g->g.getWorkspace().equals(ws)).collect(Collectors.toList());
	}
	
	@Override
	public List<WSGroup> findWSGroupsForWSGrouping(WSGrouping wsg, boolean onlyActive) {
		if(onlyActive) {
			return wsgroups.values().stream().filter(g->g.getGrouping().equals(wsg) && g.isActive()).collect(Collectors.toList());
		} else {
			return wsgroups.values().stream().filter(g->g.getGrouping().equals(wsg)).collect(Collectors.toList());
		}
	}
	
	@Override
	public List<WSGroup> findActiveWSGroupsForWSGroupingAndUser(WSGrouping wsg, User user) {
		return wsgroups.values().stream().filter(g->g.getGrouping().equals(wsg) && g.isActive() && g.getUsers().contains(user)).collect(Collectors.toList());
	}
	
	@Override
	public List<WSGroup> findEmptyActiveNonfixedWSGroups(WSGrouping gr) {
		return wsgroups.values().stream().filter(g->g.getGrouping().equals(gr) && g.isActive() && !g.isFixed() && g.getUsers().isEmpty()).collect(Collectors.toList());
	}

	private Map<Long,WSActivity> wsactivities = new HashMap<>();

	@Override
	public List<WSActivity> findActivitiesForWorkspace(Workspace ws) {
		return wsactivities.values().stream().filter(a->a.getWorkspace().equals(ws)).collect(Collectors.toList());
	}
	
	@Override
	public void saveWSActivity(WSActivity activity) {
		if(activity.getId()==null) {
			Optional<Long> lastID = wsactivities.keySet().stream().max(Long::compare);
			Long newID = lastID.isPresent() ? lastID.get()+1 : 1L;
			activity.setId(newID);
		}
		wsactivities.put(activity.getId(), activity);
	}

	@Override
	public void saveXMLQuestionDefinition(XMLQuestionDefinition def) {
		commonqdefs.put(def.getId(), def);
	}
	
	@Override
	public List<DXMLQDef> listXMLQuestionsForOwner(User user, String lang) {
		return commonqdefs.values().stream().filter(qd->(qd instanceof XMLQuestionDefinition) && qd.getOwner().equals(user) && qd.getTitles().containsKey(lang)).map(qd->new DXMLQDef(qd.getId(), qd.getTitles().get(lang), qd.getUploadedOn(), qd.getOwner().getId(), qd.getOwner().getLastName(), qd.getOwner().getFirstName(), qd.getOwner().getAuthDomain())).collect(Collectors.toList());
	}

	private Map<Long, WSQuestionDefinition> wsqdefs = new HashMap<>();
	
	@Override
	public void saveWSQuestionDefinition(WSQuestionDefinition wsqd) {
		if(wsqd.getId()==null) {
			Optional<Long> lastID = wsqdefs.keySet().stream().max(Long::compare);
			Long newID = lastID.isPresent() ? lastID.get()+1 : 1L;
			wsqd.setId(newID);
		}
		wsqdefs.put(wsqd.getId(), wsqd);
	}
	
	@Override
	public WSActivity findWSActivityByID(Long id) {
		return wsactivities.get(id);
	}
	
	@Override
	public XMLQuestionDefinition findXMLQuestionDefinition(String id) {
		return (XMLQuestionDefinition)commonqdefs.get(id);
	}
	
	@Override
	public List<DWSQuestionDefinition> listDWSQuestionDefinitions(Workspace ws, WorkspacePage pg, String lang) {
		return wsqdefs.values().stream().filter(qd->qd.getWorkspace().equals(ws) && qd.getSubpage().equals(pg)).map(q->new DWSQuestionDefinition(q.getId(), q.getTitles().getOrDefault(lang, "???"), q.getCreatedAt(), q.getMaxScore(), q.getWorkflow(), q.getWorkflowCurrentState(), q.getSubpage(), q.getGrouping()==null ? null : q.getGrouping().getTitles().getOrDefault(lang, "????"), q.getActivity()==null ? null : q.getActivity().getTitles().getOrDefault(lang, "????"), q.isGroupAssignment())).collect(Collectors.toList());
	}
	
	@Override
	public WSQuestionDefinition findWSQuestionDefinition(Long id) {
		return wsqdefs.get(id);
	}
	
	@Override
	public List<WSQuestionDefinition> findActiveQuestions(Workspace ws, WorkspacePage subpage) {
		List<WSQDWorkflowState> okstates = Arrays.asList(WSQDWorkflowState.OPEN, WSQDWorkflowState.VERIFY, WSQDWorkflowState.INSPECT, WSQDWorkflowState.AUTO, WSQDWorkflowState.AUTOREP, WSQDWorkflowState.AUTOSUB);
		return wsqdefs.values().stream()
				.filter(qd->qd.getWorkspace().equals(ws) && qd.getSubpage().equals(subpage) && okstates.contains(qd.getWorkflowCurrentState()))
				.collect(Collectors.toList());
	}
	
	private Map<Long,WSQuestionInstance> wsqiMap = new HashMap<>();
	
	@Override
	public List<WSQuestionInstance> findWSQuestionInstances(WSQuestionDefinition qd, User u) {
		return wsqiMap.values().stream().filter(qi->qi.getDefinition().equals(qd)&&u.equals(qi.getUser())).collect(Collectors.toList());
	}
	
	@Override
	public List<WSQuestionInstance> findWSQuestionInstances(WSQuestionDefinition qd, WSGroup g) {
		return wsqiMap.values().stream().filter(qi->qi.getDefinition().equals(qd)&&g.equals(qi.getGroup())).collect(Collectors.toList());
	}
	
	@Override
	public void saveWSQuestionInstance(WSQuestionInstance wsqi) {
		if(wsqi.getId()==null) {
			Optional<Long> lastID = wsqiMap.keySet().stream().max(Long::compare);
			Long newID = lastID.isPresent() ? lastID.get()+1 : 1L;
			wsqi.setId(newID);
		}
		wsqiMap.put(wsqi.getId(), wsqi);
	}	

	private Map<Long,XMLQuestionInstance> xmlqiMap = new HashMap<>();

	@Override
	public void saveXMLQuestionInstance(XMLQuestionInstance qi) {
		if(qi.getId()==null) {
			Optional<Long> lastID = xmlqiMap.keySet().stream().max(Long::compare);
			Long newID = lastID.isPresent() ? lastID.get()+1 : 1L;
			qi.setId(newID);
		}
		xmlqiMap.put(qi.getId(), qi);
	}
	
	@Override
	public WSQuestionInstance findWSQuestionInstanceByID(Long id) {
		return wsqiMap.get(id);
	}
	
	@Override
	public XMLQuestionInstance findXMLQuestionInstanceByID(Long id) {
		return xmlqiMap.get(id);
	}
	
	@Override
	public List<WSQuestionInstance> findWSQuestionInstances(WSQuestionDefinition qd, boolean additional) {
		return wsqiMap.values().stream().filter(qi->qi.getDefinition().equals(qd)&&qi.isAdditional()==additional).collect(Collectors.toList());
	}

	Map<Long, WSActivitySource> wssourcesMap = new HashMap<>();
	Map<WSActivityScorePK, WSActivityScore> wsscoresMap = new HashMap<>();
	
	@Override
	public void saveWSActivityScore(WSActivityScore score) {
		wsscoresMap.put(score.getId(), score);
	}
	
	@Override
	public void saveWSActivitySource(WSActivitySource src) {
		if(src.getId()==null) {
			Optional<Long> lastID = wssourcesMap.keySet().stream().max(Long::compare);
			Long newID = lastID.isPresent() ? lastID.get()+1 : 1L;
			src.setId(newID);
		}
		wssourcesMap.put(src.getId(), src);
	}

	@Override
	public WSActivityScore findWSActivityScore(WSActivityScorePK pk) {
		return wsscoresMap.get(pk);
	}
	
	@Override
	public List<WSActivityScore> findWSActivityScores(Workspace ws, User user) {
		return wsscoresMap.values().stream()
				.filter(s->s.getSource().getActivity().getWorkspace().equals(ws) && s.getUser().equals(user))
				.collect(Collectors.toList());
	}
	
	@Override
	public List<WSActivitySource> findAllWSActivitySources(Workspace ws) {
		return wssourcesMap.values().stream().filter(s->s.getActivity().getWorkspace().equals(ws)).collect(Collectors.toList());
	}
	
	private Map<Long, XMLQuestionDefinitionAttachment> xmlattMap = new HashMap<>();

	@Override
	public void saveXMLQuestionDefinitionAttachment(XMLQuestionDefinitionAttachment a) {
		if(a.getId()==null) {
			Optional<Long> lastID = xmlattMap.keySet().stream().max(Long::compare);
			Long newID = lastID.isPresent() ? lastID.get()+1 : 1L;
			a.setId(newID);
		}
		xmlattMap.put(a.getId(), a);
	}
	
	@Override
	public void removeXMLQuestionDefinitionAttachment(XMLQuestionDefinitionAttachment a) {
		xmlattMap.remove(a.getId());
	}
	
	@Override
	public XMLQuestionDefinitionAttachment findXMLQuestionDefinitionAttachment(XMLQuestionDefinition definition, String filename) {
		List<XMLQuestionDefinitionAttachment> list = xmlattMap.values().stream().filter(a->a.getQuestion().equals(definition) && a.getName().equals(filename)).collect(Collectors.toList());
		if(list.size()!=1) return null;
		return list.get(0);
	}
	
	@Override
	public List<XMLQuestionDefinitionAttachment> findAllXMLQuestionDefinitionAttachments(
			XMLQuestionDefinition definition) {
		List<XMLQuestionDefinitionAttachment> list = xmlattMap.values().stream().filter(a->a.getQuestion().equals(definition)).collect(Collectors.toList());
		return list;
	}
	
	@Override
	public List<WSActivityScore> listAllWorkspaceValidActivityUserScores(Workspace ws) {
		return wsscoresMap.values().stream().filter(s->!s.getSource().isInvalid() && !s.isPending() && s.getSource().getActivity().getWorkspace().equals(ws)).collect(Collectors.toList());
	}
	
	@Override
	public List<JSQuestionDefinition> listAllJSQuestionsForOwner(User owner) {
		return commonqdefs.entrySet().stream().filter(e->(e.getValue() instanceof JSQuestionDefinition) && e.getValue().getOwner().equals(owner)).map(e->(JSQuestionDefinition)e.getValue()).collect(Collectors.toList());
	}
	
	@Override
	public List<CommonQuestionDefinition> listAllCommonQuestionsForOwner(User owner) {
		return commonqdefs.entrySet().stream().filter(e->e.getValue().getOwner().equals(owner)).map(e->e.getValue()).collect(Collectors.toList());
	}
	
	@Override
	public List<DQuestionInstance> findPublicUsersJSQuestionInstances(User user, JSQuestionDefinition qd) {
		return jSQuestionInstances.values().stream()
			.filter(qi->qi.getQuestion().equals(qd) && qi.getUser().equals(user) && qi.getContext()==null)
			.map(qi->new DQuestionInstance(qi.getId(), qi.getStatus(), qi.getCorrectness(), qi.isSolved(), qi.getConfigName(), qi.getCreatedOn(), qi.getEvaluatedOn()))
			.collect(Collectors.toList());
	}
	
	@Override
	public List<DQuestionInstance> findPublicUsersXMLQuestionInstances(User user, XMLQuestionDefinition qd) {
		return xmlqiMap.values().stream()
			.filter(qi->qi.getQuestion().equals(qd) && qi.getUser().equals(user) && qi.getContext()==null)
			.map(qi->new DQuestionInstance(qi.getId(), qi.getStatus(), qi.getCorrectness(), qi.isSolved(), qi.getConfigName(), qi.getCreatedOn(), qi.getEvaluatedOn()))
			.collect(Collectors.toList());
	}
}
