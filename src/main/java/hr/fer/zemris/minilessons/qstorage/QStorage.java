package hr.fer.zemris.minilessons.qstorage;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

/**
 * This is abstraction of storage for question files. Storage can be read-only.
 * 
 * @author marcupic
 */
public interface QStorage {
	/**
	 * Returns a path into the filesystem where question-relative
	 * resource can be found.
	 * 
	 * @param questionKey unique identifier of question; depending on DAO implementation this could be question ID
	 * @param path question-relative path of some resource; if <code>null</code>, path to question root directory will be returned
	 * @return path on filesystem for requested resource
	 */
	Path getRealPathFor(String questionKey, String path);
	/**
	 * Get a list of all question root directories which are found on filesystem.
	 * 
	 * @return list of roots
	 * @throws IOException if folders could not be listed
	 */
	List<Path> allQuestionRoots() throws IOException;
	/**
	 * <p>Storage engine will copy given package from temporary location which is
	 * given as <code>srcDir</code>. After that, temporary location can be deleted.</p>
	 * 
	 * @param questionID question identifier for package being copied
	 * @param srcDir source directory (root directory of package to be deployed)
	 * @throws IOException if problem occur while persisting
	 */
	void persistQuestionPackage(String questionID, Path srcDir) throws IOException;
	/**
	 * <p>Storage engine will copy given package from temporary location which is
	 * given as <code>srcDir</code>. After that, temporary location can be deleted.
	 * Since this operation assumes that previous version of files already exists,
	 * to prevent errors in middle of copying, implementation is expected to first
	 * copy content into new temporary location on the same filesystem as is the repository
	 * and then delete old directory and rename fresh copy (or some similar strategy).</p>
	 * 
	 * @param questionID question identifier for package being copied
	 * @param srcDir source directory (root directory of package to be deployed)
	 * @throws IOException if problem occur while persisting
	 */
	void updateQuestionPackage(String questionID, Path srcDir) throws IOException;
}
