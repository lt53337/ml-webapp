package hr.fer.zemris.minilessons.xmlmodel;

import java.util.List;

public class XMLQuestion extends XMLItem {

	private List<XMLOption> optionKeys;
	private String type;
	private String handler;
	private List<XMLPage> templates;
	private List<XMLView> views;
	
	public List<XMLView> getViews() {
		return views;
	}
	
	public String getType() {
		return type;
	}
	
	public String getHandler() {
		return handler;
	}

	public List<XMLOption> getOptionKeys() {
		return optionKeys;
	}
	
	public List<XMLPage> getTemplates() {
		return templates;
	}
}
