package hr.fer.zemris.minilessons.xmlmodel;

import java.util.List;

public class XMLView {

	private String name;
	private String renderPageId;
	private List<XMLOffer> offers;
	
	public String getName() {
		return name;
	}
	
	public List<XMLOffer> getOffers() {
		return offers;
	}
	
	public String getRenderPageId() {
		return renderPageId;
	}
}
