package hr.fer.zemris.minilessons.xmlmodel;

import java.util.List;

public class XMLAbout {
	
	private List<XMLString> title;
	private List<XMLAuthor> authors;
	private List<XMLString> tags;
	private List<XMLLanguage> supportedLanguages;
	
	public List<XMLAuthor> getAuthors() {
		return authors;
	}
	
	public List<XMLString> getTags() {
		return tags;
	}
	
	public List<XMLString> getTitle() {
		return title;
	}

	public List<XMLLanguage> getSupportedLanguages() {
		return supportedLanguages;
	}
	
}
