/**
 * This package contains classes which are used by xstream parser
 * to convert <code>descriptor.xml</code> into Java objects. The structure
 * of these classes follow the specification of <code>descriptor.xml</code>
 * and will be changed as this specification is changed. For this reason,
 * the classes in this package are not documented to avoid mistaking this
 * documentation as normative specification of <code>descriptor.xml</code>
 * format.
 */
package hr.fer.zemris.minilessons.xmlmodel;

