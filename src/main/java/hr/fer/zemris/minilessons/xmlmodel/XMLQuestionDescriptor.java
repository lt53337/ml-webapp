package hr.fer.zemris.minilessons.xmlmodel;

import java.util.List;

public class XMLQuestionDescriptor {

	private String uid;
	private int apiLevel = 1;
	private int version = 1;
	private int minorVersion;
	private XMLAbout about;
	private List<XMLConfiguration> configurations;
	
	public XMLAbout getAbout() {
		return about;
	}
	
	public String getUid() {
		return uid;
	}
	
	public int getVersion() {
		return version;
	}
	
	public int getMinorVersion() {
		return minorVersion;
	}

	public List<XMLConfiguration> getConfigurations() {
		return configurations;
	}
	
	public int getApiLevel() {
		return apiLevel;
	}
	
}
