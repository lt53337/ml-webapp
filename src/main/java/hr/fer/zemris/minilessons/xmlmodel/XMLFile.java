package hr.fer.zemris.minilessons.xmlmodel;

public class XMLFile {

	private String name;
	private String lang;
	private String format;
	
	public String getFormat() {
		return format;
	}
	public String getLang() {
		return lang;
	}
	
	public String getName() {
		return name;
	}
}
