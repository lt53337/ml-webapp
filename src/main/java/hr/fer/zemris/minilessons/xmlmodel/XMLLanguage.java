package hr.fer.zemris.minilessons.xmlmodel;

public class XMLLanguage {
	
	private String lang;

	public String getLang() {
		return lang;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((lang == null) ? 0 : lang.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof XMLLanguage))
			return false;
		XMLLanguage other = (XMLLanguage) obj;
		if (lang == null) {
			if (other.lang != null)
				return false;
		} else if (!lang.equals(other.lang))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return lang;
	}
}
