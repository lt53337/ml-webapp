package hr.fer.zemris.minilessons.xmlmodel;

public class XMLOffer {

	private String key;
	private String action;
	private boolean completed;
	
	public String getKey() {
		return key;
	}
	
	public String getAction() {
		return action;
	}
	
	public boolean isCompleted() {
		return completed;
	}
}
