package hr.fer.zemris.minilessons.xmlmodel;

import hr.fer.zemris.minilessons.dao.model.QuestionConfigVariability;

public class XMLConfiguration {
	private String name;
	private QuestionConfigVariability variability;
	
	public XMLConfiguration() {
	}

	public XMLConfiguration(String name, QuestionConfigVariability variability) {
		super();
		this.name = name;
		this.variability = variability;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public QuestionConfigVariability getVariability() {
		return variability;
	}
	public void setVariability(QuestionConfigVariability variability) {
		this.variability = variability;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((variability == null) ? 0 : variability.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof XMLConfiguration))
			return false;
		XMLConfiguration other = (XMLConfiguration) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (variability != other.variability)
			return false;
		return true;
	}
}
