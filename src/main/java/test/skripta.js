/**
 * 
 */

function questionInitialize() {
	ispisivac.println("Pozdrav iz skripte!");
	ispisivac.println(userData.drugi.ime);
	userData.novi = "Perica";
}

function f2(vid) {
	  res = {};
	  res.options = ['cont'];
	  res.questionState = {enteredText: 12};
	  res.view = {action: "page:4-1"};
	  return JSON.stringify(res);
}

function f3() {
	return {
		kljuc1: 4,
		kljuc2: "pero",
		kljuc3: [1,2,3],
		kljuc4: [{k: 1, v:2},{k: 3, v:4}],
		kljuc5: {
			a1: [1,2,3],
			a2: [{i: 1},{i: 2},{i: 3}]
		}
	};
}

function f4() {
	userData.x = ["a","b","c"];
	userData.y = {};
}

function f5() {
	var res = JSON.stringify({a:[{a:1,b: ["x","y","z"]},2,3]});
	print("Iz javascripta: ", res)
	return res;
}