package test;

import org.pegdown.PegDownProcessor;

public class TestMD1 {

	public static void main(String[] args) {
		PegDownProcessor pdp = new PegDownProcessor();
		System.out.println(pdp.markdownToHtml("Ovo je proba\r\n==================\n\nJedan <i>redak</i> koji je italic."));
	}
	
}
