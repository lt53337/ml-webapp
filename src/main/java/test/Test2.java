package test;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import javax.script.Bindings;
import javax.script.Invocable;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Test2 {

	public static void main(String[] args) throws ScriptException, NoSuchMethodException, IOException {
		ScriptEngineManager scEngMan = new ScriptEngineManager();
		ScriptEngine jsengine = scEngMan.getEngineByName("nashorn");
		//Compilable jsengineCompilable = (Compilable)jsengine;
		Invocable invocable = (Invocable)jsengine;
		//CompiledScript compiledScript = jsengineCompilable.compile(new InputStreamReader(Test2.class.getResourceAsStream("skripta.js"), StandardCharsets.UTF_8));

		Map<String, Object> data = new HashMap<>();
		data.put("ime", "marko");
		data.put("godine", 11);
		Map<String, Object> data2 = new HashMap<>();
		data2.put("ime", "janko");
		data.put("drugi", data2);
		
		Bindings bindings = jsengine.createBindings();
		bindings.put("questionCompleted", true);
		bindings.put("userData", data);
		bindings.put("ispisivac", System.out);

		jsengine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);
		jsengine.eval(new InputStreamReader(Test2.class.getResourceAsStream("skripta.js"), StandardCharsets.UTF_8));
		invocable.invokeFunction("questionInitialize");

		Object o = data.get("novi");
		System.out.println("o="+o);
		System.out.println("o.class = " + o.getClass());
		
		ObjectMapper mapper = new ObjectMapper();
		System.out.println(mapper.writer().writeValueAsString(data));
		
		Object res = invocable.invokeFunction("f2", "x");
		String sres = res.toString();
		
		Map<String,Object> map = mapper.readValue(sres, new TypeReference<Map<String,Object>>() {});
		map.forEach((k,v)->{
			System.out.println("key = " + k);
			System.out.println("value = " + v);
			System.out.println("valueclass = " + v.getClass());
			System.out.println();
		});
		
	}

}
