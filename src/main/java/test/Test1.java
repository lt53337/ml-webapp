package test;

import java.io.FileInputStream;
import java.io.IOException;

import hr.fer.zemris.minilessons.DescriptorParserUtil;
import hr.fer.zemris.minilessons.xmlmodel.XMLMinilesson;

public class Test1 {

	public static void main(String[] args) throws IOException {
	
		String fileName = "/home/marcupic/fer/diglog-test.txt";
		
		XMLMinilesson ml = DescriptorParserUtil.parseInputStream(new FileInputStream(fileName));
		
		System.out.println(ml.getUid());
	}
	
}
