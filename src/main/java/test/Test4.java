package test;

import hr.fer.zemris.minilessons.util.VideoSpec;
import hr.fer.zemris.minilessons.util.VideoSpecParser;

public class Test4 {

	public static void main(String[] args) {
		String text = "width=320|height=240|poster=movie.jpg|src=movie.mp4#type=video/mp4|src=movie.ogg#type=video/ogg|subtitles=movie.en.vtt#language=en#label=English";

		VideoSpec vs = VideoSpecParser.parse(text);
		
		System.out.println(vs);
	}
}
