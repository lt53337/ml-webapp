package hr.fer.zemris.minilessons.periodicals;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;

import org.junit.Test;

import hr.fer.zemris.minilessons.periodicals.periods.EachNHours;
import hr.fer.zemris.minilessons.periodicals.periods.PeriodicalTimeSpec;

public class Test2 {

	@Test
	public void testEachNMinutes1() {
		PeriodicalTimeSpec pts = new EachNHours().get("xh01+00:30:25");
		
		LocalDateTime t1 = LocalDateTime.of(2016, 9, 2, 10, 30, 24);
		
		LocalDateTime t2 = pts.nextTime(t1);
		
		assertEquals(LocalDateTime.of(2016, 9, 2, 10, 30, 25), t2);
	}
	
	@Test
	public void testEachNMinutes2() {
		PeriodicalTimeSpec pts = new EachNHours().get("xh01+00:30:25");
		
		LocalDateTime t1 = LocalDateTime.of(2016, 9, 2, 10, 30, 25);
		
		LocalDateTime t2 = pts.nextTime(t1);
		
		assertEquals(LocalDateTime.of(2016, 9, 2, 11, 30, 25), t2);
	}
	
	@Test
	public void testEachNMinutes3() {
		PeriodicalTimeSpec pts = new EachNHours().get("xh01+00:30:25");
		
		LocalDateTime t1 = LocalDateTime.of(2016, 9, 2, 0, 30, 24);
		
		LocalDateTime t2 = pts.nextTime(t1);
		
		assertEquals(LocalDateTime.of(2016, 9, 2, 0, 30, 25), t2);
	}
	
	@Test
	public void testEachNMinutes4() {
		PeriodicalTimeSpec pts = new EachNHours().get("xh01+00:30:25");
		
		LocalDateTime t1 = LocalDateTime.of(2016, 9, 2, 0, 30, 25);
		
		LocalDateTime t2 = pts.nextTime(t1);
		
		assertEquals(LocalDateTime.of(2016, 9, 2, 1, 30, 25), t2);
	}
	
	@Test
	public void testEachNMinutes4b() {
		PeriodicalTimeSpec pts = new EachNHours().get("xh01+00:30:25");
		
		LocalDateTime t1 = LocalDateTime.of(2016, 9, 2, 23, 30, 24);
		
		LocalDateTime t2 = pts.nextTime(t1);
		
		assertEquals(LocalDateTime.of(2016, 9, 2, 23, 30, 25), t2);
	}
	
	@Test
	public void testEachNMinutes4c() {
		PeriodicalTimeSpec pts = new EachNHours().get("xh01+00:30:25");
		
		LocalDateTime t1 = LocalDateTime.of(2016, 9, 2, 23, 30, 25);
		
		LocalDateTime t2 = pts.nextTime(t1);
		
		assertEquals(LocalDateTime.of(2016, 9, 3, 0, 30, 25), t2);
	}
	
	@Test
	public void testEachNMinutes5() {
		PeriodicalTimeSpec pts = new EachNHours().get("xh03+00:30:25");
		
		LocalDateTime t1 = LocalDateTime.of(2016, 9, 2, 2, 1, 20);
		
		LocalDateTime t2 = pts.nextTime(t1);
		
		assertEquals(LocalDateTime.of(2016, 9, 2, 3, 30, 25), t2);
	}
	
	@Test
	public void testEachNMinutes6() {
		PeriodicalTimeSpec pts = new EachNHours().get("xh03+00:30:25");
		
		LocalDateTime t1 = LocalDateTime.of(2016, 9, 2, 3, 30, 26);
		
		LocalDateTime t2 = pts.nextTime(t1);
		
		assertEquals(LocalDateTime.of(2016, 9, 2, 6, 30, 25), t2);
	}
	
	@Test
	public void testEachNMinutes7() {
		PeriodicalTimeSpec pts = new EachNHours().get("xh03+02:30:25");
		
		LocalDateTime t1 = LocalDateTime.of(2016, 9, 2, 2, 30, 24);
		
		LocalDateTime t2 = pts.nextTime(t1);
		
		assertEquals(LocalDateTime.of(2016, 9, 2, 2, 30, 25), t2);
	}
	
	@Test
	public void testEachNMinutes8() {
		PeriodicalTimeSpec pts = new EachNHours().get("xh03+02:30:25");
		
		LocalDateTime t1 = LocalDateTime.of(2016, 9, 2, 2, 30, 25);
		
		LocalDateTime t2 = pts.nextTime(t1);
		
		assertEquals(LocalDateTime.of(2016, 9, 2, 5, 30, 25), t2);
	}
	
	@Test
	public void testEachNMinutes9() {
		PeriodicalTimeSpec pts = new EachNHours().get("xh03+02:30:25");
		
		LocalDateTime t1 = LocalDateTime.of(2016, 9, 2, 5, 30, 24);
		
		LocalDateTime t2 = pts.nextTime(t1);
		
		assertEquals(LocalDateTime.of(2016, 9, 2, 5, 30, 25), t2);
	}
	
	@Test
	public void testEachNMinutes10() {
		PeriodicalTimeSpec pts = new EachNHours().get("xh03+02:30:25");
		
		LocalDateTime t1 = LocalDateTime.of(2016, 9, 2, 5, 30, 25);
		
		LocalDateTime t2 = pts.nextTime(t1);
		
		assertEquals(LocalDateTime.of(2016, 9, 2, 8, 30, 25), t2);
	}
	
	@Test
	public void testEachNMinutes11() {
		PeriodicalTimeSpec pts = new EachNHours().get("xh03+02:30:25");
		
		LocalDateTime t1 = LocalDateTime.of(2016, 9, 2, 20, 30, 24);
		
		LocalDateTime t2 = pts.nextTime(t1);
		
		assertEquals(LocalDateTime.of(2016, 9, 2, 20, 30, 25), t2);
	}
	
	@Test
	public void testEachNMinutes12() {
		PeriodicalTimeSpec pts = new EachNHours().get("xh03+02:30:25");
		
		LocalDateTime t1 = LocalDateTime.of(2016, 9, 2, 20, 30, 25);
		
		LocalDateTime t2 = pts.nextTime(t1);
		
		assertEquals(LocalDateTime.of(2016, 9, 2, 23, 30, 25), t2);
	}
	
	@Test
	public void testEachNMinutes13() {
		PeriodicalTimeSpec pts = new EachNHours().get("xh03+02:30:25");
		
		LocalDateTime t1 = LocalDateTime.of(2016, 9, 2, 23, 30, 25);
		
		LocalDateTime t2 = pts.nextTime(t1);
		
		assertEquals(LocalDateTime.of(2016, 9, 3, 2, 30, 25), t2);
	}
}
